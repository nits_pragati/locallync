const React = require("react-native");
const { StyleSheet, Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
    tabItemWarp: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        marginBottom: 10,
        borderRadius: 6
    },
    tabItemImageWarp: {
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#e1e1e1',
        height: 45,
        width: 45,
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabItemImage: {
        width: 40,
        height: 40
    },
    tabItemMdlTextWarp: {
        flex: 1,
        marginLeft: 15,
        justifyContent: 'center'
    },
    tabItemName: {
        fontSize: 13,
        fontFamily: 'Roboto-Medium'
    },
    tabItemPrice: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    tabItemPriceText: {
        lineHeight: 12,
        fontSize: 10
    },
    prsnt: {
        height: 12,
        width: 12,
        marginRight: 10
    },
    seeOffer: {
        borderWidth: 1,
        borderColor: '#3ab3ce',
        borderRadius: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    seeOfferText: {
        fontSize: 10,
        color: '#3ab3ce'
    },
    hder:{ 
        color: '#0077b5', 
        fontWeight:'600',
        marginBottom: 15,
        fontSize:16
    }
};
