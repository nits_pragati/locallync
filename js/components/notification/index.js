import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Text, AsyncStorage } from "react-native";
import { Container, Header, Body, Content, Footer, FooterTab } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import api from "../../api";

class Notification extends Component {
    constructor(params) {
        super(params)
        this.state = {
            newNotificationList:[],
            earlierNotificationList:[],
            unreadcount:0
        }
    }


    componentDidMount() {
        AsyncStorage.getItem("UserDetails", (err, result)=>{
            if(result)
            {
                debugger;
                const parsedData=JSON.parse(result);
                 api.post('notifications/unreadCount.json', {
                     user_id: parsedData.details.id
                 }).then((notification) => {
                     if (notification.ack == 1) {
                         debugger;
                         this.setState({
                             unreadcount: notification.chat_unread+notification.connect_unread+notification.review_unread+notification.offer_unread
                         })
                         debugger
                     }
                 })
                 
                api.post('notifications/userNotification.json', {user_id:parsedData.details.id}).then((res)=>{
                    debugger
                   if(res.ack==1)
                   {
                       let newNotificationList=[];
                       let earlierNotificationList=[];
                   
                       res.notification.map((item)=>{
                           if (item.is_view)
                           {
                             earlierNotificationList.push(item);
                             debugger;
                           }
                           else
                           {
                               debugger;
                               newNotificationList.push(item);
                           }
                       });
                       this.setState({
                           newNotificationList:newNotificationList,
                           earlierNotificationList:earlierNotificationList
                       })
                       api.post('notifications/readNotification.json', {user_id:parsedData.details.id}).then((res)=>{
                           
                       })
                   }
                    
                })
            }
        })
        
    }


    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header searchBar style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText}>NOTIFICATION</Text>
                    </Body>

                    <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                    </TouchableOpacity>

                </Header>
                <Content style={{ flex: 1, padding: 10, backgroundColor: '#f0f0f0' }}>

                    <Text style={styles.hder}>New</Text>
                    {
                        this.state.newNotificationList.length>0?(
                            <View>
                                {
                                  this.state.newNotificationList.map((item, key)=>{
                            return(
                                  <View style={styles.tabItemWarp} key={key}>
                        <View style={[styles.tabItemImageWarp, { height: 50, width: 50 }]}>
                        { item.business_logo ? 
                        (<Image source={{ uri:item.image_url + item.business_logo}} style={styles.tabItemImage}></Image>
						) : (<Image source={require('../../../img/icons/no-image.png')} style={styles.tabItemImage}></Image>
							)
                        }
                            {/* <Image source={require('../../../img/icons/m.png')} style={styles.tabItemImage} /> */}
                        </View>
                        <View style={styles.tabItemMdlTextWarp}>
                            <View>
                                {/* <Text style={styles.tabItemName}>McDonald’s</Text> */}
                                <Text style={{ fontSize: 10, marginBottom: 4 }} numberOfLines={1}>{item.details}</Text>
                                {/* <View style={styles.tabItemPrice}>
                                    <Image source={require('../../../img/icons/favorite2.png')} style={styles.prsnt} />
                                    <Text style={styles.tabItemPriceText}>8.6 km  |  Food & Drink</Text>
                                </View> */}
                            </View>
                        </View>
                    </View>
                            )
                        })
                                }
                            </View>
                        ):(
                            <View>
                                <Text style={{fontSize:15, color:'#333', padding:8, marginBottom:30}}>No records found.</Text>
                            </View>
                        )
                       
                    }
                   
                    <Text style={styles.hder}>Earlier</Text>
                    {
                        this.state.earlierNotificationList.length>0?(
                            <View>
                                {
                        this.state.earlierNotificationList.map((item, key)=>{
                         return(
                              <View style={styles.tabItemWarp} key={key}>
                        <View style={[styles.tabItemImageWarp, { height: 50, width: 50 }]}>
                        { item.business_logo ? 
                        (<Image source={{ uri:item.image_url + item.business_logo}} style={styles.tabItemImage}></Image>
						) : (<Image source={require('../../../img/icons/no-image.png')} style={styles.tabItemImage}></Image>
							)
                        }
                            {/* <Image source={require('../../../img/icons/m.png')} style={styles.tabItemImage} /> */}
                        </View>
                        <View style={styles.tabItemMdlTextWarp}>
                            <View>
                                {/* <Text style={styles.tabItemName}>McDonald’s</Text> */}
                                <Text style={{ fontSize: 10, marginBottom: 4 }} numberOfLines={1}>{item.details}</Text>
                                {/* <View style={styles.tabItemPrice}>
                                    <Image source={require('../../../img/icons/favorite2.png')} style={styles.prsnt} />
                                    <Text style={styles.tabItemPriceText}>8.6 km  |  Food & Drink</Text>
                                </View> */}
                            </View>
                        </View>
                    </View>
                         )
                        })
                    }
                            </View>
                        ):(
                             <View>
                                <Text style={{fontSize:15, color:'#333', padding:8, marginBottom:30}}>No records found.</Text>
                            </View>
                        )
                    }
                    
                </Content>

                <Footer>
                    <FooterTab style={commonStyles.footerTab}>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Home')} >
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/iconHome_noActive.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={commonStyles.footerItemText}>Home</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Connections')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/connection.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={[commonStyles.footerItemText]}>Connection</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Favorite')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/love.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={[commonStyles.footerItemText]}>Favorite</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Notification')} >
                            <View style={[commonStyles.footerItemImageWarp, { position: 'relative', }]}>
                            {this.state.unreadcount != 0?
                             <Text style={commonStyles.notificationText}>{this.state.unreadcount}</Text>
                            :
                                null
                            }
                               
                                <Image source={require('../../../img/icons/notification_active.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={[commonStyles.footerItemText, commonStyles.footerItemTextActive]} >Notification</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('MyAccount')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/profile.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={commonStyles.footerItemText} >Profile</Text>
                        </TouchableOpacity>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}
export default Notification;
