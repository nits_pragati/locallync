import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import PropTypes from 'prop-types';
import { View, StatusBar, TouchableOpacity, Text, AsyncStorage, Alert } from "react-native";
import { Container, Header, Body, Content } from "native-base";
import styles from "./styles";
import defaultStyle from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { userUpdate } from '../accounts/elements/authActions';
import api from '../../api';
import { connect } from 'react-redux';
import FSpinner from 'react-native-loading-spinner-overlay';
const resetAction = NavigationActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'Home' })],
});

class DistrictLocation extends Component {
    constructor(params) {
        super(params)
        this.state = {
            loader: false
        }
    }
    

    manualLocation(){
        this.props.navigation.navigate('DistrictLocationAutocomplete');
    }

    updateLocation() {
        let getPosition = function (options) {
            return new Promise(function (resolve, reject) {
                navigator.geolocation.getCurrentPosition(resolve, reject, options);
            });
        }
        getPosition().then((position) => {
            if (position) {
                const latitude = position.coords.latitude;
                const longitude = position.coords.longitude;

            AsyncStorage.getItem('UserDetails', (err, result) => {
                let data = JSON.parse(result);
                let is_notify = data.details.is_notify
                const id = data.details.id + '';
                const device_token_id = "";
               
                // const blockchain_address = data.details.blockChainAddress.address[0];
                // const blockchain_id = data.details.blockChainAddress.id;
                // const blockchain_password = data.details.blockChainPassword;
                let sendData = { id, latitude, longitude, device_token_id, is_notify  }
                this.setState({
                    loader: true
                })
                api.post('Users/UpdateLocation.json', sendData).then(res => {
                    
                    if (res.ack == 1) {
                      
                        data.details.country_name = res.country_name;
                        data.details.city = res.city;
                        data.details.latitude = latitude;
                        data.details.longitude = longitude;
                        AsyncStorage.setItem('UserDetails', JSON.stringify(data), (err, result) => {
                            this.props.userUpdate(data);
                            this.setState({ loader: false });
                            // this.props.navigation.navigate('Home');
                            this.props.navigation.dispatch(resetAction);

                        })
                    }
                }).catch((err) => {
                    this.setState({ loader: false });
                    // this.props.navigation.dispatch(resetAction);
                })
            });
            }
        }).catch((err) => {
            Alert.alert('', 'Please turn on the location');
        })
        // ,
        // (error) => {
        //     console.log("error :", error);
        // }, { enableHighAccuracy: false })
    }

    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header style={[defaultStyle.headerWarp]} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={defaultStyle.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={[defaultStyle.headerBackIcon]} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={[defaultStyle.headerMiddleText]}>LOCATION</Text>
                    </Body>

                    <TouchableOpacity style={defaultStyle.headerRightBtn} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                    </TouchableOpacity>

                </Header>

                <Content>
                    <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                    <View style={[styles.locationWarp, styles.whiteContent]}>

                        <TouchableOpacity style={[styles.signInBtn, styles.locationBtn]} onPress={()=> this.updateLocation()}>
                            <FontAwesome name='location-arrow' style={[styles.signInBtnIcon, styles.locationBtnIcon]} />
                            <Text style={[styles.signInBtnTxt, styles.locationBtnTxt]}>Use my current location</Text>
                        </TouchableOpacity>

                        <View style={styles.orWarp}>
                            <View style={styles.orDividerLine}></View>
                            <View>
                                <Text style={styles.orText}>or</Text>
                            </View>
                            <View style={styles.orDividerLine}></View>
                        </View>

                        <TouchableOpacity style={styles.locationBtn2} onPress={ ()=> this.manualLocation() }>
                            <Text style={[styles.signInBtnTxt, styles.locationBtnTxt2]}>Select location manually</Text>
                        </TouchableOpacity>

                    </View>
                    {/* <View>
                        <View style={{ flexDirection: 'row', paddingLeft: 10, alignItems: 'center' }}>
                            <Text style={{ flex: 1, fontSize: 13 }}>RECENT LOCATIONS</Text>
                            <TouchableOpacity style={{ padding: 10 }}>
                                <Text style={{ color: '#3ab3ce', fontSize: 12 }}>Clear</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.whiteContent}>
                            <TouchableOpacity style={styles.locationList}>
                                <Text style={styles.locationListText}>District Name 1 , City</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.locationList}>
                                <Text style={styles.locationListText}>District Name 2 , City</Text>
                            </TouchableOpacity>
                        </View>
                    </View> */}
                </Content>
            </Container>
        );
    }
}

DistrictLocation.propTypes = {
    auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
}

const mapDispatchToProps = dispatch => ({
    userUpdate: (data) => dispatch(userUpdate(data))
});

// export default DistrictLocation;
export default connect(mapStateToProps, mapDispatchToProps)(DistrictLocation);
