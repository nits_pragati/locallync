import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavigationActions } from "react-navigation";
import { ScrollView, TouchableOpacity, Image, TextInput, Dimensions, BackHandler, Alert, AsyncStorage } from 'react-native';
import { View, Container, Button, Header, Content, List, ListItem, Thumbnail, Text, Body, Left, Right, Title } from 'native-base';
const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;
import defaultStyle from "../../assets/styles";
import { userUpdate } from '../accounts/elements/authActions';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import styles from "./styles";
import MapView from 'react-native-maps';
import { Marker } from 'react-native-maps';
import { connect } from 'react-redux';
import FSpinner from 'react-native-loading-spinner-overlay';
import api from '../../api';

const resetAction = NavigationActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'Home' })],
});
class DistrictLocationAutocomplete extends Component {

    state = {
        latitude: '',
        longitude: '',
        region: {
            latitude: 22.60183739999999,
            longitude: 88.3830838,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },
        locationName: '',
        loader: false,
    }


    componentWillMount()
    {
       this.gettingCurrentUserLocation();
    }
     gettingCurrentUserLocation() {
         let getPosition = function (options) {
             return new Promise(function (resolve, reject) {
                 navigator.geolocation.getCurrentPosition(resolve, reject, options);
             });
         }
         getPosition().then(position => {
                 if (position) {
                     this.setState({
                         region:{
                             latitude: position.coords.latitude,
                            longitude: position.coords.longitude,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                         }
                     })
                    //  this.setState({
                    //      latitude: position.coords.latitude,
                    //      longitude: position.coords.longitude
                    //  });
                 }
             })
             .catch((err) => {
                 // Alert.alert('', 'Please turn on the location');
                 console.log("location not getting");
             })
     }

    /** getting location  */
    gettingLocation() {
        if (this.state.latitude.length == 0 && this.state.longitude.length == 0) {
            Alert.alert(
                '',
                'Select the location first to continue.', [{
                    text: 'OK',
                    onPress: () => console.log('ok')
                },], {
                    cancelable: false
                }
            )
        } else {
            this.updateLocation();
            // this.props.navigation.navigate('Home');
        }
    }

    updateLocation() {

        let getPosition = function (options) {
            return new Promise(function (resolve, reject) {
                navigator.geolocation.getCurrentPosition(resolve, reject, options);
            });
        }

        getPosition().then((position) => {
            if (position) {
                const latitude = this.state.latitude;
                const longitude = this.state.longitude;

                AsyncStorage.getItem('UserDetails', (err, result) => {
                    let data = JSON.parse(result);
                    let is_notify = data.details.is_notify
                    const id = data.details.id + '';
                    const device_token_id = "";
                    let sendData = { id, latitude, longitude, device_token_id, is_notify }
                    this.setState({
                        loader: true
                    })
                    debugger;
                    api.post('Users/UpdateLocation.json', sendData).then(res => {
                        if (res.ack == 1) {
                            
                            data.details.country_name = res.country_name;
                            data.details.city = res.city;
                            data.details.latitude = latitude;
                            data.details.longitude = longitude;
                            AsyncStorage.setItem('UserDetails', JSON.stringify(data), (err, result) => {
                                this.props.userUpdate(data);
                                this.setState({ loader: false });
                                // this.props.navigation.navigate('Home');
                                this.props.navigation.dispatch(resetAction);
                            })
                        }
                    }).catch((err) => {
                        this.setState({ loader: false });
                        // this.props.navigation.dispatch(resetAction1);
                    })
                });
            }
        }).catch((err) => {
            Alert.alert('', 'Please turn on the location');
        })
        // ,
        //     (error) => {
        //         console.log("error :", error);
        //     }, { enableHighAccuracy: false })
    }

    /** on region change location update */
    onRegionChange = (region) => {
        console.log("region :", region);
        this.setState({ region: region });
    }

    /** getting location on marker drag */
    gettingLocationOnMarkerDrag(e) {
        console.log("location :", e.nativeEvent.coordinate);
        this.setState({
            latitude: e.nativeEvent.coordinate.latitude,
            longitude: e.nativeEvent.coordinate.longitude
        });
        const region = {
            latitude: e.nativeEvent.coordinate.latitude,
            longitude: e.nativeEvent.coordinate.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        }
        this.onRegionChange(region);

        fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + e.nativeEvent.coordinate.latitude + ',' + e.nativeEvent.coordinate.longitude + '&key=' + 'AIzaSyCaZ7DeAIwVCOzQjPHyOs6ZRWMBB3R-F3A')
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson) {
                    this.setState({
                        locationName: JSON.stringify(responseJson.results[0].formatted_address)
                    });
                }
            });
    }
    render() {
        return (
            <Container style={{ backgroundColor: '#fff' }}>
                <Header style={[defaultStyle.headerWarp, { backgroundColor: '#fff' }]} androidStatusBarColor="#133567" noShadow>
                    <TouchableOpacity style={defaultStyle.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={[defaultStyle.headerBackIcon, { color: '#323232' }]} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={[defaultStyle.headerMiddleText, { color: '#323232' }]}>LOCATION</Text>
                    </Body>

                    <TouchableOpacity style={defaultStyle.headerRightBtn} activeOpacity={1}>
                    </TouchableOpacity>
                </Header>
                <Content>
                    <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />                    
                    <View>
                        <GooglePlacesAutocomplete
                            placeholder='Select Location'
                            minLength={2} // minimum length of text to search
                            autoFocus={false}
                            returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                            listViewDisplayed='auto'    // true/false/undefined
                            fetchDetails={true}
                            renderDescription={(row) => row.structured_formatting.main_text}
                            // renderDescription={(row) => row.structured_formatting.main_text} // custom description render
                            onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                                console.log("data :", data);
                                console.log("details :", details.geometry.location);
                                if (details.geometry.location) {
                                    this.setState({
                                        latitude: details.geometry.location.lat,
                                        longitude: details.geometry.location.lng,

                                    });
                                    const region = {
                                        latitude: details.geometry.location.lat,
                                        longitude: details.geometry.location.lng,
                                        latitudeDelta: 0.0922,
                                        longitudeDelta: 0.0421,
                                    }
                                    this.onRegionChange(region);
                                }
                                if (data) {
                                    this.setState({
                                        locationName: data.structured_formatting.main_text
                                    });
                                }
                            }}
                            getDefaultValue={() => {
                                return ''; // text input default value
                            }}
                            query={{
                                // available options: https://developers.google.com/places/web-service/autocomplete
                               // key: 'AIzaSyCaZ7DeAIwVCOzQjPHyOs6ZRWMBB3R-F3A',
                                key: 'AIzaSyCaZ7DeAIwVCOzQjPHyOs6ZRWMBB3R-F3A',
                                language: 'en', // language of the results
                            }}
                            styles={{
                                description: {
                                    fontWeight: 'bold'
                                },
                                predefinedPlacesDescription: {
                                    color: '#1faadb'
                                },
                            }}

                            currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
                            // currentLocationLabel="Current location"
                            // nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                            GoogleReverseGeocodingQuery={{
                                // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                            }}
                            GooglePlacesSearchQuery={{
                                // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                                rankby: 'distance',
                                types: 'food'
                            }}

                            filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
                            debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
                        />
                    </View>
                    <View>
                        <MapView
                            style={{ marginTop: 10, width: deviceWidth, height: 330 }}
                            zoomEnabled
                            zoomControlEnabled
                            maxZoomLevel={20}
                            minZoomLevel={14}
                            region={this.state.region}
                        // onRegionChangeComplete={this.onRegionChange}
                        // onRegionChange={this.onRegionChange}
                        >
                            <Marker draggable
                                coordinate={{
                                    latitude: this.state.region.latitude,
                                    longitude: this.state.region.longitude
                                }}
                                onDragEnd={(e) => this.gettingLocationOnMarkerDrag(e)}
                            />
                        </MapView>
                    </View>
                    <View>
                        <TextInput
                            placeholder="Location Name"
                            style={{ width: deviceWidth, height: 80 }}
                            editable={false}
                            value={this.state.locationName}
                        />
                    </View>

                </Content>
                <View>
                    <TouchableOpacity style={[styles.signInBtn, styles.locationBtn]} onPress={() => this.gettingLocation()}>
                        <Text style={{ }}>Use this Location</Text>
                    </TouchableOpacity>
                </View>
            </Container>
        )
    }
}

// export default LocationAutocomplete;
DistrictLocationAutocomplete.propTypes = {
    auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
}

const mapDispatchToProps = dispatch => ({
    userUpdate: (data) => dispatch(userUpdate(data))
});

// export default DistrictLocation;
export default connect(mapStateToProps, mapDispatchToProps)(DistrictLocationAutocomplete);