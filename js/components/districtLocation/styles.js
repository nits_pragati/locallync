const React = require("react-native");
const { StyleSheet, Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {

    orWarp: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 20
    },
    orDividerLine: {
        flex: 1,
        height: 1,
        backgroundColor: '#f1f1f1'
    },
    orText: {
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 15
    },
    signInBtn: {
        backgroundColor: '#2096AB',
        borderRadius: 40 / 2,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        flexDirection: 'row'
    },
    signInBtnTxt: {
        color: '#fff',
        fontSize: 13
    },
    signInBtnIcon: {
        paddingRight: 10,
        color: '#fff'
    },


    whiteContent:{
        backgroundColor: '#fff'
    },
    locationWarp: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 30,
        paddingBottom: 30
    },
    locationText1: {
        width: '100%',
        textAlign: 'center',
        fontSize: 16,
        color: '#3ab3ce'
    },
    locationText2: {
        textAlign: 'center',
        fontSize: 11
    },
    locationImageWarp: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        marginBottom: 20
    },
    locationImage: {
        width: 300,
        height: 133
    },
    locationBtn: {
        backgroundColor: '#FCD5B2',
        borderRadius: 0
    },
    locationBtnIcon: {
        color: '#323232'
    },
    locationBtnTxt: {
        color: '#323232'
    },
    locationBtn2: {
        backgroundColor: 'transparent',
        alignItems: 'center'
    },
    locationBtnTxt2: {
        textDecorationLine: 'underline',
        color: '#323232'
    },
    locationList:{ 
        padding: 10, 
        borderBottomColor: '#f0f0f0', 
        borderBottomWidth: 1 
    },
    locationListText: { 
        fontSize: 12 
    }


};
