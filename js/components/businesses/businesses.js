import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, AsyncStorage } from "react-native";
import { Container, Header, Body, Content, Tabs, Tab, TabHeading, ScrollableTab, Footer, FooterTab, Button, } from "native-base";
import styles from "./styles";
import defaultStyle from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import FSpinner from 'react-native-loading-spinner-overlay';
import api from '../../api';
import { connect } from 'react-redux';


class Businesses extends Component {
    constructor(params) {
        super(params)
        this.state = {
            businessesId: this.props.navigation.state.params.businessesId ? this.props.navigation.state.params.businessesId : '',
            headerName: '',
            loader: false,
            list_sub_category: [],
            image_url: '',
            home_list: [],
            userId:''
        }
        console.log("props data:",this.props.navigation.state.params)
    }



    selectedFunction(id) {
        debugger;
        let new_list_sub_category = this.state.list_sub_category;
        debugger
        new_list_sub_category.map((data) => {
            if (id == data.id)
                data.selected = !data.selected;
        })
        this.setState({
            list_sub_category: new_list_sub_category
        })
        debugger
    }

    async loop(business_token_list) {
        for (let i = 0; i < business_token_list.length; i++) {
            let toPayAddress = { bizWalletId: business_token_list[i].bizWalletId, bizWalletAddress: business_token_list[i].bizWalletAddress, custWalletAddress: business_token_list[i].custWalletAddress, amount: Number(business_token_list[i].amount) };
            await new Promise(resolve => {
                api.walletPost('reward-customer', toPayAddress).then((res) => {
                    resolve();
                }).catch((err) => {
                    resolve();
                })
            })
        }
        debugger;
        if (this.props.navigation.state.params && this.props.navigation.state.params.IsFromSignup) {
            this.props.navigation.navigate('Home', { IsFromSignup: this.props.navigation.state.params.IsFromSignup });
        }
        else {
            this.props.navigation.navigate('Home');
        }
    }
    businessSelectDone() {
        AsyncStorage.getItem('UserDetails', (err, UserDetails) => {
            debugger
            let data = JSON.parse(UserDetails);
            let id = data.details.id + "";
            this.setState({ loader: true });
            let business_id_list = [];
            let business_token_list = [];
            let amount=0;
            debugger
            this.state.list_sub_category.map((data1) => {
                debugger;
                if (data1.selected == true) {

                    let item = { bid: data1.user_business_id }
                    business_id_list.push(item);
                    if (data1.connect_token && data1.blockchain_id && data1.blockchain_address) {
                        if(data1.connect_token != null || data1.connect_token == ''){
                            amount=  data1.connect_token;
                        }else{
                             amount=  0;
                        }
                        let item1 = { bizWalletId: data1.blockchain_id, bizWalletAddress: data1.blockchain_address, custWalletAddress: data.details.blockchain_address, amount: amount };
                        business_token_list.push(item1);
                    }
                }
            })
            debugger;



            api.post('UserBusinessess/connectBusiness.json', { user_id: id, business_id: business_id_list }).then(res => {
               debugger
                if (res.ack == 1) {
                    debugger

                    if (business_token_list.length > 0) {
                        /*************************Transfer token******************************************* */

                        this.loop(business_token_list);
                    }
                    else {
                        if (this.props.navigation.state.params && this.props.navigation.state.params.IsFromSignup) {
                            debugger
                            this.props.navigation.navigate('Home', { IsFromSignup: this.props.navigation.state.params.IsFromSignup });
                        }
                        else {
                            this.props.navigation.navigate('Home');
                        }
                    }


                }
                else {
                    alert('', res.msg);
                }
                this.setState({
                    loader: false,
                });

            }).catch((err) => {
                debugger
                this.setState({ loader: false });
                console.log(err);
            })
        })
    }


    componentDidMount() {

        AsyncStorage.getItem('UserDetails', (err, result) => {
            let data = JSON.parse(result);
           this.setState({
               userId:data.details.id
           })
          debugger  
          })

        this.setState({
            loader: true,
        })
        
        debugger
        const category_id = this.state.businessesId + '';
        const user_id = this.props.navigation.state.params.fromScreen && this.props.navigation.state.params.fromScreen == "connection" ? this.props.auth.data.details.id :this.props.auth.data.details.id;
        debugger
        api.post('UserBusinessess/ListBusiness.json', { category_id: this.state.businessesId, user_id: user_id }).then(res => {
           debugger
            res.details.map((data) => {
                data.image = data.business_logo ? res.image_url + data.business_logo : '';
                data.selected = false;
            });
            console.log(res.details);
            debugger
            this.setState({
                list_sub_category: res.details,
                home_list: res.details,
                loader: false,
                headerName: res.categoryname,
                image_url: res.image_url
            });
            debugger
        }).catch((err) => {
            debugger
            this.setState({ loader: false });
            console.log(err);
        })
    }

    /** search */
    supportSearch(text) {
        if (text) {
            const regex = new RegExp(`${text.trim()}`, 'i');

            let items = this.state.home_list.filter(
                item => item.business_name.search(regex) >= 0);
            this.setState({ list_sub_category: items });
        }
        else {
            const home_list = this.state.home_list;
            this.setState({ list_sub_category: home_list });
        }
    }

    render() {
        console.log("auth :", this.props.auth);
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header style={defaultStyle.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={defaultStyle.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={defaultStyle.headerBackIcon} />
                    </TouchableOpacity>
                    <Body style={styleSelf.tac}>
                        <Text style={defaultStyle.headerMiddleText}>{this.state.headerName}</Text>
                    </Body>
                    <TouchableOpacity style={defaultStyle.headerRightBtn} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                    </TouchableOpacity>
                </Header>

                <View style={defaultStyle.searchWarp}>
                    <View style={defaultStyle.searchinnr}>
                        <Ionicons style={defaultStyle.searchIcon} name='ios-search' />
                        <TextInput style={defaultStyle.searchInput} underlineColorAndroid='transparent' placeholder='Search by name or keyword...' onChangeText={(text) => this.supportSearch(text)} />
                    </View>
                </View>

                <Content style={styles.whiteContent}>
                    <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                    <View style={{backgroundColor:'#fff', padding:5,textAlign:'center',}}>
                        <Text style={styles.shoppingText1}>Connect and save</Text>
                        {/* <Text style={{color:'#3a3a3a',fontSize:12,textAlign:'center'}}>you will also be able to connect to more business from home screen</Text> */}
                    </View>
                    <View style={{ padding: 10 }}> 
                        <View style={styles.shoppingMainWarp}>

                            {
                                this.state.list_sub_category != 0 ? (
                                    this.state.list_sub_category.map((data, key) => {
                                        return (
                                            <View style={styles.homeItemWarp} key={key}>
                                                <View style={styles.homeItemImageWarp}>
                                                    {
                                                        data.image ? (
                                                            <Image source={{ uri: data.image }} style={styles.homeItemImage}></Image>
                                                        ) : (
                                                                <Image source={require('../../../img/icons/no-image.png')} style={styles.homeItemImage}></Image>
                                                            )
                                                    }

                                                </View>
                                                <View style={styles.homeItemTextWarp}>
                                                    <Text style={styles.homeItemHead}>{data.business_name}</Text>
                                                    <View style={[styles.tabItemPrice, { marginBottom: 8 }]}>
                                                        <Image source={require('../../../img/icons/prsnt.png')} style={styles.prsnt} />
                                                        <Text style={[styles.tabItemPriceText, { fontSize: 10 }]}>{data.offerno} Offers from {data.outletno} outlets</Text>
                                                    </View>
                                                    {/* {
                                                        data.offerdata ? (
                                                            <Text numberOfLines={1} style={styles.discountText}>{data.offerdata.length && data.offerdata.length > 0 ? data.offerdata[0].discount : ''}% exclusive discount & more</Text>
                                                        ) : (
                                                                <View></View>
                                                            )
                                                    } */}
                                                    {data.total_token ?
                                                        <Text style={styles.cuisines} numberOfLines={1} >Plus Reward Tokens</Text> :
                                                        null

                                                    }

                                                    {/* <Text style={styles.cuisines} numberOfLines={1} >{data.business_name}</Text> */}
                                                    {/* <Text style={styles.cuisinesDown} numberOfLines={1}>Sea food, Indian, American</Text> */}
                                                    <View style={[styles.tabItemPrice, { marginBottom: 5 }]}>
                                                        <Entypo name='location-pin' />
                                                        <Text style={styles.locationText} numberOfLines={1}>Nearest outlet : <Text style={styles.locationTextInr}>{data.distance} km</Text></Text>
                                                    </View>
                                                    
                                                    <View>
                                                        {
                                                            data.selected?(
                                                                <View><Text style={{color:'#3ab3ce', fontSize:14,marginBottom:3,textAlign:'center'}}> Connection Added</Text>
                                                                    <TouchableOpacity style={[styles.connectbtn, {backgroundColor:'#ccc'}]} onPress={() => this.selectedFunction(data.id)}>                                                            
                                                                        <Text style={{color:'#3ab3ce'}}>Cancel</Text>                                                               
                                                                    </TouchableOpacity>
                                                                </View>
                                                                
                                                            ):(
                                                                <TouchableOpacity style={[styles.connectbtn]} onPress={() => this.selectedFunction(data.id)}>                                                            
                                                                <Text style={{color:'#3ab3ce'}}>Connect</Text>                                                               
                                                            </TouchableOpacity>
                                                            )
                                                        }
                                                       
                                                    </View>

                                                    {/* <View style={{ alignItems: 'flex-end' }}>
                                                       
                                                        <TouchableOpacity style={{ paddingBottom: 8 }} onPress={() => this.selectedFunction(data.id)}>
                                                            {
                                                                data.selected ?
                                                                    <Ionicons name='ios-checkmark-circle' style={{ fontSize: 18, color: '#3ab3ce' }} /> :
                                                                    <Entypo name='circle' style={{ fontSize: 17 }} />
                                                            }
                                                        </TouchableOpacity>
                                                       


                                                    </View> */}
                                                </View>

                                            </View>
                                        )
                                    })
                                ) : (
                                        <Text style={{ width: '100%', textAlign: 'center' }}>No record found(s)</Text>
                                    )
                            }

                            {/* <View style={styles.homeItemWarp}>
                                <View style={styles.homeItemImageWarp}>
                                    <Image source={require('../../../img/icons/soft.png')} style={styles.homeItemImage}></Image>
                                </View>
                                <View style={styles.homeItemTextWarp}>
                                    <Text style={styles.homeItemHead}>Name Goes here</Text>
                                    <View style={[styles.tabItemPrice, { marginBottom: 8 }]}>
                                        <Image source={require('../../../img/icons/prsnt.png')} style={styles.prsnt} />
                                        <Text style={[styles.tabItemPriceText, { fontSize: 10 }]}>Offers from 2 outlets</Text>
                                    </View>
                                    <Text numberOfLines={1} style={styles.discountText}>35% exclusive discount & more</Text>
                                    <Text style={styles.cuisines} numberOfLines={1} >Cuisines:</Text>
                                    <Text style={styles.cuisinesDown} numberOfLines={1}>Sea food, Indian, American</Text>
                                    <View style={[styles.tabItemPrice, { marginBottom: 5 }]}>
                                        <Entypo name='location-pin' />
                                        <Text style={styles.locationText} numberOfLines={1}>Nearest outlet : <Text style={styles.locationTextInr}>2.4 km</Text></Text>
                                    </View>
                                    <View style={{ alignItems: 'flex-end' }}>
                                    <TouchableOpacity style={{ paddingBottom: 8 }}>
                                        {
                                            true ? 
                                                <Ionicons name='ios-checkmark-circle' style={{ fontSize: 18, color: '#3ab3ce' }}/>:
                                                <Entypo name='circle' style={{ fontSize: 17}}/>
                                        }
                                    </TouchableOpacity>
                                        
                                    </View>
                                </View>

                            </View>
                            <View style={styles.homeItemWarp}>
                                <View style={styles.homeItemImageWarp}>
                                    <Image source={require('../../../img/icons/soft.png')} style={styles.homeItemImage}></Image>
                                </View>
                                <View style={styles.homeItemTextWarp}>
                                    <Text style={styles.homeItemHead}>Name Goes here</Text>
                                    <View style={[styles.tabItemPrice, { marginBottom: 8 }]}>
                                        <Image source={require('../../../img/icons/prsnt.png')} style={styles.prsnt} />
                                        <Text style={[styles.tabItemPriceText, { fontSize: 10 }]}>Offers from 2 outlets</Text>
                                    </View>
                                    <Text numberOfLines={1} style={styles.discountText}>35% exclusive discount & more</Text>
                                    <Text style={styles.cuisines} numberOfLines={1} >Cuisines:</Text>
                                    <Text style={styles.cuisinesDown} numberOfLines={1}>Sea food, Indian, American</Text>
                                    <View style={[styles.tabItemPrice, { marginBottom: 5 }]}>
                                        <Entypo name='location-pin' />
                                        <Text style={styles.locationText} numberOfLines={1}>Nearest outlet : <Text style={styles.locationTextInr}>2.4 km</Text></Text>
                                    </View>
                                    <View style={{ alignItems: 'flex-end' }}>
                                        <TouchableOpacity style={{ paddingBottom: 8 }}>
                                            {
                                                false ?
                                                    <Ionicons name='ios-checkmark-circle' style={{ fontSize: 18, color: '#3ab3ce' }} /> :
                                                    <Entypo name='circle' style={{ fontSize: 17 }} />
                                            }
                                        </TouchableOpacity>

                                    </View>
                                </View>

                            </View>
                            <View style={styles.homeItemWarp}>
                                <View style={styles.homeItemImageWarp}>
                                    <Image source={require('../../../img/icons/soft.png')} style={styles.homeItemImage}></Image>
                                </View>
                                <View style={styles.homeItemTextWarp}>
                                    <Text style={styles.homeItemHead}>Name Goes here</Text>
                                    <View style={[styles.tabItemPrice, { marginBottom: 8 }]}>
                                        <Image source={require('../../../img/icons/prsnt.png')} style={styles.prsnt} />
                                        <Text style={[styles.tabItemPriceText, { fontSize: 10 }]}>Offers from 2 outlets</Text>
                                    </View>
                                    <Text numberOfLines={1} style={styles.discountText}>35% exclusive discount & more</Text>
                                    <Text style={styles.cuisines} numberOfLines={1} >Cuisines:</Text>
                                    <Text style={styles.cuisinesDown} numberOfLines={1}>Sea food, Indian, American</Text>
                                    <View style={[styles.tabItemPrice, { marginBottom: 5 }]}>
                                        <Entypo name='location-pin' />
                                        <Text style={styles.locationText} numberOfLines={1}>Nearest outlet : <Text style={styles.locationTextInr}>2.4 km</Text></Text>
                                    </View>
                                    <View style={{ alignItems: 'flex-end' }}>
                                        <TouchableOpacity style={{ paddingBottom: 8 }}>
                                            {
                                                false ?
                                                    <Ionicons name='ios-checkmark-circle' style={{ fontSize: 18, color: '#3ab3ce' }} /> :
                                                    <Entypo name='circle' style={{ fontSize: 17 }} />
                                            }
                                        </TouchableOpacity>

                                    </View>
                                </View>

                            </View>
                            <View style={styles.homeItemWarp}>
                                <View style={styles.homeItemImageWarp}>
                                    <Image source={require('../../../img/icons/soft.png')} style={styles.homeItemImage}></Image>
                                </View>
                                <View style={styles.homeItemTextWarp}>
                                    <Text style={styles.homeItemHead}>Name Goes here</Text>
                                    <View style={[styles.tabItemPrice, { marginBottom: 8 }]}>
                                        <Image source={require('../../../img/icons/prsnt.png')} style={styles.prsnt} />
                                        <Text style={[styles.tabItemPriceText, { fontSize: 10 }]}>Offers from 2 outlets</Text>
                                    </View>
                                    <Text numberOfLines={1} style={styles.discountText}>35% exclusive discount & more</Text>
                                    <Text style={styles.cuisines} numberOfLines={1} >Cuisines:</Text>
                                    <Text style={styles.cuisinesDown} numberOfLines={1}>Sea food, Indian, American</Text>
                                    <View style={[styles.tabItemPrice, { marginBottom: 5 }]}>
                                        <Entypo name='location-pin' />
                                        <Text style={styles.locationText} numberOfLines={1}>Nearest outlet : <Text style={styles.locationTextInr}>2.4 km</Text></Text>
                                    </View>
                                    <View style={{ alignItems: 'flex-end' }}>
                                        <TouchableOpacity style={{ paddingBottom: 8 }}>
                                            {
                                                false ?
                                                    <Ionicons name='ios-checkmark-circle' style={{ fontSize: 18, color: '#3ab3ce' }} /> :
                                                    <Entypo name='circle' style={{ fontSize: 17 }} />
                                            }
                                        </TouchableOpacity>

                                    </View>
                                </View>

                            </View>
                            <View style={styles.homeItemWarp}>
                                <View style={styles.homeItemImageWarp}>
                                    <Image source={require('../../../img/icons/soft.png')} style={styles.homeItemImage}></Image>
                                </View>
                                <View style={styles.homeItemTextWarp}>
                                    <Text style={styles.homeItemHead}>Name Goes here</Text>
                                    <View style={[styles.tabItemPrice, { marginBottom: 8 }]}>
                                        <Image source={require('../../../img/icons/prsnt.png')} style={styles.prsnt} />
                                        <Text style={[styles.tabItemPriceText, { fontSize: 10 }]}>Offers from 2 outlets</Text>
                                    </View>
                                    <Text numberOfLines={1} style={styles.discountText}>35% exclusive discount & more</Text>
                                    <Text style={styles.cuisines} numberOfLines={1} >Cuisines:</Text>
                                    <Text style={styles.cuisinesDown} numberOfLines={1}>Sea food, Indian, American</Text>
                                    <View style={[styles.tabItemPrice, { marginBottom: 5 }]}>
                                        <Entypo name='location-pin' />
                                        <Text style={styles.locationText} numberOfLines={1}>Nearest outlet : <Text style={styles.locationTextInr}>2.4 km</Text></Text>
                                    </View>
                                    <View style={{ alignItems: 'flex-end' }}>
                                        <TouchableOpacity style={{ paddingBottom: 8 }}>
                                            {
                                                false ?
                                                    <Ionicons name='ios-checkmark-circle' style={{ fontSize: 18, color: '#3ab3ce' }} /> :
                                                    <Entypo name='circle' style={{ fontSize: 17 }} />
                                            }
                                        </TouchableOpacity>

                                    </View>
                                </View>

                            </View>
                            <View style={styles.homeItemWarp}>
                                <View style={styles.homeItemImageWarp}>
                                    <Image source={require('../../../img/icons/soft.png')} style={styles.homeItemImage}></Image>
                                </View>
                                <View style={styles.homeItemTextWarp}>
                                    <Text style={styles.homeItemHead}>Name Goes here</Text>
                                    <View style={[styles.tabItemPrice, { marginBottom: 8 }]}>
                                        <Image source={require('../../../img/icons/prsnt.png')} style={styles.prsnt} />
                                        <Text style={[styles.tabItemPriceText, { fontSize: 10 }]}>Offers from 2 outlets</Text>
                                    </View>
                                    <Text numberOfLines={1} style={styles.discountText}>35% exclusive discount & more</Text>
                                    <Text style={styles.cuisines} numberOfLines={1} >Cuisines:</Text>
                                    <Text style={styles.cuisinesDown} numberOfLines={1}>Sea food, Indian, American</Text>
                                    <View style={[styles.tabItemPrice, { marginBottom: 5 }]}>
                                        <Entypo name='location-pin' />
                                        <Text style={styles.locationText} numberOfLines={1}>Nearest outlet : <Text style={styles.locationTextInr}>2.4 km</Text></Text>
                                    </View>
                                    <View style={{ alignItems: 'flex-end' }}>
                                        <TouchableOpacity style={{ paddingBottom: 8 }}>
                                            {
                                                false ?
                                                    <Ionicons name='ios-checkmark-circle' style={{ fontSize: 18, color: '#3ab3ce' }} /> :
                                                    <Entypo name='circle' style={{ fontSize: 17 }} />
                                            }
                                        </TouchableOpacity>
                                    </View>
                                </View>

                            </View>
                            <View style={styles.homeItemWarp}>
                                <View style={styles.homeItemImageWarp}>
                                    <Image source={require('../../../img/icons/soft.png')} style={styles.homeItemImage}></Image>
                                </View>
                                <View style={styles.homeItemTextWarp}>
                                    <Text style={styles.homeItemHead}>Name Goes here</Text>
                                    <View style={[styles.tabItemPrice, { marginBottom: 8 }]}>
                                        <Image source={require('../../../img/icons/prsnt.png')} style={styles.prsnt} />
                                        <Text style={[styles.tabItemPriceText, { fontSize: 10 }]}>Offers from 2 outlets</Text>
                                    </View>
                                    <Text numberOfLines={1} style={styles.discountText}>35% exclusive discount & more</Text>
                                    <Text style={styles.cuisines} numberOfLines={1} >Cuisines:</Text>
                                    <Text style={styles.cuisinesDown} numberOfLines={1}>Sea food, Indian, American</Text>
                                    <View style={[styles.tabItemPrice, { marginBottom: 5 }]}>
                                        <Entypo name='location-pin' />
                                        <Text style={styles.locationText} numberOfLines={1}>Nearest outlet : <Text style={styles.locationTextInr}>2.4 km</Text></Text>
                                    </View>
                                    <View style={{ alignItems: 'flex-end' }}>
                                        <TouchableOpacity style={{ paddingBottom: 8 }}>
                                            {
                                                false ?
                                                    <Ionicons name='ios-checkmark-circle' style={{ fontSize: 18, color: '#3ab3ce' }} /> :
                                                    <Entypo name='circle' style={{ fontSize: 17 }} />
                                            }
                                        </TouchableOpacity>

                                    </View>
                                </View>

                            </View>
                            <View style={styles.homeItemWarp}>
                                <View style={styles.homeItemImageWarp}>
                                    <Image source={require('../../../img/icons/soft.png')} style={styles.homeItemImage}></Image>
                                </View>
                                <View style={styles.homeItemTextWarp}>
                                    <Text style={styles.homeItemHead}>Name Goes here</Text>
                                    <View style={[styles.tabItemPrice, { marginBottom: 8 }]}>
                                        <Image source={require('../../../img/icons/prsnt.png')} style={styles.prsnt} />
                                        <Text style={[styles.tabItemPriceText, { fontSize: 10 }]}>Offers from 2 outlets</Text>
                                    </View>
                                    <Text numberOfLines={1} style={styles.discountText}>35% exclusive discount & more</Text>
                                    <Text style={styles.cuisines} numberOfLines={1} >Cuisines:</Text>
                                    <Text style={styles.cuisinesDown} numberOfLines={1}>Sea food, Indian, American</Text>
                                    <View style={[styles.tabItemPrice, { marginBottom: 5 }]}>
                                        <Entypo name='location-pin' />
                                        <Text style={styles.locationText} numberOfLines={1}>Nearest outlet : <Text style={styles.locationTextInr}>2.4 km</Text></Text>
                                    </View>
                                    <View style={{ alignItems: 'flex-end' }}>
                                        <TouchableOpacity style={{ paddingBottom: 8 }}>
                                            {
                                                false ?
                                                    <Ionicons name='ios-checkmark-circle' style={{ fontSize: 18, color: '#3ab3ce' }} /> :
                                                    <Entypo name='circle' style={{ fontSize: 17 }} />
                                            }
                                        </TouchableOpacity>

                                    </View>
                                </View>

                            </View> */}

                        </View>
                    </View>
                </Content>
                <Footer style={styles.footer}>
                    <FooterTab >
                        <TouchableOpacity style={defaultStyle.footerNext} onPress={() => this.businessSelectDone()}>
                            <Text style={[defaultStyle.footerNextText, {fontWeight:'600',}]}> NEXT</Text>
                        </TouchableOpacity>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps)(Businesses);
