const React = require("react-native");
const { StyleSheet, Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
    shoppingText1:{
        fontSize: 16,
        color: '#3ab3ce',
        width: '100%',
        textAlign: 'center'
    },
    tabItemPrice: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    prsnt: {
        height: 15,
        width: 9,
        marginRight: 10
    },
    tabItemPriceText: {
        lineHeight: 12,
        fontSize: 11
    },
    homeItemWarp: {
        backgroundColor: '#fff',
        borderColor: '#ebebeb',
        borderWidth: 1,
        position: 'relative',
        width: '48%',
        marginLeft: 3,
        marginRight: 3,
        marginBottom: 6
    },
    homeItemLove: {
        position: 'absolute',
        top: 6,
        right: 6,
        zIndex: 99
    },
    homeItemLoveIcon: {
        fontSize: 20
    },
    homeItemImageWarp: {
        borderBottomColor: '#ebebeb',
        alignItems: 'center',
        borderBottomWidth: 1,
        marginBottom: 5,
        paddingTop: 0,
        paddingBottom: 8
    },
    homeItemImage: {
        height: 120,
        width: '100%'
    },
    homeItemTextWarp: {
        flex: 1,
        paddingLeft: 6,
        paddingRight: 6,
        justifyContent: 'flex-start'
    },
    homeItemHead: {
        fontSize: 13,
        color: '#000',
        fontFamily: 'Roboto-Medium',
        marginBottom: 5,
        textAlign:'center'
    },
    discountText: {
        fontSize: 10,
        marginBottom: 5
    },
    cuisines: {
        fontSize: 10,
        marginBottom: 0,
        color: '#000',
        fontFamily: 'Roboto-Medium'
    },
    cuisinesDown: {
        fontSize: 10,
        marginBottom: 0
    },
    locationText: {
        fontSize: 10,
        marginTop: 2
    },
    locationTextInr: {
        color: '#61C0B7'
    },
    shoppingMainWarp:{ 
        flexDirection: 'row', 
        flexWrap: 'wrap', 
        justifyContent: 'space-between', 
        marginTop: 10 
    },
    footer:{ 
        height: 40 
    },
    btncn:{
        width:'100%',
        height:34,
        color:'#fff',
        borderRadius:35,
        marginBottom:20,
        // backgroundColor:'#00BBD7',
        // backgroundColor:'#7CA8AF'
    },
    btntxt:{
        fontSize:14,
        color:'#fff',
        textAlign:'center',
        lineHeight:25,
    },
    cntxt:{
        textAlign:'center',
        color:'#66C0B7',
        fontSize:14,
    },
    connectbtn:{
        flex:1,
        flexDirection:'row',
        padding:6,
        paddingTop:3,
        paddingBottom:3,
        backgroundColor:'#fff',
        borderWidth:1,
        borderColor:'#3ab3ce',
        borderRadius:35,
        justifyContent:'center',
        alignItems:'center',
        marginTop:10,
        marginBottom:10
        
    }

};
