import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, ImageBackground, AsyncStorage } from "react-native";
import { Container, Header, Body, Content, Tabs, Tab, TabHeading, ScrollableTab, Footer, FooterTab, Button, } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import api from '../../api';
import FSpinner from 'react-native-loading-spinner-overlay';


class Connections extends Component {
    constructor(params) {
        super(params)
        this.state = {
            myConnection: [],
            imageUri: '',
            home_list: [],
            category_list: [],
            activeTab: 0,
            unreadcount: 0
        }
    }




    componentDidMount() {

        this.setState({ loader: true });
        AsyncStorage.getItem('UserDetails', (err, result) => {
            debugger
            let data = JSON.parse(result);
            let user_id = data.details.id + '';
            api.post('notifications/unreadCount.json', { user_id: user_id }).then((notification) => {
                if (notification.ack == 1) {
                    this.setState({
                        unreadcount: notification.chat_unread+notification.connect_unread+notification.review_unread+notification.offer_unread
                    })
                }
                debugger
            })
            api.get('categories/list_category.json').then(reslist_category => {
                if (reslist_category.ack == 1) {
                    this.setState({
                        category_list: reslist_category.details
                    });
                    api.post('UserBusinessess/MyConnectBusiness.json', {
                        user_id: user_id
                    }).then(res => {

                        //  this.setState({
                        //      loader: false
                        //  });
                        if (res.ack == 1) {
                            debugger
                            res.details.map((data) => {
                                data.business_logo = res.image_url + data.business_logo,
                                    data.user_business_id = data.user_business_categories[0].user_business_id,
                                    data.category_id = ''
                            });
                            //  this.setState({
                            //      myConnection: res.details,
                            //      home_list: res.details,
                            //      imageUri: res.image_url,
                            //      loader:false
                            //  });

                            //  console.log(this.state.myConnection);
                            let finalData = [];
                            for (let i = 0; i < reslist_category.details.length; i++) {
                                let data = {};

                                data["category"] = reslist_category.details[i];
                                data["connections"] = [];
                                for (let j = 0; j < res.details.length; j++) {

                                    if (res.details[j].user_business_categories && res.details[j].user_business_categories.length && res.details[j].user_business_categories.length > 0) {
                                        if (res.details[j].user_business_categories[0].category_id == reslist_category.details[i].id) {
                                            data.connections.push(res.details[j])
                                        }
                                    }

                                }
                                finalData.push(data);
                            }
                            this.setState({
                                myConnection: finalData,
                                home_list: finalData,
                                imageUri: res.image_url,
                                loader: false
                            });
                            debugger

                        }
                        else {
                            let finalData = [];
                            for (let i = 0; i < reslist_category.details.length; i++) {
                                let data = {};

                                data["category"] = reslist_category.details[i];
                                data["connections"] = [];

                                finalData.push(data);
                            }
                            this.setState({
                                myConnection: finalData,
                                home_list: finalData,
                                imageUri: res.image_url,
                                loader: false
                            });
                        }
                    }).catch((err) => {
                        this.setState({
                            loader: false
                        });
                        console.log(err);
                    })

                }
            }).catch(err => {
                this.setState({ loader: false });
                console.log(err);
            })

        })
    }

    /** search */
    supportSearch(text) {
        console.log(this.state.myConnection);
        if (text) {
            console.log("text :", text);
            const regex = new RegExp(`${text.trim()}`, 'i');

            // let items = this.state.home_list.filter(
            //     item => item.business_name.search(regex) >= 0);
            // this.setState({ myConnection: items });

            let toUpdateData = [];
            let IsActiveTabSet = false;
            let activeTabIndex = 0;

            for (let i = 0; i < this.state.home_list.length; i++) {
                let data = {}
                data["category"] = this.state.home_list[i].category;
                data["connections"] = [];

                if (this.state.home_list[i].connections.length > 0) {

                    let items = this.state.home_list[i].connections.filter(
                        item => item.business_name.search(regex) >= 0);
                    if (items.length > 0) {
                        // toUpdateData.push(this.state.home_list[i]);
                        data.connections = items;
                        if (!IsActiveTabSet) {
                            IsActiveTabSet = true;
                            activeTabIndex = i;
                        }
                    }
                }
                toUpdateData.push(data);
            }

            console.log(this._tabs)
            this.setState({
                myConnection: toUpdateData,
                activeTab: activeTabIndex

            });
        }
        else {
            const home_list = this.state.home_list;
            this.setState({
                myConnection: home_list,
                activeTab: 0
            });
        }
    }


    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header searchBar style={[commonStyles.headerWarp, { height: 30, paddingTop: 5 }]} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.navigate('Login')} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText}>CONNECTIONS</Text>
                    </Body>

                    <TouchableOpacity style={commonStyles.headerRightBtn} onPress={() => this.props.navigation.navigate('ConnectWithBusinesses', { fromScreen: 'connection' })}>
                        <SimpleLineIcons name='plus' style={commonStyles.headerRightIcon} />
                    </TouchableOpacity>

                </Header>

                <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />

                <View style={{ backgroundColor: '#2096AB', padding: 10, paddingTop: 0, paddingBottom: 5 }}>
                    <View style={{ backgroundColor: '#fff', borderRadius: 20, flexDirection: 'row', alignItems: 'center' }}>
                        <Ionicons style={{ color: '#2096AB', marginRight: 5, fontSize: 20, marginLeft: 15 }} name='ios-search' />
                        <TextInput style={{ height: 30, flex: 1, marginBottom: 0, paddingTop: 2, paddingBottom: 0, fontSize: 11 }} underlineColorAndroid='transparent' placeholder='Search business by name or keyword' onChangeText={(text) => this.supportSearch(text)} />
                    </View>
                </View>
                {
                    this.state.myConnection && this.state.myConnection.length > 0 ? (
                        <Tabs
                            tabBarUnderlineStyle={{ backgroundColor: '#f6f6f6', height: 2, borderBottomWidth: 0 }}
                            page={this.state.activeTab}
                            ref={component => this._tabs = component}

                            renderTabBar={() => <ScrollableTab tabsContainerStyle={{ backgroundColor: '#f6f6f6' }} />} >



                            {this.state.myConnection.map((data, key) => {
                                return (
                                    <Tab heading={data.category ? data.category.name : ''} tabStyle={{ backgroundColor: '#f6f6f6', }} textStyle={{ color: '#888888', fontSize: 11 }} activeTabStyle={{ backgroundColor: '#f6f6f6' }} activeTextStyle={{ color: '#3ab3ce', fontSize: 11 }} key={key}>
                                        {
                                            data.connections && data.connections.length > 0 ? (
                                                <View>
                                                    {
                                                        data.connections.map((data1, key1) => {
                                                            return (
                                                                <View style={styles.tabItemWarp} key={key1}>

                                                                    
                                                                       
                                                                            <View style={styles.tabItemImageWarp}>
                                                                                { data1.business_logo ? (
                                                                                <Image source={{ uri: data1.business_logo}} style={styles.tabItemImage} />) :
                                                                            
                                                                                 <Image source={require('../../../img/icons/no-image.png')} style={styles.tabItemImage} />
                                                                                }
                                                                                 </View>
                                                                   

                                                                    <View style={styles.tabItemMdlTextWarp}>
                                                                        <View>                                                                            
                                                                            <View style={styles.tabItemPrice}>
                                                                                
                                                                                <Text style={styles.tabItemName}>{data1.business_name}</Text>
                                                                            </View>
                                                                        </View>
                                                                    </View>
                                                                    <TouchableOpacity style={styles.seeOffer} onPress={() => this.props.navigation.navigate('Details', { Details: data1 })}>
                                                                        <View style={styles.seeOfferInner}>
                                                                            <Text style={styles.seeOfferText}>See Offer</Text>
                                                                        </View>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            )
                                                        })
                                                    }
                                                </View>
                                            ) : (
                                                    <Text style={{ width: '100%', textAlign: 'center', padding: 20 }}> No Connections Found </Text>
                                                )
                                        }
                                    </Tab>
                                )
                            })}



                        </Tabs>
                    ) : null
                }


                <Footer>
                    <FooterTab style={styles.footerTab}>
                        <TouchableOpacity style={styles.footerItemWarp} onPress={() => this.props.navigation.navigate('Home')} >
                            <View style={styles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/iconHome_noActive.png')} style={styles.footerItemImage} />
                            </View>
                            <Text style={styles.footerItemText}>Home</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.footerItemWarp} onPress={() => this.props.navigation.navigate('Connections')}>
                            <View style={styles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/connection_active.png')} style={styles.footerItemImage} />
                            </View>
                            <Text style={[styles.footerItemText, , styles.footerItemTextActive]}>Connection</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.footerItemWarp} onPress={() => this.props.navigation.navigate('Favorite')}>
                            <View style={styles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/love.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={styles.footerItemText}>Favorite</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.footerItemWarp} onPress={() => this.props.navigation.navigate('Notification')}>
                            <View style={[styles.footerItemImageWarp, { position: 'relative', }]}>
                            {this.state.unreadcount != 0 ?
                                <Text style={styles.notificationText}>{this.state.unreadcount}</Text>:
                                null
                            }
                                
                                <Image source={require('../../../img/icons/notification.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={styles.footerItemText} >Notification</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.footerItemWarp} onPress={() => this.props.navigation.navigate('MyAccount')}>
                            <View style={styles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/profile.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={styles.footerItemText} >Profile</Text>
                        </TouchableOpacity>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}
export default Connections;
