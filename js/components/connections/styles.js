const React = require("react-native");
const { StyleSheet, Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
    tabItemWarp:{ 
        backgroundColor: '#fff', 
        flexDirection: 'row', 
        alignItems: 'center', 
        paddingLeft: 10, 
        paddingRight: 10,
        paddingTop: 10, 
        paddingBottom: 10, 
        borderBottomColor: '#f3f3f3', 
        borderBottomWidth: 1 
    },
    tabItemImageWarp: { 
        borderRadius: 8, 
        borderWidth: 1, 
        borderColor: '#e1e1e1', 
        height: 45, 
        width: 45, 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
    tabItemImage: { 
        width: 40,
        height: 40 
    },
    tabItemMdlTextWarp: { 
        flex: 1, 
        marginLeft: 15, 
        justifyContent: 'center' 
    },
    tabItemName: { 
        fontSize: 14,
        marginLeft:5,
        color:'#333' 
    },
    tabItemPrice:{        
        flexDirection: 'row',        
    },
    tabItemPriceText: {
        lineHeight: 12,
        fontSize: 11
    },
    prsnt: {
        height: 34,
        width: 50,
        marginRight: 5,
        borderRadius:5
    },
    seeOffer: {  
        borderRadius: 35, 
        padding:6,
        paddingLeft: 15, 
        paddingRight: 15,
        backgroundColor: '#3ab3ce' 
    },
    seeOfferText:{ 
        fontSize: 10,
        color: '#ffffff' 
    },
    // footer:Start
    footerTab: {
        backgroundColor: '#ffffff'
    },
    footerItemWarp: {
        flex: 1,
        width: '25%',
        maxWidth: '25%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    footerItemImageWarp: {
        height: 20
    },
    footerItemImage: {
        width: 20,
        height: 20
    },
    footerItemText: {
        color: '#838383',
        fontSize: 10
    },
    footerItemTextActive: {
        color: '#3ab3ce'
    },
    notificationText: {
        marginTop: -2,
        position: 'absolute',
        top: -5,
        right: -5,
        backgroundColor: '#2096AB',
        fontSize: 9,
        zIndex: 99,
        borderRadius: 10,
        paddingRight: 5,
        paddingLeft: 5,
        color: '#fff'
    }
  // footer:end
};
