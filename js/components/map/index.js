import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { View, StatusBar, TouchableOpacity, Text, Picker } from "react-native";
import { Container, Header, Body, Content } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';


class Favorite extends Component {
    constructor(params) {
        super(params)
        this.state = {
        }
    }


    componentDidMount() {}


    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={{ width: 40 }} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={{ color: '#fff', fontSize: 20 }} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText}>FAVORITES</Text>
                    </Body>

                    <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                    </TouchableOpacity>

                </Header>

                <View style={styles.pickerMainWarp}>
                    <TouchableOpacity style={{ paddingRight: 10 }} onPress={() => this.props.navigation.navigate('DistrictLocation')}>
                        <Entypo name="location-pin" style={{ fontSize: 14 }} />
                        <Text style={{ fontSize: 9 }}>Dist.</Text>
                    </TouchableOpacity>
                    <View style={styles.pickerWarp}>
                        <Picker
                            style={styles.picker }
                            itemStyle={{ fontSize: 12, color: '#b5b4b4' }}
                            onValueChange={(itemValue, itemIndex) => this.setState({ language: itemValue })}>
                            <Picker.Item label="All Categories" value="1" />
                            <Picker.Item label="categories 1" value="2" />
                            <Picker.Item label="categories 2" value="3" />
                            <Picker.Item label="categories 3" value="4" />
                            <Picker.Item label="categories 4" value="5" />
                        </Picker>
                    </View>
                    <TouchableOpacity style={{ paddingLeft: 10 }} onPress={() => this.props.navigation.navigate('Favorite')}>
                        <MaterialIcons name="format-list-bulleted" style={{ fontSize: 20 }} />
                    </TouchableOpacity>
                </View>
                <Content style={{ flex: 1, backgroundColor: '#fff' }}>
                    <View style={{ minHeight: 400, alignItems: 'center', justifyContent: 'center' } }>
                        <Text>Map</Text>
                    </View>
                </Content>

            </Container>
        );
    }
}


export default Favorite;
