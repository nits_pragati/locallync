const React = require("react-native");
const { StyleSheet, Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
    pickerMainWarp: {
        backgroundColor: '#f0f0f0',
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    pickerWarp: {
        backgroundColor: '#fff',
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
        paddingLeft: 10
    },
    picker: {
        height: 30,
        flex: 1,
        color: '#b5b4b4'
    },
};
