import React, { Component } from "react";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, ImageBackground, AsyncStorage, ScrollView, Alert, BackHandler, NetInfo } from "react-native";
import { Container, Header, Body, Content, Tabs, Tab, TabHeading, ScrollableTab, Footer, FooterTab, Button, Badge } from "native-base";
import styles from "./styles";
import { userUpdate } from '../accounts/elements/authActions';
import commonStyles from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import SplashScreen from 'react-native-splash-screen';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import FSpinner from 'react-native-loading-spinner-overlay';
import api from '../../api';
import SQLite from "react-native-sqlite-storage";
import * as firebase from 'firebase';
import Dialog from "react-native-dialog";
import FCM, {
	FCMEvent,
	RemoteNotificationResult,
	WillPresentNotificationResult,
	NotificationType,

} from "react-native-fcm";
const firebaseConfig = {
	apiKey: "AIzaSyBSW6dhfaSEP2pAHGAbWfuZ-kAPF7uA63w",
	authDomain: "locallync-859ff.firebaseapp.com",
	databaseURL: "https://locallync-859ff.firebaseio.com",
	projectId: "locallync-859ff",
	storageBucket: "locallync-859ff.appspot.com",
	messagingSenderId: "718735440621"
};
const deviceWidth = Dimensions.get("window").width;
class Home extends Component {
	constructor(params) {
		super(params)
		this.state = {
			loader: false,
			home_list: [],
			image_url: '',
			image_url2: '',
			dist: '',
			all_home_list: [],
			home_nearBy_list: [],
			city: '',
			userId: '',
			message: '',
			id: '',
			home_list_cbbu: [],
			sqlLiteDb: '',
			connectionInfo: '',
			scannerRef: '',
			IsShowDialog: false,
			IsShowDialog1: false,
			snapShotVal: '',
			validOffer: "",
			unreadcount: 0,
			customer_blockchain_address:"",
			businessdata:[]
		}

		function handleFirstConnectivityChange(connectionInfo) {
			console.log('First change, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType);

		}
		NetInfo.addEventListener(
			'connectionChange',
			handleFirstConnectivityChange
		);

		if (!firebase.apps.length) {
			firebase.initializeApp(firebaseConfig);
			this.state.scannerRef = firebase.database().ref().child('scannerRef');
		}
		else {
			this.state.scannerRef = firebase.database().ref().child('scannerRef');
		}
		// if (!firebase.apps.length) {
		// 	firebase.initializeApp(firebaseConfig);
		// 	this.state.chatRef = firebase.database().ref().child('messages');
		// } else {
		// 	this.state.chatRef = firebase.database().ref().child('messages');
		// }
		var child_added_first = true;

		this.state.scannerRef.on('child_changed', (snapshot) => {
			console.warn("snapshot val:",snapshot);
			const snapShotVal = snapshot.val();
			console.warn("snapshot val():",snapShotVal);
			AsyncStorage.getItem('UserDetails').then((userDetails) => {

				const parsedUserDetails = JSON.parse(userDetails);

				if (snapShotVal.userId == parsedUserDetails.details.id.toString()) {
					this.setState({
						IsShowDialog: true,
						snapShotVal: snapShotVal
					})
				}
				// setTimeout(() => {
				// 	if (this.refs && this.refs.ScrollViewStart) {
				// 		this.refs.ScrollViewStart.scrollToEnd(true);
				// 	}

				// }, 400);
			})
		})

		FCM.requestPermissions();
		FCM.getFCMToken().then(token => {
			debugger;
			AsyncStorage.getItem("UserDetails", (err, result) => {
				debugger;
				if (result) {
					const data = JSON.parse(result);

					api.post('Users/UpdateLocation.json', {
						id: data.details.id,
						device_token_id: token,
						latitude: data.details.latitude, longitude: data.details.longitude, is_notify: data.details.is_notify
					}).then((resEdit) => {
						console.log("success");

					}).catch((err) => { });
				}
			});

		});

		// This method get all notification from server side.
		FCM.getInitialNotification().then(notif => {
			// console.log(notif);
			// setTimeout(() => {
			// 	debugger;
			// 	AsyncStorage.getItem("userToken").then((userToken) => {
			// 		if (userToken) {
			// 			const userToken1 = JSON.parse(userToken);
			// 			this.props.getUserDetail(userToken1.userId, userToken1.id).then(userRes => {
			// 				if (notif.screenType) {
			// 					if (notif.screenType == 'JobDetails') {
			// 						api.post('Jobs/getJobDetailsById', {
			// 							"id": Number(notif.jobId)
			// 						}).then((resJob) => {
			// 							this.props.navigation.dispatch(
			// 								NavigationActions.reset({
			// 									index: 1,
			// 									actions: [
			// 										NavigationActions.navigate({
			// 											routeName: 'Menu'
			// 										}),
			// 										NavigationActions.navigate({
			// 											routeName: 'JobDetails',
			// 											params: {
			// 												jobId: notif.jobId,
			// 												jobDetails: resJob.response.message[0]
			// 											}
			// 										}),
			// 									],
			// 								})
			// 							);
			// 						}).catch((err) => {
			// 							connect.log('err', err);
			// 						});
			// 					} else {
			// 						this.props.navigation.navigate('Home');
			// 					}
			// 				} else {
			// 					this.props.navigation.navigate('Home');
			// 				}

			// 			}).catch(err => {
			// 				Alert.alert('Please login');
			// 				this.props.navigation.navigate("Login")
			// 			})
			// 		} else {
			// 			this.props.navigation.navigate('Home');
			// 		}
			// 	})
			// }, 4000);
			// if (notif && notif.screenType && notif.screenType == 'JobDetails') {
			// 	// this.props.navigation.navigate('JobDetails', { jobDetails: notif.jobId });
			// 	this.setState({
			// 		isPush: true,
			// 		jobId: notif.jobId
			// 	});
			// }

		});


		// This method give received notifications to mobile to display.
		this.notificationUnsubscribe = FCM.on(FCMEvent.Notification, notif => {
			console.log("noti :", notif)
			console.log("okkkkkkk");
			// if (notif) {
			// 	// if(notif.category_id && notif.user_business_id)
			// 	// {
			// 	// const Details = {
			// 	// 	category_id: notif.category_id,
			// 	// 	user_business_id: notif.user_business_id
			// 	// }
			// 	// this.props.navigation.dispatch(
			// 	// 	NavigationActions.reset({
			// 	// 		index: 1,
			// 	// 		actions: [
			// 	// 			NavigationActions.navigate({
			// 	// 				routeName: 'Home'
			// 	// 			}),
			// 	// 			NavigationActions.navigate({
			// 	// 				routeName: 'Details',
			// 	// 				params: {
			// 	// 					Details: Details
			// 	// 				}
			// 	// 			}),
			// 	// 		],
			// 	// 	})
			// 	// );
			// 	// return;
			// 	// }




			// }
			if (notif && notif.local_notification) {
				if (notif.screenType) {
					if (notif.screenType == 'JobDetails') {
						api.post('Jobs/getJobDetailsById', {
							"id": Number(notif.jobId)
						}).then((resJob) => {
							this.props.navigation.dispatch(
								NavigationActions.reset({
									index: 1,
									actions: [
										NavigationActions.navigate({
											routeName: 'Menu'
										}),
										NavigationActions.navigate({
											routeName: 'JobDetails',
											params: {
												jobId: notif.jobId,
												jobDetails: resJob.response.message[0]
											}
										}),
									],
								})
							);
						}).catch((err) => {
							connect.log('err', err);
						});
					} else {
						this.props.navigation.navigate('Home');
					}
				} else {
					this.props.navigation.navigate('Home');
				}
				//return;
			}

			this.sendRemote(notif);
		});

		// this method call when FCM token is update(FCM token update any time so will get updated token from this method)
		this.refreshUnsubscribe = FCM.on(FCMEvent.Notification, token => {

			FCM.getFCMToken().then(token => {
				AsyncStorage.getItem("UserDetails", (err, result) => {
					if (result) {
						const data = JSON.parse(result);

						api.post('Users/UpdateLocation.json', {
							id: data.details.id,
							device_token_id: token,
							latitude: data.details.latitude,
							longitude: data.details.longitude,
							is_notify: data.details.is_notify
						}).then((resEdit) => {
							console.log("success");

						}).catch((err) => { });
					}
				})

			});
		});

	}
	sendRemote(notif) {
		// const notificationChannelId = 'my_notif_channel';
		// debugger;
		// FCM.createNotificationChannel({
		// 	id: 'default',
		// 	name: 'Default',
		// 	description: 'general usage',
		// 	priority: 'high'
		// });
		debugger;
		FCM.presentLocalNotification({
			id: new Date().valueOf().toString(),
			title: notif.fcm.body,
			body: notif.fcm.body,
			ticker: notif.fcm.body,
			priority: "high",
			click_action: notif.click_action,
			show_in_foreground: true,
			local: true,
			vibrate: 300,
			wake_screen: true,
			lights: true,
			auto_cancel: true,
			group: "group",
			icon: "ic_launcher",
			large_icon: "ic_launcher"
		});
	}


	componentWillReceiveProps() {
		NetInfo.getConnectionInfo().then((connectionInfo) => {
			this.setState({
				connectionInfo: connectionInfo.type
			})
			this.homeData();
		});
		if (this.props.auth.data && this.props.auth.data.details.city) {
			this.setState({ city: this.props.auth.data.details.city })
		}


	}


	componentDidMount() {

		SplashScreen.hide();
		const that = this;

		if (SQLite) {
			SQLite.openDatabase("localLync.db", "1.0", "Demo", -1, successCb, errorCb);
		}


		AsyncStorage.getItem("UserDetails", (err, result) => {
			debugger
			this.setState({
				customer_blockchain_address:JSON.parse(result).details.blockchain_address
			});
			debugger
			const id = JSON.parse(result).details.id;
			api.post('notifications/unreadCount.json', {
				user_id: id
			}).then((notification) => {
				if (notification.ack == 1) {
					debugger;
					this.setState({
						unreadcount: notification.chat_unread+notification.connect_unread+notification.review_unread+notification.offer_unread
					})
					debugger;
				}
			})
		})

		function successCb(db) {
			that.setState({
				sqlLiteDb: db
			});

		}
		function errorCb(err) {
			console.log(err);

		}

		if (this.props.navigation.state.params && this.props.navigation.state.params.IsFromSignup == "signup") {
			Alert.alert('', 'Great! Enjoy the offers. You can also add more businesses from the connection tab')
		}




		BackHandler.addEventListener('hardwareBackPress', function () {
			if (this.props.currentRoute === 'Home') {
				Alert.alert(
					'Confirm',
					'Are you sure you want to exit the app?',
					[
						{ text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
						{ text: 'Ok', onPress: () => BackHandler.exitApp() },
					],
					{ cancelable: false }
				);
				return true;
			} else {
				this.props.navigation.goBack(null);
				return true;
			}

		}.bind(this));

		/** getting userId from auth redux */
		console.log("userId :", this.props.auth.data.details.id);
		if (this.props.auth.data) {
			this.setState({
				userId: this.props.auth.data.details.id
			});
		}
		if (this.props.auth.data.details.city === undefined || this.props.auth.data.details.city === null) {
			AsyncStorage.getItem('UserDetails', (err, result) => {
				let data = JSON.parse(result);
				let id = data.details.id + '';

				let getPosition = function (options) {
					return new Promise(function (resolve, reject) {
						navigator.geolocation.getCurrentPosition(resolve, reject, options);
					});
				}
				getPosition().then((position) => {
					let latitude = position.coords.latitude;
					let longitude = position.coords.longitude;
					api.post('Users/UpdateLocation.json', { id: id, latitude: latitude, longitude: longitude, device_token_id: "", is_notify: "1" }).then(res => {
						this.loader({ loader: false })
						if (res.ack == 1) {
							data.details.country_name = res.country_name;
							data.details.city = res.city;
							this.setState({
								city: res.city,
							});
							console.log('now city ', this.state.city);
							//this.homeData();
							AsyncStorage.setItem('UserDetails', JSON.stringify(data), (err, result) => {
								this.props.userUpdate(data);
								//this.homeData();
							})
						}
					}).catch((err) => {
						this.setState({
							loader: false
						})
					})
				}).catch((err) => {
					Alert.alert('', 'Please turn on the location');
				});




			})
		} else if (this.props.auth.data.details.city) {
			this.setState({ city: this.props.auth.data.details.city });
			//this.homeData();
		}
		// else{
		// 	AsyncStorage.getItem('UserDetails', (err, result) => {
		// 		let data = JSON.parse(result);
		// 		let id = data.details.id + '';
		// 		navigator.geolocation.getCurrentPosition((position) => {
		// 			let latitude = position.coords.latitude;
		// 			let longitude = position.coords.longitude;
		// 			api.post('Users/UpdateLocation.json', { id: id, latitude: latitude, longitude: longitude, device_token_id: "", is_notify: "1" }).then(res => {
		// 				if (res.ack == 1) {
		// 					data.details.country_name = res.country_name;
		// 					data.details.city = res.city;
		// 					AsyncStorage.setItem('UserDetails', JSON.stringify(data), (err, result) => {
		// 						this.props.userUpdate(data);
		// 					})
		// 				}
		// 			}).catch((err) => { })
		// 		})
		// 	})
		// }


		// for making user online and offline;

		// AsyncStorage.getItem('UserDetails', (err, result)=>{
		//    if(result)
		//    {

		// 	   const user_id=JSON.parse(result).details.id;

		// 	     this.state.chatRef.orderByChild('userId').equalTo(user_id).once('value').then((snapshot)=>{
		// 			 if(snapshot && snapshot.val())
		// 			 {
		// 				   const key = Object.keys(snapshot.val())[0];
		// 				   const ref = this.state.chatRef.child(key);
		// 				   let toUpdate=snapshot.val();
		// 				     toUpdate[key].IsOnline=true;
		// 				// const toUpdateData={
		// 				// 	userId:user_id,
		// 				// 	IsOnline:false
		// 				// }
		// 				   ref.update(toUpdate[key]).then((res) => {

		// 				   }).catch((Err)=>{

		// 				   })
		// 			 }
		// 			 else
		// 			 {

		// 				 const data1={userId:user_id, IsOnline:true};
		// 				 this.state.chatRef.push(data1);
		// 			 }
		// 		 })
		//    }
		// })

	}


	goToRatingPage() {
		this.setState({
			IsShowDialog1: false
		});
		setTimeout(() => {
			this.props.navigation.navigate('FeedbackRating', { snapshot: this.state.snapShotVal });
		}, 100)

	}

	selectedFunction(id) {
		let home_nearBy_list = this.state.home_nearBy_list;
		home_nearBy_list.map((data) => {
			if (data.Offers.length != 0) {
				data.Offers.map((data1) => {
					if (id == data1.id) {
						data1.selected = !data1.selected;
					}
				});
			}
		})
		this.setState({
			home_nearBy_list: home_nearBy_list
		});
		debugger
	}


	homeData() {
		const that1 = this;
		if (this.state.connectionInfo != "none") {
			AsyncStorage.getItem('UserDetails', (err, result) => {
				if (result) {
					let UserDetails = JSON.parse(result);
					console.log("UserDetails :", UserDetails);
					let user_id = UserDetails.details.id + '';
					api.post('UserBusinessess/ListConnectBusinessByUser.json', { user_id: user_id }).then(res => {
						console.log('result:',res)
						debugger
						if (res.ack == 1) {


							console.log('home_list', res.details);
							this.setState({
								home_list: res.details,
								image_url: res.image_url,
								all_home_list: res.details,
								home_list_cbbu: res.details
							})

							AsyncStorage.setItem("home_list_cbbu", JSON.stringify(this.state.home_list_cbbu));
							if (this.state.sqlLiteDb) {
								this.state.sqlLiteDb.transaction(function (tx) {

									//tx.executeSql('DROP TABLE IF EXISTS businessList');
									//tx.executeSql('DROP TABLE IF EXISTS businessDetails');
									tx.executeSql('CREATE TABLE IF NOT EXISTS businessList (id integer , description text, image text, is_active boolean, is_deleted boolean, name text, slug text, UserBusinessCategories text, image_url text, noOfOffers text )');
									tx.executeSql('CREATE TABLE IF NOT EXISTS businessDetails (id integer primary key, outlets text, offers text, business_name text, business_images text, user_business_id text, category_id text, image_url text, business_logo text)');
									(async function loop() {
										for (let i = 0; i < res.details.length; i++) {

											await new Promise(resolve => {
												that1.state.sqlLiteDb.transaction((tx2) => {

													tx2.executeSql('SELECT * FROM businessList where id=' + res.details[i].id, [], function (tx, results) {

														let IsInsertBusiness = false;
														let IsUpdate = false;


														if (results.rows.length == 0) {
															IsInsertBusiness = true;
														}
														else {
															const parsedUserBusinessCategories = JSON.parse(results.rows.item(0).UserBusinessCategories);
															if (parsedUserBusinessCategories.length != res.details[i].UserBusinessCategories.length) {
																IsUpdate = true;
															}
														}
														if (IsInsertBusiness) {

															that1.state.sqlLiteDb.transaction((tx3) => {
																tx3.executeSql("INSERT INTO businessList (id, description, image,is_active ,is_deleted,name, slug,UserBusinessCategories, image_url, noOfOffers ) VALUES (?,?,?,?,?,?,?,?,?, ?)", [res.details[i].id, res.details[i].description, res.details[i].image, res.details[i].is_active, res.details[i].is_deleted, res.details[i].name, res.details[i].slug, JSON.stringify(res.details[i].UserBusinessCategories), res.image_url, res.validoffer], function (tx, response) {

																	(async function loop2() {
																		for (let j = 0; j < res.details[i].UserBusinessCategories.length; j++) {

																			await new Promise(resolve1 => {

																				that1.state.sqlLiteDb.transaction((tx4) => {

																					tx4.executeSql('SELECT * FROM businessDetails ', [], function (tx, results) {

																						let IsInsert = false;
																						if (results.rows.length == 0) {
																							IsInsert = true;
																						} else {

																							var len = results.rows.length;
																							for (let k = 0; k < len; k++) {

																								if (results.rows.item(k).user_business_id == res.details[i].UserBusinessCategories[j].user_business_id && results.rows.item(k).category_id == res.details[i].UserBusinessCategories[j].category_id) {
																								}
																								else {
																									IsInsert = true;

																								}
																							}
																							//IsInsert=true;
																						}
																						if (IsInsert) {

																							api.post('UserBusinessess/BusinessOfferDetails.json', {
																								user_business_id: res.details[i].UserBusinessCategories[j].user_business_id,
																								category_id: res.details[i].UserBusinessCategories[j].category_id,
																								user_id: user_id
																							}).then(businessDetails => {

																								if (businessDetails.ack == 1) {
																									debugger
																									let businessData = {
																										Outlets: JSON.stringify(businessDetails.Outlets),
																										offers: JSON.stringify(businessDetails.offers),
																										business_name: businessDetails.business_name,
																										business_images: JSON.stringify(businessDetails.business_images),
																										image_url: businessDetails.image_url,
																										business_logo: businessDetails.business_logo
																									};

																									that1.state.sqlLiteDb.transaction(function (tx1) {

																										tx1.executeSql("INSERT INTO businessDetails (outlets, offers,business_name ,business_images,user_business_id,category_id, image_url, business_logo ) VALUES (?,?,?,?,?,?,?,?)", [businessData.Outlets, businessData.offers, businessData.business_name, businessData.business_images, res.details[i].UserBusinessCategories[j].user_business_id.toString(), res.details[i].UserBusinessCategories[j].category_id.toString(), businessData.image_url, businessData.business_logo], function (tx2, response2) {
																											resolve1();
																										}, function (e) {
																											console.log("ERROR: " + e.message);
																										});

																									})

																								}
																							}).catch((err) => {

																							})
																						} else {
																							resolve1();
																						}



																					}, null);
																				})


																			});

																		}
																		resolve();
																	})();
																	//resolve();
																}, function (e) {
																	console.log("ERROR: " + e.message);
																});
															})

														}
														else {
															if (IsUpdate == true) {


																that1.state.sqlLiteDb.transaction((tx3) => {

																	tx3.executeSql("UPDATE  businessList SET UserBusinessCategories= ? where id=?", [JSON.stringify(res.details[i].UserBusinessCategories), res.details[i].id], function (tx, response) {


																		console.log(response)
																			(async function loop2() {
																				for (let j = 0; j < res.details[i].UserBusinessCategories.length; j++) {

																					await new Promise(resolve1 => {

																						that1.state.sqlLiteDb.transaction((tx4) => {

																							tx4.executeSql('SELECT * FROM businessDetails ', [], function (tx, results) {

																								let IsInsert = false;
																								if (results.rows.length == 0) {
																									IsInsert = true;
																								} else {

																									var len = results.rows.length;
																									for (let k = 0; k < len; k++) {

																										if (results.rows.item(k).user_business_id == res.details[i].UserBusinessCategories[j].user_business_id && results.rows.item(k).category_id == res.details[i].UserBusinessCategories[j].category_id) { } else {
																											IsInsert = true;

																										}
																									}
																								}
																								if (IsInsert) {

																									api.post('UserBusinessess/BusinessOfferDetails.json', {
																										user_business_id: res.details[i].UserBusinessCategories[j].user_business_id,
																										category_id: res.details[i].UserBusinessCategories[j].category_id,
																										user_id: user_id
																									}).then(businessDetails => {

																										if (businessDetails.ack == 1) {


																											let businessData = {
																												Outlets: JSON.stringify(businessDetails.Outlets),
																												offers: JSON.stringify(businessDetails.offers),
																												business_name: businessDetails.business_name,
																												business_images: JSON.stringify(businessDetails.business_images),
																												image_url: businessDetails.image_url,
																												business_logo: businessDetails.business_logo
																											};

																											that1.state.sqlLiteDb.transaction(function (tx1) {

																												tx1.executeSql("INSERT INTO businessDetails (outlets, offers,business_name ,business_images,user_business_id,category_id,image_url, business_logo ) VALUES (?,?,?,?,?,?, ?, ?)", [businessData.Outlets, businessData.offers, businessData.business_name, businessData.business_images, res.details[i].UserBusinessCategories[j].user_business_id.toString(), res.details[i].UserBusinessCategories[j].category_id.toString(), businessData.image_url, businessData.business_logo], function (tx2, response2) {
																													resolve1();
																												}, function (e) {
																													console.log("ERROR: " + e.message);
																												});

																											})

																										}
																									}).catch((err) => {

																									})
																								} else {
																									resolve1();
																								}



																							}, null);
																						})


																					});

																				}
																				resolve();
																			})();
																		//resolve();
																	}, function (e) {


																		console.log("ERROR: " + e.message);
																	});
																})
															}
															else {
																(async function loop2() {
																	for (let j = 0; j < res.details[i].UserBusinessCategories.length; j++) {

																		await new Promise(resolve1 => {
																			that1.state.sqlLiteDb.transaction((tx5) => {
																				tx5.executeSql('SELECT * FROM businessDetails ', [], function (tx, results) {
																					let IsInsert = false;

																					if (results.rows.length == 0) {
																						IsInsert = true;
																					} else {
																						var len = results.rows.length;
																						for (let k = 0; k < len; k++) {

																							if (results.rows.item(k).user_business_id == res.details[i].UserBusinessCategories[j].user_business_id && results.rows.item(k).category_id == res.details[i].UserBusinessCategories[j].category_id) { } else {
																								IsInsert = true;

																							}
																						}
																						//IsInsert = true;
																					}

																					if (IsInsert) {
																						api.post('UserBusinessess/BusinessOfferDetails.json', {
																							user_business_id: res.details[i].UserBusinessCategories[j].user_business_id,
																							category_id: res.details[i].UserBusinessCategories[j].category_id,
																							user_id: res.details[i].UserBusinessCategories[j].user_id
																						}).then(businessDetails => {

																							if (businessDetails.ack == 1) {

																								let businessData = {
																									Outlets: JSON.stringify(businessDetails.Outlets),
																									offers: JSON.stringify(businessDetails.offers),
																									business_name: businessDetails.business_name,
																									business_images: JSON.stringify(businessDetails.business_images),
																									image_url: businessDetails.image_url,
																									business_logo: businessDetails.business_logo
																								};

																								that1.state.sqlLiteDb.transaction(function (tx1) {
																									tx1.executeSql("INSERT INTO businessDetails (outlets, offers,business_name ,business_images,user_business_id,category_id, image_url,business_logo ) VALUES (?,?,?,?,?,?, ?,?)", [businessData.Outlets, businessData.offers, businessData.business_name, businessData.business_images, res.details[i].UserBusinessCategories[j].user_business_id.toString(), res.details[i].UserBusinessCategories[j].category_id.toString(), businessData.image_url, businessData.business_logo], function (tx2, response2) {
																										resolve1();
																									}, function (e) {
																										console.log("ERROR: " + e.message);
																									});

																								})

																							}
																						}).catch((err) => {

																						})
																					} else {
																						resolve1();
																					}



																				}, null);
																			})


																		});

																	}
																	resolve();
																})();
															}
															//  resolve();

														}

													}, null);
												})


											});

										}

									})();



								});
							}
						}
						let getPosition = function (options) {
							return new Promise(function (resolve, reject) {
								navigator.geolocation.getCurrentPosition(resolve, reject, options);
							});
						}
						getPosition().then((position) => {
							let latitude = position.coords.latitude;
							let longitude = position.coords.longitude;

							debugger
							// api.post('Offers/ListOffernearby.json', { latitude: latitude, longitude: longitude, user_id: user_id }).then(resNearBy => {
								api.post('UserBusinessess/NotConnectedByUser.json', {user_id: user_id }).then(resNearBy => {

								debugger
								console.log("rearnearby :", resNearBy);
								if (resNearBy.ack == 1) {

									this.setState({
										home_nearBy_list: resNearBy.details,
										image_url2: resNearBy.business_url
									})
									AsyncStorage.setItem("home_nearBy_list", JSON.stringify(this.state.home_nearBy_list));
									// if (this.state.sqlLiteDb) {
									// 	this.state.sqlLiteDb.transaction(function (tx) {
									// 		//tx.executeSql('DROP TABLE IF EXISTS businessList');

									// 		tx.executeSql('CREATE TABLE IF NOT EXISTS nearByBusinessList (id integer primary key, description text, image text, is_active boolean, is_deleted boolean, name text, slug text, Offers text, image_url text, noOfOffers text )');
									// 		tx.executeSql('SELECT * FROM nearByBusinessList', [], function (tx, results) {
									// 			if (results.rows.length == 0) {
													// (async function loop1() {
													// 	for (let i = 0; i < resNearBy.details.length; i++) {
													// 		await new Promise(resolve => {
													// 			tx.executeSql("INSERT INTO nearByBusinessList (description, image,is_active ,is_deleted,name, slug,Offers, image_url,noOfOffers ) VALUES (?,?,?,?,?,?,?, ?,?)", [resNearBy.details[i].description, resNearBy.details[i].image, resNearBy.details[i].is_active, resNearBy.details[i].is_deleted, resNearBy.details[i].name, resNearBy.details[i].slug, JSON.stringify(resNearBy.details[i].Offers, resNearBy.business_url),], function (tx, res) {
													// 				resolve();
													// 			}, function (e) {
													// 				console.log("ERROR: " + e.message);
													// 			});

													// 		});

													// 	}

													// })
													//();

									// 			}
									// 		}, null);

									// 	});
									// }
								}
								this.setState({
									loader: false
								});
							}).catch((err) => {

								this.setState({ loader: false });
								console.log(err);
							})
						}).catch((err) => {
							Alert.alert('', 'Please turn on the location');
						})
					}).catch((err) => {
						this.setState({ loader: false });
						console.log(err);
					})
				}

			})
		}
		else {

			if (SQLite) {
				SQLite.openDatabase("localLync.db", "1.0", "Demo", -1, successCb, errorCb);
			}

			function successCb(db) {
				db.transaction(function (tx) {
					//tx.executeSql('DROP TABLE IF EXISTS businessList');

					tx.executeSql('CREATE TABLE IF NOT EXISTS businessList (id integer primary key, description text, image text, is_active boolean, is_deleted boolean, name text, slug text, UserBusinessCategories text, image_url text )');
					tx.executeSql('SELECT * FROM businessList', [], function (tx, results) {

						if (results.rows.length != 0) {
							let toPushData = [];
							let imageUrl;
							var len = results.rows.length, i;
							for (i = 0; i < len; i++) {
								let data = { id: results.rows.item(i).id, description: results.rows.item(i).description, image: results.rows.item(i).image, is_active: results.rows.item(i).is_active, is_deleted: results.rows.item(i).is_deleted, name: results.rows.item(i).name, slug: results.rows.item(i).slug, UserBusinessCategories: JSON.parse(results.rows.item(i).UserBusinessCategories) };
								imageUrl = results.rows.item(i).image_url;
								toPushData.push(data);
							}
							that1.setState({
								home_list: toPushData,
								image_url: imageUrl,
								all_home_list: toPushData,
								home_list_cbbu: toPushData,
								sqlLiteDb: db
							})

						}

					}, null);

					tx.executeSql('SELECT * FROM nearByBusinessList', [], function (tx, results) {
						if (results.rows.length != 0) {
							let toPushData = [];
							let imageUrl;
							var len = results.rows.length, i;
							for (i = 0; i < len; i++) {
								let data = { id: results.rows.item(i).id, description: results.rows.item(i).description, image: results.rows.item(i).image, is_active: results.rows.item(i).is_active, is_deleted: results.rows.item(i).is_deleted, name: results.rows.item(i).name, slug: results.rows.item(i).slug, Offers: JSON.parse(results.rows.item(i).Offers) };
								imageUrl = results.rows.item(i).business_url;
								toPushData.push(data);
							}
							that1.setState({
								home_nearBy_list: toPushData,
								image_url2: imageUrl
							})
							debugger

						}

					}, null);

				});
			}
			function errorCb(err) {
				console.log(err);

			}

		}

	}

	favourite(business_id) {
		if (this.state.connectionInfo != "none") {
			AsyncStorage.getItem('UserDetails', (err, details) => {
				let UserDetails = JSON.parse(details);
				let user_id = UserDetails.details.id + '';
				this.setState({
					loader: true
				});
				api.post('UserBusinessess/addFavourite.json', { user_id: user_id, business_id: business_id }).then(res => {
					// this.homeData();
					this.homeData();
					this.setState({
						loader: false
					});
				}).catch((err) => {
					this.setState({ loader: false });
					console.log(err);
				})
			});
		}
		else {
			Alert.alert("", "Please connect to internet to be able to make it as favourite.");
		}

	}

	// supportSearch(text) {
	// 	if (text) {
	// 		const regex = new RegExp(`${text.trim()}`, 'i');
	// 		let items = this.state.all_home_list.filter(
	// 			item => item.name.search(regex) >= 0 || item.description.search(regex) >= 0);
	// 		if (items.length > 0) {
	// 			this.setState({ home_list: items });
	// 		}
	// 		else {
	// 			let categoriesList = [];
	// 			for (let i = 0; i < this.state.all_home_list.length; i++) {
	// 				let IsPresent = false;
	// 				for (let j = 0; j < this.state.all_home_list[i].UserBusinessCategories.length; j++) {
	// 					if (this.state.all_home_list[i].UserBusinessCategories[j].business_name.toLowerCase().includes(text.toLowerCase())) {
	// 						IsPresent = true;
	// 					}
	// 				}
	// 				if (IsPresent) {
	// 					categoriesList.push(this.state.all_home_list[i]);
	// 				}
	// 			}
	// 			this.setState({ home_list: categoriesList });
	// 		}
	// 	}
	// 	else {
	// 		const all_home_list = this.state.all_home_list;
	// 		this.setState({ home_list: all_home_list });
	// 	}
	// }


	supportSearch(text) {
		AsyncStorage.getItem("home_list_cbbu").then((res) => {
			debugger
			AsyncStorage.getItem("home_nearBy_list").then((res1) => {
				debugger
				let all_home_list_new = JSON.parse(res);
				let home_nearBy_list = JSON.parse(res1);
				if (text) {
					debugger
					//let all_home_list_new = this.state.home_list_cbbu;
					let home_list_new = [];
					let home_nearBy_list_new = [];
					const regex = new RegExp(`${text.trim()}`, 'i');
					debugger
					for (let i = 0; i < all_home_list_new.length; i++) {
						let item2;
						item2 = all_home_list_new[i].UserBusinessCategories.filter(
							item2 => item2.business_name.search(regex) >= 0);
						all_home_list_new[i].UserBusinessCategories = item2;
						if (item2.length) {
							home_list_new.push(all_home_list_new[i]);
						}

					}
					debugger
					for (let i = 0; i < home_nearBy_list.length; i++) {
						let item2;
						item2 = home_nearBy_list[i].UserBusinessCategories.filter(
							item2 => item2.business_name.search(regex) >= 0);
						home_nearBy_list[i].UserBusinessCategories = item2;
						if (item2.length) {
							home_nearBy_list_new.push(home_nearBy_list[i]);
						}

					}
					debugger
					this.setState({
						home_list: home_list_new,
						home_nearBy_list: home_nearBy_list_new
					});
				debugger
				}
				else {
					this.homeData();
					// console.log(home_nearBy_list);
					// this.setState({
					// 	home_list: all_home_list_new,
					// 	home_nearBy_list: home_nearBy_list
					// });
				}
			})
		})
	
	}

   

	/** connect and get offers */
	connectAndGetOffer(business_id) {
			debugger
		
		this.setState({
			//id: id,
			loader: true,
		
		});
      				
		const data = {
			"user_id": this.state.userId,
			"business_id": [
				{
					"bid": business_id
				}
			]
		};
		
		api.post('UserBusinessess/BusinessDetails.json', {id:business_id}).then(result => {
			debugger
			this.setState({
				businessdata : result.details
			});
			
			debugger

			 	// if (this.state.id == '' || this.state.id != id) {
			api.post('UserBusinessess/connectBusiness.json', data).then(res => {
				//api.post('UserBusinessess/addFavourite.json', data).then(res => {
					
				debugger
				let amount = 0;
				if (res) {

					if (this.state.businessdata.connect_token && res.businessuserdetails[0].blockchain_id && res.businessuserdetails[0].blockchain_address) {
						if (this.state.businessdata.connect_token != null || this.state.businessdata.connect_token == '') {
							amount = this.state.businessdata.connect_token;
						} else {
							amount = 0;
						}

					}

					let item1 = { bizWalletId: res.businessuserdetails[0].blockchain_id, bizWalletAddress: res.businessuserdetails[0].blockchain_address, custWalletAddress: this.state.customer_blockchain_address, amount: amount };
					debugger
					api.walletPost('reward-customer', item1).then((res) => {
						this.homeData();
						//Alert.alert('', "Connection added");  
					}).catch((err) => {
						
					})
					this.setState({
						message: res.msg,
						loader: false
					});

					this.homeData();
				}
				// Alert.alert('', "Connection added");
			}).catch((err) => {
				this.setState({
					loader: false
				});
			});
			this.homeData();
			Alert.alert('', "Connection added,Congratulations!"+amount+" tokens added to your wallet");                    
			}).catch((err) => {
				console.log("error",err)
				 
				this.setState({
					loader: false
				});
			});

	  

	}


	// showAnotherDialog() {
	// 	this.setState({
	// 		IsShowDialog: false,
	// 		IsShowDialog1: true
	// 	})
	// }

	// goToOffersPage() {

	// 	//console.warn('snap',this.state.snapShotVal)
	// 	if (this.state.snapShotVal) {
	// 		debugger;
	// 		this.setState({
	// 			loader: true,
	// 			IsShowDialog: false,
	// 			IsShowDialog1: false
	// 		})
	// 		debugger
	// 		setTimeout(() => {
	// 			api.post('UserBusinessess/BusinessDetails.json', { id: this.state.snapShotVal.businessId }).then(res => {
	// 				this.props.navigation.navigate('Details', { Details: res.details })
	// 			}).catch((err) => {
	// 				this.setState({ loader: false });

	// 			})
	// 		}, 100)
	// 	}
	// }

	// goWalletPage() {
	// 	this.setState({
	// 		IsShowDialog1: false,
	// 		IsShowDialog: false
	// 	});
	// 	setTimeout(() => {
	// 		AsyncStorage.getItem('user_business_id', (err, result) => {
	// 			if (result) {
	// 				console.warn(result);
	// 				let data = JSON.parse(result);
	// 				this.props.navigation.navigate('Wallet', { user_business_id: data, snapshot: this.state.snapShotVal });
	// 			} else {
	// 				console.log("err in gowalletpage :", err);
	// 			}
	// 		});
	// 	}, 100)

	// }

	render() {
		return (
			<Container >
				<StatusBar
					backgroundColor="#133567"
				/>


				<Header style={[commonStyles.headerWarp, { height: 30, paddingTop: 5 }]} androidStatusBarColor="#133567" noShadow>

					<TouchableOpacity style={commonStyles.headerBackBtn}
					// onPress={() => this.props.navigation.navigate('Login')} 
					>
						{/* <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} /> */}
					</TouchableOpacity>

					<Body style={styleSelf.tac}>
						<TouchableOpacity style={styles.disWarp} onPress={() => this.props.navigation.navigate('DistrictLocation')}>
							<Entypo name='location-pin' style={styles.disbeforeIcon} />
							{
								this.state.city ? (
									<Text style={styles.disText}>{
										this.state.city.toLowerCase() == 'not found' ? 'District' : this.state.city
									}</Text>
								) : (
										<Text style={styles.disText}>District</Text>
									)
							}
							<Ionicons name='ios-arrow-down' style={styles.disafterIcon} />
						</TouchableOpacity>
					</Body>
					<TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1} >
						{/* <Ionicons name='ios-arrow-back' style={{ color: '#fff', fontSize: 20 }} /> */}
					</TouchableOpacity>
				</Header>
				<FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
				<View style={{ backgroundColor: '#2096AB', padding: 10, paddingTop: 0, paddingBottom: 5 }}>
					<View style={{ backgroundColor: '#fff', borderRadius: 20, flexDirection: 'row', alignItems: 'center' }}>
						<Ionicons style={{ color: '#444444', marginRight: 5, fontSize: 20, marginLeft: 15 }} name='ios-search' />
						<TextInput style={{ height: 30, flex: 1, marginBottom: 0, paddingTop: 2, paddingBottom: 0, fontSize: 11 }} underlineColorAndroid='transparent' placeholder='Search by name or keyword...' onChangeText={(text) => this.supportSearch(text)} />
					</View>
				</View>
				{/* <Dialog.Container visible={this.state.IsShowDialog}>
					<Dialog.Title>Confirm</Dialog.Title>
					<Dialog.Description>
						Do you want to redeem more offer?
          </Dialog.Description>
					<Dialog.Button label="No" onPress={() => this.showAnotherDialog()} />
					<Dialog.Button label="Yes" onPress={() => this.goToOffersPage()} />
				</Dialog.Container> */}

				{/* <Dialog.Container visible={this.state.IsShowDialog1}>
					<Dialog.Title>Confirm</Dialog.Title>
					<Dialog.Description>
						Do you want to use your tokens to pay for the service?
          </Dialog.Description>
					<Dialog.Button label="No" onPress={() => this.goToRatingPage()} />
					<Dialog.Button label="Yes" onPress={() => this.goWalletPage()} />
				</Dialog.Container> */}

				<Tabs
					locked={true}
					tabBarUnderlineStyle={{ backgroundColor: '#3ab3ce', height: 2, borderBottomWidth: 0 }}
				>
					<Tab
						heading={'Offers from connections '}
						tabStyle={{ backgroundColor: '#f6f6f6', }}
						textStyle={{ color: '#888888', fontSize: 13 }}
						activeTabStyle={{ backgroundColor: '#f6f6f6' }}
						activeTextStyle={{ color: '#3ab3ce', fontSize: 13 }}
					>
						<Content style={styles.homeContent}>
							{
								this.state.home_list.length != 0 ? (
									this.state.home_list.map((data, key) => {
										return (
											<View key={key}>
												<View style={styles.homehdWarp}>
													<View style={styles.homeTextWarp}>
														<Text style={styles.homeTextWarpText}> {data.name} </Text>
													</View>
													<TouchableOpacity style={styles.seeAll} onPress={() => this.props.navigation.navigate('SubCategories', { subId: data.id, subname: data.name, city: this.state.city })}>
														<Text style={styles.seeAllText}>See all</Text>
														<MaterialIcons name='navigate-next' style={styles.seeAllIcon} />
													</TouchableOpacity>
												</View>
												{
													data.UserBusinessCategories.length != 0 ? (
														<ScrollView
															showsHorizontalScrollIndicator={false}
															horizontal={true}
															pagingEnabled={true}
														>
															{
																data.UserBusinessCategories.map((subdata, subkey) => {
																	return (
																		<TouchableOpacity style={styles.homeItemWarp} key={subkey} onPress={() => this.props.navigation.navigate('Details',
																			// { user_business_id: subdata.id, category_id: data.id }
																			{ Details: subdata }
																		)}>
																		{subdata.review_unread?
																			(<TouchableOpacity style={styles.leftIcon}>
																				<FontAwesome name='envelope' style={[styles.rightIconTag, { color: '#73F12C' }]} />
																				< Badge danger style={styles.roundBadge}>
																					<Text style={styles.badgeText}>{subdata.review_unread}</Text>
																				</Badge>
																			</TouchableOpacity>)
																		:
																			null
																		}
																			
																			<TouchableOpacity style={styles.homeItemLove} onPress={() => this.favourite(subdata.user_business_id)}>
																				{
																					subdata.favourite_status ? (
																						<Entypo name='heart' style={[styles.homeItemLoveIcon, { color: 'red' }]} />
																					) : (
																							<Entypo name='heart-outlined' style={styles.homeItemLoveIcon} />
																						)
																				}
																			</TouchableOpacity>
																			<View style={styles.homeItemImageWarp}>
																				
																				{
																					subdata.business_logo ? (
																						<Image source={{ uri: this.state.image_url + subdata.business_logo}} style={styles.homeItemImage}></Image>
																					) : (
																							<Image source={require('../../../img/icons/no-image.png')} style={styles.homeItemImage}></Image>
																						)
																				}
																				{/* { <Image source={{ uri: this.state.image_url2 + subdata.business_logo }} style={styles.homeItemImage}></Image> } */}

																			</View>
																			<View style={styles.homeItemTextWarp}>
																				<Text style={styles.homeItemHead}> {subdata.business_name} </Text>
																				{
																					subdata.offerdata ? (
																						// < View >
																						// 	<Text style={[styles.cuisines, {fontSize:10}]} numberOfLines={1} >OFFER:</Text>
																						// 	<Text style={[styles.homeItemHead, {fontSize:10}       ]}>Get {subdata.offerdata.discount} %Discount and more </Text>
																						<View>
																							<Image source={require('../../../img/icons/prsnt.png')} style={styles.prsnt} />
																							<Text style={[styles.cuisines, {fontSize:10}]} numberOfLines={1} >{subdata.offerno} Offer(s) from {subdata.outletno} outlet(s)</Text>
																						
																							{/* <Text style={styles.cuisines} numberOfLines={1} >Plus {subdata.total_token} Reward Tokens</Text> */}
																							{subdata.reward_token ?
																							<Text style={styles.cuisines} >{subdata.reward_token} Tokens for your feedback</Text>:
																							null
																							}
																							{subdata.total_token && (Number(subdata.total_token)-Number(subdata.reward_token)!=0) ?
																							
																							<Text style={styles.cuisines} >More Tokens for offer redemption</Text>:
																							null
																							}
																							
																							{/* <Text style={styles.homeItemHead}>{subdata.total_token} </Text> */}
																						</View>
																					) : (
																							<View></View>
																						)
																				}



																				{/* <View style={[styles.tabItemPrice, { marginBottom: 8 }]}>
																					<Image source={require('../../../img/icons/prsnt.png')} style={styles.prsnt} />
																					<Text style={[styles.tabItemPriceText, { fontSize: 10 }]}>Offers from {subdata.validoffer ? subdata.validoffer : 0} outlets</Text>
																				</View> */}
																				{/* <Text numberOfLines={1} style={styles.discountText}>10% exclusive discount &amp; more</Text> */}
																				{/* <Text style={styles.cuisines} numberOfLines={1} >Cuisines:</Text>
																				<Text style={[styles.cuisinesDown]} numberOfLines={1}>Sea food, Indian, American</Text> */}
																				{/* <Text>
																				{
																					subdata && subdata.validOffer?
																						<Text style={[styles.cuisinesDown]} numberOfLines={1}>Offers from {subdata.validoffer} outlet</Text>
																					:
																					''
																				}
																				</Text> */}
																				<View style={[styles.tabItemPrice]}>
																					<Entypo name='location-pin' />
																					<Text style={styles.locationText} numberOfLines={1}>Nearest outlet : <Text style={{ color: '#5dbcb1' }}>{subdata.distance ? subdata.distance + "km" : "N/A"}</Text></Text>
																				</View>
																			</View>
																			{/* <TouchableOpacity style={[styles.connectbtn]} onPress={() => this.selectedFunction(data.id)}>                                                            
																				<Text style={{color:'#fff'}}>Connect</Text>                                                               
																			</TouchableOpacity> */}
																		</TouchableOpacity>
																	)
																})
															}
														</ScrollView>
													) : null
												}

											</View>
										)
									})

								) : (<Text style={{ width: '100%', textAlign: 'center', marginTop: 30 }}>No data found</Text>)
							}
							<View style={{ marginTop: 20 }}></View>
						</Content>
					</Tab>

					<Tab
						heading={'Other nearby offers'}
						tabStyle={{ backgroundColor: '#f6f6f6', }}
						textStyle={{ color: '#888888', fontSize: 13 }}
						activeTabStyle={{ backgroundColor: '#f6f6f6' }}
						activeTextStyle={{ color: '#3ab3ce', fontSize: 13 }}
					>
						<Content style={styles.homeContent}>

							{
								this.state.home_nearBy_list.length != 0 ? (
									this.state.home_nearBy_list.map((data, key) => {
										return (
											<View key={key}>
												<View style={styles.homehdWarp}>
													<View style={styles.homeTextWarp}>
														<Text style={styles.homeTextWarpText}> {data.name} </Text>
													</View>
													<TouchableOpacity style={styles.seeAll} onPress={() => this.props.navigation.navigate('NearBysubCategories', { subId: data.id, subname: data.name })}>
														<Text style={styles.seeAllText}>See all</Text>
														<MaterialIcons name='navigate-next' style={styles.seeAllIcon} />
													</TouchableOpacity>
												</View>
												{
													data.UserBusinessCategories.length != 0 ? (
														<ScrollView	showsHorizontalScrollIndicator={false}	horizontal={true}	>
															{
																data.UserBusinessCategories.map((subdata, subkey) => {
																	return (
																		<View style={styles.homeItemWarp} key={subkey}>																			
																			{/* <TouchableOpacity style={styles.homeItemLove} onPress={() => this.favourite(subdata.id)}>
																				{
																					subdata.favourite_status ? (
																						<Entypo name='heart' style={[styles.homeItemLoveIcon, { color: 'red' }]} />
																					) : (
																							<Entypo name='heart-outlined' style={styles.homeItemLoveIcon} />
																						)
																				}
																			</TouchableOpacity> */}

																			{
																				//subdata.business_images === undefined ? null : (
																					subdata.business_logo === undefined ? null : (
																					<View style={styles.homeItemImageWarp}>
																						{
																							//subdata.business_images.length == 0 || subdata.business_images.length === undefined ? (
																								subdata.business_logo == 0 || subdata.business_logo === null ? (
																								<Image source={require('../../../img/icons/no-image.png')} style={styles.homeItemImage}></Image>
																							) : (
																									<Image source={{ uri: this.state.image_url + subdata.business_logo }} style={styles.homeItemImage}></Image>
																								)
																						}
																						{/* <Image source={{ uri: this.state.image_url2 + subdata.business_logo }} style={styles.homeItemImage}></Image> */}
																					</View>
																				)
																			}


																			<View style={styles.homeItemTextWarp}>
																				<Text style={styles.homeItemHead}>{subdata.offer_name}</Text>
																				<View style={[styles.tabItemPrice, { marginBottom: 8,flexDirection:'column' }]}>
																				<View>
																					<Text style={styles.homeItemHead}> {subdata.business_name} </Text>
																					</View>
																					< View  style={{flexDirection:'row', marginBottom:5}}>
																					<Image source={require('../../../img/icons/prsnt.png')} style={styles.prsnt} />
																					
																							<Text style={[styles.cuisines, {fontSize:10}]} numberOfLines={1} >{subdata.offerno} Offer(s) from {subdata.outletno} outlet(s)</Text>
																					</View>
																							{/* <Text style={styles.cuisines} numberOfLines={1} >Plus {subdata.total_token} Reward Tokens</Text> */}
																							{subdata.connect_token || subdata.reward_token?
																								<Text style={styles.cuisines} numberOfLines={1} >Plus Reward Tokens</Text>:
																								null
																							}
																							
																							{/* <Text style={styles.homeItemHead}>{subdata.total_token} </Text> */}
																						
																					{/* <Text style={[styles.tabItemPriceText, { fontSize: 10 }]}>Business name:{subdata.business_name}</Text> */}
																					{/* <Text style={[styles.tabItemPriceText, { fontSize: 10 }]}>Offers from {data.validoffers ? data.validoffers : 0} outlets</Text> */}
																				</View>


																				{/* <Text numberOfLines={1} style={styles.discountText}>{subdata.discount_amount}% exclusive discount & more</Text>
																				<Text style={styles.cuisines} numberOfLines={1} >Details:</Text>
																				<Text style={[styles.cuisinesDown]} numberOfLines={1}>{subdata.description}</Text>
																				<Text style={styles.cuisines} numberOfLines={1}>Business name:</Text>
																				<Text style={[styles.cuisinesDown]} numberOfLines={1}>{subdata.business_name}</Text> */}
																				<View style={[styles.tabItemPrice]}>
																					<Entypo name='location-pin' />
																					<Text style={styles.locationText} numberOfLines={1}>Nearest outlet : <Text style={{ color: '#5dbcb1' }}>{subdata.distance ? subdata.distance + "km" : "N/A"}</Text></Text>
																				</View>
																				{/* <View style={[styles.tabItemPrice]}>
																					
																					<Entypo name='location-pin' />
																					<Text style={styles.locationText} numberOfLines={1}>Nearest outlet : <Text style={styles.locationTextInr}>2.4 km</Text></Text>
																				</View> */}
																				{/* <View style={styles.connectAndGetOffer}>
																					<TouchableOpacity style={styles.connectAndGetOfferBtn} onPress={() => this.connectAndGetOffer(subdata.business_id, subdata.id)}>
																						<Text style={styles.connectAndGetTxt}>Add and get offer</Text>
																					</TouchableOpacity>
																				</View> */}
																				<View>
																					{/* {
																						subdata.selected ?
																							<View>
																								<Text style={styles.cntxt}>Connection Added</Text>
																								<TouchableOpacity
																									style={[styles.btncn, { backgroundColor: '#7CA8AF' }]}
																									onPress={() => this.selectedFunction(subdata.id)}
																								>
																									<Text style={styles.btntxt}>Disconnect</Text>
																								</TouchableOpacity>
																								<TouchableOpacity
																									style={[styles.btncn, { backgroundColor: '#00BBD7' }]}
																									onPress={() => this.connectAndGetOffer(subdata.business_id, subdata.id)}
																								>
																									<Text style={styles.btntxt}>Submit</Text>
																								</TouchableOpacity>
																							</View>
																							:
																							<TouchableOpacity
																								style={[styles.btncn, { backgroundColor: '#00BBD7' }]}
																								onPress={() => this.selectedFunction(subdata.id)}
																							>
																								<Text style={styles.btntxt}>Connect</Text>
																							</TouchableOpacity>
																					} */}

																					<TouchableOpacity
																						style={[styles.btncn, { backgroundColor: '#fff',borderColor:'#00BBD7',borderWidth:1, marginTop:10 }]}
																						onPress={() => this.connectAndGetOffer(subdata.user_business_id)}
																					>
																						<Text style={styles.btntxt}>Connect</Text>
																					</TouchableOpacity>
																				</View>
																			</View>

																		</View>
																	)
																})
															}
														</ScrollView>
													) : null
												}

											</View>
										)
									})

								) : (<Text style={{ width: '100%', textAlign: 'center', marginTop: 30 }}>No data found</Text>)
							}
							<View style={{ marginTop: 20 }}></View>
						</Content>
					</Tab>

				</Tabs>

				<Footer>
					<FooterTab style={commonStyles.footerTab}>
						<TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Home')} >
							<View style={commonStyles.footerItemImageWarp}>
								<Image source={require('../../../img/icons/iconHome.png')} style={commonStyles.footerItemImage} />
							</View>
							<Text style={[commonStyles.footerItemText, commonStyles.footerItemTextActive]}>Home</Text>
						</TouchableOpacity>
						<TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Connections')}>
							<View style={commonStyles.footerItemImageWarp}>
								<Image source={require('../../../img/icons/connection.png')} style={commonStyles.footerItemImage} />
							</View>
							<Text style={commonStyles.footerItemText}>Connection</Text>
						</TouchableOpacity>
						<TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Favorite')}>
							<View style={commonStyles.footerItemImageWarp}>
								<Image source={require('../../../img/icons/love.png')} style={commonStyles.footerItemImage} />
							</View>
							<Text style={commonStyles.footerItemText}>Favorite</Text>
						</TouchableOpacity>
						<TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Notification')}>
							<View style={[commonStyles.footerItemImageWarp, { position: 'relative', }]}>
							{this.state.unreadcount!=0 ?
								(<Text style={commonStyles.notificationText}>{this.state.unreadcount}</Text>):
								null
							}
							<Image source={require('../../../img/icons/notification.png')} style={commonStyles.footerItemImage} />
							</View>
							<Text style={commonStyles.footerItemText} >Notification</Text>
						</TouchableOpacity>
						<TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('MyAccount')}>
							<View style={commonStyles.footerItemImageWarp}>
								<Image source={require('../../../img/icons/profile.png')} style={commonStyles.footerItemImage} />
							</View>
							<Text style={commonStyles.footerItemText} >Profile</Text>
						</TouchableOpacity>
					</FooterTab>
				</Footer>
			</Container>
		);
	}
}
Home.propTypes = {
	auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
	return {
		auth: state.auth,
		currentRoute: state.RouterOwn.currentRoute,
		prevRoute: state.RouterOwn.prevRoute
	}
}

const mapDispatchToProps = dispatch => ({
	userUpdate: (data) => dispatch(userUpdate(data))
});
export default connect(mapStateToProps, mapDispatchToProps)(Home);
