import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, ImageBackground, AsyncStorage, ScrollView } from "react-native";
import { Container, Header, Body, Content, Tabs, Tab, TabHeading, ScrollableTab, Footer, FooterTab, Button, } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import FSpinner from 'react-native-loading-spinner-overlay';
import api from '../../api'

const deviceWidth = Dimensions.get("window").width;

class SubCategories extends Component {
    constructor(params) {
        super(params)
        this.state = {
            loader: false,
            home_list: [],
            image_url: '',
            query: 'Hello',
            all_home_list: [],
            city: this.props.navigation.state.params && this.props.navigation.state.params.city?this.props.navigation.state.params.city:'',
              unreadcount: 0
        }
    }


    componentDidMount() {
        this.AllData();
    }

    AllData() {
        if (this.props.navigation.state.params) {
            AsyncStorage.getItem('UserDetails', (err, details) => {
                this.setState({ loader: true })
                let UserDetails = JSON.parse(details);
                let user_id = UserDetails.details.id + '';
                this.setState({ loader: true });
                 api.post('notifications/unreadCount.json', {
                     user_id: user_id
                 }).then((notification) => {
                     if (notification.ack == 1) {
                         this.setState({
                             unreadcount: notification.chat_unread+notification.connect_unread+notification.review_unread
                         })
                     }
                 })
                api.post('UserBusinessess/ListConnectBusinessByCat.json', { category_id: this.props.navigation.state.params.subId, user_id: user_id }).then(res => {
                    debugger
                    if (res.ack == 1) {
                        this.setState({
                            home_list: res.details[0].UserBusinessCategories,
                            image_url: res.image_url,
                            all_home_list: res.details[0].UserBusinessCategories,
                            loader: false
                        })
                        debugger
                    }
                    else {
                        this.setState({ loader: false });
                    }
                }).catch((err) => {
                    this.setState({ loader: false });
                    console.log(err);
                })
            }
            )
        }
    }
    supportSearch(text) {
        if (text) {
            const regex = new RegExp(`${text.trim()}`, 'i');
            let items = this.state.all_home_list.filter(
                item => item.business_name.search(regex) >= 0);
            this.setState({ home_list: items });
        }
        else {
            const all_home_list = this.state.all_home_list;
            this.setState({ home_list: all_home_list });
        }

    }

    favourite(business_id) {
        AsyncStorage.getItem('UserDetails', (err, details) => {
            let UserDetails = JSON.parse(details);
            let user_id = UserDetails.details.id + '';
            this.setState({
                loader: true
            });
            api.post('UserBusinessess/addFavourite.json', { user_id: user_id, business_id: business_id }).then(res => {
                this.AllData();
                this.setState({
                    loader: false
                });
            }).catch((err) => {
                this.setState({ loader: false });
                console.log(err);
            })
        });
    }


    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header style={[commonStyles.headerWarp, { height: 30, paddingTop: 5 }]} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn}
                    // onPress={() => this.props.navigation.navigate('Login')} 
                    >
                        {/* <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} /> */}
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <TouchableOpacity style={styles.disWarp} onPress={() => this.props.navigation.navigate('DistrictLocation')}>
                            <Entypo name='location-pin' style={styles.disbeforeIcon} />
                            <Text style={styles.disText}>{this.state.city}</Text>
                            <Ionicons name='ios-arrow-down' style={styles.disafterIcon} />
                        </TouchableOpacity>
                    </Body>
                    <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1} >
                        {/* <Ionicons name='ios-arrow-back' style={{ color: '#fff', fontSize: 20 }} /> */}
                    </TouchableOpacity>
                </Header>
                <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                <View style={{ backgroundColor: '#2096AB', padding: 10, paddingTop: 0, paddingBottom: 5 }}>
                    <View style={{ backgroundColor: '#fff', borderRadius: 20, flexDirection: 'row', alignItems: 'center' }}>
                        <Ionicons style={{ color: '#444444', marginRight: 5, fontSize: 20, marginLeft: 15 }} name='ios-search' />
                        <TextInput onChangeText={(text) => this.supportSearch(text)} style={{ height: 30, flex: 1, marginBottom: 0, paddingTop: 2, paddingBottom: 0, fontSize: 11 }} underlineColorAndroid='transparent' placeholder='Search by name or keyword...' />
                    </View>
                </View>

                <Content style={styles.homeContent}>
                    <View style={styles.homehdWarp}>
                        <View style={styles.homeTextWarp}>
                            <Text style={styles.homeTextWarpText}> Business List For {this.props.navigation.state.params.subname} </Text>
                        </View>
                    </View>
                    <View style={{ flexWrap: 'wrap', flexDirection: 'row' }}>

                        {
                            this.state.home_list.map((subdata, subkey) => {
                                return (
                                    <TouchableOpacity style={[styles.homeItemWarp, { marginBottom: 16 }]} key={subkey} onPress={() => this.props.navigation.navigate('Details', { Details: subdata })}>
                                        <TouchableOpacity style={styles.homeItemLove} onPress={() => this.favourite(subdata.user_business_id)}>
                                            {
                                                subdata.favourite_status ? (
                                                    <Entypo name='heart' style={[styles.homeItemLoveIcon, { color: 'red' }]} />
                                                ) : (
                                                        <Entypo name='heart-outlined' style={styles.homeItemLoveIcon} />
                                                    )
                                            }
                                        </TouchableOpacity>
                                        <View style={styles.homeItemImageWarp}>
                                            {/* <Image source={{ uri: this.state.image_url + subdata.business_logo }} style={styles.homeItemImage}></Image> */}
                                            {
                                               // subdata.business_images.length == 0 ? (
                                                subdata.business_logo == null || subdata.business_logo == ''  ? (
                                                    <Image source={require('../../../img/icons/no-image.png')} style={styles.homeItemImage}></Image>
                                                ) : (
                                                        <Image source={{ uri: this.state.image_url + subdata.business_logo }} style={styles.homeItemImage}></Image>
                                                    )
                                            }
                                        </View>
                                        <View style={styles.homeItemTextWarp}>
                                            <Text style={styles.homeItemHead}> {subdata.business_name} </Text>
                                            {/* <View style={[styles.tabItemPrice, { marginBottom: 8 }]}>
                                               
                                                <Text style={[styles.tabItemPriceText, { fontSize: 10 }]}>{subdata.offerno} Offers from {subdata.outletno} outlets</Text>
                                            </View> */}
                                            <Text style={[styles.cuisines, { fontSize: 10 }]} numberOfLines={1} >{subdata.offerno} Offer(s) from {subdata.outletno} outlet(s)</Text>

                                            {/* <Text style={styles.cuisines} numberOfLines={1} >Plus {subdata.total_token} Reward Tokens</Text> */}
                                            {subdata.reward_token ?
                                                <Text style={styles.cuisines}  >{subdata.reward_token} Tokens for your feedback</Text> :
                                                null
                                            }
                                            {subdata.total_token && (Number(subdata.total_token) - Number(subdata.reward_token) != 0) ?

                                                <Text style={styles.cuisines}>More Tokens for offer redemption</Text> :
                                                null
                                            }
                                            {/* <Text numberOfLines={1} style={styles.discountText}>10% exclusive discount &amp; more</Text> */}
                                            {/* <Text style={styles.cuisines} numberOfLines={1} >Cuisines:</Text>
                                            <Text style={[styles.cuisinesDown]} numberOfLines={1}>Sea food, Indian, American</Text> */}
                                            <View style={[styles.tabItemPrice]}>
                                                <Entypo name='location-pin' />
                                                <Text style={styles.locationText} numberOfLines={1}>Nearest outlet : <Text style={{ color: '#5dbcb1' }}>{subdata.distance ? subdata.distance + "km" : "N/A"}</Text></Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                )
                            })


                        }
                    </View>


                </Content>

                <Footer>
                    <FooterTab style={commonStyles.footerTab}>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Home')} >
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/iconHome.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={[commonStyles.footerItemText, commonStyles.footerItemTextActive]}>Home</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Connections')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/connection.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={commonStyles.footerItemText}>Connection</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Favorite')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/love.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={commonStyles.footerItemText}>Favorite</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Notification')}>
							<View style={[commonStyles.footerItemImageWarp, { position: 'relative', }]}>
							{this.state.unreadcount!=0 ?
								(<Text style={commonStyles.notificationText}>{this.state.unreadcount}</Text>):
								null
							}
							<Image source={require('../../../img/icons/notification.png')} style={commonStyles.footerItemImage} />
							</View>
							<Text style={commonStyles.footerItemText} >Notification</Text>
						</TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('MyAccount')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/profile.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={commonStyles.footerItemText} >Profile</Text>
                        </TouchableOpacity>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}
export default SubCategories;
