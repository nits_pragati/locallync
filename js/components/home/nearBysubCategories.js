import React, { Component } from "react";
import PropTypes from 'prop-types';
import {
    connect
} from 'react-redux';
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, ImageBackground, AsyncStorage, ScrollView, Alert } from "react-native";
import { Container, Header, Body, Content, Tabs, Tab, TabHeading, ScrollableTab, Footer, FooterTab, Button, } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import FSpinner from 'react-native-loading-spinner-overlay';
import api from '../../api'

const deviceWidth = Dimensions.get("window").width;

class NearBysubCategories extends Component {
    constructor(params) {
        super(params)
        this.state = {
            loader: false,
            home_list: [],
            image_url: '',
            query: '',
            all_home_list: [],
            hearderName: '',
            city: '',
            userId: '',
            unreadcount: 0,
            businessdata:[]
        }
    }


    componentDidMount() {
        this.AllData();
    }
    componentWillReceiveProps() {

        if (this.props.auth.data && this.props.auth.data.details.city) {
            this.setState({
                city: this.props.auth.data.details.city
            })
        }


    }

    AllData() {
        if (this.props.navigation.state.params) {
            debugger
            AsyncStorage.getItem('UserDetails', (err, details) => {
                this.setState({ loader: true })
                let UserDetails = JSON.parse(details);
                let user_id = UserDetails.details.id + '';
                api.post('notifications/unreadCount.json', {
                    user_id: user_id
                }).then((notification) => {
                    if (notification.ack == 1) {
                        this.setState({
                            unreadcount: notification.chat_unread+notification.connect_unread+notification.review_unread+notification.offer_unread
                        })
                    }
                    debugger
                })
                let getPosition = function (options) {
                    return new Promise(function (resolve, reject) {
                        navigator.geolocation.getCurrentPosition(resolve, reject, options);
                    });
                }

                getPosition().then((position) => {
                    let latitude = position.coords.latitude;
                    let longitude = position.coords.longitude;

                   // api.post('Offers/ListOffernearbyAllcat.json', { latitude: latitude, longitude: longitude, category_id: this.props.navigation.state.params.subId, user_id: user_id }).then(res => {
                      api.post('UserBusinessess/NotConnectedByCat.json', { category_id: this.props.navigation.state.params.subId, user_id: user_id }).then(res => {
                          debugger;
                        if (res.ack == 1) {
                            debugger
                            this.setState({
                                // home_list: res.details[0].Offers,
                                // image_url: res.business_url,
                                // all_home_list: res.details[0].Offers,
                                // loader: false,
                                // userId: user_id
                                home_list: res.details[0].UserBusinessCategories,
                                image_url: res.image_url,
                                all_home_list: res.details,
                                loader: false,
                                userId: user_id
                            })
                            debugger
                        }
                        else {
                            this.setState({ loader: false });
                        }
                    }).catch((err) => {
                        debugger
                        this.setState({ loader: false });
                        console.log(err);
                    })
                }).catch((err) => {
                    debugger
                    Alert.alert('', 'Please turn on the location');
                })
            })

        }
    }
    supportSearch(text) {
        if (text) {
            const regex = new RegExp(`${text.trim()}`, 'i');
            let items = this.state.all_home_list.filter(
                item => item.offer_name.search(regex) >= 0);
            this.setState({ home_list: items });
        }
        else {
            const all_home_list = this.state.all_home_list;
            this.setState({ home_list: all_home_list });
        }
    }

    favourite(business_id) {
        AsyncStorage.getItem('UserDetails', (err, details) => {
            let UserDetails = JSON.parse(details);
            let user_id = UserDetails.details.id + '';
            this.setState({
                loader: true
            });
            api.post('UserBusinessess/addFavourite.json', { user_id: user_id, business_id: business_id }).then(res => {
                this.AllData();
                this.setState({
                    loader: false
                });
            }).catch((err) => {
                this.setState({ loader: false });
                console.log(err);
            })
        });
    }

    
    connectAndGetOffer(business_id, id) {
        debugger
    
    this.setState({
        id: id,
        loader: true,
    
    });
                  
    const data = {
        "user_id": this.state.userId,
        "business_id": [
            {
                "bid": business_id
            }
        ]
    };
    
    api.post('UserBusinessess/BusinessDetails.json', {id:business_id}).then(result => {
        debugger
        this.setState({
            businessdata : result.details
        });
        
        debugger

             // if (this.state.id == '' || this.state.id != id) {
        api.post('UserBusinessess/connectBusiness.json', data).then(res => {
            //api.post('UserBusinessess/addFavourite.json', data).then(res => {
            debugger
            let amount = 0;
            if (res) {

                if (this.state.businessdata.connect_token && res.businessuserdetails[0].blockchain_id && res.businessuserdetails[0].blockchain_address) {
                    if (this.state.businessdata.connect_token != null || this.state.businessdata.connect_token == '') {
                        amount = this.state.businessdata.connect_token;
                    } else {
                        amount = 0;
                    }

                }

                let item1 = { bizWalletId: res.businessuserdetails[0].blockchain_id, bizWalletAddress: res.businessuserdetails[0].blockchain_address, custWalletAddress: this.state.customer_blockchain_address, amount: amount };
                debugger
                api.walletPost('reward-customer', item1).then((res) => {
                    this.AllData();	
                }).catch((err) => {
                   
                })
                this.setState({
                    message: res.msg,
                    loader: false
                });

                this.AllData();	
            }
            // Alert.alert('', "Connection added");
        }).catch((err) => {
            this.setState({
                loader: false
            });
        });
        Alert.alert('', "Connection added"); 
        this.AllData();                   
        }).catch((err) => {
            console.log("error",err)
            Alert.alert('', "Please try again later");  
            this.setState({
                loader: false
            });
        });

  

}


    render() {
       
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header style={[commonStyles.headerWarp, { height: 30, paddingTop: 5 }]} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn}
                    // onPress={() => this.props.navigation.navigate('Login')} 
                    >
                        {/* <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} /> */}
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <TouchableOpacity style={styles.disWarp} onPress={() => this.props.navigation.navigate('DistrictLocation')}>
                            <Entypo name='location-pin' style={styles.disbeforeIcon} />
                            {
                                this.state.city ? (
                                    <Text style={styles.disText}>{
                                        this.state.city.toLowerCase() == 'not found' ? 'District' : this.state.city
                                    }</Text>
                                ) : (
                                        <Text style={styles.disText}>District</Text>
                                    )
                            }
                            <Ionicons name='ios-arrow-down' style={styles.disafterIcon} />
                        </TouchableOpacity>
                    </Body>
                    <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1} >
                        {/* <Ionicons name='ios-arrow-back' style={{ color: '#fff', fontSize: 20 }} /> */}
                    </TouchableOpacity>
                </Header>
                <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                <View style={{ backgroundColor: '#2096AB', padding: 10, paddingTop: 0, paddingBottom: 5 }}>
                    <View style={{ backgroundColor: '#fff', borderRadius: 20, flexDirection: 'row', alignItems: 'center' }}>
                        <Ionicons style={{ color: '#444444', marginRight: 5, fontSize: 20, marginLeft: 15 }} name='ios-search' />
                        <TextInput onChangeText={(text) => this.supportSearch(text)} style={{ height: 30, flex: 1, marginBottom: 0, paddingTop: 2, paddingBottom: 0, fontSize: 11 }} underlineColorAndroid='transparent' placeholder='Search by name or keyword...' />
                    </View>
                </View>

                <Content style={styles.homeContent}>
                    <View style={styles.homehdWarp}>
                        <View style={styles.homeTextWarp}>
                            <Text style={styles.homeTextWarpText}>{this.props.navigation.state.params.subname} </Text>
                        </View>
                    </View>
                    <View style={{ flexWrap: 'wrap', flexDirection: 'row' }}>

                        {
                            this.state.home_list.map((subdata, subkey) => {
                                return (
                                    <View style={styles.homeItemWarp} key={subkey}>
                                        <TouchableOpacity style={styles.homeItemLove} onPress={() => this.favourite(subdata.business_id)}>
                                            {
                                                subdata.favourite_status ? (
                                                    <Entypo name='heart' style={[styles.homeItemLoveIcon, { color: 'red' }]} />
                                                ) : (
                                                        <Entypo name='heart-outlined' style={styles.homeItemLoveIcon} />
                                                    )
                                            }
                                        </TouchableOpacity>
                                        <View style={styles.homeItemImageWarp}>
                                            {/* <Image source={{ uri: this.state.image_url + subdata.business_logo }} style={styles.homeItemImage}></Image> */}
                                            {
                                                //subdata.business_images.length == 0 ? (
                                                    subdata.business_logo == 0 ? (
                                                    <Image source={require('../../../img/icons/no-image.png')} style={styles.homeItemImage}></Image>
                                                ) : (
                                                        <Image source={{ uri: this.state.image_url + subdata.business_logo }} style={styles.homeItemImage}></Image>
                                                    )
                                            }
                                        </View>
                                        <View style={styles.homeItemTextWarp}>
                                            <Text style={styles.homeItemHead}>{subdata.offer_name}</Text>
                                            <View style={[styles.tabItemPrice, { marginBottom: 8,flexDirection:'column' }]}>
                                                {/* <Image source={require('../../../img/icons/prsnt.png')} style={styles.prsnt} /> */}
                                                {/* <Text style={[styles.tabItemPriceText, { fontSize: 10 }]}>Offers from {subdata.outlet_count} outlets</Text> */}
                                                <Text style={styles.homeItemHead}> {subdata.business_name} </Text>

                                                <Text style={[styles.cuisines, {fontSize:10}]} numberOfLines={1} >{subdata.offerno} Offer(s) from {subdata.outletno} outlet(s)</Text>
																					
												{/* <Text style={styles.cuisines} numberOfLines={1} >Plus Reward Tokens:</Text>
                                                <Text style={styles.homeItemHead}>{subdata.total_token} </Text> */}
                                                {subdata.connect_token || subdata.reward_token?
                                                 <Text style={styles.cuisines} numberOfLines={1} >Plus Reward Tokens</Text>
                                                :
                                                 null
                                                }
                                               
                                            </View>
                                            
                                            {/* <Text style={[styles.cuisinesDown]} numberOfLines={1}>{subdata.description}</Text> */}
                                            <View style={[styles.tabItemPrice]}>
                                                <Entypo name='location-pin' />
                                                <Text style={styles.locationText} numberOfLines={1}>Nearest outlet : <Text style={{ color: '#5dbcb1' }}>{subdata.distance ? subdata.distance + "km" : "N/A"}</Text></Text>
                                            </View>
                                            {/* <Text style={styles.cuisines} numberOfLines={1}>Business name:</Text>
                                            <Text style={[styles.cuisinesDown]} numberOfLines={1}>{subdata.business_name}</Text> */}
                                            {/* <View style={styles.connectAndGetOffer}>
                                                <TouchableOpacity style={styles.connectAndGetOfferBtn} onPress={() => this.connectAndGetOffer(subdata.business_id, subdata.id)}>
                                                    <Text style={styles.connectAndGetTxt}>Add and get offer</Text>
                                                </TouchableOpacity>
                                            </View> */}

                                            <View>
                                                {/* <TouchableOpacity
                                                    style={[styles.btncn, { backgroundColor: '#00BBD7' }]}
                                                    onPress={() => this.selectedFunction(data.id)}
                                                >
                                                    <Text style={styles.btntxt}>Connect</Text>
                                                </TouchableOpacity> */}
                                                <TouchableOpacity style={[styles.btncn]} onPress={() => this.connectAndGetOffer(subdata.user_business_id, subdata.id)}>
                                                    <Text style={styles.btntxt}>Connect</Text>
                                                </TouchableOpacity>

                                            </View>
                                        </View>

                                    </View>
                                )
                            })
                        }
                    </View>


                </Content>

                <Footer>
                    <FooterTab style={commonStyles.footerTab}>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Home')} >
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/iconHome.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={[commonStyles.footerItemText, commonStyles.footerItemTextActive]}>Home</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Connections')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/connection.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={commonStyles.footerItemText}>Connection</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Favorite')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/love.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={commonStyles.footerItemText}>Favorite</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Notification')}>
							<View style={[commonStyles.footerItemImageWarp, { position: 'relative', }]}>
							{this.state.unreadcount!=0 ?
								(<Text style={commonStyles.notificationText}>{this.state.unreadcount}</Text>):
								null
							}
							<Image source={require('../../../img/icons/notification.png')} style={commonStyles.footerItemImage} />
							</View>
							<Text style={commonStyles.footerItemText} >Notification</Text>
						</TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('MyAccount')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/profile.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={commonStyles.footerItemText} >Profile</Text>
                        </TouchableOpacity>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}
// export default NearBysubCategories;

NearBysubCategories.propTypes = {
    auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        currentRoute: state.RouterOwn.currentRoute,
        prevRoute: state.RouterOwn.prevRoute
    }
}

const mapDispatchToProps = dispatch => ({
    userUpdate: (data) => dispatch(userUpdate(data))
});
export default connect(mapStateToProps, mapDispatchToProps)(NearBysubCategories);