const React = require("react-native");
const { StyleSheet, Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;


export default {
  tabItemWarp: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderBottomColor: '#f3f3f3',
    borderBottomWidth: 1
  },
  tabItemImageWarp: {
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#e1e1e1',
    height: 45,
    width: 45,
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabItemImage: {
    width: 40
  },
  tabItemMdlTextWarp: {
    flex: 1,
    marginLeft: 15,
    justifyContent: 'center'
  },
  tabItemName: {
    fontSize: 13
  },
  tabItemPrice: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 0,
    marginBottom: 5
  },
  tabItemPriceText: {
    lineHeight: 12,
    fontSize: 11
  },
  prsnt: {
    height: 15,
    width: 9,
    marginRight: 10
  },
  seeOffer: {
    borderWidth: 1,
    borderColor: '#3ab3ce',
    borderRadius: 10,
    paddingLeft: 15,
    paddingRight: 15
  },
  seeOfferText: {
    fontSize: 10,
    color: '#3ab3ce'
  },
  homeContent: {
    flex: 1,
    backgroundColor: '#f0f0f0'
  },
  homehdWarp: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10
  },
  homeTextWarp: {
    flex: 1
  },
  homeTextWarpText: {
    fontFamily: 'Roboto-Medium',
    color: '#3ab3ce',
    fontSize: 16,
    fontWeight:'bold'
  },
  seeAll: {
    flexDirection: 'row'
  },
  seeAllText: {
    fontSize: 11,
    color: '#3ab3ce'
  },
  seeAllIcon: {
    fontSize: 15,
    color: '#3ab3ce'
  },
  homeItemWarp: {
    backgroundColor: '#fff',
    borderColor: '#ebebeb',
    borderWidth: 1,
    position: 'relative',
    width: (deviceWidth - 32) / 2,
    marginLeft: 8,
    marginRight: 8,    
  },
  homeItemLove: {
    position: 'absolute',
    top: 6,
    right: 15,
    zIndex: 99
  },
  homeItemLoveIcon: {
    fontSize: 18,
    color: '#3ab3ce'
  },
  homeItemImageWarp: {
    borderBottomColor: '#fff',
    alignItems: 'center',
    borderBottomWidth: 1,
    marginBottom: 5,
    paddingTop: 0,
    paddingBottom: 8
  },
  homeItemImage: {
    height: 100,
    width: '100%'
  },
  homeItemTextWarp: {
    paddingLeft: 8,
    paddingRight: 8,
    paddingBottom:15
  },
  homeItemHead: {
    fontSize: 13,
    color: '#000',
    fontFamily: 'Roboto-Medium',
    marginBottom: 5
  },
  discountText: {
    fontSize: 10,
    marginBottom: 5
  },
  cuisines: {
    fontSize: 12,
    marginBottom: 5,
    color: '#199BB0',
    fontFamily: 'Roboto-Medium'
  },
  cuisinesDown: {
    fontSize: 10,
    marginBottom: 0
  },
  locationText: {
    fontSize: 10
  },

  locationTextInr: {
    color: '#fff',
    fontSize: 14,    
  },
  connectAndGetOffer: {
    marginBottom: 10
  },
  connectAndGetOfferBtn: {
    borderColor: '#3ab3ce',
    backgroundColor: '#3ab3ce',
    borderWidth: 1,
    borderRadius: 10
  },
  connectAndGetTxt: {
    fontSize: 10,
    color: '#fff',
    width: '100%',
    textAlign: 'center'
  },
  disWarp: {
    flexDirection: 'row'
  },
  disbeforeIcon: {
    color: '#fff',
    fontSize: 18,
    marginRight: 3
  },
  disafterIcon: {
    color: '#fff',
    fontSize: 18,
    marginLeft: 6
  },
  disText: {
    color: '#fff'
  },
  btncn: {
    width: '100%',
    height: 24,
    color: '#fff',
    borderRadius: 35,
    marginTop: 20,
    borderWidth:1,
    borderColor:'#00BBD7'
    // backgroundColor:'#00BBD7',
    // backgroundColor:'#7CA8AF'
  },
  btntxt: {
    fontSize: 13,
    color: '#00BBD7',
    textAlign: 'center',
    lineHeight: 19,
  },
  cntxt: {
    textAlign: 'center',
    color: '#66C0B7',
    fontSize: 14,
  },
  nbtn:{
    alignItems:'center',
    justifyContent:'center',   
    backgroundColor:'#00AEC7',
    height:34
  },
  ntxt:{
    color:'#fff',
    textAlign:'center',
    fontSize:16,
  },
  connectbtn:{
    flex:1,
    flexDirection:'row',
    padding:6,
    paddingTop:3,
    paddingBottom:3,
    backgroundColor:'#3ab3ce',
    borderRadius:35,
    justifyContent:'center',
    alignItems:'center',
    marginTop:10,
    marginBottom:10    
},
leftIcon: {
  width: 36,
  display: 'flex',
  alignItems: 'flex-start',
  justifyContent: 'center',
  color: '#fff',  
  position: 'absolute',
  top:8,
  left:10,
  zIndex:99
},
rightIconTag: {  
  fontSize: 16
},
roundBadge: {
  borderRadius: 50,
  width: 15,
  height: 15,
  position: 'absolute',
  left: 12,  
  padding:0,
  paddingLeft:4,
  paddingRight:0,
  textAlign:'center'
},
badgeText: {
  color: '#fff',
  padding: 0,
  margin: 0,
  fontSize: 9,
  lineHeight: 14
},

};
