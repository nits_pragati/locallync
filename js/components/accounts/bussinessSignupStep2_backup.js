import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, Alert, AsyncStorage, ScrollView } from "react-native";
import { Container, Content, Button, ActionSheet } from "native-base";
import styles from "./style";
import defaultStyle from "../../assets/styles";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FSpinner from 'react-native-loading-spinner-overlay';
import ImagePicker from 'react-native-image-crop-picker';
import api from '../../api';
import SplashScreen from 'react-native-splash-screen';
import MultiSelect from 'react-native-multiple-select';
const launchscreenLogo = require('../../../img/logo.png');

var BUTTONS = [
    { text: 'camera', icon: "ios-camera", iconColor: "#2c8ef4" },
    { text: 'file', icon: "ios-images", iconColor: "#f42ced" }
];


class BussinessSignupStep2 extends Component {
    constructor(params) {
        super(params)
        this.state = {
            business_name: '',
            contact_name: '',
            contact_phone: '',
            website: '',
            loader: false,
            uploadImage: '',
            business_logo: '',
            selectedItems: [],
            items: [],
            imageArray: [],
            activeImage: true,
        }
    }

    componentDidMount() {
        SplashScreen.hide();
        api.get('categories/list_category.json').then(res => {
            res.details.map((data) => {
                data.image = res.image_url + data.image;
            })
            this.setState({
                items: res.details,
                loader: false,
            });

        }).catch((err) => {
            this.setState({ loader: false });
            console.log(err);
        })
    }

    signUp2() {
        const business_name = this.state.business_name;
        const contact_name = this.state.contact_name;
        const contact_phone = this.state.contact_phone;
        if (business_name && contact_name) {
            if (contact_phone && contact_phone.trim() && contact_phone.length == 10) {
                const website = this.state.website;
                // const business_logo = this.state.business_logo;
                let business_logo = [];
                this.state.imageArray.map((data) => {
                    business_logo.push(data.business_logo);
                })
                console.log(business_logo);
                let category_id = []
                if (this.state.selectedItems.length) {
                    this.state.selectedItems.map((item) => {
                        selectItem = { "cid": item }
                        category_id.push(selectItem);
                    })
                }
                this.setState({
                    loader: true
                });

                AsyncStorage.getItem('UserDetails', (err, result) => {
                    let data = JSON.parse(result);
                    let user_id = data.details.id + '';
                    api.post('UserBusinessess/addBusiness.json', { user_id: user_id, business_name: business_name, contact_name: contact_name, contact_phone: contact_phone, website: website, is_verified: "1", category_id: category_id, business_logo: business_logo }).then(res => {
                        this.setState({ loader: false });
                        this.props.navigation.navigate('VendorHome');
                    }).catch((err) => {
                        this.setState({ loader: false });
                        console.log(err);
                    })
                })
            } else {
                Alert.alert('', "Please give a valid phone number.");
            }
        }
        else {
            Alert.alert('', "Please give a contact name and business name.");
        }



    }

    fileUploadType(buttonIndex) {
        if (buttonIndex == 0) {
            this.setState({ cameraButton: false });
            ImagePicker.openCamera({
                width: 400,
                height: 300,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                this.setState({
                    business_logo: business_logo,
                });
                this.setState({ loader: true });
                let uri;
                if (!response.path) {
                    uri = response.uri;
                } else {
                    uri = response.path;
                }
                const file = {
                    uri,
                    name: `${Math.floor((Math.random() * 100000000) + 1)}_.png`,
                    type: response.mime || 'image/png',
                };
                this.setState({
                    loader: false,
                    currentImage: file.uri
                });
                this.setState({
                    uploadImage: file.uri,
                });
            }).catch((err) => {
                this.setState({ loader: false });
                this.setState({ cameraButton: true });
            });
        }
        if (buttonIndex == 1) {
            ImagePicker.openPicker({
                width: 400,
                height: 300,
                cropping: true,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                this.setState({
                    business_logo: business_logo,
                    loader: true
                });
                let uri;
                if (!response.path) {
                    uri = response.uri;
                } else {
                    uri = response.path;
                }
                const file = {
                    uri,
                    name: `${Math.floor((Math.random() * 100000000) + 1)}_.png`,
                    type: response.mime || 'image/png',
                };

                this.setState({
                    loader: true,
                    uploadImage: file.uri
                });
            }).catch((err) => {
                this.setState({ loader: false });
            });
        }
    }


    multipleFileUploadType(buttonIndex) {
        if (buttonIndex == 0) {
            this.setState({ cameraButton: false });
            ImagePicker.openCamera({
                width: 400,
                height: 300,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                this.setState({ loader: false });
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                response.business_logo = business_logo;
                let imageArray = this.state.imageArray;
                imageArray.push(response);
                this.setState({ imageArray });
            }).catch((err) => {
                this.setState({ loader: false });
                this.setState({ cameraButton: true });
            });
        }
        if (buttonIndex == 1) {
            ImagePicker.openPicker({
                width: 400,
                height: 300,
                cropping: true,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                this.setState({ loader: false });
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                response.business_logo = business_logo;
                let imageArray = this.state.imageArray;
                imageArray.push(response);
                this.setState({ imageArray });
            }).catch((err) => {
                this.setState({ loader: false });
            });
        }
    }



    uploadedImageChange(buttonIndex, index) {
        if (buttonIndex == 0) {
            this.setState({ cameraButton: false });
            ImagePicker.openCamera({
                width: 400,
                height: 300,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                this.setState({ loader: false });
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                response.business_logo = business_logo;
                let imageArray = this.state.imageArray;
                imageArray[index] = response;
                this.setState({ imageArray });
            }).catch((err) => {
                this.setState({ loader: false });
                this.setState({ cameraButton: true });
            });
        }
        if (buttonIndex == 1) {
            ImagePicker.openPicker({
                width: 400,
                height: 300,
                cropping: true,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                this.setState({ loader: false });
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                response.business_logo = business_logo;
                let imageArray = this.state.imageArray;
                imageArray[index] = response;
                this.setState({ imageArray });
            }).catch((err) => {
                this.setState({ loader: false });
            });
        }
    }



    uploadImageFunction(buttonIndex) {
        this.setState({ loader: true });
        if (buttonIndex == 0) {
            this.setState({ cameraButton: false });
            ImagePicker.openCamera({
                width: 400,
                height: 300,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                this.setState({ loader: false });
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                response.business_logo = business_logo;
                return response;
            }).catch((err) => {
                this.setState({
                    cameraButton: true,
                    loader: false
                });
                return false;
            });
        }
        if (buttonIndex == 1) {
            ImagePicker.openPicker({
                width: 400,
                height: 300,
                cropping: true,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                this.setState({ loader: false });
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                response.business_logo = business_logo;
                return response;
            }).catch((err) => {
                this.setState({ loader: false });
            });
        }
    }

    onSelectedItemsChange = (selectedItems) => {
        this.setState({ selectedItems });
        console.log(this.state.selectedItems);
    };

    // addImageRow(){
    //     let imageArray = this.state.imageArray;
    //     let inewImage;
    //     imageArray.push(inewImage);
    //     this.setState({
    //         imageArray
    //     });
    //     console.log(this.state.imageArray.length)
    // }
    // imageChange(buttonIndex , index){
    // let response = (buttonIndex)=> this.uploadImageFunction(buttonIndex);
    // console.log();
    // if (response){
    //     let imageArray = this.state.imageArray;
    //     imageArray[index] = response;
    // }
    // }


    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#fff"
                    hidden={true}
                />
                <Content style={styles.mainContenner}>
                    <FSpinner loader={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                    <View style={styles.topLogoWarp}>
                        <Image source={launchscreenLogo} style={styles.topLogo} />
                    </View>
                    <View style={styles.topHeadingWarp}>
                        {/* <Text style={styles.topHeading}> ADD BUSSINESS </Text> */}
                    </View>
                    <View style={styles.inputFieldMainWarp}>

                        {/*<View style={{ alignItems: 'center', justifyContent: 'center' }}>
                             <TouchableOpacity
                                onPress={() =>
                                    ActionSheet.show(
                                        {
                                            options: BUTTONS,
                                        },
                                        (buttonIndex) => {
                                            this.setState({ clicked: BUTTONS[buttonIndex] });
                                            this.fileUploadType(buttonIndex);
                                        },
                                    )}
                            >
                                {
                                    this.state.uploadImage ? (<Image source={{ uri: this.state.uploadImage }} style={{ height: 100, width: 100, borderWidth: 1, borderColor: '#0077b5', borderRadius: 100 / 2, marginBottom: 20 }} />) : (
                                        <Image source={require('../../../img/icons/add_logo.png')} style={{ height: 100, width: 100, borderWidth: 1, borderColor: '#0077b5', borderRadius: 100 / 2, marginBottom: 20 }} />
                                    )
                                }

                            </TouchableOpacity>
                        </View> */}
                        <ScrollView
                            showsHorizontalScrollIndicator={false}
                            horizontal={true}
                            style={{ marginBottom: 20 }}
                        >
                            {
                                this.state.imageArray.length != 0 ? (
                                    this.state.imageArray.map((data, key) => {
                                        return (
                                            // <TouchableOpacity style={{ borderWidth: 1, borderColor: '#000', height: 50, width: 50 }} key={key} onPress={() => this.uploadedImageChange(`${key}`)}><Image source={{ uri: data.path }} style={{height: 50, width: 50}} /></TouchableOpacity>    
                                            <TouchableOpacity
                                                style={{
                                                    borderWidth: 1, borderColor: '#000', height: 50, width: 50, marginRight: 8
                                                }}
                                                key={key}
                                                onPress={() =>
                                                    ActionSheet.show(
                                                        {
                                                            options: BUTTONS,
                                                        },
                                                        (buttonIndex) => {
                                                            this.setState({ clicked: BUTTONS[buttonIndex] });
                                                            this.uploadedImageChange(buttonIndex, `${key}`);
                                                        },
                                                    )}
                                            >
                                                <Image source={{ uri: data.path }} style={{ height: 50, width: 50 }} />

                                            </TouchableOpacity>
                                        )
                                    })
                                ) : null
                            }
                            <TouchableOpacity
                                style={{ borderWidth: 1, borderColor: '#e2e2e2', height: 50, width: 50, alignItems: 'center', justifyContent: 'center' }}
                                onPress={() =>
                                    ActionSheet.show(
                                        {
                                            options: BUTTONS,
                                        },
                                        (buttonIndex) => {
                                            this.setState({ clicked: BUTTONS[buttonIndex] });
                                            this.multipleFileUploadType(buttonIndex);
                                        },
                                    )}
                            >
                                <Text style={{ color: '#e2e2e2' }}>+</Text>

                            </TouchableOpacity>
                            {/* <TouchableOpacity style={{ borderWidth: 1, borderColor: '#000', height: 50, width: 50}} onPress={()=> this.addImageRow()}></TouchableOpacity> */}
                        </ScrollView>
                        <View style={[styles.inputWarp]}>
                            <FontAwesome name='building-o' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Business Name' onChangeText={(text) => this.setState({ business_name: text })} value={this.state.business_name} />
                        </View>

                        <View style={[styles.inputWarp]}>
                            <MaterialIcons name='person' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Contact Name' onChangeText={(text) => this.setState({ contact_name: text })} value={this.state.contact_name} />
                        </View>

                        <View style={[styles.inputWarp]}>
                            <MaterialIcons name='phone' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Contact Phone' onChangeText={(text) => this.setState({ contact_phone: text })} value={this.state.contact_phone} keyboardType={'numeric'} />
                        </View>

                        <View style={[styles.inputWarp]}>
                            <MaterialIcons name='web' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Website' onChangeText={(text) => this.setState({ website: text })} value={this.state.website} />
                        </View>

                        <View style={{ flex: 1, paddingLeft: 0, paddingBottom: 0, paddingLeft: 20, borderBottomColor: '#ccc' }}>
                            <MultiSelect
                                items={this.state.items}
                                onSelectedItemsChange={this.onSelectedItemsChange}
                                selectedItems={this.state.selectedItems}
                                selectText="Select Category"
                                searchInputPlaceholderText="Search Categorys..."
                                displayKey="name"
                                uniqueKey="id"
                                selectedItemTextColor="#0077b5"
                                selectedItemIconColor="#0077b5"
                                itemFontSize={12}
                                autoFocusInput={false}
                                hideTags
                                searchInput={false}
                                hideSubmitButton
                            />
                        </View>
                        <TouchableOpacity style={[styles.signInBtn, { marginBottom: 15 }]} onPress={() => this.signUp2()}>
                            <Text style={styles.signInBtnTxt}> SIGNUP </Text>
                        </TouchableOpacity>

                    </View>
                </Content>
            </Container>
        );
    }
}


export default BussinessSignupStep2;
