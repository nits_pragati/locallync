import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import PropTypes from 'prop-types';
import { Location } from './elements/authActions'
import { connect } from 'react-redux';
import { Image, View, StatusBar, TouchableOpacity, Text, TextInput, Alert, AsyncStorage } from 'react-native';
// import FCM, { FCMEvent, NotificationType } from "react-native-fcm";
import ValidationComponent from 'react-native-form-validator';
import { Container, Content } from 'native-base';
import styles from './style';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SplashScreen from 'react-native-splash-screen';
import FSpinner from 'react-native-loading-spinner-overlay';
import { signup } from './elements/authActions';
import api from '../../api'
const launchscreenLogo = require('../../../img/logo.png');
const resetActionUserOtp = NavigationActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'UserOtpVerification' })],
});


class Signup extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      fname: '',
      lname: '',
      email: '',
      password: '',
      customer_code: '',
      phone: '',
      fnameError: '',
      lnameError: '',
      emailError: '',
      phoneError: '',
      passwordError: '',
      passwordHide: true,
      // userType: this.props.navigation.state.params.userType ? this.props.navigation.state.params.userType : ''
      userType: "2",
      gencode: ''
      // userType: ''
    }
  }

  signUp() {
    if (this.state.userType == '2') {
      this.validate({
        fname: { required: true },
        lname: { required: true },
        email: { email: true, required: true },
        password: { required: true, minlength: 5 },
        // customer_code: { required: true }
      });
      if (this.state.phone.length > 0 && this.state.phone.length < 10) {
        this.validate({
          fname: { required: true },
          lname: { required: true },
          email: { email: true, required: true },
          password: { required: true, minlength: 5 },
          phone: { numeric: true, minlength: 9 }
        });
      }
    }
    if (this.state.userType == 'business') {
      this.validate({
        fname: { minlength: 1, required: true },
        lname: { required: true },
        email: { email: true, required: true },
        password: { required: true, minlength: 5 }
      });
    }

    if (this.isFormValid()) {
      const first_name = this.state.fname.trim();
      const last_name = this.state.lname.trim();
      const email = this.state.email.trim();
      const password = this.state.password.trim();
      const phone = this.state.phone.trim();
      const customer_code = this.state.customer_code.trim();
      let utype = this.state.userType;
      let gencode = this.state.gencode ? this.state.gencode : ''
      let role_type = '';
      // if (this.state.userType == 'user') {
      //   utype = '2';
      //   role_type = 'Customer';
      // }
      // if (this.state.userType == 'business') {
      //   utype = '1';
      //   role_type = 'Business';
      // }
      this.setState({ loader: true });
      this.props.signup(first_name, last_name, email, password, utype, role_type, phone, customer_code, gencode).then(res => {

        this.setState({ loader: false });
        if (res.ack == 1) {
          AsyncStorage.setItem("UserDetails", JSON.stringify(res)).then((result)=>{
            this.props.navigation.dispatch(resetActionUserOtp);
          });

          }
          else {
        if (res.details){
            Alert.alert('', res.details.message);
        }
        }

        // if (res.ack == 1) {
        //   debugger
        //   // AsyncStorage.getItem('UserDetails', (err, result) => {
        //   // let data = JSON.parse(result)
        //   // if (this.state.userType == 'user') {
        //   //   // this.props.navigation.dispatch(resetAction1);
        //   //   this.props.navigation.navigate('Location');
        //   // }
        //   // if (this.state.userType == 'business') {
        //   //   //this.props.navigation.navigate('AddBusiness');
        //   //   this.props.navigation.navigate('BusinessOtpVerification');
        //   // }
        //   const randomPassword = Math.floor(1000 + Math.random() * 9000);
        //   debugger
        //   if (res.acktoken == 0) {
        //     Alert.alert("", "The code is either invalid or is expired.");
        //   }
        //   api.walletPost('create-wallet', { password: randomPassword.toString() }).then((walletRes) => {
        //     debugger

        //     AsyncStorage.getItem("UserDetails", (err, storageRes) => {
        //       debugger
        //       if (err) {
        //         this.setState({ loader: false });
        //          this.props.navigation.navigate('UserOtpVerification');
        //         //this.props.navigation.navigate('Location');
        //       }
        //       else {
        //         if (storageRes) {
        //           debugger
        //           let toChangeData = JSON.parse(storageRes);
        //           // toChangeData.details.blockChainAddress = walletRes;
        //           // toChangeData.details.blockChainPassword=randomPassword;
        //           toChangeData.details.blockchain_address = walletRes.Addresses[0];
        //           toChangeData.details.blockChainPassword = randomPassword;
        //           toChangeData.details.blockchain_id = walletRes.id

        //           if (res.acktoken == 1) {
        //             debugger
        //             api.walletPost('buy-tokens', { buyerAddress: toChangeData.details.blockchain_address, amount: Number(res.acktokenval) }).then((walletTransfer) => {

        //               Alert.alert('', "You have been rewared with points. Pleae check your wallet.")
        //             })
        //           }
        //           api.post('users/updateuser_service.json', { blockchain_address: walletRes.Addresses[0], id: toChangeData.details.id, blockchain_id: walletRes.id, blockchain_password: randomPassword }).then(userUpdateRes => {

        //             debugger
        //             if (userUpdateRes.ack == 1) {
        //               AsyncStorage.setItem("UserDetails", JSON.stringify(toChangeData)).then((resy2) => {
        //                 debugger;
        //                 this.setState({ loader: false });
                      
        //                this.props.navigation.navigate('UserOtpVerification');
        //               }).catch((err)=>{
        //                 debugger
        //               });

        //             }
        //           }).catch((err) => {
        //             debugger
        //             this.setState({ loader: false });
        //             console.log(err);
        //           });

        //         }
        //       }
        //     })
        //     //this.props.navigation.navigate('Location');
        //   }).catch((err2) => {
        //     debugger
        //     this.setState({ loader: false });
        //     Alert.alert('', "Please try again later.");
        //   })

        //   // });
        // } else {
        //   this.setState({ loader: false });
        //   Alert.alert('', res.details.message);
        // }
      }).catch(err => {
        debugger
        this.setState({ loader: false });
        console.log(err);
      })
    } else {

      let fnameError = this.isFieldInError('fname');
      let lnameError = this.isFieldInError('lname');
      let emailError = this.isFieldInError('email');
      let passwordError = this.isFieldInError('password');
      let phoneError = this.isFieldInError('phone');

      this.setState({
        fnameError: fnameError,
        lnameError: lnameError,
        emailError: emailError,
        passwordError: passwordError,
        phoneError: phoneError
      });
    }
  }

  render() {
    return (
      <Container >
        <StatusBar
          backgroundColor="#fff"
          hidden={true}
        />
        <Content style={styles.mainContenner}>
          <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
          <View style={styles.topLogoWarp}>
            <Image source={launchscreenLogo} style={styles.topLogo} />
          </View>
          <View style={styles.topHeadingWarp}>
            <Text style={styles.topHeading}> SIGN UP </Text>
          </View>
          <View style={styles.inputFieldMainWarp}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <View style={[styles.inputWarp, this.state.fnameError ? { borderColor: '#FF0000' } : null]}>
                  <MaterialIcons name='person' style={styles.inputIcon} />
                  <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='First Name' onChangeText={(text) => this.setState({ fname: text })} value={this.state.fname} />
                </View>
              </View>
              <View style={{ flex: 1 }}>
                <View style={[styles.inputWarp, { marginLeft: 5 }, this.state.lnameError ? { borderColor: '#FF0000' } : null]}>
                  {/* <MaterialIcons name='person' style={styles.inputIcon} /> */}
                  <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Last Name' onChangeText={(text) => this.setState({ lname: text })} value={this.state.lname} />
                </View>
              </View>
            </View>

            <View style={[styles.inputWarp, this.state.emailError ? { borderColor: '#FF0000' } : null]}>
              <MaterialIcons name='email' style={styles.inputIcon} />
              <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Email address' onChangeText={(text) => this.setState({ email: text })} value={this.state.email} keyboardType={'email-address'} />
            </View>


            {
              this.state.userType == '2' ? (
                <View>
                  <View style={[styles.inputWarp, this.state.phoneError ? { borderColor: '#FF0000' } : null]}>
                    <MaterialIcons name='phone' style={styles.inputIcon} />
                    <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Phone' onChangeText={(text) => this.setState({ phone: text })} value={this.state.phone} keyboardType={'numeric'} maxLength={10} />
                  </View>
                  {/* <View style={[styles.inputWarp, this.state.phone ? { borderColor: '#FF0000' } : null]}>
                    <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Customer Code' onChangeText={(text) => this.setState({ customer_code: text })} value={this.state.customer_code} />
                  </View> */}
                </View>
              ) : null
            }


            <View style={[styles.inputWarp, this.state.passwordError ? { borderColor: '#FF0000' } : null]}>
              <FontAwesome name='unlock-alt' style={styles.inputIcon} />
              <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Password ( min. 6 Characters )' secureTextEntry={this.state.passwordHide} onChangeText={(text) => this.setState({ password: text })} value={this.state.password} />
              {
                this.state.passwordHide ? <TouchableOpacity onPress={() => this.setState({ passwordHide: !this.state.passwordHide })}>
                  <Ionicons name='ios-eye' style={styles.inputIcon} />
                </TouchableOpacity> : <TouchableOpacity onPress={() => this.setState({ passwordHide: !this.state.passwordHide })}>
                    <Ionicons name='ios-eye-off' style={[styles.inputIcon, { color: '#3ab3ce' }]} />
                  </TouchableOpacity>
              }
            </View>

            <View style={styles.inputWarp}>
              <FontAwesome name='bitcoin' style={styles.inputIcon} />
              <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='If you have referral code, enter it here' onChangeText={(text) => this.setState({ gencode: text })} value={this.state.gencode} />
            </View>
            <TouchableOpacity style={styles.signInBtn} onPress={() => this.signUp()}>
              {
                this.state.userType == '2' ? <Text style={styles.signInBtnTxt}> SIGN UP </Text> : <Text style={styles.signInBtnTxt}> Next </Text>
              }

            </TouchableOpacity>

            <View style={styles.orWarp}>
              <View style={styles.orDividerLine}></View>
              <View>
                <Text style={styles.orText}> or </Text>
              </View>
              <View style={styles.orDividerLine}></View>
            </View>

            <View style={styles.nAM}>
              <Text style={styles.nAMText}> Already have an account ? </Text>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                <Text style={styles.touchText}> Sign In </Text>
              </TouchableOpacity>
            </View>

            <View style={styles.loginPageLastTextWarp}>
              <Text style={styles.loginPageLastText} > By signing up, you agree to the "Terms of service & Privacy Policy" </Text>
            </View>

          </View>
        </Content>
      </Container>
    );
  }
}

Signup.propTypes = {
  auth: PropTypes.object.isRequired
}
const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signup: (first_name, last_name, email, password, utype, role_type, phone, customer_code, gencode) => dispatch(signup(first_name, last_name, email, password, utype, role_type, phone, customer_code, gencode)),
    // bussinessSignup: (first_name, last_name, email, password, utype, role_type) => dispatch(bussinessSignup(first_name, last_name, email, password, utype, role_type))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
// export default Signup;
