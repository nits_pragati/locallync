import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, Alert, AsyncStorage } from "react-native";
import { Container, Header, Body, Content, Tabs, Tab, TabHeading, ScrollableTab, Footer, FooterTab, Button, ActionSheet } from "native-base";
import styles from "./style";
import defaultStyle from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FSpinner from 'react-native-loading-spinner-overlay';
import api from '../../api';
import SplashScreen from 'react-native-splash-screen';
import ImagePicker from 'react-native-image-crop-picker';
const launchscreenLogo = require('../../../img/logo.png');
const profileImage = require('../../../img/icons/no-image-available.png');

var BUTTONS = [
    { text: 'camera', icon: "ios-camera", iconColor: "#2c8ef4" },
    { text: 'file', icon: "ios-images", iconColor: "#f42ced" }
];


class BussinessSignupStep3 extends Component {
    constructor(params) {
        super(params)
        this.state = {
            business_id: this.props.navigation.state.params.business_id,
            loader: false,
            currentImage: '',
            uploadImage: false
        }
    }

    componentDidMount() {
        SplashScreen.hide();
    }

    fileUploadType(buttonIndex) {
        if (buttonIndex == 0) {
            // this.captureFile();
            this.setState({ cameraButton: false });

            ImagePicker.openCamera({
                width: 400,
                height: 300,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                this.setState({ visible: true });
                let uri;
                if (!response.path) {
                    uri = response.uri;
                } else {
                    uri = response.path;
                }
                const file = {
                    uri,
                    name: `${Math.floor((Math.random() * 100000000) + 1)}_.png`,
                    type: response.mime || 'image/png',
                };
                this.setState({ 
                    visible: false,
                    currentImage: file.uri
                 });
                let UserDetails_data = JSON.parse(UserDetails);
                let id = this.state.business_id;
                api.post('UserBusinessess/updateImageBusiness.json', { id: id, business_logo: business_logo }).then(ImageRes => {
                    if (ImageRes.ack == 1) {
                        this.setState({
                            loader: false,
                            responcedata: ImageRes,
                            uploadImage: true
                        });
                    }

                }).catch((err) => {
                    this.setState({ loader: false });
                })
            }).catch((err) => {
                this.setState({ loader: false });
                this.setState({ cameraButton: true });
            });
        }
        if (buttonIndex == 1) {
            // this.attachFile();
            ImagePicker.openPicker({
                width: 400,
                height: 300,
                cropping: true,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                this.setState({ visible: true });
                let uri;
                if (!response.path) {
                    uri = response.uri;
                } else {
                    uri = response.path;
                }
                const file = {
                    uri,
                    name: `${Math.floor((Math.random() * 100000000) + 1)}_.png`,
                    type: response.mime || 'image/png',
                };

                this.setState({ visible: true });
                let id = this.state.business_id;
                api.post('UserBusinessess/updateImageBusiness.json', { id: id, business_logo: business_logo }).then(ImageRes => {
                        if (ImageRes.ack == 1) {
                            this.setState({
                                loader: false,
                                responcedata: ImageRes,
                                uploadImage: true
                            });
                        }

                    }).catch((err) => {
                        this.setState({ loader: false });
                    })
            }).catch((err) => {
                this.setState({ visible: false });
            });
        }
    }


    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#fff"
                    hidden={true}
                />
                <Content style={styles.mainContenner}>
                    <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                    <View style={{ backgroundColor: '#10acf9', height: 160, alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity
                            onPress={() =>
                                ActionSheet.show(
                                    {
                                        options: BUTTONS,
                                    },
                                    (buttonIndex) => {
                                        this.setState({ clicked: BUTTONS[buttonIndex] });
                                        this.fileUploadType(buttonIndex);
                                    },
                                )}
                        >
                        {
                                this.state.uploadImage ? (<Image source={{ uri: this.state.responcedata.image_url+this.state.responcedata.image_name }} style={{ height: 80, width: 80, borderRadius: 80 / 2, borderColor: '#0077b5', borderWidth: 1 }} />): (
                                    <Image source={ profileImage } style={{ height: 80, width: 80, borderRadius: 80 / 2, borderColor: '#0077b5', borderWidth: 1 }} />
)
                        }
                        
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity style={styles.signInBtn} onPress={() => this.props.navigation.navigate('VendorHome')}>
                            <Text style={styles.signInBtnTxt}> SIGNUP </Text>
                        </TouchableOpacity>
                    </View>
                </Content>
            </Container>
        );
    }
}



export default BussinessSignupStep3;
