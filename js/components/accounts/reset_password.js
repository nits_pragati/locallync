import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker } from "react-native";
import { Container, Header, Body, Content, Tabs, Tab, TabHeading, ScrollableTab, Footer, FooterTab, Button, } from "native-base";
import styles from "./style";
import defaultStyle from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const lockImage = require("../../../img/lock.png");


class ResetPassword extends Component {
	constructor(params) {
		super(params)
		this.state = {
			otp: '',
			newPassword: '',
			hidePassword: true,
		}
	}


	render() {
		return (
			<Container >
				<StatusBar
					backgroundColor="#133567"
				/>

				<Header style={[defaultStyle.headerWarp]} androidStatusBarColor="#133567" noShadow>

					<TouchableOpacity style={defaultStyle.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
						<Ionicons name='ios-arrow-back' style={[defaultStyle.headerBackIcon]} />
					</TouchableOpacity>

					<Body style={styleSelf.tac}>
						<Text style={[defaultStyle.headerMiddleText]}>RESET PASSWORD</Text>
					</Body>

					<TouchableOpacity style={defaultStyle.headerRightBtn} activeOpacity={1}>
						{/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
					</TouchableOpacity>

				</Header>

				<Content style={[styles.whiteContent]}>

					<View style={[styles.locationWarp]}>
						<View style={{ flexDirection: 'row', justifyContent: 'center', height: 120, top: 20 }}>
							<Image source={lockImage} style={{ width: 80, height: 80 }} />
						</View>
						<View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
							<View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
								<Text style={{ color: '#81cdc7', fontSize: 20 }}>*</Text>
								<Text style={{ marginTop: -20, color: '#81cdc7' }}>___</Text>
							</View>
							<View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginLeft: 2 }}>
								<Text style={{ color: '#81cdc7', fontSize: 20 }}>*</Text>
								<Text style={{ marginTop: -20, color: '#81cdc7' }}>___</Text>
							</View>
							<View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginLeft: 2 }}>
								<Text style={{ color: '#81cdc7', fontSize: 20 }}>*</Text>
								<Text style={{ marginTop: -20, color: '#81cdc7' }}>___</Text>
							</View>
							<View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginLeft: 2 }}>
								<Text style={{ color: '#81cdc7', fontSize: 20 }}>*</Text>
								<Text style={{ marginTop: -20, color: '#81cdc7' }}>___</Text>
							</View>
							<View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginLeft: 2 }}>
								<Text style={{ color: '#81cdc7', fontSize: 20 }}>*</Text>
								<Text style={{ marginTop: -20, color: '#81cdc7' }}>___</Text>
							</View>
							<View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginLeft: 2 }}>
								<Text style={{ color: '#81cdc7', fontSize: 20 }}>*</Text>
								<Text style={{ marginTop: -20, color: '#81cdc7' }}>___</Text>
							</View>
						</View>
						<Text style={[styles.locationText1, { marginBottom: 20, marginTop: 0, paddingTop: 0 }]}>Don't worry, it happens to all of us</Text>
						<Text style={[styles.locationText2, { marginBottom: 30 }]}>Enter your email and we'll send you a link to reset your password</Text>
						<View style={styles.inputWarp}>
							<MaterialIcons name='vpn-key' style={styles.inputIcon} />
							<TextInput style={[styles.input,{ textAlign: 'center' }]} underlineColorAndroid='transparent' placeholder='OTP' onChangeText={(text) => this.setState({ otp: text })} value={this.state.otp} keyboardType={ 'numeric' } secureTextEntry />
						</View>
						<View style={styles.inputWarp}>
							<FontAwesome name='unlock-alt' style={styles.inputIcon} />
							<TextInput style={[styles.input, { textAlign: 'center' }]} underlineColorAndroid='transparent' placeholder='New Password' onChangeText={(text) => this.setState({ newPassword: text })} value={this.state.newPassword} secureTextEntry />
						</View>

						<TouchableOpacity style={[styles.signInBtn, styles.locationBtn, { marginBottom: 0 }]} onPress={() => this.props.navigation.navigate('ResetPassword')}>
							<Text style={[styles.signInBtnTxt, styles.locationBtnTxt]}>Save Password</Text>
						</TouchableOpacity>

					</View>
				</Content>
			</Container>
		);
	}
}


export default ResetPassword;
