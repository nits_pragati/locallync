import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, Alert } from "react-native";
import { Container, Header, Body, Content, Tabs, Tab, TabHeading, ScrollableTab, Footer, FooterTab, Button, } from "native-base";
import styles from "./style";
import defaultStyle from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FSpinner from 'react-native-loading-spinner-overlay';
import api from '../../api'


class ForgotPassword extends Component {
	constructor(params) {
		super(params)
		this.state = {
			email: '',
			loader: false,
			step: 1,
			otp: '',
			password: '',
			conformPassword: ''
		}
	}
	sendEmail(){

		let regEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

		if (this.state.email.trim() == '') {
			Alert.alert('Please enter your email');
			return;
		}

		if (!regEmail.test(this.state.email)) {
			Alert.alert('Please enter a valid email');
			return;
		}

		const email = this.state.email.trim();
		this.setState({ loader: true });
		
		api.post('users/forgotpassword.json', { email: email }).then(res => {
			this.setState({ loader: false });
			if ( res.ack == 1 ){
				this.setState({ step: 2 });
			}else{
				Alert.alert(res.message);
			}
			}).catch((err) => {
				this.setState({ loader: false });
				console.log(err);
			})
	}

	sendOtp(){

		if (this.state.otp.trim() == '') {
			Alert.alert('Please enter your OTP');
			return;
		}
		if (!this.state.otp.trim().length == 4) {
			Alert.alert('Please enter a valid OTP');
			return;
		}
		const otp = this.state.otp.trim();
		const email = this.state.email.trim();

		this.setState({ loader: true });		
		api.post('users/verify_otp.json', { email: email, otp: otp }).then(res => {
			this.setState({ loader: false });
			if (res.ack == 1) {
				this.setState({
					step: 3
				})
			}else{
				Alert.alert(res.message);
			}
		}).catch((err) => {
			this.setState({ loader: false })
			console.log(err);
		})
	}

	changePassword(){
		if (this.state.password.trim() == '') {
			Alert.alert('Please enter a password');
			return;
		}
		if (this.state.password.trim().length < 6) {
			Alert.alert('Password length minimum 6 characters');
			return;
		}
		if (this.state.conformPassword.trim() == '') {
			Alert.alert('Please enter a Confirm Password');
			return;
		}
		if ( this.state.password.trim() != this.state.conformPassword.trim() ) {
			Alert.alert('Password and confirm password are not match');
			return;
		}

		const email = this.state.email.trim();
		const password = this.state.password.trim();
		const cpassword = this.state.conformPassword.trim();

		this.setState({ loader: true });		
		api.post('users/updatepassword.json', { email: email, password: password, cpassword: cpassword }).then(res => {
			this.setState({ loader: false })
			Alert.alert(res.message);
			if (res.ack == 1) {
				this.props.navigation.navigate('Login');
			}
		}).catch((err) => {
			this.setState({ loader: false })
			console.log(err);
		})
	}


	render() {
		return (
			<Container >
				<StatusBar
					backgroundColor="#133567"
				/>

				<Header style={[defaultStyle.headerWarp]} androidStatusBarColor="#133567" noShadow>

					<TouchableOpacity style={defaultStyle.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
						<Ionicons name='ios-arrow-back' style={[defaultStyle.headerBackIcon]} />
					</TouchableOpacity>

					<Body style={styleSelf.tac}>
						<Text style={[defaultStyle.headerMiddleText]}>FORGOT PASSWORD</Text>
					</Body>

					<TouchableOpacity style={defaultStyle.headerRightBtn} activeOpacity={1}>
						{/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
					</TouchableOpacity>

				</Header>

				<Content style={[styles.whiteContent]}>
					<FSpinner visible={ this.state.loader } textContent={'Loading...'} textStyle={{ color: '#FFF' }} />	
					{
						this.state.step ==1? (
							<View style={[styles.locationWarp, { marginTop: 50 }]}>
							<Text style={[styles.locationText1, { marginBottom: 20, marginTop: 0, paddingTop: 0 }]}>Don't worry, it happens to all of us</Text>
							<Text style={[styles.locationText2, { marginBottom: 30 }]}>Enter your email and we'll send you a OTP to reset your password</Text>
							<View style={styles.inputWarp}>
								<MaterialIcons name='email' style={styles.inputIcon} />
								<TextInput style={[styles.input]} underlineColorAndroid='transparent' placeholder='Email address' onChangeText={(text) => this.setState({ email: text })} value={this.state.email} keyboardType={'email-address'} />
							</View>

							<TouchableOpacity style={[styles.signInBtn, styles.locationBtn, { marginBottom: 0 }]} onPress={() => this.sendEmail()}>
								<Text style={[styles.signInBtnTxt, styles.locationBtnTxt]}>SEND EMAIL</Text>
							</TouchableOpacity>
						</View>
						) : this.state.step == 2 ? (
							<View style={[styles.locationWarp, { marginTop: 50 }]}>
								<Text style={[styles.locationText1, { marginBottom: 20, marginTop: 0, paddingTop: 0 }]}>Please enter the OTP</Text>
									<Text style={[styles.locationText2, { marginBottom: 30 }]}>Please check your email for otp.</Text>
								<View style={styles.inputWarp}>
									{/* <MaterialIcons name='email' style={styles.inputIcon} /> */}
										<TextInput style={[styles.input, { textAlign: 'center' }]} underlineColorAndroid='transparent' placeholder='OTP' onChangeText={(text) => this.setState({ otp: text })} value={this.state.otp} keyboardType={'numeric'} secureTextEntry maxLength={4} />
								</View>

								<TouchableOpacity style={[styles.signInBtn, styles.locationBtn, { marginBottom: 0 }]} onPress={() => this.sendOtp()}>
									<Text style={[styles.signInBtnTxt, styles.locationBtnTxt]}>SEND OTP</Text>
								</TouchableOpacity>
							</View>
							) : this.state.step == 3 ? (
								<View style={[styles.locationWarp, { marginTop: 50 }]}>
									<Text style={[styles.locationText1, { marginBottom: 20, marginTop: 0, paddingTop: 0 }]}>Enter new password</Text>
									{/* <Text style={[styles.locationText2, { marginBottom: 30 }]}>Enter your email and we'll send you a link to reset your password</Text> */}
									
									<View style={styles.inputWarp}>
										<FontAwesome name='unlock-alt' style={styles.inputIcon} />										
										<TextInput style={[styles.input]} underlineColorAndroid='transparent' placeholder='New password' onChangeText={(text) => this.setState({ password: text })} value={this.state.password} secureTextEntry />
									</View>
									
									<View style={styles.inputWarp}>
										<FontAwesome name='unlock-alt' style={styles.inputIcon} />										
										<TextInput style={[styles.input]} underlineColorAndroid='transparent' placeholder='Confirm password' onChangeText={(text) => this.setState({ conformPassword: text })} value={this.state.conformPassword} secureTextEntry />
									</View>

									<TouchableOpacity style={[styles.signInBtn, styles.locationBtn, { marginBottom: 0 }]} onPress={() => this.changePassword()}>
										<Text style={[styles.signInBtnTxt, styles.locationBtnTxt]}>SAVE PASSWORD</Text>
									</TouchableOpacity>
								</View>
							): null
					}		
					
				</Content>
			</Container>
		);
	}
}


export default ForgotPassword;
