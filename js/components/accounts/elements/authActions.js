import authApi from './authApi';
import * as TYPES from '../../../actions/actionTypes';
import { AsyncStorage } from 'react-native';
export function login(email, password, latitude, longitude) {
  return function (dispatch) {
    console.log(latitude);
    console.log(longitude);
    dispatch(authStateBusy());
    return authApi.login(email, password, latitude, longitude).then((res) => {
      console.log("userlogin res :",res);
      AsyncStorage.setItem('UserDetails', JSON.stringify(res), (err, result) => {
      });
      res.type = 'success';
      dispatch(authStateSuccess(res));
      return res;
    }).catch((err) => {
      err.type = 'error';
      dispatch(authStateFailed());
      return err;
    });
  };
}

export function LogedInData(id) {
  return function (dispatch) {
    dispatch(authStateBusy());
    return authApi.LogedInData(id).then((res) => {
      AsyncStorage.setItem('UserDetails', JSON.stringify(res), (err, result) => {
      });
      res.type = 'success';
      dispatch(authStateSuccess(res))
      return res;
    }).catch((err) => {
      err.type = 'error';
      dispatch(authStateFailed());
      return err;
    });
  };
}


export function saveDataForOffline(details) {
  return function (dispatch) {
    dispatch(authStateSuccess(details))
  };
}



export function navigateAndSaveCurrentScreen(data)
{
  return function (dispatch) {
    dispatch(authStateSuccess(data));
  };
} 

export function priceUpdate(data) {
  return function (dispatch) {
    dispatch(authStateSuccess(data));
  };
}
export function userUpdate(data) {
  return function (dispatch) {
    dispatch(authStateSuccess(data));
  };
}


export function getUserDetail(id, auth) {
  return function (dispatch) {
    dispatch(authStateBusy());
    return authApi.getUserDetail(id, auth).then((res) => {
      res.type = 'success';
      dispatch(authStateSuccess(res));
      return res;
    }).catch((err) => {
      err.type = 'error';
      dispatch(authStateFailed());
      return err;
    });
  };
}

export function signup(first_name, last_name, email, password, utype, role_type, phone, customer_code, gencode) {
  
  
  return function (dispatch) {
    dispatch(authStateBusy());
    return authApi.signup(first_name, last_name, email, password, utype, role_type, phone, customer_code, gencode).then((res) => {
      
      if (res.ack == 1){
          AsyncStorage.setItem('UserDetails', JSON.stringify(res), (err, result) => {
          // AsyncStorage.getItem('UserDetails', (err, result) => { 
          // })
        });
      }
      res.type = 'success';
      dispatch(authStateSuccess(res));
      return res;
    }).catch((err) => {
      err.type = 'error';
      dispatch(authStateFailed());
      return err;
    });
  };
}
// export function bussinessSignup(first_name, last_name, email, password, utype, role_type) {
//   return function (dispatch) {
//     dispatch(authStateBusy());
//     return authApi.signup(first_name, last_name, email, password, utype, role_type).then((res) => {

//       if (res.ack == 1) {
//         AsyncStorage.setItem('UserDetails', JSON.stringify(res), (err, result) => {
//           AsyncStorage.getItem('UserDetails', (err, result) => {
//           })
//         });
//       }
//       res.type = 'success';
//       dispatch(authStateSuccess(res));
//       return res;
//     }).catch((err) => {
//       err.type = 'error';
//       dispatch(authStateFailed());
//       return err;
//     });
//   };
// }

export function checkAuth(cb) {
  return function (dispatch) {
    dispatch(authStateBusy());
    AsyncStorage.getItem('userToken', (err, result) => {
      if (result) {
        const data = JSON.parse(result);
        dispatch(authStateSuccess(data));
        cb(data);
      } else{
        dispatch(authStateFailed());
        cb(false);
      }
    });
  };
}
export function getAllLanguagesList(id, auth) {
  return function (dispatch) {
    return authApi.getAllLanguagesList(id, auth).then(res => {
      res.type = 'success';
      return res

    }).catch(err => {
      err.type = 'error';
      return err
    })
  }
}
export function getAllCurrencyList(id, auth) {
  return function (dispatch) {
    return authApi.getAllCurrencyList(id, auth).then(res => {
      res.type = 'success';
      return res

    }).catch(err => {
      err.type = 'error';
      return err
    })
  }
}
export function logout(cb) {
  return function (dispatch) {
    dispatch(authStateBusy());
    AsyncStorage.removeItem('userToken', (err, res) => {
      dispatch(authStateFailed());
      cb(true);
    });
  };
}


export function authStateBusy() {
  return {
    type: TYPES.AUTH_STATE_BUSY,
  };
}

export function authStateFailed() {
  return {
    type: TYPES.AUTH_STATE_FAILED,
  };
}

export function authStateSuccess(data) {
  return {
    type: TYPES.AUTH_STATE_SUCCESS,
    data,
  };
}
