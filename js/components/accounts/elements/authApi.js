import api from '../../../api/index'
class authApi {

	//login API call
	static login(email,password, latitude, longitude){
		return new Promise((resolve,reject)=>{
			api.post('users/token.json', { email: email, password: password, latitude: latitude, longitude: longitude, utype: "2"}).then(responseJson=>{
				resolve(responseJson)
			}).catch(err=>{
				reject(err)
			})
		})
	}

	static LogedInData(user_id) {
		return new Promise((resolve, reject) => {
			api.post('users/details.json', { user_id: user_id}).then(responseJson => {
				resolve(responseJson)
			}).catch(err => {
				reject(err)
			})
		})
	}

	static signup(first_name, last_name, email, password, utype, role_type, phone, customer_code, gencode) {
	
		return new Promise((resolve,reject)=>{
			api.post('users/register.json', {
					first_name: first_name,
					last_name: last_name,
					email: email,
					password: password,
					utype: utype,
					role_type: role_type,
					phone: phone,
					customer_code: customer_code,
					gencode:gencode}).then(responseJson => {
						
				resolve(responseJson)
			}).catch(err=>{
				reject(err)
			})
		})
	}

	// static bussinessSignup(first_name, last_name, email, password, utype, role_type) {
	// 	return new Promise((resolve, reject) => {
	// 		api.post('users/register.json', { first_name: first_name, last_name: last_name, email: email, password: password, utype: utype, role_type: role_type }).then(responseJson => {
	// 			resolve(responseJson)
	// 		}).catch(err => {
	// 			reject(err)
	// 		})
	// 	})
	// }

	static getUserDetail(id,auth){
		return new Promise((resolve,reject)=>{
			api.get('Customers/'+id+'?access_token='+auth).then(responseJson=>{
				resolve(responseJson)
			}).catch(err=>{
					reject(err)
			})
		})
	}

	static getAllLanguagesList() {
		return new Promise((resolve, reject) => {
			api.get('Languages').then(responseJson => {
				resolve(responseJson)
			}).catch(err => {
				reject(err)
			})
		})
	}
	static getAllCurrencyList() {
		return new Promise((resolve, reject) => {
			api.get('Currencies').then(responseJson => {
				resolve(responseJson)
			}).catch(err => {
				reject(err)
			})
		})
	}






}

export default authApi
