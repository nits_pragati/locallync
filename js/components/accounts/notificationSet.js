import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, StatusBar, TouchableOpacity, Text, AsyncStorage } from 'react-native';
import { Container, Content } from 'native-base';
import { userUpdate } from '../accounts/elements/authActions';
import styles from './style';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FSpinner from 'react-native-loading-spinner-overlay';

import api from '../../api';

const resetAction1 = NavigationActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'ConnectWithBusinesses', params:{fromScreen:'signup'} })],
});

class NotificationSet extends Component {
    constructor(props) {
        super(props);
        this.state = {
            longitude: this.props.navigation.state.params.locdata.longitude ? this.props.navigation.state.params.longitude : '',
            latitude: this.props.navigation.state.params.locdata.latitude ? this.props.navigation.state.params.latitude : '',
            loader: false,
            latitude: '',
            longitude: ''
        }
    }

    componentDidMount() {
        if (this.props.navigation.state) {
            this.setState({
                latitude: this.props.navigation.state.params.locdata.latitude ? this.props.navigation.state.params.locdata.latitude : this.props.navigation.state.params.locdata.coords.latitude,
                longitude: this.props.navigation.state.params.locdata.longitude ? this.props.navigation.state.params.locdata.longitude : this.props.navigation.state.params.locdata.coords.longitude
            });
        }
        AsyncStorage.getItem('UserDetails', (err, result) => {
            let data = JSON.parse(result);
        });
    }

    locationAndNotification(val) {

        AsyncStorage.getItem('UserDetails', (err, result) => {
            let data = JSON.parse(result);
            // const latitude = this.props.navigation.state.params.locdata.coords.latitude;
            // const longitude = this.props.navigation.state.params.locdata.coords.longitude;

            const latitude = this.state.latitude.toString();
            const longitude = this.state.longitude.toString();
            const value = val + '';
            const id = data.details.id + '';
            this.setState({
                loader: true
            })
        
            api.post('Users/UpdateLocation.json', { id: id, latitude: latitude, longitude: longitude, device_token_id: "", is_notify: value }).then(res => {
                console.log("res in noti :", res);
                this.setState({ loader: false });
                if (res.ack == 1) {
                    data.details.country_name = res.country_name;
                    data.details.city = res.city;
                    data.details.latitude = latitude;
                    data.details.longitude = longitude;
                    AsyncStorage.setItem('UserDetails', JSON.stringify(data), (err, result) => {
                        this.props.userUpdate(data);
                        this.props.navigation.dispatch(resetAction1);
                    })
                    // console.log(this.props.auth.data);
                }
            }).catch((err) => {
                console.log("err in noti :", err);
                this.setState({ loader: false });
                this.props.navigation.dispatch(resetAction1);
            })
        });




        // })


    }

    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#fff"
                />
                <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                <View style={[styles.mainContenner, { justifyContent: 'center', flex: 1 }]}>
                    <View>
                        <View style={{ alignItems: 'center' }}>
                            <View style={{ position: 'relative' }}>
                                <Text style={[styles.notificationText, styles.notificationTextNew]}>2</Text>
                                <Ionicons name='ios-notifications' style={styles.notificationIcon} />
                            </View>
                        </View>
                        <View style={styles.notificationTxtWarp}>
                            <Text style={styles.notificationTxt1}>Get Notified</Text>
                            <Text style={styles.notificationTxt2}>Never miss out on new Offer & Discounts</Text>
                        </View>
                        <View style={styles.notificationBtnWarp}>
                            <View style={styles.notificationBtnWarpInr}>
                                <TouchableOpacity style={[styles.signInBtn, styles.notificationBtn1]} onPress={() => this.locationAndNotification('1')}>
                                    <Text style={[styles.signInBtnTxt, {color:'#3a3a3a'}]}>No Thanks</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.notificationBtnWarpInr}>
                                <TouchableOpacity style={[styles.signInBtn, styles.notificationBtn2]} onPress={() => this.locationAndNotification('1')}>
                                    <Text style={[styles.signInBtnTxt, styles.notificationBtnText]}>Notify Me</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </Container>
        );
    }
}

NotificationSet.propTypes = {
    auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
}

const mapDispatchToProps = dispatch => ({
    userUpdate: (data) => dispatch(userUpdate(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(NotificationSet);
// export default NotificationSet;
