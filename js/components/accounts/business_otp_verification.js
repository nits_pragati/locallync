import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, Alert, AsyncStorage } from "react-native";
import { Container, Header, Body, Content, Tabs, Tab, TabHeading, ScrollableTab, Footer, FooterTab, Button, } from "native-base";
import styles from "./style";
import defaultStyle from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FSpinner from 'react-native-loading-spinner-overlay';
import api from '../../api';
import SplashScreen from 'react-native-splash-screen';

const resetAction1 = NavigationActions.reset({
	index: 0,
	actions: [NavigationActions.navigate({ routeName: 'Login' })],
});

class BusinessOtpVerification extends Component {
	constructor(params) {
		super(params)
		this.state = {
			email: '',
			loader: false,
			step: 1,
			otp: '',
			password: '',
			conformPassword: ''
		}
	}
	componentDidMount(){
		SplashScreen.hide();
	}

	sendOtp(){

		if (this.state.otp.trim() == '') {
			Alert.alert('Please enter your OTP');
			return;
		}
		if (!this.state.otp.trim().length == 4) {
			Alert.alert('Please enter a valid OTP');
			return;
		}
        const otp_signup = this.state.otp.trim();
        let id;
        
        AsyncStorage.getItem('UserDetails', (err, result) => {
            let data = JSON.parse(result);
            id =data.details.id;
            api.post('Users/verify_otp_signup.json', { id, otp_signup }).then(res => {
                if (res.ack == 1) {
                    this.props.navigation.navigate('AddBusiness');
                }else{
                    Alert.alert(res.message);
                }
            }).catch((err) => {
                console.log(err);
            })
        });
		
		
	}

	goLogin(){
		AsyncStorage.setItem('UserDetails', '', (err, result) => { });
		this.props.navigation.dispatch(resetAction1);
	}



	render() {
		return (
			<Container >
				<StatusBar
					backgroundColor="#133567"
				/>

				<Header style={[defaultStyle.headerWarp]} androidStatusBarColor="#133567" noShadow>

					<TouchableOpacity style={defaultStyle.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
						<Ionicons name='ios-arrow-back' style={[defaultStyle.headerBackIcon]} />
					</TouchableOpacity>

					<Body style={styleSelf.tac}>
						<Text style={[defaultStyle.headerMiddleText]}>FORGOT PASSWORD</Text>
					</Body>

					<TouchableOpacity style={defaultStyle.headerRightBtn} activeOpacity={1}>
						{/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
					</TouchableOpacity>

				</Header>

				<Content style={[styles.whiteContent]}>
					<FSpinner visible={ this.state.loader } textContent={'Loading...'} textStyle={{ color: '#FFF' }} />	
                        <View style={[styles.locationWarp, { marginTop: 50 }]}>
                            <Text style={[styles.locationText1, { marginBottom: 20, marginTop: 0, paddingTop: 0 }]}>Thank you for Signing up</Text>
                                <Text style={[styles.locationText2, { marginBottom: 30 }]}> We sent you a confirmation code to your email. </Text>
                                <Text style={[styles.locationText2, { marginBottom: 30 }]}> Please check your email and enter the code below to continue adding your business details. </Text>
                            <View style={styles.inputWarp}>
                                <TextInput style={[styles.input, { textAlign: 'center' }]} underlineColorAndroid='transparent' placeholder='Enter Code' onChangeText={(text) => this.setState({ otp: text })} value={this.state.otp} keyboardType={'numeric'} secureTextEntry maxLength={4} />
                            </View>

                            <TouchableOpacity style={[styles.signInBtn, styles.locationBtn, { marginBottom: 0 }]} onPress={() => this.sendOtp()}>
                                <Text style={[styles.signInBtnTxt, styles.locationBtnTxt]}>OK</Text>
                            </TouchableOpacity>

							<View style={styles.orWarp}>
								<View style={styles.orDividerLine}></View>
								<View>
									<Text style={styles.orText}> or </Text>
								</View>
								<View style={styles.orDividerLine}></View>
							</View>

							<View style={styles.nAM}>
								<Text style={styles.nAMText}> Already have an account ? </Text>
								<TouchableOpacity onPress={() => this.goLogin()}>
									<Text style={styles.touchText}> Sign In </Text>
								</TouchableOpacity>
							</View>

                        </View>	
					
				</Content>
			</Container>
		);
	}
}


export default BusinessOtpVerification;
