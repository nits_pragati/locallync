import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Text, TextInput, Alert } from "react-native";
import { Container, Header, Body, Content } from "native-base";
import styles from "./style";
import defaultStyle from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import api from '../../api';
import FSpinner from 'react-native-loading-spinner-overlay';
import SplashScreen from 'react-native-splash-screen';


class ConnectWithBusinesses extends Component {

    constructor(params) {
        super(params)
        this.state = {
            loader: false,
            list_category: [],
            image_url: '',
            home_list: []
        }
        console.log("connect_with",this.props)
    }

    componentDidMount() {
        console.log(this.props.navigation);
        SplashScreen.hide();

        this.setState({
            loader: true,
        });
        api.get('categories/list_category.json').then(res => {

            res.details.map((data) => {
                data.image = res.image_url + data.image;
            })
            this.setState({
                list_category: res.details,
                home_list: res.details,
                loader: false,
            });

        }).catch((err) => {
            this.setState({ loader: false });
            console.log(err);
        });
        // if (this.props.navigation.state.params.fromScreen == 'signup'){
        //     Alert.alert('', 'Great! Enjoy the offers. You can also add more businesses from the connection tab')
        // }
    }

    /** search */
    // supportSearch(text) {
    //     if (text) {
    //         const regex = new RegExp(`${text.trim()}`, 'i');

    //         let items = this.state.home_list.filter(
    //             item => item.name.search(regex) >= 0);
    //         if (items.length > 0) {
    //             this.setState({ list_category: items });
    //         }
    //         else {
    //             let categoriesList = [];
    //             for (let i = 0; i < this.state.home_list.length; i++) {
    //                 let IsPresent = false;
    //                 for (let j = 0; j < this.state.home_list[i].UserBusinessCategories.length; j++) {
    //                     if (this.state.home_list[i].UserBusinessCategories[j].business_name.toLowerCase().includes(text.toLowerCase())) {
    //                         IsPresent = true;
    //                     }
    //                 }
    //                 if (IsPresent) {
    //                     categoriesList.push(this.state.home_list[i]);
    //                 }
    //             }
    //             this.setState({ list_category: categoriesList });
    //         }
    //     }
    //     else {
    //         const home_list = this.state.home_list;
    //         this.setState({ list_category: home_list });
    //     }
    // }

    supportSearch(text) {
        if (text) {
            console.log("t :", text);
            const regex = new RegExp(`${text.trim()}`, 'i');

            let items = this.state.home_list.filter(
                item => item.name.search(regex) >= 0);
            console.log("i :", items);
            if (items.length > 0) {
                this.setState({ list_category: items });
            }
            else {
                let categoriesList = [];
                for (let i = 0; i < this.state.home_list.length; i++) {
                    let IsPresent = false;
                    for (let j = 0; j < this.state.home_list[i].UserBusinessCategories.length; j++) {
                        if (this.state.home_list[i].UserBusinessCategories[j].business_name != undefined) {
                            if (this.state.home_list[i].UserBusinessCategories[j].business_name.toLowerCase().includes(text.toLowerCase())) {
                                IsPresent = true;
                            }
                        } else {
                            this.setState({
                                list_category: []
                            });
                        }
                    }
                    if (IsPresent) {
                        categoriesList.push(this.state.home_list[i]);
                    }
                }
                this.setState({ list_category: categoriesList });
            }
        }
        else {
            const home_list = this.state.home_list;
            this.setState({ list_category: home_list });
        }
    }

    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header style={defaultStyle.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={defaultStyle.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={defaultStyle.headerBackIcon} />
                    </TouchableOpacity>
                    <Body style={styleSelf.tac}>
                        <Text style={defaultStyle.headerMiddleText}>Connect with businesses</Text>
                    </Body>
                    <TouchableOpacity style={defaultStyle.headerRightBtn} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                    </TouchableOpacity>
                </Header>

                <View style={defaultStyle.searchWarp}>
                    <View style={defaultStyle.searchinnr}>
                        <Ionicons style={defaultStyle.searchIcon} name='ios-search' />
                        <TextInput style={defaultStyle.searchInput} underlineColorAndroid='transparent' placeholder='Search by name or keyword...' onChangeText={(text) => this.supportSearch(text)} />
                    </View>
                </View>

                <Content style={styles.whiteContent}>
                    <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                    <View style={{ padding: 10 }}>
                        <Text style={[styles.connectText1, {color: '#0077b5'}]}>Connect and Save</Text>
                        <Text style={[styles.connectText2, {color: '#313335'}]}>
                            Get exclusive offers and LYNK tokens
                            {/* <Text style={[styles.connectText3, {color: '#313335'}]}> and more!</Text> */}
                        </Text>    
                        <Text style={{ fontSize:10, textAlign: 'center',  paddingTop: 10 }}>
                            In addition to excluisve offers, you will get LYNK tokens that you can use at any participating businesses
                        </Text>                   
                        <View style={[styles.connectMainWarp, { marginTop: 10 }]}>

                            {
                                this.state.list_category.length != 0 ? (
                                    this.state.list_category.map((data, key) => {
                                        return (
                                            <TouchableOpacity style={styles.connectWarp} onPress={() => this.props.navigation.navigate('Businesses', { businessesId: data.id, fromScreen: this.props.navigation.state.params.fromScreen ? this.props.navigation.state.params.fromScreen : '', IsFromSignup: this.props.navigation.state.params.fromScreen })} key={key}>
                                                <Image source={{ uri: data.image }} style={styles.connectImage} />
                                                <Text style={styles.connectText}>{data.name}</Text>
                                            </TouchableOpacity>
                                        )
                                    })

                                ) : (
                                        <Text style={{ width: '100%', textAlign: 'center' }}>No data found</Text>
                                    )
                            }
                        </View>
                    </View>
                </Content>
            </Container>
        );
    }
}


export default ConnectWithBusinesses;
