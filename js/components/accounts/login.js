import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import PropTypes from 'prop-types';
import { login, LogedInData, saveDataForOffline } from './elements/authActions';
import { connect } from 'react-redux';
import { Image, View, StatusBar, TouchableOpacity, Text, TextInput, Alert, AsyncStorage, NetInfo } from 'react-native';
import { Container, Content } from 'native-base';
import styles from './style';
import SplashScreen from 'react-native-splash-screen';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import api from '../../api';
import FSpinner from 'react-native-loading-spinner-overlay';
import ValidationComponent from 'react-native-form-validator';
const launchscreenLogo = require('../../../img/logo.png');

// const resetAction1 = NavigationActions.reset({
//   index: 0,
//   actions: [NavigationActions.navigate({ routeName: 'Home' })],
// });

// const resetAction2 = NavigationActions.reset({
//   index: 0,
//   actions: [NavigationActions.navigate({ routeName: 'VendorHome' })],
// });

// const resetAction3 = NavigationActions.reset({
//   index: 0,
//   actions: [NavigationActions.navigate({ routeName: 'AddBusiness' })],
// });
const resetAction1 = NavigationActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Home' })],
});
const resetAction3 = NavigationActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Location', params:{fromScreen:'login'} })],
});

const resetAction4 = NavigationActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'AddBusiness' })],
});

const resetAction2 = NavigationActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'VendorHome' })],
});
const resetAction5 = NavigationActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'BusinessOtpVerification' })],
});

class Login extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      loader: false,
      errorPassword: false,
      errorEmail: false,
      pageVisible: false,
      latitude:'',
      longitude:''
    };
    SplashScreen.hide();
  }

  componentDidMount() {
    AsyncStorage.getItem('UserDetails', (err, UserDetails) => {
      console.log(UserDetails);
      let data = JSON.parse(UserDetails);
      let logedIn = data ? data.ack : 0
      if (logedIn) {
        NetInfo.getConnectionInfo().then((connectionInfo) => {
          if(connectionInfo.type!="none")
          {
            this.props.LogedInData(data.details.id).then((resLogin) => {
              if (resLogin.details.utype == 2) {
                // this.props.navigation.navigate('Home');
                if (data.details.latitude && data.details.longitude) {
                  this.props.navigation.dispatch(resetAction1);
                } else {
                  this.props.navigation.dispatch(resetAction3);
                }
              }
              if (resLogin.details.utype == 1) {
                console.log(resLogin.details);
                // this.props.navigation.dispatch(resetAction2);
                if (resLogin.details.otp_signup_verify == 1) {
                  if (resLogin.details.business_status == 1) {
                    this.props.navigation.dispatch(resetAction2);
                  }
                  if (resLogin.details.business_status == 0) {
                    this.props.navigation.dispatch(resetAction3);
                  }
                } else {
                  this.props.navigation.dispatch(resetAction5);
                }
              }
            }).catch((err) => {
              console.log(err);
              this.setState({
                pageVisible: true,
              })
              SplashScreen.hide();
            });
          }
          else
          {
            this.props.saveDataForOffline(data);
            if (data.details.utype == 2) {
              // this.props.navigation.navigate('Home');
              if (data.details.latitude && data.details.longitude) {
                this.props.navigation.dispatch(resetAction1);
              } else {
                this.props.navigation.dispatch(resetAction3);
              }
            }

          }
         

        });

      }
      else {
        this.setState({
          pageVisible: true,
        })
        SplashScreen.hide();
      }
    });

    this.gettingCurrentUserLocation();
  }

  /** getting user current location */
  gettingCurrentUserLocation() {
    let getPosition = function (options) {
      return new Promise(function (resolve, reject) {
        navigator.geolocation.getCurrentPosition(resolve, reject, options);
      });
    }
    getPosition().then(position => {
      if (position) {
        this.setState({
          latitude: position.coords.latitude.toString(),
          longitude: position.coords.longitude.toString()
        });
      }
    })
      .catch((err) => {
        // Alert.alert('', 'Please turn on the location');
        console.log("location not getting");
      })
  }

  loginTask() {
    this.validate({
      password: { required: true },
      email: { email: true, required: true }
    });

    if (this.isFormValid()) {
      const email = this.state.email.trim();
      const password = this.state.password.trim();
      const latitude = this.state.latitude;
      const longitude = this.state.longitude;

      
      this.setState({ loader: true });
      this.props.login(email, password, latitude, longitude).then((resLogin) => {
        
        this.setState({ loader: false });
        if (resLogin.ack == 1) {
          let id = resLogin.details.id + '';
        
          if (resLogin.details.utype == 2) {
            //this.props.navigation.dispatch(resetAction1);
            if (resLogin.details.latitude && resLogin.details.longitude) {
              this.props.navigation.dispatch(resetAction1);
            }
            else {
              this.props.navigation.dispatch(resetAction3);
            }
          }
          if (resLogin.details.utype == 1) {
            if (resLogin.details.business_status == 1) {
              this.props.navigation.dispatch(resetAction2);
            }
            if (resLogin.details.business_status == 0) {
              this.props.navigation.dispatch(resetAction3);
            }

          }
        } else {
          Alert.alert('', resLogin.message);
          this.setState({
            errorPassword: true,
            errorEmail: true
          })
        }
      }).catch((err) => {
        this.setState({ loader: false });
        console.log(err);
      })
    } else {
      let errorPassword = this.isFieldInError('password');
      let errorEmail = this.isFieldInError('email');
      this.setState({
        errorPassword: errorPassword,
        errorEmail: errorEmail
      })
    }
  }

  render() {
    return (
      this.state.pageVisible ? (
        <Container >
          <StatusBar
            backgroundColor="#fff"
            hidden={true}
          />
          <Content style={styles.mainContenner}>
            <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
            <View style={styles.topLogoWarp}>
              <Image source={launchscreenLogo} style={styles.topLogo} />
            </View>
            <View style={styles.topHeadingWarp}>
              <Text style={styles.topHeading}>LOG IN</Text>
            </View>
            <View style={styles.inputFieldMainWarp}>
              <View style={[styles.inputWarp, this.state.errorEmail ? { borderColor: '#FF0000' } : null]}>
                <MaterialIcons name='email' style={styles.inputIcon} />
                <TextInput style={[styles.input]} underlineColorAndroid='transparent' placeholder='Email address' onChangeText={(text) => this.setState({ email: text })} value={this.state.email} keyboardType={'email-address'} />
              </View>
              <View style={[styles.inputWarp, this.state.errorPassword ? { borderColor: '#FF0000' } : null]}>
                <FontAwesome name='lock' style={styles.inputIcon} />
                <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Password' onChangeText={(text) => this.setState({ password: text })} value={this.state.password} secureTextEntry={true} />
              </View>
              <View style={styles.forgotP}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgotPassword')}>
                  <Text style={styles.forgotPText}>Forgot Password?</Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity style={styles.signInBtn} onPress={() => this.loginTask()}>
                <Text style={styles.signInBtnTxt}>LOG IN</Text>
              </TouchableOpacity>
              {/* <View style={styles.orWarp}>
                <View style={styles.orDividerLine}></View>
                <View>
                  <Text style={styles.orText}>or</Text>
                </View>
                <View style={styles.orDividerLine}></View>
              </View>
              <TouchableOpacity style={styles.linkedin}>
                <FontAwesome name='linkedin' style={styles.linkedinIcon} />
                <Text style={styles.linkedinTxt}> CONNECT WITH LINKEDIN </Text>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.linkedin, { backgroundColor: '#3b5998' }]}>
                <FontAwesome name='facebook-official' style={styles.linkedinIcon} />
                <Text style={styles.linkedinTxt}> CONNECT WITH FACEBOOK </Text>
              </TouchableOpacity> */}

              <View style={styles.nAM}>
                <Text style={styles.nAMText}> Not a member? </Text>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Signup')}>
                  <Text style={styles.touchText}> Create account </Text>
                </TouchableOpacity>
              </View>
              <View style={styles.loginPageLastTextWarp}>
                <Text style={styles.loginPageLastText}> By logged in, you agree to the Terms of Service & Privacy Policy. </Text>
              </View>
            </View>
          </Content>
        </Container>
      ) : (
          <Container ></Container>
        )

    );
  }
}

Login.propTypes = {
  auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    login: (email, password, latitude, longitude) => dispatch(login(email, password, latitude, longitude)),
    LogedInData: (id) => dispatch(LogedInData(id)),
    saveDataForOffline: (details) => dispatch(saveDataForOffline(details))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
