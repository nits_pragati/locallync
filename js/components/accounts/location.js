import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { connect } from 'react-redux';
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, AsyncStorage, Alert } from "react-native";
import { Container, Header, Body, Content, Tabs, Tab, TabHeading, ScrollableTab, Footer, FooterTab, Button, } from "native-base";
import styles from "./style";
import { locationAdd } from './elements/authActions'
import defaultStyle from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SplashScreen from 'react-native-splash-screen';


class Location extends Component {
    constructor(params) {
        super(params)
        this.state = {
            longitude: '',
            latitude: ''

        }
    }

    componentDidMount() {

        SplashScreen.hide();

        /** getting latitude and longitude from select location  */

        // if (this.props.navigation.state.params.latitude.length != 0 && this.props.navigation.state.params.longitude.length != 0) {
        //     this.setState({
        //         latitude: this.props.navigation.state.params.latitude,
        //         longitude: this.props.navigation.state.params.longitude
        //     });

        //     if (this.state.latitude.length != 0 && this.state.longitude.length != 0) {

        //     }
        // }
    }

    locationUpdate() {
        let getPosition = function (options) {
            return new Promise(function (resolve, reject) {
                navigator.geolocation.getCurrentPosition(resolve, reject, options);
            });
        }
        getPosition().then(position => {
            if(position){
                console.log("position :", position);
                this.props.navigation.navigate('NotificationSet', { locdata: position });
            }
            // this.setState({
            //     latitude: position.coords.latitude,
            //     longitude: position.coords.longitude,
            // });
            // locdata = {
            //     latitude: position.coords.latitude,
            //     longitude: position.coords.longitude
            // }
        })
        // ,
        //     (error) => {
        //         console.log("error :", error);
        //     }, { enableHighAccuracy: false })
        .catch((err)=>{
            Alert.alert('', 'Please turn on the location');
        })
    }


    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header style={[defaultStyle.headerWarp, { backgroundColor: '#fff' }]} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={defaultStyle.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={[defaultStyle.headerBackIcon, { color: '#323232' }]} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={[defaultStyle.headerMiddleText, { color: '#323232' }]}>LOCATION</Text>
                    </Body>

                    <TouchableOpacity style={defaultStyle.headerRightBtn} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                    </TouchableOpacity>

                </Header>

                <Content style={styles.whiteContent}>

                    <View style={styles.locationWarp}>
                        <Text style={styles.locationText1}>Want to see offers near you?</Text>
                        <Text style={styles.locationText2}>Location services will be enabled to show you nearby offers</Text>

                        <View style={styles.locationImageWarp}>
                            <Image source={require('../../../img/icons/map.png')} style={styles.locationImage} />
                        </View>

                        <TouchableOpacity style={[styles.signInBtn, styles.locationBtn]} onPress={() => this.locationUpdate()}>
                            <FontAwesome name='location-arrow' style={[styles.signInBtnIcon, styles.locationBtnIcon]} />
                            <Text style={[styles.signInBtnTxt, styles.locationBtnTxt]}>Use my current location</Text>
                        </TouchableOpacity>

                        <View style={styles.orWarp}>
                            <View style={styles.orDividerLine}></View>
                            <View>
                                <Text style={styles.orText}>or</Text>
                            </View>
                            <View style={styles.orDividerLine}></View>
                        </View>

                        <TouchableOpacity style={styles.locationBtn2} onPress={() => this.props.navigation.navigate('LocationAutocomplete')}>
                            <Text style={[styles.signInBtnTxt, styles.locationBtnTxt2]}>Select location manually</Text>
                        </TouchableOpacity>
                    </View>
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        locationAdd: (first_name, last_name, email, password, utype, role_type) => dispatch(locationAdd(first_name, last_name, email, password, utype, role_type))
    }
}
// export default Location;
export default connect(mapStateToProps, mapDispatchToProps)(Location);

