import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Image, View, StatusBar, TouchableOpacity, Text, TextInput } from 'react-native';
// import FCM, { FCMEvent, NotificationType } from "react-native-fcm";
import { Container, Content } from 'native-base';
import styles from './style';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
const launchscreenLogo = require('../../../img/logo.png');
import SplashScreen from 'react-native-splash-screen';

class UserType extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    componentDidMount() {
        SplashScreen.hide();
    }

    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#fff"
                />
                <View style={ { flex: 1, justifyContent: 'center' }}>
                        <View style={{ justifyContent: 'center' }}>
                            <View style={styles.topLogoWarp}>
                                <Image source={launchscreenLogo} style={styles.topLogo} />
                            </View>
                            <View style={styles.topHeadingWarp}>
                                <Text style={[styles.topHeading, { fontSize: 20 }]}>SIGN UP TYPE</Text>
                            </View>
                            <View style={styles.userTypeWarp}>
                                <TouchableOpacity style={styles.userTypeBtn} onPress={() => this.props.navigation.navigate('Signup', { userType: 'user' })}>
                                    <Image source={require('../../../img/icons/user.png')} style={styles.userTypeBtnImage} />
                                    <Text>USER</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.userTypeBtn} onPress={() => this.props.navigation.navigate('Signup', { userType: 'business' })} >
                                    <Image source={require('../../../img/icons/hand-shake.png')} style={styles.userTypeBtnImage} />
                                    <Text>BUSINESS</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                </View>
            </Container>
        );
    }
}

export default UserType;
