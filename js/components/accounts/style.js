const React = require("react-native");
const { StyleSheet, Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
    mainContenner: { 
        backgroundColor: '#fff',
    },
    topLogoWarp: { 
        alignItems: 'center', 
        marginTop: 30 
    },
    topLogo: { 
        width: 136, 
        height: 66 
    },
    topHeadingWarp: { 
        alignItems: 'center', 
        justifyContent: 'center', 
        marginTop: 20, 
        marginBottom: 10 
    },
    topHeading: {
        fontSize: 15, 
        color: '#2096AB'
    },
    inputFieldMainWarp:{ 
        paddingLeft: 10, 
        paddingRight: 10 
    },
    inputWarp: { 
        borderWidth: 1, 
        borderColor: '#e2e2e2', 
        borderRadius: 40, 
        alignItems: 'center', 
        flexDirection: 'row', 
        paddingLeft: 15, 
        paddingRight: 15, 
        marginBottom: 15,
        alignItems: 'center',
        justifyContent: 'center' 
    },
    inputIcon: { 
        fontSize: 20,
        color: '#888888' 
    },
    input: { 
        paddingLeft: 10, 
        paddingRight: 10, 
        flex: 1,
        height: 40,
        paddingBottom: 5,
        paddingTop: 8,
        fontSize: 12 
    },
    signInBtn: { 
        backgroundColor: '#3ab3ce', 
        borderRadius: 40 / 2, 
        height: 40, 
        alignItems: 'center', 
        justifyContent: 'center', 
        marginTop: 15,
        flexDirection: 'row' 
    },
    signInBtnTxt: { 
        color: '#fff', 
        fontSize: 13 
    },
    signInBtnIcon: { 
        paddingRight: 10, 
        color: '#fff' 
    },
    nAM: { 
        flexDirection: 'row', 
        justifyContent: 'center', 
        marginBottom: 10, 
        marginTop: 15,
    },
    nAMText: {
        fontSize: 12
    },
    touchText: { 
        color: '#3ab3ce',
        fontSize: 12 
    },
    linkedin:{ 
        backgroundColor: '#3775aa', 
        borderRadius: 40 / 2, 
        height: 40, 
        alignItems: 'center', 
        justifyContent: 'center', 
        flexDirection: 'row' ,
        marginBottom: 15
    },
    linkedinTxt:{ 
        color: '#fff', 
        fontSize: 13 
    },
    linkedinIcon:{
        color: '#fff', 
        marginRight: 5
    },
    orWarp: { 
        flexDirection: 'row', 
        alignItems: 'center', 
        marginTop: 20, 
        marginBottom: 20 
    },
    orDividerLine:{ 
        flex: 1, 
        height: 1, 
        backgroundColor: '#f1f1f1' 
    },
    orText:{ 
        paddingLeft: 10, 
        paddingRight: 10, 
        fontSize: 15 
    },

    forgotP:{ 
        alignItems: 'flex-end', 
        width: '100%' 
    },
    forgotPText:{ 
        color: '#3ab3ce', 
        fontSize: 12 
    },
    loginPageLastTextWarp: {
        width: '100%',
        marginBottom: 10
    },
    loginPageLastText:{ 
        textAlign: 'center', 
        width: '100%', 
        fontSize: 11 
    },
    // Sign Up Type
    
    userTypeWarp: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 30,
        marginBottom: 30
    },
    userTypeBtn:{ 
        borderWidth: 1, 
        borderColor: '#3ab3ce', 
        width: 140, 
        marginLeft: 5, 
        marginRight: 5, 
        alignItems: 'center', 
        justifyContent: 'center', 
        borderRadius: 200, 
        height: 140, 
        width: 140 
    },
    userTypeBtnImage: { 
        height: 70, 
        width: 70, 
        marginBottom: 10 
    },
    whiteContent: { 
        flex: 1, 
        backgroundColor: '#fff' 
    },

    // location : start

    locationWarp: { 
        paddingLeft: 10, 
        paddingRight: 10, 
        marginTop: 30 
    },
    locationText1:{ 
        width: '100%', 
        textAlign: 'center', 
        fontSize: 16, 
        color: '#3ab3ce' 
    },
    locationText2:{ 
        textAlign: 'center', 
        fontSize: 11 
    },
    locationImageWarp:{ 
        alignItems: 'center', 
        justifyContent: 'center', 
        marginTop: 20, 
        marginBottom: 20 
    },
    locationImage:{
        width: 300, 
        height: 133
    },
    locationBtn:{ 
        backgroundColor: '#3ab3ce', 
        borderRadius: 0 
    },
    locationBtnIcon:{ 
        color: '#323232' 
    },
    locationBtnTxt:{
        color: '#fff'
    },
    locationBtn2: { 
        backgroundColor: 'transparent', 
        alignItems: 'center' 
    },
    locationBtnTxt2:{ 
        textDecorationLine: 'underline', 
        color: '#323232' 
    },
    // location : end
    notificationText: {
        marginTop: -2,
        position: 'absolute',
        top: -5,
        right: -5,
        backgroundColor: '#2096AB',
        fontSize: 9,
        zIndex: 99,
        borderRadius: 10,
        paddingRight: 5,
        paddingLeft: 5,
        color: '#fff'
    },

    // connect with businesses: start

    connectWarp: { 
        width: '46%', 
        minWidth:140,
        borderWidth: 1, 
        borderColor: '#e0e0e0', 
        margin: 4, 
        flexDirection: 'row', 
        alignItems: 'center', 
        padding: 8, 
        borderRadius: 4, 
        paddingTop: 10,
        paddingBottom: 10, 
        backgroundColor: '#fff'
    },
    connectImage:{ 
        height: 34, 
        width: 34,
        marginRight:5
    },
    connectText: { 
        paddingLeft: 5,
        fontSize: 12,
        flex: 1
    },
    connectMainWarp: { 
        flexDirection: 'row', 
        flexWrap: 'wrap',
        justifyContent: 'center' ,
        paddingTop:30
    },
    connectText1: { 
        width: '100%', 
        textAlign: 'center', 
        fontSize: 16 
    },
    connectText2: { 
        color: '#2096AB', 
        width: '100%', 
        textAlign: 'center', 
        fontSize: 12,
        fontWeight: 'bold' 
    },
    connectText3:{ 
        color: '#3ab3ce', 
        fontWeight: 'normal' 
    },

    // connect with businesses: end

    // connect with notification: start

    notificationTextNew: { 
        backgroundColor: '#FF0000', 
        fontSize: 16, 
        paddingLeft: 10, 
        paddingRight: 10, 
        borderRadius: 20, 
        marginRight: -9, 
        marginTop: -5 
    },
    notificationIcon: { 
        fontSize: 50, 
        color: '#000' 
    },
    notificationTxtWarp:{ 
        marginTop: 15 
    },
    notificationTxt1:{ 
        width: '100%', 
        textAlign: 'center', 
        fontSize: 18 
    },
    notificationTxt1: {
        width: '100%',
        textAlign: 'center',
        fontSize: 18
    },
    notificationTxt2:{ 
        width: '100%', 
        textAlign: 'center', 
        marginTop: 10 
    },
    notificationBtnWarp:{ 
        flexDirection: 'row', 
        paddingLeft: 10, 
        paddingRight: 10, 
        marginTop: 40 
    },
    notificationBtnWarpInr:{ 
        flex: 1, 
        marginLeft: 5, 
        marginRight: 5 
    },
    notificationBtn1: {
        backgroundColor: '#f0f0f0', 
        borderRadius: 4
    },
    notificationBtn2: {
        backgroundColor:'#3ab3ce',
        borderRadius: 4,
        color:'#fff'
    },
    notificationBtnText:{ 
        color: '#fff' 
    }

    // connect with notification: end
};
