import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text } from "react-native";
import { Container, Header, Body, Content } from "native-base";
import commonStyles from "../../assets/styles"; 
import styles from "./styles"; 
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import SplashScreen from 'react-native-splash-screen';


class VendorHome extends Component {
	// eslint-disable-line
	constructor(params) {
		super(params)
		this.state = {}
	}


	componentDidMount() {
		SplashScreen.hide();
		// this.props.navigation.navigate('Login');
	}


	render() {
		return (
			<Container >
				<StatusBar
					backgroundColor="#133567"
				/>
				<Header searchBar style={commonStyles.headerWarp} androidStatusBarColor="#133567">

					<TouchableOpacity style={commonStyles.headerBackBtn} 
					// onPress={() => this.props.navigation.navigate('Login')} 
					>
						{/* <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon}/> */}
					</TouchableOpacity>

					<Body style={styleSelf.tac}>
						<Text style={commonStyles.headerMiddleText}>LOCALLYNC PLATFORM</Text>
					</Body>
					<TouchableOpacity style={commonStyles.headerBtnRight} onPress={() => this.props.navigation.navigate('Connections')}>
						<SimpleLineIcons name='options-vertical' style={commonStyles.headerBackIcon} />
					</TouchableOpacity>

				</Header>
				<Content style={{ backgroundColor: '#fff' }}>
					<View style={{ padding: 13 }}>
						<View style={{ borderColor: '#ebebeb', borderWidth: 1, alignItems: 'center', paddingTop: 15, paddingBottom: 15 }}>
							<Image source={ require('../../../img/icons/cofe.png') } style={{ width: 100, height: 78 }}/>
						</View>
						<View style={styles.categoryWarp}>
							<TouchableOpacity style={styles.categoryItem} >
								<View style={styles.categoryItemInner}>
									<Image source={require('../../../img/icons/1.png')} />
								</View>
								<Text style={styles.categoryItemText} numberOfLines={1}>Customers</Text>
							</TouchableOpacity>
							<TouchableOpacity style={styles.categoryItem} >
								<View style={[styles.categoryItemInner, { backgroundColor: '#f7c519' }]}>
									<Image source={require('../../../img/icons/2.png')} />
								</View>
								<Text style={styles.categoryItemText} numberOfLines={1}>Customer Orders</Text>
							</TouchableOpacity>
							<TouchableOpacity style={styles.categoryItem} onPress={() => this.props.navigation.navigate('MyBusiness')}>
								<View style={[styles.categoryItemInner, { backgroundColor: '#3c557c' }]}>
									<Image source={require('../../../img/icons/6.png')} style={{ height: 44, width: 44}} />
								</View>
								<Text style={styles.categoryItemText} numberOfLines={1}>Manage Business</Text>
							</TouchableOpacity>
							<TouchableOpacity style={styles.categoryItem} >
								<View style={[styles.categoryItemInner, , { backgroundColor: '#e88524' }]}>
									<Image source={require('../../../img/icons/7.png')} style={{ height: 44, width: 44 }} />
								</View>
								<Text style={styles.categoryItemText} numberOfLines={1}>Customers Feedback</Text>
							</TouchableOpacity>
							{/* <TouchableOpacity style={styles.categoryItem} >
								<View style={[styles.categoryItemInner, { backgroundColor: '#fd6835' }]}>
									<Image source={require('../../../img/icons/3.png')} />
								</View>
								<Text style={styles.categoryItemText} numberOfLines={1}>Customers Feedback</Text>
							</TouchableOpacity> */}
							<TouchableOpacity style={styles.categoryItem} >
								<View style={[styles.categoryItemInner, { backgroundColor: '#629b53' }]}>
									<Image source={require('../../../img/icons/4.png')} />
								</View>
								<Text style={styles.categoryItemText} numberOfLines={1}>Manage Offers</Text>
							</TouchableOpacity>
							<TouchableOpacity style={styles.categoryItem} >
								<View style={[styles.categoryItemInner, { backgroundColor: '#9354c3' }]}>
									<Image source={require('../../../img/icons/5.png')} />
								</View>
								<Text style={styles.categoryItemText} numberOfLines={1}>Scanner</Text>
							</TouchableOpacity>
						</View>
					</View>
					
				</Content>
			</Container>
		);
	}
}

styleSelf = {
	TimingText: {
		fontSize: 20,
	},
	TimingContainer: {
		width: 50,
		height: 60,
		justifyContent: 'center',
	},
	TimingContainerFirst: {
		width: 50,
		height: 60,
		justifyContent: 'center'
	},
	hdClr: {
		color: '#1e3768',
		fontSize: 22
	},
	appHdr2: {
		backgroundColor: '#2893b7',
		alignItems: 'center'

	},
	backBt: {
		fontSize: 16,
		color: "#000"
	},
	tac: {
		alignItems: 'center',
		flex: 1,
		paddingLeft: 15
	},
	menuCardIcon: {
		height: 30,
		width: 30
	},
	headIcon: { fontSize: 28, color: '#fff' },
	headIcon1: { fontSize: 35, color: '#fff', left: 20 },
	headIconForSearch: { fontSize: 15, color: 'black', paddingLeft: 5 },
	headIcon2: { fontSize: 28, color: '#C0C0C0' },
	headIcon3: { fontSize: 24, color: '#C0C0C0',marginLeft:4 },
	headIcon4: { fontSize: 28, color: '#C0C0C0', bottom:10 },
	headIcon5: { fontSize: 38, color: '#C0C0C0',top:2},
	headIcon6: { fontSize: 25, color: '#C0C0C0',}
}

export default VendorHome;
