const React = require("react-native");

const { StyleSheet, Dimensions, Platform } = React;

const deviceHeight = Dimensions.get("window").height;

export default {
  categoryWarp:{ 
    backgroundColor: '#f9f9f9', 
    borderColor: '#ebebeb', 
    borderWidth: 1, 
    flexWrap: 'wrap', 
    flexDirection: 'row', 
    marginTop: 15, 
    justifyContent: 'center' 
  },
  categoryItem:{ 
    alignItems: 'center',
    width: '50%', 
    paddingTop: 10, 
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ebebeb',
    marginBottom: -1 
  },
  categoryItemInner:{ 
    borderRadius: 68 / 2, 
    height: 68, 
    width: 68, 
    backgroundColor: '#0f9ae1', 
    alignItems: 'center', 
    justifyContent: 'center' 
  },
  categoryItemText:{
    fontSize: 11,
    padding: 5,
    paddingBottom: 0
  }

};
