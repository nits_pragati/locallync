import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Text, Alert, TextInput, AsyncStorage } from "react-native";
import { Container, Header, Body, Content, Footer, FooterTab } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';
import FSpinner from 'react-native-loading-spinner-overlay';
import Dialog from "react-native-dialog";
import { Calendar } from 'react-native-calendars';
import DateTimePicker from 'react-native-modal-datetime-picker';
import api from "../../api";

class BookTable extends Component {
    constructor(params) {
        super(params)
        console.log(this.props);
        debugger
        this.state = {
            IsShowDatePicker: false,
            IsSelected: true,
            business_name: this.props.navigation.state.params ? this.props.navigation.state.params.business_name : '',
            BookingType: this.props.navigation.state.params ? this.props.navigation.state.params.BookingType : '',
            business_id: this.props.navigation.state.params ? this.props.navigation.state.params.business_id : '',
            offer: this.props.navigation.state.params ? this.props.navigation.state.params.offer : [],
            people_count: '',
            booking_day: '',
            occassion: '',
            IsAnniversary: false,
            IsBirthDay: false,
            total_days: '',
            from_date: '',
            to_date: '',
            IsShowDatePickerFrom: false,
            IsShowDatePickerTo: false,
            special_request: '',
            IsShowTimePicker: false,
            booking_time: '',
            loader: false

        }
    }
    timeSelected = (date) => {
        const d = new Date(date);
        const n = d.getHours() + ":" + d.getMinutes();
        this.setState({
            booking_time: n,
            IsShowTimePicker: false
        })
    }


    toggleDatePicker() {
        let currentValue = this.state.IsShowDatePicker;
        if (currentValue) {
            this.setState({
                IsShowDatePicker: false
            })
        }
        else {
            this.setState({
                IsShowDatePicker: true
            })
        }
    }
    toggleDatePickerFrom() {
        let currentValue = this.state.IsShowDatePickerFrom;
        if (currentValue) {
            this.setState({
                IsShowDatePickerFrom: false
            })
        } else {
            this.setState({
                IsShowDatePickerFrom: true
            })
        }
    }
    toggleDatePickerTo() {
        let currentValue = this.state.IsShowDatePickerTo;
        if (currentValue) {
            this.setState({
                IsShowDatePickerTo: false
            })
        } else {
            this.setState({
                IsShowDatePickerTo: true
            })
        }
    }
    toggleTimePicker() {
        let currentValue = this.state.IsShowTimePicker;
        if (currentValue) {
            this.setState({
                IsShowTimePicker: false
            })
        }
        else {
            this.setState({
                IsShowTimePicker: true
            })
        }
    }
    onDaySelect(day, type, starting) {
        if (type == 'food') {
            this.setState({
                IsShowDatePicker: false,
                booking_day: day.dateString
            })
        }
        else if (type == 'hotel') {
            if (starting) {
                this.setState({
                    IsShowDatePickerFrom: false,
                    from_date: day.dateString
                })
            }
            else {
                this.setState({
                    IsShowDatePickerTo: false,
                    to_date: day.dateString
                })
            }
        }
    }

    sendBooking() {
        this.setState({
            loader: true
        })
        let data;
        AsyncStorage.getItem("UserDetails", (err, result) => {
            if (result) {
                const parsed = JSON.parse(result);
                if (this.state.BookingType == 'Book Table') {
                    if (this.state.people_count && this.state.booking_day && this.state.booking_time) {
                        console.log(this.state);
                        debugger;
                        data = {
                            "user_id": parsed.details.id,
                            "offer_id": this.state.offer ? this.state.offer.id : '',
                            "business_id": this.state.business_id,
                            "people_count": this.state.people_count,
                            "occassion": this.state.occassion,
                            "booking_day": this.state.booking_day,
                            "booking_time": this.state.booking_time
                        }
                    }
                    else {
                        this.setState({
                            loader: false
                        });
                        Alert.alert("", "Please fill all the fields.");
                        return;
                    }
                }
                else if (this.state.BookingType == 'Reserve') {
                    if (this.state.people_count && this.state.total_days && this.state.from_date && this.state.to_date) {
                        console.log(this.state);
                        debugger;
                        data = {
                            "user_id": parsed.details.id,
                            "offer_id": this.state.offer ? this.state.offer.id : '',
                            "business_id": this.state.business_id,
                            "people_count": this.state.people_count,
                            "total_days": this.state.total_days,
                            "from_date": this.state.from_date,
                            "to_date": this.state.to_date,
                            "special_request": this.state.special_request
                        }
                    } else {
                        this.setState({
                            loader: false
                        });
                        Alert.alert("", "Please fill all the fields.");
                        return;
                    }
                } else if (this.state.BookingType == 'Appointment') {
                    if (this.state.people_count && this.state.booking_day && this.state.booking_time) {
                        console.log(this.state);
                        debugger;
                        data = {
                            "user_id": parsed.details.id,
                            "offer_id": this.state.offer ? this.state.offer.id : '',
                            "business_id": this.state.business_id,
                            "people_count": this.state.people_count,
                            "occassion": "",
                            "booking_day": this.state.booking_day,
                            "booking_time": this.state.booking_time
                        }
                    }
                    else {
                        this.setState({
                            loader: false
                        });
                        Alert.alert("", "Please fill all the fields.");
                        return;
                    }
                } else if (this.state.BookingType == 'Attraction') {
                    if (this.state.people_count && this.state.booking_day && this.state.booking_time) {
                        console.log(this.state);
                        debugger;
                        data = {
                            "user_id": parsed.details.id,
                            "offer_id": this.state.offer ? this.state.offer.id : '',
                            "business_id": this.state.business_id,
                            "people_count": this.state.people_count,
                            "occassion": "",
                            "booking_day": this.state.booking_day,
                            "booking_time": this.state.booking_time
                        }
                    }
                    else {
                        this.setState({
                            loader: false
                        });
                        Alert.alert("", "Please fill all the fields.");
                        return;
                    }
                } else {
                    data = {
                        "user_id": parsed.details.id,
                        "offer_id": this.state.offer ? this.state.offer.id : '',
                        "business_id": this.state.business_id
                    };

                }
                if (data) {
                    api.post('orders/placeOrder.json', data).then((res) => {
                        this.setState({
                            loader: false
                        });
                        Alert.alert("", "Order placed successfully.")
                        this.props.navigation.navigate('Home')
                    }).catch((err) => {
                        this.setState({
                            loader: false
                        });
                        Alert.alert("", "Please try again later.");
                    })
                }
                else {
                    this.setState({
                        loader: false
                    });
                    Alert.alert("", "No data found.")
                }


            }
        })


    }
    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header searchBar style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText}>{this.state.BookingType} Request</Text>
                    </Body>

                    <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                    </TouchableOpacity>

                </Header>
                <Content style={{ flex: 1, padding: 10, backgroundColor: '#ffffff' }}>
                    <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />

                    <View>
                        {
                            this.state.BookingType == 'Book Table' ? (
                                <Text style={{ lineHeight: 20, color: '#101010' }}>Your booking request is about to sent to <Text style={{ fontWeight: 'bold', color: '#00BBD7' }}>{this.state.business_name}</Text></Text>
                            ) : (
                                    <View>
                                        {
                                            this.state.BookingType == 'Reserve' ? (
                                                <Text style={{ lineHeight: 20, color: '#101010' }}>Your reservation request is about to sent to <Text style={{ fontWeight: 'bold', color: '#00BBD7' }}>{this.state.business_name}</Text></Text>
                                            ) : (
                                                    <View>
                                                        {
                                                            this.state.BookingType == 'Put on hold' ? (
                                                                <Text style={{ lineHeight: 20, color: '#101010' }}>Your put on hold request is about to sent to <Text style={{ fontWeight: 'bold', color: '#00BBD7' }}>{this.state.business_name}</Text></Text>
                                                            ) : (
                                                                    <View>{
                                                                        this.state.BookingType == 'Appointment' ? (
                                                                            <Text style={{ lineHeight: 20, color: '#101010' }}>Your appointment request is about to sent to <Text style={{ fontWeight: 'bold', color: '#00BBD7' }}>{this.state.business_name}</Text></Text>
                                                                        ) : (
                                                                                <View>
                                                                                    {
                                                                                        this.state.BookingType == 'Attraction' ? (
                                                                                            <Text style={{ lineHeight: 20, color: '#101010' }}>Your reservation request is about to sent to <Text style={{ fontWeight: 'bold', color: '#00BBD7' }}>{this.state.business_name}</Text></Text>
                                                                                        ) : (
                                                                                                null
                                                                                            )
                                                                                    }
                                                                                </View>
                                                                            )
                                                                    }</View>
                                                                )
                                                        }
                                                    </View>
                                                )
                                        }
                                    </View>
                                )
                        }
                    </View>
                    {/* <Text style={{ lineHeight: 20, color: '#101010' }}>Your booking request is about to sent to <Text style={{ fontWeight: 'bold', color: '#101010' }}>{this.state.business_name}</Text></Text> */}
                    <Text style={{ lineHeight: 20, color: '#101010' }}>For the following offer:</Text>
                    <Text style={{ fontWeight: 'bold', color: '#00BBD7' }}>{this.state.offer ? this.state.offer.offer_name : ''}</Text>
                    {
                        this.state.BookingType == 'Book Table' ? (
                            <View style={{ marginTop: 15, marginBottom: 15 }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#101010' }}>For when do you want Booking?</Text>
                            </View>
                        ) : (
                                <View>
                                    {
                                        this.state.BookingType == 'Reserve' ? (
                                            <View style={{ marginTop: 15, marginBottom: 15 }}>
                                                <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#101010' }}>For when do you want Reservation?</Text>
                                            </View>
                                        ) : (
                                                <View>
                                                    {
                                                        this.state.BookingType == 'Put on hold' ? (
                                                            null
                                                        ) : (
                                                                <View>{
                                                                    this.state.BookingType == 'Appointment' ? (
                                                                        <View style={{ marginTop: 15, marginBottom: 15 }}>
                                                                            <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#101010' }}>For when do you want Appointment?</Text>
                                                                        </View>
                                                                    ) : (
                                                                            <View>
                                                                                {
                                                                                    this.state.BookingType == 'Attraction' ? (
                                                                                        <View style={{ marginTop: 15, marginBottom: 15 }}>
                                                                                            <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#101010' }}>For when do you want Reservation?</Text>
                                                                                        </View>
                                                                                    ) : (
                                                                                            null
                                                                                        )
                                                                                }
                                                                            </View>
                                                                        )
                                                                }</View>
                                                            )
                                                    }
                                                </View>
                                            )
                                    }
                                </View>
                            )
                    }

                    {
                        this.state.BookingType == 'Book Table' ? (
                            <View>
                                <View style={styles.formGroup}>
                                    <View style={styles.formLabel}>
                                        <Text style={{ color: '#101010', }}>WHAT DAY?</Text>
                                    </View>
                                    <TouchableOpacity style={styles.inputField} onPress={() => this.toggleDatePicker()}>
                                        {
                                            this.state.IsShowDatePicker ? (
                                                <Calendar
                                                    onDayPress={(day) => this.onDaySelect(day, 'food')}
                                                    monthFormat={'MMM yyyy'}
                                                    hideArrows={false}
                                                    hideExtraDays={true}
                                                    disableMonthChange={false}
                                                    markedDates={{
                                                        [this.state.DOB]: { selected: true, selectedColor: '#3ab3ce' }
                                                    }}
                                                    minDate={new Date()}

                                                    theme={{
                                                        backgroundColor: '#ffffff',
                                                        calendarBackground: '#ffffff',
                                                        textSectionTitleColor: '#2d4150',
                                                        selectedDayBackgroundColor: '#2d4150',
                                                        selectedDayTextColor: '#ffffff',
                                                        todayTextColor: '#3ab3ce',
                                                        dayTextColor: '#2d4150',
                                                        weekTextColor: '#000',
                                                        textDisabledColor: '#ccc',
                                                        arrowColor: '#3ab3ce',
                                                        textDayFontSize: 14,
                                                        textMonthFontSize: 16,
                                                        textDayHeaderFontSize: 16,

                                                    }}
                                                />
                                            ) : (
                                                    <View style={styles.datepIcon}>
                                                        <Text>{this.state.booking_day}</Text>
                                                        <FontAwesome name='calendar' style={{ color: '#999', fontSize: 20 }} />
                                                    </View>
                                                )
                                        }


                                    </TouchableOpacity>
                                </View>
                                <View style={styles.formGroup}>
                                    <View style={styles.formLabel}>
                                        <Text style={{ color: '#101010', }}>WHAT TIME?</Text>
                                    </View>
                                    <View style={styles.inputField}>
                                        {/* <Text style={{ color: '#101010' }}>Dropdown time</Text> */}
                                        {/* <DateTimePicker
                                            isVisible={this.state.IsShowTimePicker}
                                          
                                            mode="time"
                                        /> */}
                                        <TouchableOpacity style={styles.inputField} onPress={() => this.toggleTimePicker()}>
                                            {
                                                this.state.IsShowTimePicker ? (
                                                    <DateTimePicker

                                                        isVisible={this.state.IsShowTimePicker}
                                                        mode="time"
                                                        onConfirm={this.timeSelected}
                                                        onCancel={() => this.setState({ IsShowTimePicker: false })}
                                                    />
                                                ) : (
                                                        <View style={styles.datepIcon}>
                                                            <Text>{this.state.booking_time}</Text>
                                                            <FontAwesome name='calendar' style={{ color: '#999', fontSize: 20 }} />
                                                        </View>
                                                    )
                                            }


                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={styles.formGroup}>
                                    <View style={styles.formLabel}>
                                        <Text style={{ color: '#101010', }}>HOW MANY PEOPLE?</Text>
                                    </View>
                                    <View style={styles.inputField}>
                                        <TextInput style={[styles.formInput]} underlineColorAndroid='transparent' placeholder='# of people' onChangeText={(text) => this.setState({ people_count: text })} value={this.state.people_count} keyboardType='numeric' />
                                    </View>
                                </View>
                                <View style={{ marginTop: 15, marginBottom: 15 }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#101010' }}>Is it special occasion?</Text>
                                </View>
                                <View style={styles.radioContainer}>
                                    <TouchableOpacity style={styles.radioBox} onPress={() => this.setState({ IsAnniversary: true, IsBirthDay: false, occassion: 'Anniversary' })}>
                                        {this.state.IsAnniversary ? (
                                            <View style={[styles.radioWarp, true ? styles.radioWarpActive : null]}>
                                                <View style={styles.radio}></View>
                                            </View>
                                        ) : (
                                                <View style={[styles.radioWarp, false ? styles.radioWarpActive : null]}>

                                                </View>
                                            )}
                                    </TouchableOpacity>
                                    <Text style={{ color: '#101010', margin: 0, padding: 0 }}>Anniversary</Text>
                                </View>
                                <View style={styles.radioContainer}>
                                    <TouchableOpacity style={styles.radioBox} onPress={() => this.setState({ IsAnniversary: false, IsBirthDay: true, occassion: 'Birthday' })}>
                                        {this.state.IsBirthDay ? (
                                            <View style={[styles.radioWarp, true ? styles.radioWarpActive : null]}>
                                                <View style={styles.radio}></View>
                                            </View>
                                        ) : (
                                                <View style={[styles.radioWarp, false ? styles.radioWarpActive : null]}>

                                                </View>
                                            )}
                                    </TouchableOpacity>
                                    <Text style={{ color: '#101010', margin: 0, padding: 0 }}>Birthday</Text>
                                </View>
                            </View>
                        ) : (
                                <View>
                                    {
                                        this.state.BookingType == 'Put on hold' ? (
                                            <View style={{ marginTop: 30 }}>
                                                <Text>You need to show the offer in the app during the pick at store</Text>
                                            </View>
                                        ) : (
                                                <View>
                                                    {
                                                        this.state.BookingType == 'Reserve' ? (
                                                            <View>
                                                                <View style={styles.formGroup}>
                                                                    <View style={styles.formLabel}>
                                                                        <Text style={{ color: '#101010', }}>HOW MANY PEOPLE?</Text>
                                                                    </View>
                                                                    <View style={styles.inputField}>
                                                                        <TextInput style={[styles.formInput]} underlineColorAndroid='transparent' placeholder='# of people' onChangeText={(text) => this.setState({ people_count: text })} value={this.state.people_count} keyboardType='numeric' />
                                                                    </View>
                                                                </View>
                                                                <View style={styles.formGroup}>
                                                                    <View style={styles.formLabel}>
                                                                        <Text style={{ color: '#101010', }}>HOW MANY DAYS?</Text>
                                                                    </View>
                                                                    <View style={styles.inputField}>
                                                                        <TextInput style={[styles.formInput]} underlineColorAndroid='transparent' placeholder='# no of days' onChangeText={(text) => this.setState({ total_days: text })} value={this.state.total_days} keyboardType='numeric' />
                                                                    </View>
                                                                </View>
                                                                <View style={styles.formGroup}>
                                                                    <View style={styles.formLabel}>
                                                                        <Text style={{ color: '#101010', }}>FROM:</Text>
                                                                    </View>
                                                                    <TouchableOpacity style={styles.inputField} onPress={() => this.toggleDatePickerFrom()}>
                                                                        {
                                                                            this.state.IsShowDatePickerFrom ? (
                                                                                <Calendar
                                                                                    onDayPress={(day) => this.onDaySelect(day, 'hotel', true)}
                                                                                    monthFormat={'MMM yyyy'}
                                                                                    hideArrows={false}
                                                                                    hideExtraDays={true}
                                                                                    disableMonthChange={false}
                                                                                    markedDates={{
                                                                                        [this.state.DOB]: { selected: true, selectedColor: '#3ab3ce' }
                                                                                    }}

                                                                                    theme={{
                                                                                        backgroundColor: '#ffffff',
                                                                                        calendarBackground: '#ffffff',
                                                                                        textSectionTitleColor: '#2d4150',
                                                                                        selectedDayBackgroundColor: '#2d4150',
                                                                                        selectedDayTextColor: '#ffffff',
                                                                                        todayTextColor: '#3ab3ce',
                                                                                        dayTextColor: '#2d4150',
                                                                                        weekTextColor: '#000',
                                                                                        textDisabledColor: '#ccc',
                                                                                        arrowColor: '#3ab3ce',
                                                                                        textDayFontSize: 14,
                                                                                        textMonthFontSize: 16,
                                                                                        textDayHeaderFontSize: 16,

                                                                                    }}
                                                                                />
                                                                            ) : (
                                                                                    <View style={styles.datepIcon}>
                                                                                        <Text>{this.state.from_date}</Text>
                                                                                        <FontAwesome name='calendar' style={{ color: '#999', fontSize: 20 }} />
                                                                                    </View>
                                                                                )
                                                                        }


                                                                    </TouchableOpacity>
                                                                </View>
                                                                <View style={styles.formGroup}>
                                                                    <View style={styles.formLabel}>
                                                                        <Text style={{ color: '#101010', }}>TO:</Text>
                                                                    </View>
                                                                    <TouchableOpacity style={styles.inputField} onPress={() => this.toggleDatePickerTo()}>
                                                                        {
                                                                            this.state.IsShowDatePickerTo ? (
                                                                                <Calendar
                                                                                    onDayPress={(day) => this.onDaySelect(day, 'hotel', false)}
                                                                                    monthFormat={'MMM yyyy'}
                                                                                    hideArrows={false}
                                                                                    hideExtraDays={true}
                                                                                    disableMonthChange={false}
                                                                                    markedDates={{
                                                                                        [this.state.DOB]: { selected: true, selectedColor: '#3ab3ce' }
                                                                                    }}

                                                                                    theme={{
                                                                                        backgroundColor: '#ffffff',
                                                                                        calendarBackground: '#ffffff',
                                                                                        textSectionTitleColor: '#2d4150',
                                                                                        selectedDayBackgroundColor: '#2d4150',
                                                                                        selectedDayTextColor: '#ffffff',
                                                                                        todayTextColor: '#3ab3ce',
                                                                                        dayTextColor: '#2d4150',
                                                                                        weekTextColor: '#000',
                                                                                        textDisabledColor: '#ccc',
                                                                                        arrowColor: '#3ab3ce',
                                                                                        textDayFontSize: 14,
                                                                                        textMonthFontSize: 16,
                                                                                        textDayHeaderFontSize: 16,

                                                                                    }}
                                                                                />
                                                                            ) : (
                                                                                    <View style={styles.datepIcon}>
                                                                                        <Text>{this.state.to_date}</Text>
                                                                                        <FontAwesome name='calendar' style={{ color: '#999', fontSize: 20 }} />
                                                                                    </View>
                                                                                )
                                                                        }


                                                                    </TouchableOpacity>
                                                                </View>
                                                                <View style={styles.formGroup}>
                                                                    <View style={styles.formLabel}>
                                                                        <Text style={{ color: '#101010', }}>ANY OTHER REQUEST?</Text>
                                                                    </View>
                                                                    <View style={styles.inputField}>
                                                                        <TextInput style={[styles.formInput]} underlineColorAndroid='transparent' onChangeText={(text) => this.setState({ special_request: text })} value={this.state.special_request} keyboardType='default' />
                                                                    </View>
                                                                </View>
                                                            </View>
                                                        ) : (
                                                                <View>
                                                                    {
                                                                        this.state.BookingType == 'Appointment' ? (
                                                                            <View>
                                                                                <View style={styles.formGroup}>
                                                                                    <View style={styles.formLabel}>
                                                                                        <Text style={{ color: '#101010', }}>HOW MANY PEOPLE?</Text>
                                                                                    </View>
                                                                                    <View style={styles.inputField}>
                                                                                        <TextInput style={[styles.formInput]} underlineColorAndroid='transparent' placeholder='# of people' onChangeText={(text) => this.setState({ people_count: text })} value={this.state.people_count} keyboardType='numeric' />
                                                                                    </View>
                                                                                </View>
                                                                                <View style={styles.formGroup}>
                                                                                    <View style={styles.formLabel}>
                                                                                        <Text style={{ color: '#101010', }}>Date :</Text>
                                                                                    </View>
                                                                                    <TouchableOpacity style={styles.inputField} onPress={() => this.toggleDatePicker()}>
                                                                                        {
                                                                                            this.state.IsShowDatePicker ? (
                                                                                                <Calendar
                                                                                                    onDayPress={(day) => this.onDaySelect(day, 'food')}
                                                                                                    monthFormat={'MMM yyyy'}
                                                                                                    hideArrows={false}
                                                                                                    hideExtraDays={true}
                                                                                                    disableMonthChange={false}
                                                                                                    markedDates={{
                                                                                                        [this.state.DOB]: { selected: true, selectedColor: '#3ab3ce' }
                                                                                                    }}
                                                                                                    minDate={new Date()}

                                                                                                    theme={{
                                                                                                        backgroundColor: '#ffffff',
                                                                                                        calendarBackground: '#ffffff',
                                                                                                        textSectionTitleColor: '#2d4150',
                                                                                                        selectedDayBackgroundColor: '#2d4150',
                                                                                                        selectedDayTextColor: '#ffffff',
                                                                                                        todayTextColor: '#3ab3ce',
                                                                                                        dayTextColor: '#2d4150',
                                                                                                        weekTextColor: '#000',
                                                                                                        textDisabledColor: '#ccc',
                                                                                                        arrowColor: '#3ab3ce',
                                                                                                        textDayFontSize: 14,
                                                                                                        textMonthFontSize: 16,
                                                                                                        textDayHeaderFontSize: 16,

                                                                                                    }}
                                                                                                />
                                                                                            ) : (
                                                                                                    <View style={styles.datepIcon}>
                                                                                                        <Text>{this.state.booking_day}</Text>
                                                                                                        <FontAwesome name='calendar' style={{ color: '#999', fontSize: 20 }} />
                                                                                                    </View>
                                                                                                )
                                                                                        }


                                                                                    </TouchableOpacity>
                                                                                </View>
                                                                                <View style={styles.formGroup}>
                                                                                    <View style={styles.formLabel}>
                                                                                        <Text style={{ color: '#101010', }}>Time :</Text>
                                                                                    </View>
                                                                                    <View style={styles.inputField}>
                                                                                        <TouchableOpacity style={styles.inputField} onPress={() => this.toggleTimePicker()}>
                                                                                            {
                                                                                                this.state.IsShowTimePicker ? (
                                                                                                    <DateTimePicker
                                                                                                        isVisible={this.state.IsShowTimePicker}
                                                                                                        mode="time"
                                                                                                        onConfirm={this.timeSelected}
                                                                                                        onCancel={() => this.setState({ IsShowTimePicker: false })}
                                                                                                    />
                                                                                                ) : (
                                                                                                        <View style={styles.datepIcon}>
                                                                                                            <Text>{this.state.booking_time}</Text>
                                                                                                            <FontAwesome name='calendar' style={{ color: '#999', fontSize: 20 }} />
                                                                                                        </View>
                                                                                                    )
                                                                                            }
                                                                                        </TouchableOpacity>
                                                                                    </View>
                                                                                </View>
                                                                            </View>
                                                                        ) : (
                                                                                <View>
                                                                                    {
                                                                                        this.state.BookingType == 'Attraction' ? (
                                                                                            <View>
                                                                                                <View style={styles.formGroup}>
                                                                                                    <View style={styles.formLabel}>
                                                                                                        <Text style={{ color: '#101010', }}>HOW MANY PEOPLE?</Text>
                                                                                                    </View>
                                                                                                    <View style={styles.inputField}>
                                                                                                        <TextInput style={[styles.formInput]} underlineColorAndroid='transparent' placeholder='# of people' onChangeText={(text) => this.setState({ people_count: text })} value={this.state.people_count} keyboardType='numeric' />
                                                                                                    </View>
                                                                                                </View>
                                                                                                <View style={styles.formGroup}>
                                                                                                    <View style={styles.formLabel}>
                                                                                                        <Text style={{ color: '#101010', }}>Date :</Text>
                                                                                                    </View>
                                                                                                    <TouchableOpacity style={styles.inputField} onPress={() => this.toggleDatePicker()}>
                                                                                                        {
                                                                                                            this.state.IsShowDatePicker ? (
                                                                                                                <Calendar
                                                                                                                    onDayPress={(day) => this.onDaySelect(day, 'food')}
                                                                                                                    monthFormat={'MMM yyyy'}
                                                                                                                    hideArrows={false}
                                                                                                                    hideExtraDays={true}
                                                                                                                    disableMonthChange={false}
                                                                                                                    markedDates={{
                                                                                                                        [this.state.DOB]: { selected: true, selectedColor: '#3ab3ce' }
                                                                                                                    }}
                                                                                                                    minDate={new Date()}

                                                                                                                    theme={{
                                                                                                                        backgroundColor: '#ffffff',
                                                                                                                        calendarBackground: '#ffffff',
                                                                                                                        textSectionTitleColor: '#2d4150',
                                                                                                                        selectedDayBackgroundColor: '#2d4150',
                                                                                                                        selectedDayTextColor: '#ffffff',
                                                                                                                        todayTextColor: '#3ab3ce',
                                                                                                                        dayTextColor: '#2d4150',
                                                                                                                        weekTextColor: '#000',
                                                                                                                        textDisabledColor: '#ccc',
                                                                                                                        arrowColor: '#3ab3ce',
                                                                                                                        textDayFontSize: 14,
                                                                                                                        textMonthFontSize: 16,
                                                                                                                        textDayHeaderFontSize: 16,

                                                                                                                    }}
                                                                                                                />
                                                                                                            ) : (
                                                                                                                    <View style={styles.datepIcon}>
                                                                                                                        <Text>{this.state.booking_day}</Text>
                                                                                                                        <FontAwesome name='calendar' style={{ color: '#999', fontSize: 20 }} />
                                                                                                                    </View>
                                                                                                                )
                                                                                                        }


                                                                                                    </TouchableOpacity>
                                                                                                </View>
                                                                                                <View style={styles.formGroup}>
                                                                                                    <View style={styles.formLabel}>
                                                                                                        <Text style={{ color: '#101010', }}>Time :</Text>
                                                                                                    </View>
                                                                                                    <View style={styles.inputField}>
                                                                                                        <TouchableOpacity style={styles.inputField} onPress={() => this.toggleTimePicker()}>
                                                                                                            {
                                                                                                                this.state.IsShowTimePicker ? (
                                                                                                                    <DateTimePicker
                                                                                                                        isVisible={this.state.IsShowTimePicker}
                                                                                                                        mode="time"
                                                                                                                        onConfirm={this.timeSelected}
                                                                                                                        onCancel={() => this.setState({ IsShowTimePicker: false })}
                                                                                                                    />
                                                                                                                ) : (
                                                                                                                        <View style={styles.datepIcon}>
                                                                                                                            <Text>{this.state.booking_time}</Text>
                                                                                                                            <FontAwesome name='calendar' style={{ color: '#999', fontSize: 20 }} />
                                                                                                                        </View>
                                                                                                                    )
                                                                                                            }
                                                                                                        </TouchableOpacity>
                                                                                                    </View>
                                                                                                </View>
                                                                                            </View>
                                                                                        ) : (
                                                                                                null
                                                                                            )
                                                                                    }
                                                                                </View>
                                                                            )
                                                                    }
                                                                </View>
                                                            )
                                                    }
                                                </View>
                                            )
                                    }
                                </View>
                            )
                    }

                    <View style={{ marginTop: 15, marginBottom: 5 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#101010' }}>Terms & Conditions</Text>
                        {
                            this.state.BookingType == 'Book Table' ? (
                                <Text style={{ color: '#101010', }}>We will confirm your booking. If you don't show
                                up in time, the table will be released for other customers.
                        </Text>
                            ) : (
                                    <View>
                                        {
                                            this.state.BookingType == 'Reserve' ? (
                                                <Text style={{ color: '#101010', }}>This offer must be redeemed before {this.state.offer ? this.state.offer.expiry : ''}
                                                </Text>
                                            ) : (
                                                    <View>
                                                        {
                                                            this.state.BookingType == 'Put on hold' ? (
                                                                <View>
                                                                    <Text style={{ color: '#101010', }}>Must be picked up within 24 hours of the booking. Otherwise, the product will be released for other customers.
                                                                </Text>
                                                                </View>
                                                            ) : (
                                                                    <View>{
                                                                        this.state.BookingType == 'Appointment' ? (
                                                                            <View>
                                                                                <Text style={{ color: '#101010', }}>We will confirm your appointment. If for any reason you want to cancel this appointment, please cancel at least 6 hours earlier.This offer must be redeemed before the expiry date.
                                                                                </Text>
                                                                            </View>
                                                                        ) : (
                                                                                <View>
                                                                                    {
                                                                                        this.state.BookingType == 'Attraction' ? (
                                                                                            <View>
                                                                                                <Text style={{ color: '#101010', }}>We will confirm your reservation. If for any reason you want to cancel this reservation, please cancel at least 6 hours earlier.This offer must be redeemed before the expiry date.
                                                                                        </Text>
                                                                                            </View>
                                                                                        ) : (
                                                                                                null
                                                                                            )
                                                                                    }
                                                                                </View>
                                                                            )
                                                                    }</View>
                                                                )
                                                        }
                                                    </View>
                                                )
                                        }
                                    </View>
                                )
                        }
                    </View>
                    {/* <View style={{ marginBottom: 20 }}>
                        <TouchableOpacity style={styles.requestBtn} onPress={() => this.sendBooking()}>
                            <Text style={styles.blueTxt}> SEND BOOKING REQUEST </Text>
                        </TouchableOpacity>
                    </View> */}
                    <View style={{ marginBottom: 20 }}>
                        {
                            this.state.BookingType == "Book Table" ?
                                (
                                    <TouchableOpacity style={styles.requestBtn} onPress={() => this.sendBooking()}>
                                        <Text style={styles.blueTxt}> SEND BOOKING REQUEST </Text>
                                    </TouchableOpacity>
                                ) : (
                                    this.state.BookingType == "Reserve" ?
                                        (
                                            <TouchableOpacity style={styles.requestBtn} onPress={() => this.sendBooking()}>
                                                <Text style={styles.blueTxt}> SEND RESERVATION REQUEST </Text>
                                            </TouchableOpacity>
                                        ) :
                                        (
                                            this.state.BookingType == "Put on hold" ?
                                                (
                                                    <TouchableOpacity style={styles.requestBtn} onPress={() => this.sendBooking()}>
                                                        <Text style={styles.blueTxt}> AGREE & SEND REQUEST </Text>
                                                    </TouchableOpacity>
                                                ) : (
                                                    this.state.BookingType == "Appointment" ?
                                                        (
                                                            <TouchableOpacity style={styles.requestBtn} onPress={() => this.sendBooking()}>
                                                                <Text style={styles.blueTxt}> AGREE & SEND APPOINTMENT REQUEST </Text>
                                                            </TouchableOpacity>
                                                        ) : (
                                                            this.state.BookingType == "Attraction" ?
                                                                (
                                                                    <TouchableOpacity style={styles.requestBtn} onPress={() => this.sendBooking()}>
                                                                        <Text style={styles.blueTxt}> AGREE & SEND RESERVATION REQUEST </Text>
                                                                    </TouchableOpacity>
                                                                ) : (
                                                                    null
                                                                )
                                                        )
                                                )
                                        )
                                )
                        }

                    </View>
                </Content>
            </Container>
        );
    }
}
export default BookTable;
