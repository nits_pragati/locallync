import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Text, Picker, Dimensions, AsyncStorage, Alert, NetInfo } from "react-native";
import { Container, Header, Body, Content, Badge } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import ImageSlider from 'react-native-image-slider';
import Swiper from 'react-native-swiper';
import FSpinner from 'react-native-loading-spinner-overlay';
import api from '../../api';
import SQLite from "react-native-sqlite-storage";
import getDirections from 'react-native-google-maps-directions';
import * as firebase from 'firebase';
import Dialog from "react-native-dialog";
let offersNew = [];

const deviceWidth = Dimensions.get('window').width;
// const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 190;
const firebaseConfig = {
    apiKey: "AIzaSyBSW6dhfaSEP2pAHGAbWfuZ-kAPF7uA63w",
    authDomain: "locallync-859ff.firebaseapp.com",
    databaseURL: "https://locallync-859ff.firebaseio.com",
    projectId: "locallync-859ff",
    storageBucket: "locallync-859ff.appspot.com",
    messagingSenderId: "718735440621"
};

class Details extends Component {
    constructor(params) {
        super(params)

        this.state = {
            category_id: this.props.navigation.state.params ? this.props.navigation.state.params.Details.category_id : '',
            user_business_id: this.props.navigation.state.params ? this.props.navigation.state.params.Details.user_business_id : this.props.navigation.state.params.Details.id,
            details: this.props.navigation.state.params ? this.props.navigation.state.params.Details : '',
            outlets: [],
            offers: [],
            loader: false,
            business_name: '',
            image_url: '',
            image_slider: [],
            isFavouritebusiness:this.props.navigation.state.params.Details.favourite_status==1?true:false,
            business_id: '',
            displayName: '',
            offersTwo: [],
            offersAll: [],
            connectionInfo: '',
            latitude: 0,
            longitude: 0,
            chatRef: '',
            IsShowDialog: false,
            email: '',
            noOfUnread: 0,
            BookingType: '',
            selectedOutletId:'',
            business_logo:''
        };
console.log('outlet data:',this.props)
console.log('favourite:',this.state.isFavouritebusiness)
        if (this.props.navigation.state.params) {
            let user_business_id = this.props.navigation.state.params ? this.props.navigation.state.params.Details.user_business_id : this.props.navigation.state.params.Details.id;
            if (user_business_id) {
                const data = {
                    user_business_id: user_business_id
                };
                AsyncStorage.setItem('user_business_id', JSON.stringify(data), (err, result) => {
                });
            }
        }
        if (!firebase.apps.length) {
            firebase.initializeApp(firebaseConfig);
            this.state.chatRef = firebase.database().ref().child('messages');
        } else {
            this.state.chatRef = firebase.database().ref().child('messages');
        }
    }
    gettingCurrentUserLocation() {
        let getPosition = function (options) {
            return new Promise(function (resolve, reject) {
                navigator.geolocation.getCurrentPosition(resolve, reject, options);
            });
        }
        getPosition().then(position => {
            if (position) {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                });
            }
        })
            .catch((err) => {
                // Alert.alert('', 'Please turn on the location');
                console.log("location not getting");
            })
    }
    componentDidMount() {
        this.setState({
            loader: true
        })
        const that = this;
        NetInfo.getConnectionInfo().then((connectionInfo) => {
            this.setState({
                connectionInfo: connectionInfo.type
            })
            if (connectionInfo.type == "none") {
                if (SQLite) {
                    SQLite.openDatabase("localLync.db", "1.0", "Demo", -1, successCb, errorCb);
                }

                function successCb(db) {
                    db.transaction(function (tx) {
                        tx.executeSql('CREATE TABLE IF NOT EXISTS businessDetails (id integer primary key, outlets text, offers text, business_name text, business_images text, user_business_id text, category_id text)');
                        tx.executeSql('SELECT * FROM businessDetails where user_business_id=' + that.state.user_business_id, [], function (tx, results) {
                            
                           if (results.rows.length != 0) {

                                let outlets = [];
                                let offers = [];
                                let imageUrl;
                                let business_name = '';
                                let image_slider = '';
                                var len = results.rows.length, i;
                                for (i = 0; i < len; i++) {

                                    if (results.rows.item(i).user_business_id == that.state.user_business_id && results.rows.item(i).category_id == that.state.category_id) {
                                        // let data = { category_id: results.rows.item(i).category_id, business_id: results.rows.item(i).business_id, business_images: JSON.parse(results.rows.item(i).business_images), business_name: results.rows.item(i).business_name, outlets: JSON.parse(results.rows.item(i).outlets)};
                                        // toPushData.push(data);
                                        outlets = JSON.parse(results.rows.item(i).outlets);
                                        offers = JSON.parse(results.rows.item(i).offers);
                                        console.log("offers in tx:", offers);
                                        business_name = results.rows.item(i).business_name;
                                        image_slider = results.rows.item(i).image_url + results.rows.item(i).business_logo;
                                        break;
                                    }
                                }

                                that.setState({
                                    outlets: outlets,
                                    offers: offers,
                                    offersTwo: offers,
                                    business_name: business_name,
                                    loader: false,
                                    image_slider: image_slider,
                                    
                                });
                                debugger
                            }
                            else {
                                that.setState({
                                    loader: false
                                })
                            }
                        }, null);

                    });
                }
                function errorCb(err) {
                    console.log(err);
                }
            }
            else {
                AsyncStorage.getItem('UserDetails', (err, result) => {
                    let data = JSON.parse(result);
                    let user_id = data.details.id + '';
                    debugger
                    api.post('reviews/reviewunreadcount.json', { user_id: user_id, business_id: this.state.user_business_id ? this.state.user_business_id : this.props.navigation.state.params.Details.id }).then((unReadCount) => {
                        if (unReadCount.ack == 1) {
                            this.setState({
                                noOfUnread: unReadCount.unread
                            })
                        }
                        debugger
                    })
                    debugger
                    api.post('UserBusinessess/BusinessOfferDetails.json', { user_business_id: this.state.user_business_id ? this.state.user_business_id : this.props.navigation.state.params.Details.id, category_id: this.state.category_id ? this.state.category_id : this.props.navigation.state.params.Details.user_business_categories ? this.props.navigation.state.params.Details.user_business_categories[0].category_id : this.state.category_id, user_id: user_id }).then(res => {
                      
                        console.log("res in detailsssssssssssssssssssssssssssssssssss :", res);
                        debugger
                        // this.state.details;
                        if (res.ack == 1) {
                            
                            api.get('categories/list_category.json').then((reslist_category) => {
                               
                                if (reslist_category.ack == 1) {
                                   
                                    const cat_id = this.state.category_id ? this.state.category_id : this.props.navigation.state.params.Details.user_business_categories ? this.props.navigation.state.params.Details.user_business_categories[0].category_id : this.state.category_id;
                                    reslist_category.details.map((item) => {
                                        if (item.id == cat_id) {
                                            const bookingType = item.name.includes('Food') ? 'Book Table' : item.name.includes('Shopping') ? 'Put on hold' : item.name.includes('Hotel') ? 'Reserve' : item.name.includes('Health') || item.name.includes('Beauty') || item.name.includes('Service') || item.name.includes('Automobile') ? 'Appointment' : item.name.includes('Attraction') ? 'Attraction' : '';
                                            this.setState({
                                                BookingType: bookingType
                                            });
                                        }
                                    })

                                }
                            })

                            let image_slider = [];
                            if (res.business_images && res.business_images.length && res.business_images.length > 0) {
                                res.business_images.map((item) => {
                                    image_slider.push(res.image_url + item.image)
                                })

                            }
                            this.setState({
                                outlets: res.Outlets,
                                offers: res.offers ? res.offers : [],
                                offersTwo: res.offers ? res.offers : [],
                                business_name: res.business_name,
                                image_slider: image_slider,
                                business_logo:res.image_url+res.business_logo
                            });
                            if (res.Outlets && res.Outlets.length && res.Outlets.length > 0) {
                                debugger
                                this.setState({
                                    // displayName: res.Outlets[0].id
                                    displayName: res.offers.user_offer_outlets[0].outlet_id
                                })
                            }
                            if (res.favourite_status ==1) {
                                this.setState({
                                    isFavouritebusiness: true
                                })
                            }
                        }
                        this.setState({
                            loader: false
                        })
                    }).catch((err) => {
                        this.setState({ loader: false });
                        console.log(err);
                    })
                })
            }
        });

        this.gettingCurrentUserLocation();
    }

    favourite() {
        if (this.state.connectionInfo != "none") {
            AsyncStorage.getItem('UserDetails', (err, details) => {
                let UserDetails = JSON.parse(details);
                let user_id = UserDetails.details.id + '';
                let business_id = this.state.details.user_business_id;
                this.setState({
                    loader: true
                });
                api.post('UserBusinessess/addFavourite.json', { user_id: user_id, business_id: business_id }).then(res => {
                    if (res.ack == 1) {
                        this.setState({
                            isFavouritebusiness: !this.state.isFavouritebusiness,
                            loader: false
                        });
                        Alert.alert('', res.msg);
                    }
                    if (res.ack == 0) {
                        this.setState({
                            isFavouritebusiness: !this.state.isFavouritebusiness,
                            loader: false
                        });
                        Alert.alert('', res.msg);
                    }
                }).catch((err) => {
                    this.setState({ loader: false });
                    console.log(err);
                })
            });
        }
        else {
            Alert.alert("", "Please connect to internet to be able to make it as favourite.");
        }

    }

    setSelectedValue(value, res) {

        if (value == 0) {
            this.setState({
                offers: this.state.offersTwo,

            });
        } else if (value != 0) {
            offersNew = [];
            this.setState({
                displayName: value
            });

            res.map((result) => {
              //  if (result.outlet_name == value) {
                    if (result.id == value) {
                    if (this.state.offersTwo.length != 0) {
                        this.state.offersTwo.map((res) => {
                            if (res.user_offer_outlets.length != 0) {
                                res.user_offer_outlets.map((resultOne) => {
                                    if (resultOne.outlet_id == result.id) {
                                        offersNew.push(res);
                                        if (offersNew.length != 0) {
                                            this.setState({
                                                offers: offersNew
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    }
                }else{
                    this.setState({
                        offers: offersNew
                    });
                }
            });
            debugger
        }
    }

    startGoogleNavigation() {
        if (this.state.displayName && this.state.outlets && this.state.outlets.length > 0) {
            let latitude;
            let longitude;
            this.state.outlets.map((item) => {
                if (item.id == this.state.displayName) {
                    latitude = Number(item.latitude);
                    longitude = Number(item.longitude);
                }
            })
            if (latitude && longitude) {
                // const url="https://www.google.com/maps/dir/?api=1&origin="+this.state.latitude+","+this.state.longitude+"&destination="+latitude+","+this.state.longitude;
                // debugger;
                const data = {
                    source: {
                        latitude: this.state.latitude,
                        longitude: this.state.longitude
                    },
                    destination: {
                        latitude: latitude,
                        longitude: longitude
                    },
                    params: [{
                        key: "travelmode",
                        value: "driving" // may be "walking", "bicycling" or "transit" as well
                    },
                    {
                        key: "dir_action",
                        value: "navigate" // this instantly initializes navigation using the given travel mode 
                    }
                    ]
                };

                getDirections(data)
            }
        }
        else {
            Alert.alert("", "No outlets found.")
        }
    }

    startChat() {
        if (this.state.offersTwo.length > 0) {
            const toCheckUserId = this.state.offersTwo[0].user_id;

            this.state.chatRef.orderByChild('userId').equalTo(toCheckUserId).once('value').then((snapshot) => {
                if (snapshot && snapshot.val()) {

                    const key = Object.keys(snapshot.val())[0];
                    const ref = this.state.chatRef.child(key);
                    let toUpdate = snapshot.val();
                    if (toUpdate[key].IsOnline == true) {
                        this.props.navigation.navigate('Chat', {
                            businessId: toCheckUserId
                        })
                    } else {
                        Alert.alert("", "This user is not online now.")
                    }


                } else {
                    Alert.alert("", "This user is not online now.")
                }
            })
        }
        else if (this.state.outlets.length && this.state.outlets.length > 0) {
            const toCheckUserId = this.state.outlets[0].user_business_id;

            this.state.chatRef.orderByChild('userId').equalTo(toCheckUserId).once('value').then((snapshot) => {
                if (snapshot && snapshot.val()) {

                    const key = Object.keys(snapshot.val())[0];
                    const ref = this.state.chatRef.child(key);
                    let toUpdate = snapshot.val();
                    if (toUpdate[key].IsOnline == true) {
                        this.props.navigation.navigate('Chat', {
                            businessId: toCheckUserId
                        })
                    } else {
                        Alert.alert("", "This user is not online now.")
                    }


                } else {
                    Alert.alert("", "This user is not online now.")
                }
            })
        }
    }


    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />
                
                <Header searchBar style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={[commonStyles.headerBackBtn, {width:40, justifyContent: 'center' }]} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={[styleSelf.tac,]}>
                        <Text style={commonStyles.headerMiddleText} numberOfLines={1}> {this.state.business_name} </Text>
                    </Body>
                    <TouchableOpacity style={[commonStyles.headerRightBtn, {marginRight:30,}]}>

                    </TouchableOpacity>
                    
                   

                </Header>

                <Content>
                    <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                    <View>
                        {
                            this.state.image_slider && this.state.image_slider.length > 0 ? (
                                <Swiper
                                    style={{ height: 190 }}
                                    showsButtons={true}
                                    dotColor='#dadada'
                                    activeDotColor='#3ab3ce'
                                >

                                    {
                                        this.state.image_slider.map((item, key) => {
                                            return (
                                                <View key={key} style={styles.slide}>
                                                    <Image source={{ uri: item }} style={{ height: 190, width: deviceWidth }} />

                                                </View>
                                            )
                                        })
                                    }

                                </Swiper>
                            ) : (
                                    <Swiper
                                        style={{ height: 190 }}
                                        showsButtons={false}
                                        autoplay={true}
                                        dotColor='#dadada'
                                        autoplayTimeout={10}
                                        activeDotColor='#3ab3ce'
                                    >

                                        <View style={styles.slide}>
                                        <Image source={{ uri:this.state.business_logo }} style={{ height: 190, width: deviceWidth }} />
                                        </View>

                                        {/* <View style={styles.slide}>
                                            <Image source={require('../../../img/icons/food2.jpg')} style={{ height: 190, width: deviceWidth }} />
                                        </View>
                                        <View style={styles.slide}>
                                            <Image source={require('../../../img/icons/food3.jpg')} style={{ height: 190, width: deviceWidth }} />
                                        </View> */}


                                    </Swiper>
                                )
                        }
                    </View>
                    <View>

                        <Dialog.Container visible={this.state.IsShowDialog}>
                            <Dialog.Title>Share offer details</Dialog.Title>
                            <Dialog.Description>
                                Please enter the email of your friend.
                </Dialog.Description>
                            <Dialog.Input label="Enter email" value={this.state.email} onValueChange={(value) => this.setState({ email: value })}>

                            </Dialog.Input>
                            <Dialog.Button label="Cancel" onPress={() => this.setState({ IsShowDialog: false })} />
                            <Dialog.Button label="Send offer Details" onPress={() => this.setState({ IsShowDialog: false })} />
                        </Dialog.Container>
                    </View>

                    <View style={styles.iconHolder} >
                        <View style={styles.leftSide} >

                            {
                                <TouchableOpacity style={[styles.rightIcon]} onPress={() => this.favourite()}>
                                    {
                                        this.state.isFavouritebusiness ? (<Entypo name='heart' style={[styles.rightIcon, {color:'red'}]} />) : (<Entypo name='heart-outlined' style={styles.rightIcon} />)
                                    }
                                </TouchableOpacity>
                            }
                            <TouchableOpacity style={[styles.rightIcon]} onPress={() => this.startChat()}>
                                <Feather name='message-circle' style={[styles.rightIcon, { color: '#73F12C' }]} />
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.rightIcon} onPress={() => this.props.navigation.navigate('OfferShare', { businessName: this.state.business_name, image_slider: this.state.image_slider && this.state.image_slider.length > 0 ? this.state.image_slider[0] : '' })}>
                                <Feather name='upload' style={styles.rightIcon} />
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.rightIcon} onPress={() => this.startGoogleNavigation()}>
                                <FontAwesome name='map-marker' style={[styles.rightIcon, { color: '#FB9507' }]} />
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.rightIcon} onPress={() => this.props.navigation.navigate('FeedbackList', { business_id: this.state.user_business_id ? this.state.user_business_id : this.props.navigation.state.params.Details.id, businessName: this.state.business_name, image_slider: this.state.image_slider && this.state.image_slider.length > 0 ? this.state.image_slider[0] : '', details:this.state.details })}>
                                <FontAwesome name='envelope' style={[styles.rightIconTag, { color: '#73F12C' }]} />
                                {this.state.noOfUnread == 0?
                                    null
                                :
                                < Badge danger style={styles.roundBadge}>
                                 <Text style={styles.badgeText}>{this.state.noOfUnread}</Text>
                                </Badge>
                                }
                                
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.pickerMainWarp}>
                            <Text style={{fontSize:16, fontWeight:'600', marginBottom:10, color:'#333'}}>Select Outlets</Text>
                        <View style={styles.pickerWarp}>
                            {
                                this.state.outlets.length != 0 ? (
                                    <Picker
                                        style={styles.picker}
                                        selectedValue={this.state.displayName}
                                        onValueChange={(value) =>
                                            this.setSelectedValue(value, this.state.outlets)}
                                        // onValueChange={(itemValue, itemIndex) => this.setState({ language: itemValue })}
                                        itemStyle={{ fontSize: 12, color: '#000' }}
                                    >
                                        <Picker.Item label="Select offer outlets" value="0" />
                                        {
                                            this.state.outlets.map((data, key) => {
                                                return (<Picker.Item label={data.outlet_name + " " + data.distance + " km"} value={data.id} key={data.id} />)
                                            })
                                        }
                                    </Picker>
                                ) : (
                                        <Picker
                                            style={styles.picker}
                                            onValueChange={(itemValue, itemIndex) => this.setState({ language: itemValue })}
                                            itemStyle={{ fontSize: 12, color: '#000' }}
                                        >
                                            <Picker.Item label="Select offer outlets" value="0" />
                                        </Picker>
                                    )
                            }
                        </View>
                    </View>

                    <View>
                        {
                            this.state.offers.length && this.state.offers.length > 0 ? (
                                <View>{
                                    this.state.offers.map((outletsData, key) => {
                                        return (
                                            <View style={styles.detailsWarp} key={key}>
                                                <View style={styles.detailsLHS}>
                                                    <View style={styles.parsentageWarp}>
                                                        {/* <Image source={require('../../../img/icons/prsnt2.png')} style={styles.parsentImg} /> */}
                                                        <Text style={[styles.parsentTxt, {marginBottom:10}]}> {outletsData.offer_name} </Text>
                                                    </View>
                                                    {/* <Text style={styles.getDiscount}> Get <Text style={styles.discountParcentage}>{outletsData.discount_amount}%</Text> discount for connecting with us </Text> */}
                                                    <Text style={styles.font11}><Text style={styles.colorBlock}> {outletsData.discount}</Text></Text>
                                                    <Text style={styles.font11}><Text style={styles.colorBlock}> Description:</Text>  {outletsData.description} </Text>
                                                    <Text style={styles.font11}><Text style={styles.colorBlock}> Frequency: </Text> {outletsData.frequency_amount}  </Text>
                                                    <Text style={styles.font11}><Text style={styles.colorBlock}> No of use left: </Text> {outletsData.use_left ? outletsData.use_left : 'N/A'}  </Text>
                                                    <Text style={styles.font11}><Text style={styles.colorBlock}> Valid Until:</Text>  {outletsData.expiry} </Text>
                                                    <Text>
                                                        {
                                                            outletsData && outletsData.token && outletsData.token !== null ?
                                                                <Text style={styles.font11}>
                                                                    <Text style={styles.colorBlock}> Token:</Text>  {outletsData.token}
                                                                </Text>
                                                                :
                                                                ''
                                                        }
                                                    </Text>

                                                </View>
                                                {
                                                    outletsData.is_valid == 1 || !(outletsData.valid_message)? (
                                                        <View style={styles.redeemWarp}>
                                                            <TouchableOpacity style={styles.signInBtn} onPress={() => this.props.navigation.navigate('Redeem', { outletsData: outletsData, displayName:this.state.displayName })}>
                                                                <Text style={styles.signInBtnTxt}> Redeem </Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    ) : (
                                                            <View style={styles.redeemWarp}>
                                                                <TouchableOpacity style={styles.signInBtnGreyed} >
                                                                    <Text style={styles.signInBtnTxt}>{outletsData.valid_message} </Text>

                                                                </TouchableOpacity>
                                                            </View>
                                                        )
                                                }
                                                {/* {
                                                    outletsData.is_valid == 1 ? (
                                                        <View style={styles.redeemWarp}>
                                                            <TouchableOpacity style={styles.signInBtn} onPress={() => this.props.navigation.navigate('BookTable', { business_name: this.state.business_name, business_id: this.state.user_business_id, user_business_id: this.state.user_business_id, details: this.state.details, BookingType: this.state.BookingType, offer: outletsData })}>
                                                                <Text style={styles.signInBtnTxt}> {this.state.BookingType} </Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    ) : null
                                                } */}
                                            </View>
                                        )
                                    })
                                }</View>
                            ) : (
                                    <View>
                                        <Text style={{ fontSize: 15, padding: 15 }}>No offers found.</Text>
                                    </View>
                                )

                        }
                    </View>
                </Content>
            </Container>
        );
    }
}
export default Details;
