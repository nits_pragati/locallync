import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity,TextInput, Text, Picker, Dimensions, AsyncStorage, Alert, NetInfo } from "react-native";
import { Container, Header, Body, Content } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import ImageSlider from 'react-native-image-slider';
import Swiper from 'react-native-swiper';
import FSpinner from 'react-native-loading-spinner-overlay';
import api from '../../api';
import SQLite from "react-native-sqlite-storage";
import getDirections from 'react-native-google-maps-directions';
import Dialog from "react-native-dialog";
import Moment from 'moment';


const deviceWidth = Dimensions.get('window').width;
// const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 190;


class FeedbackList extends Component {
    constructor(params) {
       
        super(params)
        this.state = {
           feedbackList:[],
           business_id: this.props.navigation.state.params ? this.props.navigation.state.params.business_id:'',
           businessName: this.props.navigation.state.params ? this.props.navigation.state.params.businessName : '',
           image_slider: this.props.navigation.state.params && this.props.navigation.state.params.image_slider ? this.props.navigation.state.params.image_slider : '',
           user_id:'',
           IsSubmittedFeedback:false,
           comment:'',
           hidden_reply:false,
           hidden_reply_div_view:false,
           businessImg:this.props.navigation.state.params.details.business_logo,
           img_url:''
        };

       console.log("feedbacjList:",this.props.navigation.state.params)
    }


    componentDidMount()
    {
        if(this.state.business_id)
        {
            AsyncStorage.getItem("UserDetails", (err, result)=>{
                const data = JSON.parse(result);
                const user_id = data.details.id + '';
                this.getFeedbackList(user_id)
            })
        }
    }

    getFeedbackList(user_id)
    {
        debugger
        api.post('reviews/reviewreadall.json', {user_id:user_id, business_id:this.state.business_id}).then((apiRespone)=>{
            debugger      
            if(apiRespone.ack==1)
                   {
                       debugger
                      
                       let reviewList=[];
                       apiRespone.reviewdata.map((item)=>{
                          
                           item.ReviewFeedback.map((item1)=>{
                               
                               reviewList.push(item1)
                           })
                       })
                       this.setState({
                           feedbackList: reviewList,
                           user_id: user_id,
                           img_url:apiRespone.img_url
                       })
                   }
                })
    }

    

   
deleteFeedBack(item)
{
    debugger;
    api.post('reviews/reviewdelete.json', {
            review_id: item.review_id
        }).then((res) => {
        debugger;
        if(res.ack==1)
        {
           this.getFeedbackList(this.state.user_id);
        }
    }).catch((err)=>{
        console.log(err);
        debugger;
    })
}


    replyFeedBack(item) {
        this.setState({
            loader: false,
            hidden_reply:true,
            eachBox:item.id,
            hidden_reply_div_view:true
        });
        debugger
        
    }
    closeReplyFeedBack(item) {
        this.setState({
            loader: false,
            hidden_reply:false,
            eachBox:item.id,
            hidden_reply_div_view:false
        });
        
    }

    submitFeedback(item) {

        if (this.state.comment) {
    
            this.setState({
                loader: true
            });
            AsyncStorage.getItem("UserDetails").then((userDet) => {
                debugger
                if (userDet) {
                    const parsedUserDet = JSON.parse(userDet);
               
                        const feedbackData = {
                            // comment: this.state.comment,
                            // user_id: parsedUserDet.details.id
                       
                        business_id: this.state.business_id,
                        comment: this.state.comment,
                        user_id:parsedUserDet.details.id
                        };
                        debugger
                    api.post('UserBusinessess/addfeedback.json', feedbackData).then((response) => {
                        if(response.ack==1){
                            this.setState({
                                loader: false,
                                hidden_reply:false,
                                
                            });
                        }                        
                        alert('Thank you for your reply. ')
                    }).catch((err) => {
                        this.setState({
                            loader: false
                        });
                       
                    })
                           
                }
            })
    
    
        } else {
            Alert.alert('', "Please give us your feedback.");
        }
    }
    

    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header searchBar style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={{ width: 78, justifyContent: 'center' }} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText} numberOfLines={1}> {this.state.businessName} </Text>
                    </Body>
                    <TouchableOpacity style={[commonStyles.headerRightBtn, {marginRight:30}]}>
                    </TouchableOpacity>

                 

                </Header>

                <Content style={{ backgroundColor: '#ffffff'}}>
                    <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                     {
                         this.state.feedbackList.length>0?(
                            <View>
                               {this.state.feedbackList.map((item ,key)=>{
                                      return(
                                          <View key={key}>
                                               <View style={styles.line}></View>
                    <View style={styles.lineHolder}>
									
                        <Text style={styles.dateTxt}>{Moment(new Date(item.fbk_date)).format('DD-MM-YYYY')}</Text>
                    </View>
                    <View style={styles.contentWrap}>
                        <View style={styles.lftImage}>
                           {
                            //   this.state.image_slider?(
                                this.state.businessImg?(
                                  <Image source={{uri:this.state.img_url+this.state.businessImg}} style={{ height: 50, width: 90 }} />
                              ):(
                                   <Image source={require('../../../img/icons/food2.jpg')} style={{ height: 50, width: 90 }} />
                              )
                           }
                        </View>
                        <View style={styles.rightContent}>
                            <Text style={styles.bttext}> {this.state.businessName} </Text>
                        </View>
                    </View>
                    <View style={styles.borderWrap}>
                            <Text style={styles.gryTxt}>{item.message}</Text>
                    </View>
                    {this.state.hidden_reply_div_view == false ?
                        <View style={styles.btnWarp}>
                        <TouchableOpacity style={styles.deltBtn} onPress={() => this.deleteFeedBack(item)}>
                            <Text style={styles.delBtnTxt}>DELETE</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.deltBtn} onPress={() => this.replyFeedBack(item)}>
                            <Text style={styles.delBtnTxt}>REPLY</Text>
                        </TouchableOpacity>
                    </View> :
                    null
                    }
                   
                    { this.state.hidden_reply == true && this.state.eachBox=== (item.id) ? (
                        <View>
                        <View style={styles.borderWrap}>
                            <TextInput underlineColorAndroid="transparent" value={this.state.comment} onChangeText={(text) => this.setState({ comment: text })}/>
                        </View>
                        <View style={styles.btnWarp}>
                            <TouchableOpacity style={styles.deltBtn} onPress={() => this.submitFeedback(item)}>
                                <Text style={styles.delBtnTxt}>SEND</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.deltBtn} onPress={() => this.closeReplyFeedBack(item)}>
                            <Text style={styles.delBtnTxt}>CANCEL</Text>
                        </TouchableOpacity>
                        </View>
                        </View>

                    ) :null

                    }
                    
                   
                                        </View>
                                      )
                               })}
                            </View>
                         ):(
                             <View>
                            <Text style={{fontSize:20, fontWeight:'bold', padding:10, textAlign:'center'}}>No records found.</Text>
                             </View>
                         )
                     }
{/*                     
                    <View style={styles.line}></View>
                    <View style={styles.lineHolder}>
                        <Text style={styles.lineTxt}>Sep 10</Text>
                    </View>
                    <View style={styles.contentWrap}>
                        <View style={styles.lftImage}>
                            <Image source={require('../../../img/icons/food2.jpg')} style={{ height: 50, width: 90 }} />
                        </View>
                        <View style={styles.rightContent}>
                            <Text> Red Lobster </Text>
                        </View>
                    </View>
                    <View style={styles.borderWrap}>
                            <Text style={styles.gryTxt}>Lorem ipsum dolor sit amet conseqr td omplo tosde...</Text>
                    </View>
                   <View style={styles.deleteWarp}>
                        <TouchableOpacity style={styles.delBtn} onPress={() => this.props.navigation.navigate('Redeem', { outletsData: outletsData})}>
                            <Text style={styles.delBtnTxt}>DELETE</Text>
                        </TouchableOpacity>
                    </View> */}
                   
                </Content>
            </Container>
        );
    }
}
export default FeedbackList;
