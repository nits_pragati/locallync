import React, { Component } from 'react';
import { Image, View, StatusBar, Alert, TouchableOpacity, TextInput, ScrollView, Text, AsyncStorage } from 'react-native';
import { Footer, FooterTab, Container, Header, Button, Body, Title, ActionSheet } from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import FSpinner from 'react-native-loading-spinner-overlay';
import styles from './styles';
import * as firebase from 'firebase';
import commonStyles from "../../assets/styles";
import api from '../../api';
import ImagePicker from 'react-native-image-crop-picker';
const firebaseConfig = {
    apiKey: "AIzaSyBSW6dhfaSEP2pAHGAbWfuZ-kAPF7uA63w",
    authDomain: "locallync-859ff.firebaseapp.com",
    databaseURL: "https://locallync-859ff.firebaseio.com",
    projectId: "locallync-859ff",
    storageBucket: "locallync-859ff.appspot.com",
    messagingSenderId: "718735440621"
};
var BUTTONS = [{
        text: 'Camera',
        icon: "ios-camera",
        iconColor: "#2c8ef4"
    },
    {
        text: 'Gallery',
        icon: "ios-images",
        iconColor: "#f42ced"
    }
];


class Chat extends Component {
    constructor(props) {
        super(props);
     
        this.state = {
            businessId: this.props.navigation.state.params.businessId ? this.props.navigation.state.params.businessId:'',
            customerId: '',
            chatRoomId: '',
            chatList:[],
            businessDetails:'',
            businessImage:'',
            user:'',
            userImage:''
        }
        if (!firebase.apps.length) {
            firebase.initializeApp(firebaseConfig);
            this.state.chatRef = firebase.database().ref().child('chatMessages');
          } else {
            this.state.chatRef = firebase.database().ref().child('chatMessages');
          }
         
        


        this.state.chatRef.on('child_added', (snapshot) => {
            const snapShotVal = snapshot.val();
            if ( snapShotVal.chatRoomId == this.state.chatRoomId ){
                let chatList = this.state.chatList;
                const item = snapShotVal;
                chatList.push(item);
                this.setState({ typeMessage: '', chatList: chatList });
            }
            setTimeout(() => {
                if(this.refs && this.refs.ScrollViewStart){
                    this.refs.ScrollViewStart.scrollToEnd(true);
                } 
            }, 400);

        })




    }
    componentDidMount() {

        setTimeout(() => {
            if(this.refs && this.refs.ScrollViewStart){
                this.refs.ScrollViewStart.scrollToEnd();
            }
        }, 50);

        AsyncStorage.getItem("UserDetails", (err, result) => {
           
              let data = JSON.parse(result);
              let user_id = data.details.id + '';
              this.setState({
                  customerId:user_id,
                  user:data.details
              });
              if (data.details.pimg)
              {
                  this.setState({
                      userImage: data.details.image_url + data.details.pimg
                  })
              }
               if (user_id && this.state.businessId) {
                   const chatRoomId = user_id + "_" + this.state.businessId;
                   this.setState({
                       chatRoomId: chatRoomId
                   });

               }
               api.post('users/details.json', {user_id:this.state.businessId}).then((businessDetails)=>{
                  console.warn(businessDetails)
                  if (businessDetails.ack==1)
                  {
                    //   if (businessDetails.details.pimg)
                    //   {
                    //    this.setState({
                    //        businessDetails: businessDetails.details,
                    //        businessImage: businessDetails.details.image_url + businessDetails.details.pimg

                    //    })
                    //   }
                      if (businessDetails.business_details.business_logo)
                      {
                       this.setState({
                           businessDetails: businessDetails.details,
                           businessImage: businessDetails.business_details.image_url + businessDetails.business_details.business_logo

                       })
                      }
                      else
                      {
                          this.setState({
                              businessDetails: businessDetails.details

                          })
                      }
                       
                  }
               })
               let chatRoomId = user_id + '_' + this.state.businessId;
               this.state.chatRef.orderByChild('chatRoomId').equalTo(chatRoomId).once('value').then((snapshot) => {
                   if (snapshot.val()) {
                       var listMesage = [];
                       for (let key in snapshot.val()) {
                           listMesage.push(snapshot.val()[key]);
                       }
                       this.setState({
                           chatList: listMesage
                       });
                   }
               }).catch((Err) => {})
        })

       
    }
    sendMessage() {
       
     
        if (this.state.typeMessage && this.state.typeMessage.trim()) {
            api.post('users/details.json', {user_id:this.state.businessId}).then((res)=>{
                if(res.ack==1)
                {
                    console.warn("customer_id:",this.state.customerId)
                    api.post('UserBusinessess/testpush.json', {device_token:res.details.device_token_id, title:"New message recieved from "+this.state.user.first_name, businessuser_id:this.state.businessId,customer_id:this.state.customerId}).then((res)=>{
                    }).catch((err)=>{
                    })
                }
            })
            this.state.chatRef.push({
                "userId": this.state.customerId,
                "businessId": this.state.businessId,
                "chatRoomId": this.state.chatRoomId,
                "IsCustomerSender": true,
                "Message": this.state.typeMessage,
                "userName":this.state.user.first_name,
                "userImage":this.state.userImage,
                "businessImage":this.state.businessImage,
                "date": new Date().toLocaleDateString(),
                "Image":""
            });
        }
        else {
            Alert.alert('', 'Plesae type message to send.');
        }
        
    }

    uploadPhoto() {
        ActionSheet.show(
            {
                options: BUTTONS,
            },
            (buttonIndex) => {
                this.setState({ clicked: BUTTONS[buttonIndex] });
                this.fileUploadType(buttonIndex);
            },
        )

    }

    fileUploadType(buttonIndex) {
        if (buttonIndex == 0) {
            this.captureFile();
        }
        if (buttonIndex == 1) {
            this.attachFile();
        }
    }


    captureFile(data) {
        this.setState({ visible: true });
        ImagePicker.openCamera({
            width: 400,
            height: 300,
            cropping: true,
             includeBase64: true,
            freeStyleCropEnabled: true,
        }).then((response) => {
              debugger;
              let pimg = 'data:' + response.mime + ';base64,' + response.data;
              this.setState({
                  visible: true
              });
              let uri;
              if (!response.path) {
                  uri = response.uri;
              } else {
                  uri = response.path;
              }
              const file = {
                  uri,
                  name: `${Math.floor((Math.random() * 100000000) + 1)}_.png`,
                  type: response.mime || 'image/png',
              };
              this.setState({
                  visible: false
              });
              let data = new FormData();
              data.append('image', file);
              api.post('users/chatimageupload.json', {
                  pimg: pimg
              }).then(ImageRes => {
                  this.setState({
                      visible: false
                  })
                  debugger;
                  if (ImageRes.ack == 1) {
                      this.state.chatRef.push({
                          "userId": this.state.customerId,
                          "businessId": this.state.businessId,
                          "chatRoomId": this.state.chatRoomId,
                          "IsCustomerSender": true,
                          "Message": "",
                          "userName": this.state.user.first_name,
                          "userImage": this.state.userImage,
                          "businessImage": this.state.businessImage,
                          "date": new Date().toLocaleDateString(),
                          "MessageImage": ImageRes.imageurl
                      });
                  }

              }).catch((err) => {
                  debugger;
                  this.setState({
                      visible: false
                  });
              })
        }).catch((err) => {
            this.setState({ visible: false });
        });
    }

    attachFile() {
       
        ImagePicker.openPicker({
            width: 400,
            height: 300,
            cropping: true,
             includeBase64: true,
                 freeStyleCropEnabled: true,
        }).then((response) => {
            debugger;
            let pimg = 'data:' + response.mime + ';base64,' + response.data;
            this.setState({
                visible: true
            });
            let uri;
            if (!response.path) {
                uri = response.uri;
            } else {
                uri = response.path;
            }
            const file = {
                uri,
                name: `${Math.floor((Math.random() * 100000000) + 1)}_.png`,
                type: response.mime || 'image/png',
            };
            this.setState({
                visible: false
            });
            let data = new FormData();
            data.append('image', file);
            api.post('users/chatimageupload.json', {
                pimg: pimg
            }).then(ImageRes => {
                this.setState({
                    visible:false
                })
                debugger;
                if (ImageRes.ack == 1) {
                 this.state.chatRef.push({
                     "userId": this.state.customerId,
                     "businessId": this.state.businessId,
                     "chatRoomId": this.state.chatRoomId,
                     "IsCustomerSender": true,
                     "Message": "",
                     "userName": this.state.user.first_name,
                     "userImage": this.state.userImage,
                     "businessImage": this.state.businessImage,
                     "date": new Date(),
                     "MessageImage": ImageRes.imageurl
                 });
                }

            }).catch((err) => {
                debugger;
                this.setState({
                    visible: false
                });
            })
        }).catch((err) => {
            this.setState({ visible: false });
        });
    }



    render() {
        return (

            <Container >
                <FSpinner visible={this.state.visible} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                <StatusBar
                    backgroundColor="#81cdc7"
                />
                {/* <Header  style={[defaultStyle.headerWarp]} noShadow androidStatusBarColor="#81cdc7">
                    <TouchableOpacity transparent activeOpacity={0.5} style={{ width: 40, justifyContent: 'center' }}>
                        <Ionicons name="ios-arrow-back" style={styles.headIcon2} />
                    </TouchableOpacity>
                    <Body style={styles.headBody}>
                        <Title style={{ fontSize: 14 }}>Start live chat</Title>
                    </Body>
                    <TouchableOpacity transparent onPress={() => this.props.navigation.goBack()} activeOpacity={0.5} style={{ width: 40, justifyContent: 'center', alignItems: 'flex-end' }}>
                        <EvilIcons name="close" style={styles.headIcon2} />
                    </TouchableOpacity>
                </Header> */}
                
                <Header style={[commonStyles.headerWarp]} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={[commonStyles.headerBackIcon]} />
                    </TouchableOpacity>

                    <Body style={styles.headBody}>
                        <Title style={{ fontSize: 14 }}>Start live chat</Title>
                    </Body>

                    <TouchableOpacity transparent onPress={() => this.props.navigation.goBack()} activeOpacity={0.1} style={{ width: 60, justifyContent: 'center', alignItems: 'flex-end' }}>
                        {/* <EvilIcons name="close" style={styles.headIcon2} /> */}
                        <Text>Cancel</Text>
                    </TouchableOpacity>

                </Header>
                <View style={{flex: 1}}>
                    
                        <View style={{ backgroundColor: '#cccccc', padding: 15 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <View style={{ marginBottom: 10 }}>
                                    {
                                        this.state.businessImage ? (
                                            <Image source={{ uri: this.state.businessImage }} style={{ height: 50, width: 50, borderRadius: 70 }} />
                                        ) : (<Image source={require('../../../img/icons/no-image_black.png')} style={{ height: 50, width: 50, borderRadius: 70 }} />)
                                    }
                                    {/* <Image source={require('../../../img/atul.png')} style={{ height: 50, width: 50, borderRadius: 70 }} /> */}

                                </View>
                                <View style={{ marginLeft: 10 }}>
                                    <Text>{this.state.businessDetails?this.state.businessDetails.first_name:''}</Text>
                                    {/* <Text style={{ fontSize: 12 }}>Active in the last 15m</Text> */}
                                </View>
                            </View>
                           
                        </View>

                    <ScrollView
                        
                        ref='ScrollViewStart'
                        style={{ padding: 10 }}
                        >
                            {
                                this.state.chatList.map((data, key) => (
                                    data.IsCustomerSender ? (
                                         <View style={{ flexDirection: 'row', marginBottom: 15 }} key={key}>
                                                <View style={{ flex: 1, marginBottom: 10, overflow: 'visible', position: 'relative', alignItems: 'flex-end' }}>
                                                    <View style={{ maxWidth: '80%', padding: 8, borderRadius: 5, backgroundColor: '#fff', position: 'relative', overflow: 'visible' }}>
                                                        {
                                                            data.Message ? (
                                                                <Text style={{ fontSize: 14 }}> {data.Message} </Text>
                                                            ) : (<Image source={{ uri: data.MessageImage }} style={{ height: 100, width: 100, borderRadius: 3 }} />)
                                                        }
                                                    </View>
                                                    <Image source={require('../../../img/icon/chats2.png')} style={{ height: 12, width: 12, position: 'absolute', right: -3, bottom: -3, zIndex: 999 }} />
                                                </View>
                                                <TouchableOpacity style={{ marginLeft: 15, justifyContent: 'flex-end' }} >
                                                    {
                                                        this.state.userImage? (
                                                            <Image source={{ uri: this.state.userImage }} style={{ height: 30, width: 30, borderRadius: 70 }} />
                                                        ) : (<Image source={require('../../../img/icons/no-image_black.png')} style={{ height: 30, width: 30, borderRadius: 70 }} />)
                                                    }
                                                </TouchableOpacity>
                                            </View>
                                       
                                    ) : (
                                            <View style={{ flexDirection: 'row', marginBottom: 15 }} key={key}>
                                            <View style={{ marginRight: 15, justifyContent: 'flex-end' }}>
                                                {
                                                    this.state.businessImage ? (
                                                        <Image source={{ uri: this.state.businessImage }} style={{ height: 30, width: 30, borderRadius: 70 }} />
                                                    ) : (<Image source={require('../../../img/icons/no-image_black.png')} style={{ height: 30, width: 30, borderRadius: 70 }} />)
                                                }
                                            </View>
                                            <View style={{ marginBottom: 10, overflow: 'visible', position: 'relative', maxWidth: '80%' }}>
                                                <View style={{ padding: 8, borderRadius: 5, backgroundColor: '#fff', position: 'relative', overflow: 'visible' }}>
                                                    {
                                                        data.Message ? (
                                                            <Text style={{ fontSize: 14 }}> {data.Message} </Text>
                                                        ) : (<Image source={{ uri: data.MessageImage }} style={{ height: 100, width: 100, borderRadius: 3 }} />)
                                                    }
                                                    {/* <Text style={{ fontSize: 14 }}>{this.props.auth.data.name}</Text> */}
                                                </View>
                                                <Image source={require('../../../img/icon/chats.png')} style={{ height: 12, width: 12, position: 'absolute', left: -3, bottom: -3, zIndex: 999 }} />
                                            </View>
                                        </View>
                                        )
                                ))
                            }
                    </ScrollView>
                </View>
                <Footer>
                    <FooterTab>
                        <View style={{ backgroundColor: '#81cdc7', flexDirection: 'row', flex: 1, alignItems: 'center', }}>
                            <TouchableOpacity style={{ paddingLeft: 10, paddingRight: 10 }} onPress={() => this.uploadPhoto()}>
                                <Entypo name="camera" style={{ fontSize: 24, color: '#fff' }} />
                            </TouchableOpacity>
                            <View style={{ flex: 1, overflow: 'hidden' }}>
                                <TextInput
                                    underlineColorAndroid='transparent'
                                    style={{ backgroundColor: '#fff', borderRadius: 40, paddingLeft: 10, paddingRight: 10, height: 36, padding:0, margin:0 }}
                                    onChangeText={(text) => this.setState({ typeMessage: text })}
                                    value={this.state.typeMessage}
                                />
                            </View>
                            <TouchableOpacity onPress={() => this.sendMessage()} style={{ paddingLeft: 10, paddingRight: 10 }}>
                                <Ionicons name="md-send" style={{ fontSize: 24, color: '#fff' }} />
                            </TouchableOpacity>
                        </View>
                    </FooterTab>
                </Footer>
            </Container>
        );

    }
}


 export default Chat;
