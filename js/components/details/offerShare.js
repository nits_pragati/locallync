import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Text, Alert, Share } from "react-native";
import { Container, Header, Body, Content, Footer, FooterTab } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';
import FSpinner from 'react-native-loading-spinner-overlay';
import Dialog from "react-native-dialog";
import api from "../../api";

class OfferShare extends Component {
    constructor(params) {
        super(params)
        this.state = {
            businessName: this.props.navigation.state.params ? this.props.navigation.state.params.businessName : '',
            image_slider: this.props.navigation.state.params && this.props.navigation.state.params.image_slider ? this.props.navigation.state.params.image_slider : '',
            IsShowDialog: false,
            email: '',
            loader: false
        }
    }

    sendOffer() {
        if (this.state.email) {
            this.setState({
                IsShowDialog: false,
                loader: true
            });
            api.post('users/sendpromocode.json', { email: this.state.email, business_name: this.state.businessName }).then((res) => {
                this.setState({
                    loader: false
                });
                if (res.ack == 1) {
                    Alert.alert("", "Offer sent successfully");

                }
            }).catch((err) => {
                this.setState({
                    loader: false
                });
                Alert.alert("", "Please try again later.")
            })
        }
        else {
            Alert.alert("", "Email is required.");
        }
    }

    // whatsapp share
    whatsappShare() {
        const shareOptions = {
            title: 'Offer Details Share',
            message: 'I just want to let you know about this great offer from ' + this.state.businessName + ' on the LocalLync app. LocalLync app provides great discounts from many businesses in your local area.In addition to getting big discounts at your finger tips, if you sign up for LocalLync app, you will get 25 LYNC tokens that you can use at any participating businesses. LYNC token is a cryptocurrency based token that is based on Blockchain technology similar to BitCoin. Enter this code fplpla when you register for the app to get your tokens. I really like this app - Give it a try! Just download the app from Apple App Store or Google Play Store.'
        };
        Share.share(shareOptions);
    }

    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header searchBar style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText}>Share</Text>
                    </Body>

                    <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                    </TouchableOpacity>

                </Header>
                <Content style={{ flex: 1, padding: 10, backgroundColor: '#f0f0f0' }}>
                    <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                    <Dialog.Container visible={this.state.IsShowDialog}>
                        <Dialog.Title>Share offer details</Dialog.Title>
                        <Dialog.Description>
                            Please enter the email of your friend.
                </Dialog.Description>
                        <Dialog.Input label="Enter email" value={this.state.email} onChangeText={(text) => this.setState({ email: text.trim() })} >

                        </Dialog.Input>
                        <Dialog.Button label="Cancel" onPress={() => this.setState({ IsShowDialog: false })} />
                        <Dialog.Button label="Send offer Details" onPress={() => this.sendOffer()} />
                    </Dialog.Container>

                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-around', alignItems: 'center', height: 160 }}>
                        {
                            this.state.image_slider ? (
                                < Image source={{ uri: this.state.image_slider }}
                                    style={{ width: 100, height: 100 }}
                                />
                            ) : (
                                    < Image source={
                                        require('../../../img/icons/no-image-user.png')
                                    }
                                        style={{ width: 100, height: 100 }}
                                    />
                                )
                        }

                        <Text style={{ fontWeight: 'bold' }}>{this.state.businessName}</Text>
                    </View>
                    <View style={{ borderTopWidth: 1, backgroundColor: '#fff', borderTopColor: 'grey', padding: 10 }}>
                        <Text style={styles.hder}>Exclusive for</Text>
                        <Text style={styles.hder}>Great offer from {this.state.businessName}</Text>
                    </View>
                    <View style={{ borderTopWidth: 1, borderTopColor: 'grey' }}></View>

                    <TouchableOpacity style={styles.tabItemWarp} onPress={() => this.whatsappShare()}>
                        <View>
                            < FontAwesome name="whatsapp" style={styles.tabItemIcon} ></ FontAwesome>
                        </View>
                        <View style={styles.tabItemMdlTextWarp}>
                            <View>
                                <Text style={styles.tabItemName}>Whatsapp</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    {/* <TouchableOpacity style={styles.tabItemWarp}>
                        <View>

                            < FontAwesome name="facebook" style={styles.tabItemIcon} ></ FontAwesome>
                        </View>
                        <View style={styles.tabItemMdlTextWarp}>
                            <View>
                                <Text style={styles.tabItemName}>Facebook</Text>

                            </View>
                        </View>
                    </TouchableOpacity > */}
                    <TouchableOpacity style={styles.tabItemWarp} onPress={() => this.setState({ IsShowDialog: true })}>
                        <View>
                            <Feather name="mail" style={styles.tabItemIcon}></Feather>
                        </View>
                        <View style={styles.tabItemMdlTextWarp}>
                            <View>
                                <Text style={styles.tabItemName}>Email</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    {/* <TouchableOpacity style={styles.tabItemWarp}>
                        <View >

                            <Entypo name="message" style={styles.tabItemIcon} ></Entypo>
                        </View>
                        <View style={styles.tabItemMdlTextWarp}>
                            <View>
                                <Text style={styles.tabItemName}>SMS</Text>

                            </View>
                        </View>
                    </TouchableOpacity> */}
                    {/* <TouchableOpacity style={styles.tabItemWarp}>
                        <View >

                            < Entypo name="link" style={styles.tabItemIcon} ></ Entypo>
                        </View>
                        <View style={styles.tabItemMdlTextWarp}>
                            <View>
                                <Text style={styles.tabItemName}>Copy Link</Text>

                            </View>
                        </View>
                    </TouchableOpacity> */}

                </Content>

                {/* <Footer>
                    <FooterTab style={commonStyles.footerTab}>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Home')} >
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/iconHome_noActive.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={commonStyles.footerItemText}>Home</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Connections')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/connection.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={[commonStyles.footerItemText]}>Connection</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Favorite')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/love.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={[commonStyles.footerItemText]}>Favorite</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Notification')} >
                            <View style={[commonStyles.footerItemImageWarp, { position: 'relative', }]}>
                                <Text style={commonStyles.notificationText}>2</Text>
                                <Image source={require('../../../img/icons/notification_active.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={[commonStyles.footerItemText, commonStyles.footerItemTextActive]} >Notification</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('MyAccount')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/profile.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={commonStyles.footerItemText} >Profile</Text>
                        </TouchableOpacity>
                    </FooterTab>
                </Footer> */}
            </Container>
        );
    }
}
export default OfferShare;
