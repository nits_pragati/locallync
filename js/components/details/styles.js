const React = require("react-native");
const {
    StyleSheet,
    Dimensions,
    Platform
} = React;
const deviceHeight = Dimensions.get("window").height;

export default {
    tabItemWarp: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 50,
        paddingRight: 10,
        paddingTop: 20,
        paddingBottom: 10,
        borderBottomColor: '#f3f3f3',
        borderBottomWidth: 1,
    },
    leftIconholder: {
        width: 35
    },
    tabItemImageWarp: {
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#e1e1e1',
        height: 45,
        width: 45,
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabItemImage: {
        width: 40
    },
    tabItemIcon: {
        fontSize: 30
    },
    tabItemMdlTextWarp: {
        flex: 1,
        marginLeft: 15,
        justifyContent: 'center'
    },
    tabItemName: {
        fontSize: 13
    },
    tabItemPrice: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 0,
        marginBottom: 5
    },
    tabItemPriceText: {
        lineHeight: 12,
        fontSize: 11
    },
    prsnt: {
        height: 15,
        width: 9,
        marginRight: 10
    },
    seeOffer: {
        borderWidth: 1,
        borderColor: '#3ab3ce',
        borderRadius: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    seeOfferText: {
        fontSize: 10,
        color: '#3ab3ce'
    },
    homeContent: {
        flex: 1,
        backgroundColor: '#f0f0f0'
    },
    homehdWarp: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10
    },
    homeTextWarp: {
        flex: 1
    },
    homeTextWarpText: {
        fontFamily: 'Roboto-Medium',
        color: '#000'
    },
    seeAll: {
        flexDirection: 'row'
    },
    seeAllText: {
        fontSize: 11,
        color: '#3ab3ce'
    },
    seeAllIcon: {
        fontSize: 15,
        color: '#3ab3ce'
    },
    homeItemWarp: {
        backgroundColor: '#fff',
        borderColor: '#ebebeb',
        borderWidth: 1,
        position: 'relative',
        width: 150,
        marginLeft: 8,
        marginRight: 8
    },
    homeItemLove: {
        position: 'absolute',
        top: 6,
        right: 6,
        zIndex: 99
    },
    homeItemLoveIcon: {
        fontSize: 20
    },
    homeItemImageWarp: {
        borderBottomColor: '#ebebeb',
        alignItems: 'center',
        borderBottomWidth: 1,
        marginBottom: 5,
        paddingTop: 8,
        paddingBottom: 8
    },
    homeItemImage: {
        height: 60,
        width: 60
    },
    homeItemTextWarp: {
        paddingLeft: 6,
        paddingRight: 6,
    },
    homeItemHead: {
        fontSize: 13,
        color: '#000',
        fontFamily: 'Roboto-Medium',
        marginBottom: 5
    },
    discountText: {
        fontSize: 10,
        marginBottom: 5
    },
    cuisines: {
        fontSize: 10,
        marginBottom: 0,
        color: '#000',
        fontFamily: 'Roboto-Medium'
    },
    cuisinesDown: {
        fontSize: 10,
        marginBottom: 0
    },
    locationText: {
        fontSize: 10
    },
    locationTextInr: {
        color: '#fff',
        fontSize: 14
    },
    signInBtn: {
        backgroundColor: '#3ab3ce',
        borderRadius: 35 / 2,
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        flexDirection: 'row'
    },
    signInBtnGreyed: {
        backgroundColor: 'grey',
        borderRadius: 35 / 2,
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        flexDirection: 'row'
    },
    signInBtnTxt: {
        color: '#fff',
        fontSize: 11
    },
    wrapper: {
        height: 300
    },
    slide: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
    detailsWarp: {
        backgroundColor: '#fff',
        padding: 10,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#f0f0f0'
    },
    detailsLHS: {
        flex: 1
    },
    parsentageWarp: {
        flexDirection: 'row'
    },
    parsentImg: {
        height: 18,
        width: 18,
        marginRight: 2
    },
    parsentTxt: {
        fontSize: 13,
        color: '#222222'
    },
    font11: {
        fontSize: 11,
        marginBottom:5
    },
    colorBlock: {
        color: '#2096AB',
        marginBottom:5
    },
    redeemWarp: {
        width: 90,
        justifyContent: 'flex-end'
    },
    rightIcon: {
        width: 36,
        display: 'flex',
        alignItems: 'flex-start',
        justifyContent: 'center',
        color: '#fff',
        fontSize: 20,
        marginLeft: 10,
        marginright: 10,
        position: 'relative'
    },
    rightIconTag: {
        color: '#fff',
        fontSize: 16
    },
    socialTag: {
        color: '#999'
    },
    pickerMainWarp: {
        backgroundColor: '#f0f0f0',
        padding: 10,        
    },
    iconHolder: {
        backgroundColor: '#13AEC8',
        paddingTop: 6,
        paddingBottom: 6,
        paddingLeft: 10,
        paddingRight: 10,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    leftSide: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },    
    pickerWarp: {
        backgroundColor: '#fff',
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
        paddingLeft: 10,
        borderWidth: 1,
        borderColor:'#99C8ED'
    },
    picker: {
        height: 30,
        flex: 1,
        color: '#000'
    },
    getDiscount: {
        color: '#444444',
        fontSize: 11,
        marginBottom: 4
    },
    discountParcentage: {
        fontWeight: 'bold'
    },
    line: {
        marginTop: 20,
        borderBottomColor: '#bbbbbb',
        borderBottomWidth: 1,
        marginLeft: 10,
        marginRight: 10
    },
    lineTxt: {
        marginTop: -10,
        backgroundColor: '#fff',
        marginBottom: 20,
        width: 70,
        height: 20,
        textAlign: 'center'
    },
    lineHolder: {
        display: 'flex',
        alignItems: 'center',
    },
    contentWrap: {
        display: 'flex',
        flexDirection: 'row',
        marginLeft: 35,
        marginBottom: 15
    },
    lftImage: {
        width: 110,
    },
    borderWrap: {
        borderWidth: 1,
        borderColor: '#ddd',
        marginLeft: 35,
        marginRight: 10,
        padding: 8,
        height: 80,
        marginBottom: 8
    },
    gryTxt: {
        color: '#999999'
    },
    deleteWarp: {
        display: 'flex',
        alignItems: 'flex-end',
        paddingRight: 10,
        justifyContent:'flex-end',
        flexDirection:'row',
        width:'100%'
    },
    delBtn: {
        borderColor: '#BBBBBB',
        borderWidth: 1,
        borderRadius: 4,
        height: 34,
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        marginBottom: 20
    },
    delBtnTxt: {
        color: '#2968B3',
        fontSize: 12,
    },
    roundBadge: {
        borderRadius: 50,
        width: 17,
        height: 17,
        position: 'absolute',
        left: 15,
        fontSize:14
    },
    badgeText: {
        color: '#fff',
        padding: 0,
        margin: 0,
        fontSize: 9,
        lineHeight: 14
    },
    blueTxt: {
        color: '#2968B3',
        color: '#0076FF',
        fontSize: 12,
    },
    requestBtn: {
        borderRadius: 4,
        borderRadius: 3,
        height: 34,
        height: 35,
        alignItems: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        flexDirection: 'row',
        flexDirection: 'row',
        borderColor: '#bbbbbb',
        paddingLeft: 15,
        borderWidth: 1,
        paddingRight: 15,
        marginBottom: 20
    },
    formGroup: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: 20,
        marginBottom: 8,
        borderBottomColor: '#bbbbbb',
        borderBottomWidth: 1,
        marginLeft: 10,
        marginRight: 10
    },
    lineTxt: {
        formLabel: {
            minWidth: 95,
            marginTop: -10,
            paddingTop: 7,
            backgroundColor: '#fff',
            marginBottom: 20,
            marginRight: 5,
            width: 70,
            height: 20,
            textAlign: 'center'
        },
    },
    lineHolder: {
        inputField: {
            borderWidth: 1,
            borderColor: '#bbbbbb',
            display: 'flex',
            display: 'flex',
            flex: 1,
            padding: 5,
            position: 'relative',
            minHeight: 38,
        },
        datepIcon: {
            position: 'absolute',
            right: 5,
            top: 6,
        },
        formInput: {
            padding: 0,
            margin: 0
        },
        radioContainer: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            alignItems: 'center',
        },
        contentWrap: {
            display: 'flex',
            flexDirection: 'row',
            marginLeft: 35,
            marginBottom: 15
        },
        lftImage: {
            width: 110,
        },
        borderWrap: {
            borderWidth: 1,
            borderColor: '#BBBBBB',
            marginLeft: 35,
            marginRight: 10,
            padding: 8,
            height: 80,
            marginBottom: 8,
            marginBottom: 8
        },
    },
    gryTxt: {
        radioBox: {
            color: '#999999',
            width: 25,
            paddingLeft: 2,
            marginRight: 5
        },
    },
    deleteWarp: {
        radioWarp: {
            height: 18,
            display: 'flex',
            width: 18,
            borderRadius: 50,
            alignItems: 'flex-end',
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#fff',
            paddingRight: 10,
            borderWidth: 2,
            borderColor: '#dadada',
        },
    },
    delBtn: {
        radioWarpActive: {
            borderColor: '#BBBBBB',
            borderColor: '#0076FF'
        },
        radio: {
            height: 10,
            borderWidth: 1,
            width: 10,
            borderRadius: 50,
            backgroundColor: '#0076FF'
        },
    },
    formGroup: {
        display: 'flex',
        flexDirection: 'row',

        marginBottom: 8,
    },
    formLabel: {
        minWidth: 95,
        paddingTop: 7,
        marginRight: 5,
    },
    inputField: {
        borderWidth: 1,
        borderColor: '#bbbbbb',
        display: 'flex',
        flex: 1,
        padding: 5,
        position: 'relative',
        minHeight: 38,
    },
    datepIcon: {
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    formInput: {
        padding: 0,
        margin: 0
    },
    radioContainer: {
        // borderWidth: 1,
        //borderColor: 'green',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 8
    },
    radioBox: {
        width: 25,
        paddingLeft: 2,
        marginRight: 5
    },
    radioWarp: {
        height: 18,
        width: 18,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        borderWidth: 2,
        borderColor: '#dadada',
    },
    radioWarpActive: {
        borderColor: '#0076FF'
    },
    radio: {
        height: 10,
        width: 10,
        borderRadius: 50,
        backgroundColor: '#0076FF'
    },
    requestBtn: {
        borderRadius: 30,
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        flexDirection: 'row',
        borderColor: '#bbbbbb',
        borderWidth: 1,
        backgroundColor:'#00BBD7'
    },
    blueTxt: {
        color: '#fff',
    },
    dateTxt:{
        fontSize:15,
        padding:10,
        color:'#333',
        textAlign:'center',
        marginTop:10,
        marginBottom:10
    },
    bttext:{
        fontWeight:'bold',
        fontSize:16,
        color:'#111',
        lineHeight:29
    },
    btnWarp:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-end',
        padding:15
    },
    deltBtn:{
        borderColor: '#ddd',
        borderWidth: 1,
        borderRadius: 5,        
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop:4,
        paddingBottom:4,
        marginLeft:10

        
    },
    textArea: {
        borderWidth: 0,
    },
}
