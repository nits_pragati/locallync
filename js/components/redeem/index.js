import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { View, StatusBar, TouchableOpacity, Text, TextInput, AsyncStorage } from "react-native";
import { Container, Header, Body, Content } from "native-base";
import commonStyles from "../../assets/styles"; 
import styles from "./styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import QRCode from 'react-native-qrcode';
import api from '../../api';
import * as firebase from 'firebase';
import FCM, {
	FCMEvent,
	RemoteNotificationResult,
	WillPresentNotificationResult,
	NotificationType,

} from "react-native-fcm";
const firebaseConfig = {
	apiKey: "AIzaSyBSW6dhfaSEP2pAHGAbWfuZ-kAPF7uA63w",
	authDomain: "locallync-859ff.firebaseapp.com",
	databaseURL: "https://locallync-859ff.firebaseio.com",
	projectId: "locallync-859ff",
	storageBucket: "locallync-859ff.appspot.com",
	messagingSenderId: "718735440621"
};

class Redeem extends Component {
    constructor(params) {
        super(params)
        this.state = {
            QR: '',
            scannerRef: '',
			IsShowDialog: false,
			snapShotVal: '',
            
        }

        if (!firebase.apps.length) {
			firebase.initializeApp(firebaseConfig);
			this.state.scannerRef = firebase.database().ref().child('scannerRef');
		}
		else {
			this.state.scannerRef = firebase.database().ref().child('scannerRef');
		}
		
		var child_added_first = true;

		this.state.scannerRef.on('child_changed', (snapshot) => {
			console.warn("snapshot val:",snapshot);
			const snapShotVal = snapshot.val();
			console.warn("snapshot val():",snapShotVal);
			AsyncStorage.getItem('UserDetails').then((userDetails) => {

				const parsedUserDetails = JSON.parse(userDetails);

				if (snapShotVal.userId == parsedUserDetails.details.id.toString()) {
					this.setState({
						IsShowDialog: true,
						snapShotVal: snapShotVal
					})
				}
				
			})
		})


    }

    componentDidMount(){
        console.warn("redeem page :",this.props.navigation);
        debugger;
          if (this.props.navigation.state.params && this.props.navigation.state.params.outletsData)
        {
            AsyncStorage.getItem("UserDetails").then((userDetails)=>{
               debugger;
                const parsedUserDetails=JSON.parse(userDetails);
                // const qrCode = this.props.navigation.state.params.outletsData.offer_name + "|" + this.props.navigation.state.params.outletsData.id + "|" +
                //     this.props.navigation.state.params.outletsData.business_id + "|" + this.props.navigation.state.params.outletsData.category_id + "|" + this.props.navigation.state.params.outletsData.user_offer_outlets && this.props.navigation.state.params.outletsData.user_offer_outlets.length > 0 ? this.props.navigation.state.params.outletsData.user_offer_outlets[0].id:'' + "|" +parsedUserDetails.details.id
                const qrCode = 
                    this.props.navigation.state.params.outletsData.business_id + "|" + this.props.navigation.state.params.outletsData.id + "|" + parsedUserDetails.details.id + "|" + this.props.navigation.state.params.outletsData.token+ "|" + this.props.navigation.state.params.displayName + "|" + this.props.navigation.state.params.outletsData.user_id;
                    console.warn("qr code:",qrCode);
                    this.RendomeCode(qrCode);
            }); 
        }
         //this.RendomeCode(`${Math.floor((Math.random() * 100000000) + 1)}`);
    }

    RendomeCode(QRCode){
        this.setState({ QR: QRCode })
      
    }

	payWithToken() {
        //alert();
        AsyncStorage.getItem('user_business_id', (err, result) => {
            if (result) {
                console.warn(result);
                debugger;
                let data = JSON.parse(result);
                console.warn(this.state.snapShotVal);
                this.props.navigation.navigate('PayWithToken',{ user_business_id: data, snapshot: this.state.snapShotVal});
               // this.props.navigation.navigate('Wallet', { user_business_id: data, snapshot: this.state.snapShotVal });
            } else {
                console.log("err in gowalletpage :", err);
            }

        });
       
    }
    
    goToOffersPage() {
		//console.warn('snap',this.state.snapShotVal)
		debugger;
			this.setState({
				loader: true,
				
			})
			debugger
			setTimeout(() => {
				api.post('UserBusinessess/BusinessDetails.json', { id: this.props.navigation.state.params.outletsData.business_id }).then(res => {
					this.props.navigation.navigate('Details', { Details: res.details })
				}).catch((err) => {
					this.setState({ loader: false });

				})
			}, 100)
		
	}

    render() {
        return (
            <Container style={{ backgroundColor: '#fff' }} >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header searchBar style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText}> REDEEM </Text>
                    </Body>

                    <TouchableOpacity style={commonStyles.headerBtnRight} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={commonStyles.signInBtnTxt} /> */}
                    </TouchableOpacity>

                </Header>

                <Content>

                    <Text style={styles.text1}> Please show the barcode to the merchant </Text>
                    <View style={styles.qrCodeWarp}>
                        <QRCode
                            value={this.state.QR}
                            size={100}
                            bgColor='#000'
                            fgColor='#fff' />
                    </View>
                    {this.state.IsShowDialog ?
                       ( <View>
                            <Text style={{ fontSize: 16, color: '#333', textAlign: 'center', marginTop: 30, }}> Do You want to Redeem More offers </Text>
                            <View style={styles.btnWarp}>
                                <TouchableOpacity style={styles.grayButton} onPress={() => this.goToOffersPage()}>
                                    <Text style={styles.btnText}> YES </Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.grayButton} onPress={() => this.payWithToken()}>
                                    <Text style={styles.btnText}>NO</Text>
                                </TouchableOpacity>
                            </View>
                        </View>) :
                        null
                    }
                    

                </Content>

                {/* <Text style={styles.textend}> Give you review and collect LYNK token You can only give review after you redeem an offer </Text> */}
            </Container>
        );
    }
}

export default Redeem;
