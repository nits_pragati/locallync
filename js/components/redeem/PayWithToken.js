import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { View, StatusBar, TouchableOpacity, Text, TextInput, AsyncStorage } from "react-native";
import { Container, Header, Body, Content } from "native-base";
import commonStyles from "../../assets/styles"; 
import styles from "./styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import QRCode from 'react-native-qrcode';
import api from '../../api';


class Redeem extends Component {
    constructor(params) {
        super(params)
        this.state = {
            QR: '',
            user_business_id: this.props.navigation.state.params ? this.props.navigation.state.params.user_business_id.user_business_id : '',
            snapShotVal:this.props.navigation.state.params? this.props.navigation.state.params.snapshot: ''
        }
        console.warn('props:',this.props.navigation.state.params)
    }

    componentDidMount(){
        console.log("redeem page :",this.props.navigation);
        debugger;
          if (this.props.navigation.state.params && this.props.navigation.state.params.outletsData)
        {
            AsyncStorage.getItem("UserDetails").then((userDetails)=>{
               debugger;
                const parsedUserDetails=JSON.parse(userDetails);
                // const qrCode = this.props.navigation.state.params.outletsData.offer_name + "|" + this.props.navigation.state.params.outletsData.id + "|" +
                //     this.props.navigation.state.params.outletsData.business_id + "|" + this.props.navigation.state.params.outletsData.category_id + "|" + this.props.navigation.state.params.outletsData.user_offer_outlets && this.props.navigation.state.params.outletsData.user_offer_outlets.length > 0 ? this.props.navigation.state.params.outletsData.user_offer_outlets[0].id:'' + "|" +parsedUserDetails.details.id
                
            }); 
        }
         //this.RendomeCode(`${Math.floor((Math.random() * 100000000) + 1)}`);
    }

	    
    goWalletPage() {
				
			AsyncStorage.getItem('user_business_id', (err, result) => {
				if (result) {
					console.warn(result);
					let data = JSON.parse(result);
					this.props.navigation.navigate('Wallet', { user_business_id: data,snapshotValue: this.state.snapShotVal });
				} else {
					console.log("err in gowalletpage :", err);
				}
			});
	

	}
    
    backToFeedback(){
        this.props.navigation.navigate('FeedbackRating', {
            snapshot: this.state.snapShotVal 
        });
       
    }
    render() {
        return (
            <Container style={{ backgroundColor: '#fff' }} >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header searchBar style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText}> Pay with token </Text>
                    </Body>

                    <TouchableOpacity style={commonStyles.headerBtnRight} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={commonStyles.signInBtnTxt} /> */}
                    </TouchableOpacity>

                </Header>

                <Content>
                 
                    <Text style={{fontSize:16, color:'#333', textAlign:'center', marginTop:30,}}> Do You want to use your tokens? </Text>
                    <Text style={{fontSize:16, color:'#333', textAlign:'center', marginTop:30, paddingLeft:20,paddingRight:20}}>If your wallet balance is not sufficient,you will have to pay the remaining balance in cash </Text>
                    <View style={styles.btnWarp}>
                        <TouchableOpacity style={styles.grayButton} onPress={() => this.goWalletPage()}>
                            <Text style={styles.btnText}> YES </Text>
                        </TouchableOpacity>                       
                        <TouchableOpacity style={styles.grayButton}  onPress={() => this.backToFeedback()}>
                            <Text style={styles.btnText}>NO</Text>
                        </TouchableOpacity>
                    </View> 

                </Content>

                {/* <Text style={styles.textend}> Give you review and collect LYNK token You can only give review after you redeem an offer </Text> */}
            </Container>
        );
    }
}

export default Redeem;
