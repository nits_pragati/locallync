const React = require("react-native");
const { StyleSheet, Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
    tabItemWarp: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 10,
        paddingBottom: 10,
        borderBottomColor: '#f3f3f3',
        borderBottomWidth: 1
    },
    tabItemImageWarp: {
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#e1e1e1',
        height: 45,
        width: 45,
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabItemImage: {
        width: 40
    },
    tabItemMdlTextWarp: {
        flex: 1,
        marginLeft: 15,
        justifyContent: 'center'
    },
    tabItemName: {
        fontSize: 13
    },
    tabItemPrice: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 0,
        marginBottom: 5
    },
    tabItemPriceText: {
        lineHeight: 12,
        fontSize: 11
    },
    prsnt: {
        height: 15,
        width: 9,
        marginRight: 10
    },
    seeOffer: {
        borderWidth: 1,
        borderColor: '#3ab3ce',
        borderRadius: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    seeOfferText: {
        fontSize: 10,
        color: '#3ab3ce'
    },
    homeContent: {
        flex: 1,
        backgroundColor: '#f0f0f0'
    },
    homehdWarp: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10
    },
    homeTextWarp: {
        flex: 1
    },
    homeTextWarpText: {
        fontFamily: 'Roboto-Medium',
        color: '#000'
    },
    seeAll: {
        flexDirection: 'row'
    },
    seeAllText: {
        fontSize: 11,
        color: '#3ab3ce'
    },
    seeAllIcon: {
        fontSize: 15,
        color: '#3ab3ce'
    },
    homeItemWarp: {
        backgroundColor: '#fff',
        borderColor: '#ebebeb',
        borderWidth: 1,
        position: 'relative',
        width: 150,
        marginLeft: 8,
        marginRight: 8
    },
    homeItemLove: {
        position: 'absolute',
        top: 6,
        right: 6,
        zIndex: 99
    },
    homeItemLoveIcon: {
        fontSize: 20
    },
    homeItemImageWarp: {
        borderBottomColor: '#ebebeb',
        alignItems: 'center',
        borderBottomWidth: 1,
        marginBottom: 5,
        paddingTop: 8,
        paddingBottom: 8
    },
    homeItemImage: {
        height: 60,
        width: 60
    },
    homeItemTextWarp: {
        paddingLeft: 6,
        paddingRight: 6,
    },
    homeItemHead: {
        fontSize: 13,
        color: '#000',
        fontFamily: 'Roboto-Medium',
        marginBottom: 5
    },
    discountText: {
        fontSize: 10,
        marginBottom: 5
    },
    cuisines: {
        fontSize: 10,
        marginBottom: 0,
        color: '#000',
        fontFamily: 'Roboto-Medium'
    },
    cuisinesDown: {
        fontSize: 10,
        marginBottom: 0
    },
    locationText: {
        fontSize: 10
    },
    locationTextInr: {
        color: '#fff',
        fontSize: 14
    },
    signInBtn: {
        backgroundColor: '#3ab3ce',
        borderRadius: 35 / 2,
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        flexDirection: 'row'
    },
    signInBtnTxt: {
        color: '#fff',
        fontSize: 11
    },
    wrapper: {
        height: 300
    },
    slide: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
    detailsWarp: {
        backgroundColor: '#fff',
        padding: 10,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#f0f0f0'
    },
    detailsLHS: {
        flex: 1
    },
    parsentageWarp: {
        flexDirection: 'row'
    },
    parsentImg: {
        height: 15,
        width: 15
    },
    parsentTxt: {
        fontSize: 13
    },
    font11: {
        fontSize: 11,
        color: '#777777'
    },
    colorBlock: {
        color: '#000'
    },
    redeemWarp: {
        width: 90,
        justifyContent: 'flex-end'
    },
    rightIcon: {
        width: 26,
        display: 'flex',
        alignItems: 'flex-end'
    },
    rightIconTag: {
        color: '#fff',
        fontSize: 16
    },
    pickerMainWarp: {
        backgroundColor: '#f0f0f0',
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    pickerWarp: {
        backgroundColor: '#fff',
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
        paddingLeft: 10
    },
    picker: {
        height: 30,
        flex: 1,
        color: '#b5b4b4'
    },
    signInBtn: {
        backgroundColor: '#3ab3ce',
        borderRadius: 40 / 2,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        flexDirection: 'row'
    },
    signInBtnTxt: {
        color: '#fff',
        fontSize: 13
    },
    or:{ 
        width: '100%', 
        textAlign: 'center', 
        marginTop: 15, 
        marginBottom: 15 
    },
    outLineButton: { 
        borderWidth: 1, 
        borderColor: '#3ab3ce', 
        height: 40, 
        borderRadius: 40 / 2, 
        alignItems: 'center', 
        justifyContent: 'center',
        overflow: 'hidden', 
        borderBottomWidth: 2
    },
    outLineButtonText:{ 
        color: '#3ab3ce', 
        fontSize: 13 
    },
    textend:{ 
        width: '100%', 
        textAlign: 'center', 
        fontSize: 11, 
        marginTop: 30, 
        color: '#777777',
        paddingBottom: 10, 
        paddingTop: 10
    },
    text1:{ 
        fontSize: 13, 
        width: '100%', 
        textAlign: 'center', 
        color: '#0077b5', 
        marginTop: 30, 
        marginBottom: 30 
    },
    qrCodeWarp: { 
        alignItems: 'center',
        marginBottom: 15
    },
    btnWarp:{ 
        paddingLeft: 30, 
        paddingRight: 30 ,
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop:30
    },
    grayButton:{
        width:90,
        backgroundColor:'#f9f9f9',
        borderColor:'#ddd',
        borderWidth:1,
        marginBottom:10,
        marginRight:15,
        justifyContent:'center',
        alignItems:'center',
        padding:5,
        paddingLeft:15,
        paddingRight:15,
        borderRadius:2
    },
    btnText:{
        color: '#0077b5', 
        fontWeight:'600'
    }
};
