const React = require("react-native");
const { StyleSheet, Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
    tabItemWarp: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 10,
        paddingBottom: 10,
        borderBottomColor: '#f3f3f3',
        borderBottomWidth: 1
    },
    tabItemImageWarp: {
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#e1e1e1',
        height: 45,
        width: 45,
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabItemImage: {
        width: 40,
        height: 40
    },
    tabItemMdlTextWarp: {
        flex: 1,
        marginLeft: 15,
        justifyContent: 'center'
    },
    tabItemName: {
        fontSize: 13
    },
    tabItemPrice: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    tabItemPriceText: {
        lineHeight: 12,
        fontSize: 11
    },
    prsnt: {
        height: 15,
        width: 9,
        marginRight: 10
    },
    seeOffer: {
        borderWidth: 1,
        borderColor: '#3ab3ce',
        borderRadius: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    seeOfferText: {
        fontSize: 10,
        color: '#3ab3ce'
    },
    pickerMainWarp: {
        backgroundColor: '#f0f0f0',
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    pickerWarp: {
        backgroundColor: '#fff',
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
        paddingLeft: 10
    },
    picker: {
        height: 30,
        flex: 1,
        color: '#b5b4b4'
    },
    heart: { 
        fontSize: 18,
         color: 'red' 
    }
};
