import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, AsyncStorage } from "react-native";
import { Container, Header, Body, Content, Tabs, Tab, TabHeading, ScrollableTab, Footer, FooterTab, Button, } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons'; 
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import api from '../../api';
import FSpinner from 'react-native-loading-spinner-overlay';


class Favorite extends Component {
    constructor(params) {
        super(params)
        this.state = {
            myFavourite: [], 
            imageUri: '',
            unreadcount: 0,
            allCategory:[],
            allCategory_id:[],
            myFavourite_list:[],
            displayName:''
        }
    }

    componentDidMount() {

        this.setState({ loader: true });
        AsyncStorage.getItem('UserDetails', (err, result) => {
            let data = JSON.parse(result);
            let user_id = data.details.id + '';
            api.post('notifications/unreadCount.json', {
                user_id: user_id
            }).then((notification) => {
                if (notification.ack == 1) {
                    this.setState({
                        unreadcount: notification.chat_unread+notification.connect_unread+notification.review_unread
                    })
                }
            })

            
            api.post('Categories/list_category.json').then(resultCat => {
                debugger;
                if (resultCat.ack == 1) {
                    debugger;
                    if (resultCat.details.length != 0) {
                        resultCat.details.map((catData) => {
                               this.state.allCategory.push({name:catData.name,id:catData.id});
                                                                                       
                        });
                    }
                    
                }
                this.setState({
                    loader: false
                })
            }).catch((err) => {
                debugger;
                this.setState({ loader: false });
                console.log(err);
            })

            api.post('UserBusinessess/MyFavourites.json', {user_id: user_id }).then(res => {
                debugger;
                if (res.ack == 1) {
                    debugger;
                   
                    res.Details.map((data) => {
                        data.user_business_id = data.business_id,
                        // data.category_id = '',
                        data.image = res.image_url + data.UserBusinessess.business_logo;
                    })
                    this.setState({
                        myFavourite: res.Details,
                        myFavourite_list: res.Details,
                        imageUri: res.image_url
                    });
                    debugger;
                }
                AsyncStorage.setItem("AllFavourites", JSON.stringify(this.state.myFavourite));
                this.setState({
                    loader: false
                })
            }).catch((err) => {
                debugger;
                this.setState({ loader: false });
                console.log(err);
            })
        })
    }

    setMenuRef = ref => {
        this.menu = ref;
    };

    distance() {
        this.menu.hide();
    }

    showMenu = () => {
        this.menu.show();
    };

    setSelectedValue(value, res) {
        debugger
       
        if (value == 0) {
            this.setState({
                allCategory: this.state.allCategory,
                myFavourite: this.state.myFavourite_list,
                displayName:value
            });
            debugger
        } else if (value != 0) {
                  debugger   
                  myFavourite_new=[];  
           this.state.allCategory.map((list) => { 
                if ( list.id == value) {
                    debugger
                    if(this.state.myFavourite_list.length != 0) {
                        this.state.myFavourite_list.map((results) => {
                            if (results.category_id == list.id){
                                debugger
                                myFavourite_new.push(results)

                            }
                            this.setState({
                                myFavourite:myFavourite_new,
                                displayName:value
                               });
                             debugger                              
                        });
                    }else{
                                                                  
                    }
                 
                }else{
                   
                }
                
            });
                
            
        }
    }


    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText}>FAVORITES</Text>
                    </Body>

                    <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                    </TouchableOpacity>

                </Header>
                <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                <View style={styles.pickerMainWarp}>
                    
                    {/* <View>
                        <Menu
                            ref={this.setMenuRef}
                            button={
                                <TouchableOpacity style={{ paddingRight: 10 }} onPress={this.showMenu} >
                                    <FontAwesome name="sort-amount-desc" />
                                </TouchableOpacity>
                            }
                        >
                            <MenuItem onPress={() => this.distance()}>Sort by Distance</MenuItem>
                            <MenuItem onPress={() => this.distance()}>Sort by Name</MenuItem>
                        </Menu>
                    </View> */}
                    <View style={styles.pickerWarp}>
                        <Picker
                            style={styles.picker}
                            selectedValue={this.state.displayName}
                            itemStyle={{ fontSize: 12, color: '#b5b4b4' }}
                            onValueChange={(value) =>
                                this.setSelectedValue(value, this.state.allCategory)}>
                            <Picker.Item label="All Categories" value= '0' />
                            {
                                this.state.allCategory.map((data, key) => {
                                    return (<Picker.Item label={data.name} value={data.id} key={data.id} />)
                                })
                            }
                           
                        </Picker>
                    </View>
                    {/* <TouchableOpacity style={{ paddingLeft: 10 }} onPress={()=> this.props.navigation.navigate('Map')}>
                        <MaterialIcons name="location-on" style={{ fontSize: 20 }}/>
                    </TouchableOpacity> */}
                </View>
                <Content style={{ flex: 1 }}>

                    {
                        this.state.myFavourite.length != 0 ? (
                            this.state.myFavourite.map((data, key) => {
                                console.log('data.image', data.image);
                                return (
                                    <TouchableOpacity style={styles.tabItemWarp} key={key} onPress={() => this.props.navigation.navigate('Details', { Details: data })}>
                                        <View style={[styles.tabItemImageWarp, { height: 50, width: 50 }]}>
                                        {
                                            data.image ? (
                                                <Image source={{ uri: data.image }} style={styles.tabItemImage} />
                                            ):(
                                                <Image source={require('../../../img/icons/no-image.png') } style={styles.tabItemImage} />
                                            )
                                            

                                        }
                                        </View>
                                        <View style={styles.tabItemMdlTextWarp}>
                                            <View>
                                                <Text style={styles.tabItemName}>{data.UserBusinessess.business_name}</Text>
                                                {/* <Text style={{ fontSize: 10, marginBottom: 4 }} numberOfLines={1}>15% Exclusive discount for you</Text>
                                                <View style={styles.tabItemPrice}>
                                                    <Image source={require('../../../img/icons/favorite.png')} style={styles.prsnt} />
                                                    <Text style={styles.tabItemPriceText}>8.6 km  |  Food & Drink</Text>
                                                </View> */}
                                            </View>
                                        </View>
                                        <TouchableOpacity style={[styles.seeOffer, { borderWidth: 0, padding: 0 }]}>
                                            <FontAwesome name='heart' style={styles.heart} />
                                        </TouchableOpacity>
                                    </TouchableOpacity>
                                )
                            })
                        ) : (<Text style={{ width: '100%', textAlign: 'center' }}> No Data Found </Text>)
                    }
                </Content>

                <Footer>
                    <FooterTab style={commonStyles.footerTab}>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Home')} >
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/iconHome_noActive.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={commonStyles.footerItemText}>Home</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Connections')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/connection.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={[commonStyles.footerItemText]}>Connection</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Favorite')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/love_active.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={[commonStyles.footerItemText, commonStyles.footerItemTextActive]}>Favorite</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Notification')}>
                            <View style={[commonStyles.footerItemImageWarp, { position: 'relative', }]}>
                            {this.state.unreadcount != 0?
                            <Text style={commonStyles.notificationText}>{this.state.unreadcount}</Text>
                            :
                                null
                            }
                                
                                <Image source={require('../../../img/icons/notification.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={commonStyles.footerItemText} >Notification</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('MyAccount')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/profile.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={commonStyles.footerItemText} >Profile</Text>
                        </TouchableOpacity>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}
export default Favorite;
