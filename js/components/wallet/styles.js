const React = require("react-native");
const { StyleSheet, Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
    wrappingDiv: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        padding: 20,
        borderWidth: 1,
        borderColor: '#009688',
        backgroundColor: '#009688',
        // height: deviceHeight/0.5,


    },
    topUp:{
        color:'#009688',
        paddingLeft:10,
        fontSize: 16
    },
    wrappingDivContent: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding:10
    },
    wrappingDiv1Content:{
        flexDirection: 'row'
    },
    balanceStyle: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    wrappingDivText: {
        color: '#fff',
        lineHeight:40
    },
    wrappingDivContentText: {
        color: 'black',
        borderColor:'#fff',
        borderWidth:1,
        backgroundColor:'#fff',
        width: deviceWidth/2,
        paddingLeft:5
    },
    wrappingDiv1: {
       
        padding:10,
        paddingLeft:0,
        marginTop:10

    },
    input: {
        paddingLeft: 10,
        paddingRight: 10,
        flex: 1,
        height: 40,
        paddingBottom: 5,
        paddingTop: 8,
        fontSize: 12
    },
    wholeWrappingDiv: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        // marginTop: 40,
        padding:20,

    },
    signInBtn: {
        backgroundColor: '#3ab3ce',
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        flexDirection: 'row',
        borderRadius:40/2
    },
    signInBtnTxt: {
        color: '#fff',
        fontSize: 13
    },

    wallet:{
        padding:15,
        backgroundColor: '#363e6b',
    },
    walletItem:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop:20,
        marginBotom:20,
    },
    walletItemWallet:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems:'center'
    },
    walletBalance:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 20,
    },
    textinput:{
        width:'100%',
        color:'#fff',
    },
    walletText:{
        color:'#fff',
    },
    walletInput:{
        textAlign: 'center',
        borderWidth:.8,
        borderColor:'#ddd',
        lineHeight:22,
        fontSize:12,
        padding:8,
        color:'#fff'
    },

    wallet2:{
        padding:0,
    },
   
    walletText2:{
        color:'#000'
    },
    walletInput2:{
        backgroundColor:'#fff',
        textAlign: 'center',
        borderWidth:.8,
        borderColor:'#ddd',
        lineHeight:25,
        fontSize:12,
        width:'40%',
    },
    homeIcon:{
        fontSize:25,
        color:'#fff'
    },
    linkWallet:{
        borderWidth:1
    }
}
