import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, AsyncStorage, TouchableOpacity, NetInfo, StatusBar, Image, Dimensions, TextInput, Alert, ScrollView } from 'react-native';
import { Container, Body, Header, Footer, Content } from 'native-base';
import SplashScreen from 'react-native-splash-screen';
import { NavigationActions } from 'react-navigation';
import { login, LogedInData, saveDataForOffline } from './../accounts/elements/authActions';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import FSpinner from 'react-native-loading-spinner-overlay';
import Ionicons from 'react-native-vector-icons/Ionicons';
import commonStyles from "../../assets/styles";
import styles from "./styles";
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Modal from "react-native-modal";
import api from '../../api';
const deviceWidth = Dimensions.width;
class Wallet extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loader: false,
            IsModalVisible: false,
            token: '',
            availableToken: '',
            walletId: '',
            user_business_id: this.props.navigation.state.params ? this.props.navigation.state.params.user_business_id.user_business_id : '',
            payToBusiness: false,
            senderAddress: '',
            amount: '',
            commission:''
        };
   console.warn('params',this.props.navigation.state.params)
    }



    componentDidMount() {
        this.setState({
            loader: true
        });

        AsyncStorage.getItem("UserDetails").then((result) => {
            if (result) {
                debugger
                const data = JSON.parse(result);

                if (this.state.user_business_id) {
                    this.setState({
                        payToBusiness: true
                    });
                } else {
                    this.setState({
                        payToBusiness: false
                    });
                }
                debugger
                if (data.details && data.details.blockchain_address) {
                    debugger
                    api.getWalletBalance('balance', data.details.blockchain_address).then((response) => {
                        debugger;
                        if (response.balance) {
                            debugger;
                            this.setState({
                                loader: false,
                                availableToken: response.balance,
                                walletId: data.details.blockchain_id
                            })
                        } else {
                            this.setState({
                                loader: false
                            });
                            //Alert.alert('', 'Please try again later.');
                        }
                    }).catch((err) => {
                        this.setState({
                            loader: false,
                            walletId: data.details.blockchain_id
                        });
                        Alert.alert('', 'No transaction found');
                    })
                } else {
                    this.setState({
                        loader: false
                    });
                }

            }
        })
    }

    payMoney() {
        if (this.state.availableToken) {
            if (!this.state.amount) {
                Alert.alert('', 'Please enter the amount to pay.');
                return;
            }
            this.setState({
                loader: true
            });
            AsyncStorage.getItem("UserDetails").then((result) => {
                debugger
                if (result) {
                    const data = JSON.parse(result);
                    if (this.state.amount.trim() == "") {
                        Alert.alert('', 'Please select the amount first');
                        this.setState({
                            loader: false
                        });
                    } else if (data.details.blockchain_id) {
                        if (Number(this.state.amount) < Number(this.state.availableToken)) {
                            debugger;
                            const dataone = {
                                walletId: data.details.blockchain_id,
                                password: data.details.blockchain_password,
                                fromAddress: data.details.blockchain_address,
                                toAddress: this.props.navigation.state.params.snapshotValue.blockchain_address,
                                amount:Number(this.state.amount)
                            };
                            console.warn('pay data',dataone)
                            api.walletPost('pay-business', dataone).then((res3) => {
                                console.warn("res3 :", res3);
                                const commission = (10 * Number(this.state.amount)) / 100;
                                api.walletPost('pay-commission', {
                                    senderAddress: data.details.blockchain_address,
                                    senderWalletId: data.details.blockchain_id,
                                    "amount": commission
                                }).then((commissionSuccess) => {
                                    debugger
                                    api.getWalletBalance('balance', data.details.blockchain_address).then((response) => {
                                        debugger;
                                        if (response.balance) {
                                            debugger;
                                            //  this.setState({
                                            //      loader: false,
                                            //      availableToken: response.balance,
                                            //      walletId: data.details.blockchain_id
                                            //  })
                                            this.props.navigation.navigate('FeedbackRating', {
                                                snapshot: this.props.navigation.state.params.snapshotValue
                                            });
                                        } else {
                                            this.setState({
                                                loader: false
                                               
                                            });
                                            Alert.alert('', 'Please try again later.');
                                        }
                                    }).catch((err) => {
                                        debugger;
                                        this.setState({
                                            loader: false,
                                            walletId: data.details.blockchain_id
                                        });
                                        this.props.navigation.navigate('FeedbackRating', {
                                            snapshot: this.props.navigation.state.params.snapshotValue
                                        });
                                    })
                                }).catch((error) => {
                                    console.log(error);
                                    debugger;
                                    Alert.alert('', 'Please try again later.');
                                })
                                this.setState({
                                    loader: false,
                                    commission:commission
                                });
                                if (res3) {
                                   // Alert.alert('', 'You have successfully paid '+this.state.commission+' to business.')
                                    Alert.alert('',this.state.amount+' tokens deducted from your token balance')
                                }

                                /** Token track add */
                                api.post('UserBusinessess/tokenTrackAdd.json', {
                                    business_id: this.state.user_business_id,
                                    customer_id: data.details.id,
                                    outlet_id:this.props.navigation.state.params.snapshotValue.outlet_id,
                                    offer_id:this.props.navigation.state.params.snapshotValue.offer_id,
                                    token: this.state.amount,
                                    "type": "business"
                                }).then((tokenTrackAdd) => {
                                    this.setState({
                                        loader: false
                                    })
                                }).catch((err) => {
                                    this.setState({
                                        loader: false
                                    });
                                    Alert.alert("", "Please try again later.");
                                })

                            }).catch((err) => {
                                debugger;
                                this.setState({
                                    loader: false
                                });
                            });
                        } else {
                            this.setState({
                                loader: false
                            });
                            Alert.alert("", "Not enough balance.")
                        }
                    }
                }
            });
        } else {
            Alert.alert('', "You don't have sufficient wallet balance.");
        }

    }


    render() {
        return (
            <Container>
                <StatusBar
                    backgroundColor="#133567"
                />
                {
                    this.state.payToBusiness == false ?
                        <View>
                            <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                            <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                                <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                                    <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                                </TouchableOpacity>

                                <Body style={styleSelf.tac}>
                                    <Text style={commonStyles.headerMiddleText}>Your Smart Wallet</Text>
                                </Body>

                                <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                                </TouchableOpacity>

                            </Header>


                            <ScrollView>
                                <View style={styles.wholeWrappingDiv}>
                                    <View style={styles.wallet}>

                                        <View style={styles.walletItemWallet}>
                                            <Text style={styles.walletText}>LYNK Wallet</Text>
                                            <Image style={{ width: 32, height: 32, marginLeft: 10 }} source={require('../../../img/icons/locallink.png')} />
                                        </View>
                                        <View style={styles.walletBalance}>
                                            <Text style={styles.walletText}>Wallet Balance:</Text>
                                            <Text style={styles.walletInput}>{this.state.availableToken ? this.state.availableToken : 'Not available'} LYNC Tokens</Text>
                                        </View>
                                        <View style={styles.walletItem}>
                                            <Text style={styles.walletText}>Wallet ID {this.state.walletId ? this.state.walletId : 'Not available'}</Text>
                                            {/* <Text style={styles.walletInput}>{this.state.walletId ? this.state.walletId : 'Not available'} </Text> */}
                                        </View>
                                        {/* <View style={styles.walletItem}>
                                            <TextInput style={[styles.textinput]} underlineColorAndroid='#fff' placeholderTextColor="#ddd" placeholder="Text"/>

                                        </View>
                                        <View>
                                            <TouchableOpacity style={styles.signInBtn}>
                                                <Text style={styles.signInBtnTxt}>LOG IN</Text>
                                            </TouchableOpacity>
                                        </View> */}
                                    </View>
                                </View>
                            </ScrollView>
                        </View>
                        :
                        <View>
                            <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                            <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                                <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                                    <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                                </TouchableOpacity>

                                <Body style={styleSelf.tac}>
                                    <Text style={commonStyles.headerMiddleText}>Pay to Business</Text>
                                </Body>

                                <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                                </TouchableOpacity>

                            </Header>


                            <ScrollView>
                                <View style={styles.wholeWrappingDiv}>
                                    <View style={styles.wallet}>

                                        <View style={styles.walletItemWallet}>
                                            <Text style={styles.walletText}>LYNK Wallet</Text>
                                            <Image style={{ width: 32, height: 32, marginLeft: 10 }} source={require('../../../img/icons/locallink.png')} />
                                        </View>
                                        <View style={styles.walletBalance}>
                                            <Text style={styles.walletText}>Wallet Balance:</Text>
                                            <Text style={styles.walletInput}>{this.state.availableToken ? this.state.availableToken : 'Not available'} LYNC Tokens</Text>
                                        </View>
                                        <View style={styles.walletItem}>
                                            <Text style={styles.walletText}>Wallet ID {this.state.walletId ? this.state.walletId : 'Not available'}</Text>
                                            {/* <Text style={styles.walletInput}>{this.state.walletId ? this.state.walletId : 'Not available'} </Text> */}
                                        </View>
                                        <View style={styles.walletItem}>
                                            <Text style={styles.walletText}>Merchant Address {this.props.navigation.state.params && this.props.navigation.state.params.snapshotValue ? this.props.navigation.state.params.snapshotValue.blockchain_address : 'Not available'}</Text>

                                        </View>
                                        <View style={styles.walletItem}>
                                            <TextInput style={[styles.textinput]} underlineColorAndroid='#fff' placeholderTextColor="#ddd" placeholder="Amount" onChangeText={(text) => this.setState({ amount: text })} value={this.state.amount} keyboardType={'numeric'} />

                                        </View>
                                        <View>
                                            <TouchableOpacity style={styles.signInBtn} onPress={() => this.payMoney()}>
                                                <Text style={styles.signInBtnTxt}>Pay</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </ScrollView>
                        </View>

                }

            </Container>
        );
    }
}




export default Wallet;