import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, Alert, AsyncStorage, ScrollView  } from "react-native";
import { Container, Content, Button, ActionSheet, Header, Body, Tabs, Tab } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import defaultStyle from "../../assets/styles";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FSpinner from 'react-native-loading-spinner-overlay';
import ImagePicker from 'react-native-image-crop-picker';
import api from '../../api';
import SplashScreen from 'react-native-splash-screen';
import MultiSelect from 'react-native-multiple-select';
const launchscreenLogo = require('../../../img/logo.png');
import Ionicons from 'react-native-vector-icons/Ionicons';

var BUTTONS = [
    { text: 'camera', icon: "ios-camera", iconColor: "#2c8ef4" },
    { text: 'file', icon: "ios-images", iconColor: "#f42ced" }
];


class BusinessDetails extends Component {
    constructor(params) {
        super(params)
        this.state = {
            business_name: '',
            contact_name: '',
            contact_phone: '',
            website: '',
            loader: false,
            uploadImage: '',
            business_logo: '',
            selectedItems: [],
            items: [],
            imageArray: ['', '', '', ''],
            activeImage: true,
            email: ''
        }
    }

    componentDidMount() {
        SplashScreen.hide();
        api.get('categories/list_category.json').then(res => {
            res.details.map((data) => {
                data.image = res.image_url + data.image;
            })
            this.setState({
                items: res.details,
                loader: false,
            });

        }).catch((err) => {
            this.setState({ loader: false });
            console.log(err);
        })
    }

    fileUploadType(buttonIndex) {
        if (buttonIndex == 0) {
            this.setState({ cameraButton: false });
            ImagePicker.openCamera({
                width: 400,
                height: 300,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                this.setState({
                    business_logo: business_logo,
                });
                this.setState({ loader: true });
                let uri;
                if (!response.path) {
                    uri = response.uri;
                } else {
                    uri = response.path;
                }
                const file = {
                    uri,
                    name: `${Math.floor((Math.random() * 100000000) + 1)}_.png`,
                    type: response.mime || 'image/png',
                };
                this.setState({
                    loader: false,
                    currentImage: file.uri
                });
                this.setState({
                    uploadImage: file.uri,
                });
            }).catch((err) => {
                this.setState({ loader: false });
                this.setState({ cameraButton: true });
            });
        }
        if (buttonIndex == 1) {
            ImagePicker.openPicker({
                width: 400,
                height: 300,
                cropping: true,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                this.setState({
                    business_logo: business_logo,
                    loader: true
                });
                let uri;
                if (!response.path) {
                    uri = response.uri;
                } else {
                    uri = response.path;
                }
                const file = {
                    uri,
                    name: `${Math.floor((Math.random() * 100000000) + 1)}_.png`,
                    type: response.mime || 'image/png',
                };

                this.setState({
                    loader: true,
                    uploadImage: file.uri
                });
            }).catch((err) => {
                this.setState({ loader: false });
            });
        }
    }


    multipleFileUploadType(buttonIndex) {
        if (buttonIndex == 0) {
            this.setState({ cameraButton: false });
            ImagePicker.openCamera({
                width: 400,
                height: 300,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                this.setState({ loader: false });
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                response.business_logo = business_logo;
                let imageArray = this.state.imageArray;
                imageArray.push(response);
                this.setState({ imageArray });
            }).catch((err) => {
                this.setState({ loader: false });
                this.setState({ cameraButton: true });
            });
        }
        if (buttonIndex == 1) {
            ImagePicker.openPicker({
                width: 400,
                height: 300,
                cropping: true,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                this.setState({ loader: false });
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                response.business_logo = business_logo;
                let imageArray = this.state.imageArray;
                imageArray.push(response);
                this.setState({ imageArray });
            }).catch((err) => {
                this.setState({ loader: false });
            });
        }
    }



    uploadedImageChange(buttonIndex, index) {
        if (buttonIndex == 0) {
            this.setState({ cameraButton: false });
            ImagePicker.openCamera({
                width: 400,
                height: 300,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                this.setState({ loader: false });
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                response.business_logo = business_logo;
                let imageArray = this.state.imageArray;
                imageArray[index] = response;
                this.setState({ imageArray });
            }).catch((err) => {
                this.setState({ loader: false });
                this.setState({ cameraButton: true });
            });
        }
        if (buttonIndex == 1) {
            ImagePicker.openPicker({
                width: 400,
                height: 300,
                cropping: true,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                this.setState({ loader: false });
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                response.business_logo = business_logo;
                let imageArray = this.state.imageArray;
                imageArray[index] = response;
                this.setState({ imageArray });
            }).catch((err) => {
                this.setState({ loader: false });
            });
        }
    }



    uploadImageFunction(buttonIndex) {
        this.setState({ loader: true });
        if (buttonIndex == 0) {
            this.setState({ cameraButton: false });
            ImagePicker.openCamera({
                width: 400,
                height: 300,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                this.setState({ loader: false });
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                response.business_logo = business_logo;
                return response;
            }).catch((err) => {
                this.setState({
                    cameraButton: true,
                    loader: false
                });
                return false;
            });
        }
        if (buttonIndex == 1) {
            ImagePicker.openPicker({
                width: 400,
                height: 300,
                cropping: true,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                this.setState({ loader: false });
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                response.business_logo = business_logo;
                return response;
            }).catch((err) => {
                this.setState({ loader: false });
            });
        }
    }

    onSelectedItemsChange = (selectedItems) => {
        this.setState({ selectedItems });
        console.log(this.state.selectedItems);
    };




    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#fff"
                    hidden={true}
                />

                <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText}>Manage Business</Text>
                    </Body>

                    <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                    </TouchableOpacity>

                </Header>

                
                <Tabs
                    locked={true}
                    tabBarUnderlineStyle={{ backgroundColor: '#3ab3ce', height: 2, borderBottomWidth: 0 }}
                >
                    <Tab heading={'Business Details'} tabStyle={{ backgroundColor: '#f6f6f6', }} textStyle={{ color: '#888888', fontSize: 11 }} activeTabStyle={{ backgroundColor: '#f6f6f6' }} activeTextStyle={{ color: '#3ab3ce', fontSize: 11 }}>
                        <Content style={[styles.mainContenne, { backgroundColor: '#fff' }]}>
                            <FSpinner loader={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                            <View style={{ backgroundColor: '#fff', alignItems: 'center', paddingTop: 15, paddingBottom: 15 }}>
                                <Text>Add New Business</Text>
                            </View>
                            <View style={styles.inputFieldMainWarp}>
                                <View style={[styles.inputWarp]}>
                                    <FontAwesome name='building-o' style={styles.inputIcon} />
                                    <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Business Name' onChangeText={(text) => this.setState({ business_name: text })} value={this.state.business_name} />
                                </View>
                                <View style={[styles.inputWarp]}>
                                    <MaterialIcons name='person' style={styles.inputIcon} />
                                    <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Contact Name' onChangeText={(text) => this.setState({ contact_name: text })} value={this.state.contact_name} />
                                </View>
                                <View style={[styles.inputWarp]}>
                                    <MaterialIcons name='phone' style={styles.inputIcon} />
                                    <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Contact Phone' onChangeText={(text) => this.setState({ contact_phone: text })} value={this.state.contact_phone} keyboardType={'numeric'} />
                                </View>
                                <View style={[styles.inputWarp]}>
                                    <MaterialIcons name='email' style={styles.inputIcon} />
                                    <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Email' onChangeText={(text) => this.setState({ email: text })} value={this.state.email} />
                                </View>
                                <View style={[styles.inputWarp]}>
                                    <MaterialIcons name='web' style={styles.inputIcon} />
                                    <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Website' onChangeText={(text) => this.setState({ website: text })} value={this.state.website} />
                                </View>
                                <View style={{ flex: 1, paddingBottom: 0, paddingLeft: 5, borderBottomColor: '#ccc' }}>
                                    <MultiSelect
                                        items={this.state.items}
                                        onSelectedItemsChange={this.onSelectedItemsChange}
                                        selectedItems={this.state.selectedItems}
                                        selectText="Select Category"
                                        searchInputPlaceholderText="Search Categorys..."
                                        displayKey="name"
                                        uniqueKey="id"
                                        selectedItemTextColor="#0077b5"
                                        selectedItemIconColor="#0077b5"
                                        itemFontSize={12}
                                        autoFocusInput={false}
                                        hideTags
                                        searchInput={false}
                                        hideSubmitButton
                                    />
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 15 }}>
                                    <View>
                                        <Text style={[styles.heading, { width: '100%' }]}>Business Logo</Text>
                                    </View>
                                    <View>
                                        <TouchableOpacity
                                            style={{ borderWidth: 1, borderColor: '#e2e2e2', paddingLeft: 15, paddingRight: 15, paddingTop: 5, paddingBottom: 5, marginLeft: 15, borderRadius: 4 }}
                                            onPress={() =>
                                                ActionSheet.show(
                                                    {
                                                        options: BUTTONS,
                                                    },
                                                    (buttonIndex) => {
                                                        this.setState({ clicked: BUTTONS[buttonIndex] });
                                                        this.fileUploadType(buttonIndex);
                                                    },
                                                )}
                                        >
                                            <Text style={{ color: '#e2e2e2' }}>Upload logo</Text>

                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    {
                                        this.state.uploadImage ? (
                                            <Image source={{ uri: this.state.uploadImage }} style={{ height: 80, width: 80, borderWidth: 1, borderColor: '#e2e2e2', marginBottom: 20, borderColor: '#e2e2e2' }} />
                                        ) : (<View style={{ height: 80, width: 80, borderWidth: 1, borderColor: '#e2e2e2', marginBottom: 20, borderColor: '#e2e2e2', alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ color: '#e2e2e2', textAlign: 'center' }}>Business Logo</Text>
                                        </View>)

                                    }
                                </View>
                                <View>
                                    <Text style={styles.heading}>Business Image (Max 4)</Text>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 15, marginBottom: 10 }}>
                                        {
                                            this.state.imageArray.length != 0 ? (
                                                this.state.imageArray.map((data, key) => {
                                                    return (
                                                        // <TouchableOpacity style={{ borderWidth: 1, borderColor: '#000', height: 50, width: 50 }} key={key} onPress={() => this.uploadedImageChange(`${key}`)}><Image source={{ uri: data.path }} style={{height: 50, width: 50}} /></TouchableOpacity>    
                                                        <TouchableOpacity
                                                            style={{
                                                                borderWidth: 1, borderColor: '#e2e2e2', height: 50, width: 50, marginRight: 8, alignItems: 'center', justifyContent: 'center'
                                                            }}
                                                            key={key}
                                                            onPress={() =>
                                                                ActionSheet.show(
                                                                    {
                                                                        options: BUTTONS,
                                                                    },
                                                                    (buttonIndex) => {
                                                                        this.setState({ clicked: BUTTONS[buttonIndex] });
                                                                        this.uploadedImageChange(buttonIndex, `${key}`);
                                                                    },
                                                                )}
                                                        >
                                                            {/* <Image source={{ uri: data.path }} style={{ height: 50, width: 50 }} /> */}
                                                            {
                                                                data.path ? (
                                                                    <Image source={{ uri: data.path }} style={{ height: 50, width: 50 }} />
                                                                ) : (<Text style={{ color: '#e2e2e2' }}>+</Text>)
                                                            }


                                                        </TouchableOpacity>
                                                    )
                                                })
                                            ) : null
                                        }
                                    </View>
                                </View>
                                <TouchableOpacity style={[styles.signInBtn, { marginBottom: 15 }]} onPress={() => this.addBusiness()}>
                                    <Text style={styles.signInBtnTxt}> ADD </Text>
                                </TouchableOpacity>
                            </View>
                        </Content>
                    </Tab>

                    <Tab heading={'Add Outlet'} tabStyle={{ backgroundColor: '#f6f6f6', }} textStyle={{ color: '#888888', fontSize: 11 }} activeTabStyle={{ backgroundColor: '#f6f6f6' }} activeTextStyle={{ color: '#3ab3ce', fontSize: 11 }}>
                        <Content style={styles.homeContent}>
                        <View style={{ alignItems: 'flex-end' }}>
                                <TouchableOpacity style={{ padding: 10 }} onPress={() => this.props.navigation.navigate('AddOutlet')}>
                                <Text style={{ color: '#3ab3ce' }}>Add New Outlet</Text>
                            </TouchableOpacity>
                        </View>
                            <View style={[styles.tabItemWarp, { paddingBottom: 0, marginBottom: 0 }]}>
                                <View style={{ width: 15 }}></View>
                                <View style={styles.tabItemMdlTextWarp}>
                                    <Text style={[styles.tabItemName, { color: '#000' }]}>Outlet Name</Text>
                                </View>
                                <View style={styles.tabItemMdlTextWarp}>
                                    <Text style={[styles.tabItemName, { color: '#000' }]}>Address</Text>
                                </View>
                                <View style={{ padding: 4 }}>
                                    <Text style={{ fontSize: 12, color: '#000' }}>Status</Text>
                                </View>
                                <TouchableOpacity style={[styles.seeOffer, { backgroundColor: 'transparent', borderColor: 'transparent' }]} activeOpacity={1}>
                                    <Text style={[styles.seeOfferText, { color: '#000' }]}>Action</Text>
                                </TouchableOpacity>
                            </View>
                        
                            <View style={styles.tabItemWarp}>
                                <View style={{ width: 15 }}>
                                    <Text numberOfLines={1}>1</Text>
                                </View>
                                <View style={styles.tabItemMdlTextWarp}>
                                    <Text style={styles.tabItemName}>the Walk</Text>
                                </View>
                                <View style={styles.tabItemMdlTextWarp}>
                                    <Text style={[styles.tabItemName,{fontSize: 10}]}>10th Street - Abu Dhabi - UAE</Text>
                                </View>
                                <View style={{ padding: 4 }}>
                                    <Text style={{ fontSize: 12 }}>Pending</Text>
                                </View>
                                <TouchableOpacity style={[styles.seeOffer, { backgroundColor: 'transparent', borderColor: 'transparent' }]} onPress={() => this.props.navigation.navigate('EditBusiness', { businessesId: item.id })}>
                                    <Text style={[styles.seeOfferText, { color: '#888888' }]}>Edit</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.tabItemWarp}>
                                <View style={{ width: 15 }}>
                                    <Text numberOfLines={1}>2</Text>
                                </View>
                                <View style={styles.tabItemMdlTextWarp}>
                                    <Text style={styles.tabItemName}>the Walk</Text>
                                </View>
                                <View style={styles.tabItemMdlTextWarp}>
                                    <Text style={[styles.tabItemName, { fontSize: 10 }]}>10th Street - Abu Dhabi - UAE</Text>
                                </View>
                                <View style={{ padding: 4 }}>
                                    <Text style={{ fontSize: 12 }}>Pending</Text>
                                </View>
                                <TouchableOpacity style={[styles.seeOffer, { backgroundColor: 'transparent', borderColor: 'transparent' }]} onPress={() => this.props.navigation.navigate('EditBusiness', { businessesId: item.id })}>
                                    <Text style={[styles.seeOfferText, { color: '#888888' }]}>Edit</Text>
                                </TouchableOpacity>
                            </View>
                            
                            <View style={{ marginTop: 20 }}></View>
                        </Content>
                    </Tab>

                </Tabs>
            </Container>
        );
    }
}


export default BusinessDetails;
