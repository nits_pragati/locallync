const React = require("react-native");
const { StyleSheet, Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
    accountMainWarp:{ 
        paddingLeft: 15, 
        paddingRight: 15 
    },
    accountItemWarp: { 
        flexDirection: 'row', 
        borderBottomColor: '#e9e9e9', 
        borderBottomWidth: 1, 
        padding: 10, 
        alignItems: 'center' 
    },
    accountItemIcon:{ 
        color: '#999999', 
        fontSize: 14, 
        fontSize: 18
    },
    accountItemText: { 
        color: '#999999', 
        paddingLeft: 10, 
        flex: 1 
    },
    tabItemWarp: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 10,
        paddingBottom: 10,
        borderBottomColor: '#f3f3f3',
        borderBottomWidth: 0
    },
    tabItemImageWarp: {
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#e1e1e1',
        height: 45,
        width: 45,
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabItemImage: {
        width: 40,
        height: 40
    },
    tabItemMdlTextWarp: {
        flex: 1,
        marginLeft: 15,
        justifyContent: 'center'
    },
    tabItemName: {
        fontSize: 13
    },
    tabItemPrice: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    tabItemPriceText: {
        lineHeight: 12,
        fontSize: 11
    },
    prsnt: {
        height: 15,
        width: 9,
        marginRight: 10
    },
    seeOffer: {
        borderWidth: 1,
        borderColor: '#0077b5',
        borderRadius: 10,
        paddingLeft: 15,
        paddingRight: 15,
        backgroundColor: '#0077b5',
        width: 80,
        alignItems: 'flex-end'
    },
    seeOfferText: {
        fontSize: 14,
        color: '#ffffff'
    },
    inputWarp: {
        borderWidth: 1,
        borderColor: '#e2e2e2',
        borderRadius: 40,
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        marginBottom: 15,
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputIcon: {
        fontSize: 20,
        color: '#888888'
    },
    input: {
        paddingLeft: 10,
        paddingRight: 10,
        flex: 1,
        height: 40,
        paddingBottom: 5,
        paddingTop: 8,
        fontSize: 12
    },
    signInBtnTxt: {
        color: '#fff',
        fontSize: 13
    },
    inputFieldMainWarp: {
        padding: 10
    },
    signInBtn: {
        backgroundColor: '#3ab3ce',
        borderRadius: 40 / 2,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15
    },
    addBtn: {
        backgroundColor: '#ff9800',
        borderRadius: 0
    },
    heading: { fontSize: 12 }
};
