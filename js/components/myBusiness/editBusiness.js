import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, AsyncStorage, ScrollView } from "react-native";
import { Container, Header, Body, Content, Tabs, Tab, TabHeading, ScrollableTab, Footer, FooterTab, Button, ActionSheet } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import FSpinner from 'react-native-loading-spinner-overlay';
import ImagePicker from 'react-native-image-crop-picker';
import MultiSelect from 'react-native-multiple-select';
import api from '../../api';
var BUTTONS = [
    { text: 'camera', icon: "ios-camera", iconColor: "#2c8ef4" },
    { text: 'file', icon: "ios-images", iconColor: "#f42ced" }
];

class EditBusiness extends Component {
    constructor(params) {
        super(params)
        this.state = {
            businessList: [],
            businessesId: this.props.navigation.state.params.businessesId ? this.props.navigation.state.params.businessesId : '',  
            details: '',
            business_name: '',  
            business_name_error: false, 
            contact_name: '',  
            contact_name_error: false,  
            contact_phone: '',  
            contact_phone_error: false ,
            website: '',
            website_error: false ,
            business_logo: '',
            uploadImage: '',
            selectedItems: [],
            imageArray: [],
            business_images: [],
            image_url: ''
            
        }
    }

    componentDidMount() {
            this.setState({ loader: true })
            api.post('UserBusinessess/BusinessDetails.json', { id: this.state.businessesId }).then(res => {
                console.log(res);
                let selectedItemsArray=[];
                res.details.user_business_categories.map((item)=>{
                    selectedItemsArray.push(item.category_id);
                });
                this.setState({
                    details: res.details,
                    loader: false,
                    business_name: res.details.business_name,
                    contact_name: res.details.contact_name,
                    contact_phone: res.details.contact_phone,
                    website: res.details.website,
                    uploadImage: res.image_url + res.details.business_logo,
                    selectedItems: selectedItemsArray,
                    business_images: res.details.business_images,
                    image_url: res.image_url
                });
                this.setState({ loader: true });
                api.get('categories/list_category.json').then(reslist_category => {
                    reslist_category.details.map((data) => {
                        reslist_category.image = res.image_url + reslist_category.image;
                    })
                    this.setState({
                        items: reslist_category.details,
                        loader: false,
                    });
                }).catch((err) => {
                    this.setState({ loader: false });
                    console.log(err);
                })
            }).catch((err) => {
                this.setState({ loader: false });
                console.log(err);
            })
    }

    business_update(){
        this.setState({
            business_name_error: false,
            contact_phone_error: false,
            website_error: false
        });
        let alidationStatus = true;
        const id = this.state.businessesId;        

        const business_name = this.state.business_name.trim();
        const contact_name = this.state.contact_name.trim();
        const contact_phone = this.state.contact_phone.trim();
        const website = this.state.website.trim();
        let category_id = [];
        this.state.selectedItems.map((item)=>{
            let newItem = { "cid": item }
            category_id.push(newItem);
        })

        if (business_name == ''){
            this.setState({
                business_name_error: true
            })
            if (alidationStatus){
                alidationStatus = false;
            }
        }
        if (contact_name == '') {
            this.setState({
                contact_name_error: true
            })
            if (alidationStatus) {
                alidationStatus = false;
            }
        }
        if (contact_phone == '' || contact_phone.length != 10) {
            this.setState({
                contact_phone_error: true
            })
            if (alidationStatus) {
                alidationStatus = false;
            }
        }
        if (website == '') {
            this.setState({
                website_error: true
            })
            if (alidationStatus) {
                alidationStatus = false;
            }
        }

        if (!alidationStatus){
            return;
        }

        // debugger;

        AsyncStorage.getItem('UserDetails', (err, UserDetails) => {
        let data = JSON.parse(UserDetails);
        const user_id = data.details.id + "";
            let updateBusiness = {
                id: id, business_name: business_name, contact_name: contact_name, contact_phone: contact_phone, website: website, is_verified: "1", user_id: user_id, category_id: category_id
            }
            // if (business_logo){
            //     updateBusiness = {
            //         id: id, business_logo: business_logo, business_name: business_name, contact_name: contact_name, contact_phone: contact_phone, website: website, is_verified: "1", user_id: user_id, category_id: category_id
            //     }
            // } else{
            //     updateBusiness = {
            //         id: id, business_name: business_name, contact_name: contact_name, contact_phone: contact_phone, website: website, is_verified: "1", user_id: user_id, category_id: category_id
            //     }
            // }
            this.setState({ loader: true });
            api.post('UserBusinessess/updateBusiness.json', updateBusiness).then(res => {
            this.props.navigation.navigate('MyBusiness');
            this.setState({
                loader: false,
            });
        }).catch((err) => {
            this.setState({ loader: false });
            console.log(err);
        })
        })
    }

    fileUploadType(buttonIndex) {
        if (buttonIndex == 0) {
            this.setState({ cameraButton: false });
            ImagePicker.openCamera({
                width: 400,
                height: 300,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                this.setState({
                    business_logo: business_logo,
                });
                this.setState({ loader: true });
                let uri;
                if (!response.path) {
                    uri = response.uri;
                } else {
                    uri = response.path;
                }
                const file = {
                    uri,
                    name: `${Math.floor((Math.random() * 100000000) + 1)}_.png`,
                    type: response.mime || 'image/png',
                };
                this.setState({
                    loader: false,
                    currentImage: file.uri
                });
                this.setState({
                    uploadImage: file.uri,
                });
            }).catch((err) => {
                this.setState({ loader: false });
                this.setState({ cameraButton: true });
            });
        }
        if (buttonIndex == 1) {
            ImagePicker.openPicker({
                width: 400,
                height: 300,
                cropping: true,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                this.setState({
                    business_logo: business_logo,
                    loader: true
                });
                let uri;
                if (!response.path) {
                    uri = response.uri;
                } else {
                    uri = response.path;
                }
                const file = {
                    uri,
                    name: `${Math.floor((Math.random() * 100000000) + 1)}_.png`,
                    type: response.mime || 'image/png',
                };

                this.setState({
                    loader: true,
                    uploadImage: file.uri
                });
            }).catch((err) => {
                this.setState({ loader: false });
            });
        }
    }


    uploadNewImage(buttonIndex) {
        if (buttonIndex == 0) {
            this.setState({ cameraButton: false });
            ImagePicker.openCamera({
                width: 400,
                height: 300,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                this.setState({ loader: false });
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                this.setState({ loader: true });
                api.post('UserBusinessess/addImageBusiness.json', { business_id: this.state.businessesId, business_logo: business_logo }).then(res => {
                    if (res.ack == 1) {
                        res.image = res.image_name;
                        res.id = res.image_id;
                        let business_images = this.state.business_images;
                        business_images.push(res);
                        this.setState({
                            business_images: business_images
                        })
                    }
                    
                    this.setState({ loader: false });

                }).catch((err) => {
                    this.setState({ loader: false });
                    console.log(err);
                })
            }).catch((err) => {
                this.setState({ loader: false });
                this.setState({ cameraButton: true });
            });
        }
        if (buttonIndex == 1) {
            ImagePicker.openPicker({
                width: 400,
                height: 300,
                cropping: true,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                this.setState({ loader: false });
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                api.post('UserBusinessess/addImageBusiness.json', { business_id: this.state.businessesId, business_logo: business_logo }).then(res => {
                    if(res.ack==1){
                        res.image = res.image_name;
                        res.id = res.image_id;
                        let business_images = this.state.business_images;
                        business_images.push(res);
                        this.setState({
                            business_images: business_images
                        })
                    }
                    this.setState({
                        loader: false
                    });

                }).catch((err) => {
                    this.setState({ loader: false });
                    console.log(err);
                })
            }).catch((err) => {
                this.setState({ loader: false });
            });
        }
    }



    uploadedImageChange(buttonIndex, id) {
        console.log(this.state.business_images);
        if (buttonIndex == 0) {
            this.setState({ cameraButton: false });
            ImagePicker.openCamera({
                width: 400,
                height: 300,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                this.setState({ loader: false });
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                this.setState({ loader: true });
                api.post('UserBusinessess/updateImageBusiness.json', { id: id, business_logo: business_logo }).then(res => {
                    // debugger;
                    if (res.ack == 1) {
                        res.image = res.image_name;
                        let business_images = this.state.business_images;
                        business_images.map((data) => {
                            if (data.id == id) {
                                data.image = res.image_name;
                            }
                        });
                        this.setState({
                            business_images: business_images
                        })
                    }
                    this.setState({
                        loader: false
                    });

                }).catch((err) => {
                    this.setState({ loader: false });
                    console.log(err);
                })
            }).catch((err) => {
                this.setState({ loader: false });
                this.setState({ cameraButton: true });
            });
        }
        if (buttonIndex == 1) {
            ImagePicker.openPicker({
                width: 400,
                height: 300,
                cropping: true,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                this.setState({ loader: false });
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                api.post('UserBusinessess/updateImageBusiness.json', { id: id, business_logo: business_logo }).then(res => {
                    // debugger;
                    if (res.ack == 1) {
                        res.image = res.image_name;
                        let business_images = this.state.business_images;
                        business_images.map((data)=>{
                            if(data.id == id){
                                data.image = res.image_name;
                            }
                        });
                        this.setState({
                            business_images: business_images
                        })
                    }
                    this.setState({
                        loader: false
                    });

                }).catch((err) => {
                    this.setState({ loader: false });
                    console.log(err);
                })
            }).catch((err) => {
                this.setState({ loader: false });
            });
        }
    }



    uploadImageFunction(buttonIndex) {
        this.setState({ loader: true });
        if (buttonIndex == 0) {
            this.setState({ cameraButton: false });
            ImagePicker.openCamera({
                width: 400,
                height: 300,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                this.setState({ loader: false });
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                response.business_logo = business_logo;
                return response;
            }).catch((err) => {
                this.setState({
                    cameraButton: true,
                    loader: false
                });
                return false;
            });
        }
        if (buttonIndex == 1) {
            ImagePicker.openPicker({
                width: 400,
                height: 300,
                cropping: true,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                this.setState({ loader: false });
                let business_logo = 'data:' + response.mime + ';base64,' + response.data;
                response.business_logo = business_logo;
                return response;
            }).catch((err) => {
                this.setState({ loader: false });
            });
        }
    }

    onSelectedItemsChange = (selectedItems) => {
        this.setState({ selectedItems });
        console.log(this.state.selectedItems);
    };

 


    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText}>EDIT BUSINESS</Text>
                    </Body>

                    <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                    </TouchableOpacity>

                </Header>
                <Content style={{ flex: 1, backgroundColor: '#ffffff' }}>
                    <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                    <View style={styles.topHeadingWarp}>
                        {/* <Text style={styles.topHeading}> ADD BUSSINESS </Text> */}
                    </View>
                    <View style={styles.inputFieldMainWarp}>
                    <Text></Text>

                        {/* <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <TouchableOpacity>

                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() =>
                                    ActionSheet.show(
                                        {
                                            options: BUTTONS,
                                        },
                                        (buttonIndex) => {
                                            this.setState({ clicked: BUTTONS[buttonIndex] });
                                            this.fileUploadType(buttonIndex);
                                        },
                                    )}
                            >
                                {
                                    this.state.uploadImage ? (<Image source={{ uri: this.state.uploadImage }} style={{ height: 100, width: 100, borderWidth: 1, borderColor: '#0077b5', borderRadius: 100 / 2, marginBottom: 20 }} />) : (
                                        <Image source={require('../../../img/icons/add_logo.png')} style={{ height: 100, width: 100, borderWidth: 1, borderColor: '#0077b5', borderRadius: 100 / 2, marginBottom: 20 }} />
                                    )
                                }

                            </TouchableOpacity>
                        </View> */}
                        <ScrollView
                            showsHorizontalScrollIndicator={false}
                            horizontal={true}
                            style={{ marginBottom: 20 }}
                        >
                            {
                                this.state.business_images.length != 0 ? (
                                    this.state.business_images.map((data, key) => {
                                        return (
                                            // <TouchableOpacity style={{ borderWidth: 1, borderColor: '#000', height: 50, width: 50 }} key={key} onPress={() => this.uploadedImageChange(`${key}`)}><Image source={{ uri: data.path }} style={{height: 50, width: 50}} /></TouchableOpacity>    
                                            <TouchableOpacity
                                                style={{
                                                    borderWidth: 1, borderColor: '#000', height: 50, width: 50, marginRight: 8
                                                }}
                                                key={key}
                                                onPress={() =>
                                                    ActionSheet.show(
                                                        {
                                                            options: BUTTONS,
                                                        },
                                                        (buttonIndex) => {
                                                            this.setState({ clicked: BUTTONS[buttonIndex] });
                                                            this.uploadedImageChange(buttonIndex, data.id);
                                                        },
                                                    )}
                                            >
                                                <Image source={{ uri: this.state.image_url + data.image }} style={{ height: 50, width: 50 }} />

                                            </TouchableOpacity>
                                        )
                                    })
                                ) : null
                            }
                            <TouchableOpacity
                                style={{ borderWidth: 1, borderColor: '#e2e2e2', height: 50, width: 50, alignItems: 'center', justifyContent: 'center' }}
                                onPress={() =>
                                    ActionSheet.show(
                                        {
                                            options: BUTTONS,
                                        },
                                        (buttonIndex) => {
                                            this.setState({ clicked: BUTTONS[buttonIndex] });
                                            this.uploadNewImage(buttonIndex);
                                        },
                                    )}
                            >
                                <Text style={{ color: '#e2e2e2' }}>+</Text>

                            </TouchableOpacity>
                            {/* <TouchableOpacity style={{ borderWidth: 1, borderColor: '#000', height: 50, width: 50}} onPress={()=> this.addImageRow()}></TouchableOpacity> */}
                        </ScrollView>
                        <View style={[styles.inputWarp, this.state.business_name_error? { borderColor: 'red' }:null]}>
                            <FontAwesome name='building-o' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Business Name' onChangeText={(text) => this.setState({ business_name: text })} value={this.state.business_name} />
                        </View>

                        <View style={[styles.inputWarp, this.state.contact_name_error ? { borderColor: 'red' } : null]}>
                            <MaterialIcons name='person' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Contact Name' onChangeText={(text) => this.setState({ contact_name: text })} value={this.state.contact_name} />
                        </View>

                        <View style={[styles.inputWarp, this.state.contact_phone_error ? { borderColor: 'red' } : null]}>
                            <MaterialIcons name='phone' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Contact Phone' onChangeText={(text) => this.setState({ contact_phone: text })} value={this.state.contact_phone} keyboardType={ 'numeric' } maxLength={10} />
                        </View>

                        <View style={[styles.inputWarp, this.state.website_error ? { borderColor: 'red' } : null]}>
                            <MaterialIcons name='web' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Website' onChangeText={(text) => this.setState({ website: text })} value={this.state.website} />
                        </View>

                        <View style={{ flex: 1, paddingLeft: 0, paddingBottom: 0, paddingLeft: 20, borderBottomColor: '#ccc' }}>
                            <MultiSelect
                                items={this.state.items}
                                onSelectedItemsChange={this.onSelectedItemsChange}
                                selectedItems={this.state.selectedItems}
                                selectText="Select Category"
                                searchInputPlaceholderText="Search Categorys..."
                                displayKey="name"
                                uniqueKey="id"
                                selectedItemTextColor="#0077b5"
                                selectedItemIconColor="#0077b5"
                                itemFontSize={12}
                                autoFocusInput={false}
                                hideTags
                                searchInput={false}
                                hideSubmitButton
                            />
                        </View>

                        {/* <TouchableOpacity style={styles.signInBtn} onPress={() => this.testFunction()}>
                            <Text style={styles.signInBtnTxt}> test </Text>
                        </TouchableOpacity> */}

                        <TouchableOpacity style={[styles.signInBtn, { marginBottom: 15 }]} onPress={() => this.business_update()}>
                            <Text style={styles.signInBtnTxt}> DONE </Text>
                        </TouchableOpacity>

                    </View>
                </Content>

                <Footer>
                    <FooterTab style={commonStyles.footerTab}>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Home')} >
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/iconHome_noActive.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={commonStyles.footerItemText}>Home</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Connections')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/connection.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={[commonStyles.footerItemText]}>Connection</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Favorite')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/love.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={[commonStyles.footerItemText]}>Favorite</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Notification')}>
                            <View style={[commonStyles.footerItemImageWarp, { position: 'relative', }]}>
                                <Text style={commonStyles.notificationText}>2</Text>
                                <Image source={require('../../../img/icons/notification.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={commonStyles.footerItemText} >Notification</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('MyAccount')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/profile_active.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={[commonStyles.footerItemText, commonStyles.footerItemTextActive]} >Profile</Text>
                        </TouchableOpacity>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}
export default EditBusiness;
