import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, Alert, AsyncStorage, ScrollView } from "react-native";
import { Container, Content, Button, ActionSheet, Header, Body } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import defaultStyle from "../../assets/styles";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FSpinner from 'react-native-loading-spinner-overlay';
import ImagePicker from 'react-native-image-crop-picker';
import api from '../../api';
import SplashScreen from 'react-native-splash-screen';
import MultiSelect from 'react-native-multiple-select';
import Ionicons from 'react-native-vector-icons/Ionicons';
import RNGooglePlaces from "react-native-google-places";
const launchscreenLogo = require('../../../img/logo.png');

var BUTTONS = [
    { text: 'camera', icon: "ios-camera", iconColor: "#2c8ef4" },
    { text: 'file', icon: "ios-images", iconColor: "#f42ced" }
];


class AddOutlet extends Component {
    constructor(params) {
        super(params)
        this.state = {
            google_place_id: '',
            outlet_name: '',
            latitude: '',
            longitude: '',
            district: '',
            city: '',
            country: '',
            phone: '',
            googleRating: '',
            openingHours: '',
            website: '',
            loader: false,
            user_business_id: '',
            business_id: ''
        }
    }

    componentDidMount() {
        SplashScreen.hide();
    }

    AddOutlet(){

        this.setState({ loader: true });

        let outlet_name = this.state.outlet_name;
        let placeid = this.state.google_place_id;
        let district = this.state.district;
        let city = this.state.city;
        let country_name = this.state.country;
        let phone_number = this.state.phone;
        let longitude = this.state.longitude;
        let latitude = this.state.latitude;
        let google_rating = this.state.googleRating;
        let open_hour = this.state.openingHours;
        let user_business_id = this.state.user_business_id;
        let business_id = this.state.business_id;
        let sendData = { 
            outlet_name: outlet_name, 
            outlet_code: "", 
            placeid: placeid, 
            description: "", 
            feature: "", 
            address: "", 
            district: district, 
            city: city, 
            country_name: country_name, 
            longitude: longitude, 
            latitude: latitude, 
            phone_number: phone_number, 
            open_hour: open_hour, 
            google_rating: google_rating, 
            permently_closed: "Yes", 
            user_business_id: user_business_id, 
            business_id: business_id 
        }
        // api.get('UserBusinessess/ListOutlets.json').then(res => {
        //     this.setState({ loader: false });
        // }).catch((err) => {
        //     this.setState({ loader: false });
        // })
    }





    placeId(){
       // RNGooglePlaces.lookUpPlaceByID('ChIJZa6ezJa8j4AR1p1nTSaRtuQ')
        RNGooglePlaces.lookUpPlaceByID('AIzaSyDp8ljbyGfH73Gpa3bjJjkkwX2zhlaDCiM')
            .then((results) => {
                this.setState({
                    google_place_id: results.placeID,
                    outlet_name: results.name,
                    latitude: results.latitude,
                    longitude: results.longitude,
                    phone: results.phoneNumber,
                    googleRating: results.googleRating,
                    website: results.website
                })
            })
            .catch((error) => console.log(error.message));
        // RNGooglePlaces.openAutocompleteModal()
        //     .then((place) => {
        //         console.log(place);
        //         // place represents user's selection from the
        //         // suggestions and it is a simplified Google Place object.
        //     })
        //     .catch(error => console.log(error.message));  // error is a Javascript Error object
    }





    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#fff"
                    hidden={true}
                />

                <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText}>ADD OUTLET</Text>
                    </Body>

                    <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                    </TouchableOpacity>

                </Header>

                <Content style={[styles.mainContenne, { backgroundColor: '#fff' }]}>
                    <FSpinner loader={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                    {/* <View style={{ backgroundColor: '#fff', alignItems: 'center', paddingTop: 15, paddingBottom: 15 }}>
                        <Text>Add New Business</Text>
                    </View> */}
                    <View style={{ padding: 5, borderBottomColor: '#cccccc', borderBottomWidth: 1 }}>
                        <Text style={{ fontSize: 12, textAlign: 'center' }}>If you have Google Place ID, please enter here. </Text>
                        <Text style={{ fontSize: 12, textAlign: 'center' }}>We will fetch the outlet details from Google</Text>
                        <View style={[styles.inputWarp]}>
                            <FontAwesome name='location-arrow' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Google Place ID' onChangeText={(text) => this.setState({ google_place_id: text })} value={this.state.google_place_id} />
                        </View>
                    </View>
                    <View style={styles.inputFieldMainWarp}>
                        <View style={[styles.inputWarp]}>
                            <FontAwesome name='building-o' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Outlet Name' onChangeText={(text) => this.setState({ outlet_name: text })} value={this.state.outlet_name} />
                        </View>
                        <View style={[styles.inputWarp]}>
                            <FontAwesome name='location-arrow' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Latitude' onChangeText={(text) => this.setState({ latitude: text })} value={this.state.latitude} />
                        </View>
                        <View style={[styles.inputWarp]}>
                            <FontAwesome name='location-arrow' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Longitude' onChangeText={(text) => this.setState({ longitude: text })} value={this.state.longitude} />
                        </View>
                        <View style={[styles.inputWarp]}>
                            <FontAwesome name='location-arrow' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='District' onChangeText={(text) => this.setState({ district: text })} value={this.state.district} />
                        </View>
                        <View style={[styles.inputWarp]}>
                            <FontAwesome name='location-arrow' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='City' onChangeText={(text) => this.setState({ city: text })} value={this.state.city} />
                        </View>
                        <View style={[styles.inputWarp]}>
                            <FontAwesome name='location-arrow' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Country' onChangeText={(text) => this.setState({ country: text })} value={this.state.country} />
                        </View>
                        <View style={[styles.inputWarp]}>
                            <MaterialIcons name='phone' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Phone' onChangeText={(text) => this.setState({ phone: text })} value={this.state.phone} keyboardType={'numeric'}/>
                        </View>
                        <View style={[styles.inputWarp]}>
                            <MaterialIcons name='rate-review' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Google Rating' onChangeText={(text) => this.setState({ googleRating: text })} value={this.state.googleRating} />
                        </View>
                        <View style={[styles.inputWarp]}>
                            <FontAwesome name='clock-o' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Opening Hours' onChangeText={(text) => this.setState({ openingHours: text })} value={this.state.openingHours} />
                        </View>
                        <View style={[styles.inputWarp]}>
                            <MaterialIcons name='web' style={styles.inputIcon} />
                            <TextInput style={styles.input} underlineColorAndroid='transparent' placeholder='Website' onChangeText={(text) => this.setState({ website: text })} value={this.state.website} />
                        </View>
                        <TouchableOpacity style={[styles.signInBtn, { marginBottom: 15 }]} onPress={() => this.AddOutlet()}>
                            <Text style={styles.signInBtnTxt}> ADD </Text>
                        </TouchableOpacity>
                        {/* <TouchableOpacity style={[styles.signInBtn, { marginBottom: 15 }]} onPress={() => this.placeId()}>
                            <Text style={styles.signInBtnTxt}> test </Text>
                        </TouchableOpacity> */}
                    </View>
                </Content>
            </Container>
        );
    }
}


export default AddOutlet;
