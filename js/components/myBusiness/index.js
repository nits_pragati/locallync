import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, AsyncStorage } from "react-native";
import { Container, Header, Body, Content, Tabs, Tab, TabHeading, ScrollableTab, Footer, FooterTab, Button, ActionSheet } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'; 
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import api from '../../api';


class MyBusiness extends Component {
    constructor(params) {
        super(params)
        this.state = {
            businessList: [],
        }
    }

    componentDidMount(){
        AsyncStorage.getItem('UserDetails', (err, UserDetails) => {
            let data2 = JSON.parse(UserDetails);
            let user_id = data2.details.id + "";
            this.setState({ loader: true })
            api.post('UserBusinessess/ListBusinessByUser.json', { user_id: user_id }).then(res => {
                if (res.ack == 1) {
                    res.details.map((item) => {
                        if (item.business_images){
                            if (item.business_images.length != 0)
                            item.business_logo = res.image_url + item.business_images[0].image;
                        }
                    })

                    this.setState({
                        businessList: res.details
                    });
                    console.log(this.state.businessList)
                }
                this.setState({
                    loader: false
                });
                
            }).catch((err) => {
                this.setState({ loader: false });
                console.log(err);
            })
        })
    }

    addbusiness(){
        this.props.navigation.navigate('AddNewBusiness');
    }


    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText}>MANAGE BUSINESS</Text>
                    </Body>

                    <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                    </TouchableOpacity>

                </Header>
                <View style={{ backgroundColor: '#fff', alignItems: 'center', paddingTop: 15, paddingBottom: 15 }}>
                    <Text>Registered Businesses</Text>
                </View>

                <Content style={{ flex: 1, backgroundColor: '#ffffff' }}>
                {
                    this.state.businessList.length != 0 ? (<View style={[styles.tabItemWarp, { paddingBottom: 0, marginBottom: 0 }]}>
                        <View style={{ width: 15 }}></View>
                        <View style={[styles.tabItemImageWarp, { borderColor: 'transparent', height: 20 }]}>
                            {/* <Image source={{ uri: item.business_logo }} style={styles.tabItemImage} /> */}
                        </View>
                        <View style={styles.tabItemMdlTextWarp}>
                            <View>
                                <Text style={[styles.tabItemName, { color: '#000' }]}>Business Name</Text>
                            </View>
                        </View>
                        <View style={{ padding: 4 }}>

                            <Text style={{ fontSize: 12, color: '#000' }}>Status</Text>

                        </View>
                        <TouchableOpacity style={[styles.seeOffer, { backgroundColor: 'transparent', borderColor: 'transparent' }]} activeOpacity={1}>
                            <Text style={[styles.seeOfferText, { color: '#000' }]}>Action</Text>
                        </TouchableOpacity>
                    </View>): null
                }
                     
                    
                {
                    this.state.businessList.length != 0 ? (
                        this.state.businessList.map((item, key)=>{
                            return(
                                <View style={styles.tabItemWarp} key={key}>
                                <View style={{ width: 15 }}>
                                    <Text numberOfLines={1}>{key+1}</Text>
                                </View>
                                    <View style={styles.tabItemImageWarp}>
                                        <Image source={{ uri: item.business_logo}} style={styles.tabItemImage} />
                                    </View>
                                    <View style={styles.tabItemMdlTextWarp}>
                                        <View>
                                            <Text style={styles.tabItemName}>{item.business_name}</Text>
                                            {/* <View style={styles.tabItemPrice}>
                                                <Text style={styles.tabItemPriceText}>{item.contact_name}</Text>
                                            </View> */}
                                        </View>
                                    </View>
                                    <View style={{ padding: 4 }}>
                                    {
                                        item.is_verified==1 ? (
                                            <Text style={{ fontSize: 12 }}>Verified</Text>
                                        ):(
                                            <Text style={{ fontSize: 12 }}>Pending</Text>
                                        )
                                    }
                                        
                                    </View>
                                    <TouchableOpacity style={[styles.seeOffer, { backgroundColor: 'transparent', borderColor: 'transparent'}]} onPress={() => this.props.navigation.navigate('EditBusiness', { businessesId: item.id })}>
                                        <Text style={[styles.seeOfferText, { color: '#e2e2e2' }]}>Edit</Text>
                                    </TouchableOpacity>
                                </View>
                            )
                        })
                            
                    ): (
                        <Text style={{ width: '100%', textAlign: 'center' }}> No Data Found </Text>
                    )
                }
                    <View style={{ alignItems: 'center' }}>
                        <TouchableOpacity style={[ styles.signInBtn, styles.addBtn ]} onPress={() => this.addbusiness()}>
                            <Text style={[styles.signInBtnTxt, styles.addBtn]}>Add More Businesses</Text>
                        </TouchableOpacity>
                    </View>
                </Content>
            </Container>
        );
    }
}
export default MyBusiness;
