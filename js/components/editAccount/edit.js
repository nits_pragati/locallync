import React, { Component } from "react";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, Alert, AsyncStorage } from "react-native";
import { Container, Header, Body, Content, Tabs, Tab, TabHeading, ScrollableTab, Footer, FooterTab, Button, Form, Item, Input, Label } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import { Calendar } from 'react-native-calendars';
import api from '../../api';
import { userUpdate } from '../accounts/elements/authActions';
import FSpinner from 'react-native-loading-spinner-overlay';
import ValidationComponent from 'react-native-form-validator';



const profileImage = require('../../../img/icons/no-image-available.png');


class Edit extends Component {
    constructor(params) {
        super(params)
        this.state = {
            userDetails: this.props.auth.data ? this.props.auth.data.details : '',
            editType: this.props.navigation.state.params.evenType ? this.props.navigation.state.params.evenType : '',
            val: '',
            loader: false,
            password: '',
            newPassword: '',
            DOB: this.props.auth.data.birth_month ? this.props.auth.data.birth_month : '',
            // phoneError: flase,
            phone: '',
            phoneError: false
        }
    }
    update() {
        let data = this.props.auth.data;
        let first_name = this.state.userDetails.first_name;
        let last_name = this.state.userDetails.last_name;
        let email = this.state.userDetails.email;
        let phone = this.state.userDetails.phone;
        let customer_code = this.state.userDetails.customer_code;
        let country_name = this.state.userDetails.country_name;
        let city = this.state.userDetails.city;
        let birth_month = this.state.userDetails.birth_month ? this.state.userDetails.birth_month : '';
        let id = this.state.userDetails.id;
        // this.props.navigation.navigate('EditAccount');
        if (this.state.editType == 1) {
            first_name = this.state.val;
            data.details.first_name = first_name;
        }
        if (this.state.editType == 2) {
            last_name = this.state.val;
            data.details.last_name = last_name;
        }
        if (this.state.editType == 3) {
            birth_month = this.state.DOB;
            data.details.birth_month = birth_month;
        }
        if (this.state.editType == 6) {
            phone = this.state.val;
            data.details.phone = phone;
        }
        if (this.state.editType == 7) {
            customer_code = this.state.val;
            data.details.customer_code = customer_code;
        }
        if (this.state.editType == 8) {
            country_name = this.state.val;
            data.details.country_name = country_name;
        }
        if (this.state.editType == 9) {
            city = this.state.val;
            data.details.city = city;
        }
        if (first_name.trim() == '') {
            Alert.alert('', 'Please enter your first name.');
            return false;
        }
        // if (phone.length == 0 || phone.length == 10) {

        // } 
        if(phone.length>0 && phone.length<10) {
            Alert.alert('', 'Phone number should be at least 10 digit.');
            return false;
        }
        // this.validate({
        //     phone: { numeric: true, minlength: 9 }
        //     // customer_code: { required: true }
        // });
        this.setState({ loader: true });
        api.post('users/updateuser_service.json', { first_name: first_name, last_name: last_name, pass: "", birth_month: birth_month, email: email, id: id, phone: phone, customer_code: customer_code, country_name: country_name, city: city }).then(res => {

            if (res.ack == 1) {
                AsyncStorage.setItem('UserDetails', JSON.stringify(data), (err, result) => {
                    this.props.userUpdate(data);
                    console.log(this.props);
                    this.props.navigation.navigate('EditAccount');
                    this.setState({ loader: false });
                });

            }
            console.log(res.res);
        }).catch((err) => {
            this.setState({ loader: false });
            console.log(err);
        });
    }
    onDaySelect(date) {
        this.setState({
            DOB: date.dateString,
        })
    }

    passwordUpdate() {
        if (this.state.password.trim() == '') {
            Alert.alert('Please enter a password');
            return;
        }
        if (this.state.newPassword.trim() == '') {
            Alert.alert('Please enter a new Password');
            return;
        }
        if (this.state.newPassword.trim().length < 6) {
            Alert.alert('The new password length minimum 6 characters');
            return;
        }

        const email = this.state.userDetails.email;
        const password = this.state.password.trim();
        const new_password = this.state.newPassword.trim();
        this.setState({ loader: true });
        api.post('users/change_pass.json', { email: email, password: password, new_password: new_password }).then(res => {
            this.setState({ loader: false });
            Alert.alert(res.message);
            if (res.ack == 1) {
                this.props.navigation.navigate('EditAccount');
            }
        }).catch((err) => {
            this.setState({ loader: false });
            console.log(err);
        })
    }

    componentDidMount() {
        if (this.state.editType == 1) {
            this.setState({ val: this.state.userDetails.first_name, });
        }
        if (this.state.editType == 2) {
            this.setState({ val: this.state.userDetails.last_name, })
        }
        if (this.state.editType == 3) {
            this.setState({ val: this.state.userDetails.first_name, })
        }
        if (this.state.editType == 6) {
            this.setState({ val: this.state.userDetails.phone, })
        }
        if (this.state.editType == 7) {
            this.setState({ val: this.state.userDetails.customer_code, })
        }
        if (this.state.editType == 8) {
            this.setState({ val: this.state.userDetails.country_name, })
        }
        if (this.state.editType == 9) {
            this.setState({ val: this.state.userDetails.city, })
        }
        // if (this.state.editType == 4) {
        //     this.setState({ val: this.state.userDetails.email, })
        // }
        if (this.state.editType == 5) {
            this.setState({ val: this.state.userDetails.birth_month ? this.state.userDetails.birth_month : '' })
        }
    }

    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText}>EDIT ACCOUNT</Text>
                    </Body>

                    <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                    </TouchableOpacity>

                </Header>
                <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />

                <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#ffffff' }} >
                    {
                        this.state.editType == 1 ? (
                            <View style={{ padding: 15 }}>
                                <Text style={{ fontSize: 16, paddingLeft: 10 }}>First Name</Text>
                                <View style={styles.inputWarp}>
                                    {/* <MaterialIcons name='email' style={styles.inputIcon} /> */}
                                    <TextInput style={[styles.input]} underlineColorAndroid='transparent' placeholder='First Name' onChangeText={(text) => this.setState({ val: text })} value={this.state.val} />
                                </View>
                                <TouchableOpacity style={[styles.signInBtn, styles.locationBtn]} onPress={() => this.update()}>
                                    <Text style={[styles.signInBtnTxt, styles.locationBtnTxt]}>UPDATE FIRST NAME</Text>
                                </TouchableOpacity>
                            </View>
                        ) : this.state.editType == 2 ? (
                            <View style={{ padding: 15 }}>
                                <Text style={{ fontSize: 16, paddingLeft: 10 }}>Last Name</Text>
                                <View style={styles.inputWarp}>
                                    {/* <MaterialIcons name='email' style={styles.inputIcon} /> */}
                                    <TextInput style={[styles.input]} underlineColorAndroid='transparent' placeholder='Last Name' onChangeText={(text) => this.setState({ val: text })} value={this.state.val} />
                                </View>
                                <TouchableOpacity style={[styles.signInBtn, styles.locationBtn]} onPress={() => this.update()}>
                                    <Text style={[styles.signInBtnTxt, styles.locationBtnTxt]}>UPDATE LAST NAME</Text>
                                </TouchableOpacity>
                            </View>
                        ) : this.state.editType == 3 ? (
                            <View style={{ padding: 15 }}>
                                <Text style={{ fontSize: 16, paddingLeft: 10, width: '100%', textAlign: 'center', marginBottom: 10 }}>DATE OF BIRTH</Text>
                                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                    <View style={{ borderColor: '#ccc', borderWidth: 1, paddingLeft: 15, paddingRight: 15 }}>
                                        <Calendar
                                            onDayPress={(day) => this.onDaySelect(day)}
                                            monthFormat={'MMM yyyy'}
                                            hideArrows={false}
                                            hideExtraDays={true}
                                            disableMonthChange={false}
                                            markedDates={{
                                                [this.state.DOB]: { selected: true, selectedColor: '#3ab3ce' }
                                            }}
                                            theme={{
                                                backgroundColor: '#ffffff',
                                                calendarBackground: '#ffffff',
                                                textSectionTitleColor: '#2d4150',
                                                selectedDayBackgroundColor: '#2d4150',
                                                selectedDayTextColor: '#ffffff',
                                                todayTextColor: '#3ab3ce',
                                                dayTextColor: '#2d4150',
                                                weekTextColor: '#000',
                                                textDisabledColor: '#ccc',
                                                arrowColor: '#3ab3ce',
                                                textDayFontSize: 14,
                                                textMonthFontSize: 16,
                                                textDayHeaderFontSize: 16,

                                            }}
                                        />
                                    </View>

                                </View>
                                <TouchableOpacity style={[styles.signInBtn, styles.locationBtn, { marginTop: 20 }]} onPress={() => this.update()}>
                                    <Text style={[styles.signInBtnTxt, styles.locationBtnTxt]}>UPDATE DATE OF BIRTH</Text>
                                </TouchableOpacity>
                            </View>
                        ) : this.state.editType == 5 ? (
                            <View style={{ padding: 15 }}>
                                <Text style={{ fontSize: 16, paddingLeft: 10, marginBottom: 20 }}>Change Password</Text>
                                <View style={styles.inputWarp}>
                                    <FontAwesome name='unlock-alt' style={styles.inputIcon} />
                                    <TextInput style={[styles.input]} underlineColorAndroid='transparent' placeholder='old password' onChangeText={(text) => this.setState({ password: text })} value={this.state.password} secureTextEntry />
                                </View>
                                <View style={styles.inputWarp}>
                                    <FontAwesome name='unlock-alt' style={styles.inputIcon} />
                                    <TextInput style={[styles.input]} underlineColorAndroid='transparent' placeholder='new password' onChangeText={(text) => this.setState({ newPassword: text })} value={this.state.newPassword} secureTextEntry />
                                </View>
                                <TouchableOpacity style={[styles.signInBtn, styles.locationBtn]} onPress={() => this.passwordUpdate()}>
                                    <Text style={[styles.signInBtnTxt, styles.locationBtnTxt]}>UPDATE PASSWORD</Text>
                                </TouchableOpacity>
                            </View>
                        )

                                        : this.state.editType == 6 ? (
                                            <View style={{ padding: 15 }}>
                                                <Text style={{ fontSize: 16, paddingLeft: 10 }}>Phone</Text>
                                                <View style={styles.inputWarp}>
                                                    {/* <MaterialIcons name='email' style={styles.inputIcon} /> */}
                                                    <TextInput style={[styles.input]} underlineColorAndroid='transparent' placeholder='Phone' onChangeText={(text) => this.setState({ val: text })} value={this.state.val} keyboardType={'numeric'} maxLength={10} />
                                                </View>
                                                <TouchableOpacity style={[styles.signInBtn, styles.locationBtn]} onPress={() => this.update()}>
                                                    <Text style={[styles.signInBtnTxt, styles.locationBtnTxt]}>UPDATE PHONE</Text>
                                                </TouchableOpacity>
                                            </View>
                                        ) : this.state.editType == 7 ? (
                                            <View style={{ padding: 15 }}>
                                                <Text style={{ fontSize: 16, paddingLeft: 10 }}>Customer Code</Text>
                                                <View style={styles.inputWarp}>
                                                    {/* <MaterialIcons name='email' style={styles.inputIcon} /> */}
                                                    <TextInput style={[styles.input]} underlineColorAndroid='transparent' placeholder='Customer Code' onChangeText={(text) => this.setState({ val: text })} value={this.state.val} />
                                                </View>
                                                <TouchableOpacity style={[styles.signInBtn, styles.locationBtn]} onPress={() => this.update()}>
                                                    <Text style={[styles.signInBtnTxt, styles.locationBtnTxt]}>UPDATE CUSTOMER CODE</Text>
                                                </TouchableOpacity>
                                            </View>
                                        ) : this.state.editType == 8 ? (
                                            <View style={{ padding: 15 }}>
                                                <Text style={{ fontSize: 16, paddingLeft: 10 }}>Country Name</Text>
                                                <View style={styles.inputWarp}>
                                                    {/* <MaterialIcons name='email' style={styles.inputIcon} /> */}
                                                    <TextInput style={[styles.input]} underlineColorAndroid='transparent' placeholder='Country Name' onChangeText={(text) => this.setState({ val: text })} value={this.state.val} />
                                                </View>
                                                <TouchableOpacity style={[styles.signInBtn, styles.locationBtn]} onPress={() => this.update()}>
                                                    <Text style={[styles.signInBtnTxt, styles.locationBtnTxt]}>UPDATE COUNTYR NAME</Text>
                                                </TouchableOpacity>
                                            </View>
                                        ) : this.state.editType == 9 ? (
                                            <View style={{ padding: 15 }}>
                                                <Text style={{ fontSize: 16, paddingLeft: 10 }}>City</Text>
                                                <View style={styles.inputWarp}>
                                                    {/* <MaterialIcons name='email' style={styles.inputIcon} /> */}
                                                    <TextInput style={[styles.input]} underlineColorAndroid='transparent' placeholder='City' onChangeText={(text) => this.setState({ val: text })} value={this.state.val} />
                                                </View>
                                                <TouchableOpacity style={[styles.signInBtn, styles.locationBtn]} onPress={() => this.update()}>
                                                    <Text style={[styles.signInBtnTxt, styles.locationBtnTxt]}>UPDATE CITY</Text>
                                                </TouchableOpacity>
                                            </View>
                                        ) : null


                    }
                </View>

            </Container>
        );
    }
}
Edit.propTypes = {
    auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
}

const mapDispatchToProps = dispatch => ({
    userUpdate: (data) => dispatch(userUpdate(data)),
});
// export default Edit;
export default connect(mapStateToProps, mapDispatchToProps)(Edit);
