import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, Alert, AsyncStorage } from "react-native";
import { Container, Header, Body, Content, Tabs, Tab, TabHeading, ScrollableTab, Footer, FooterTab, Button, Form, Item, Input, Label } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";

import api from '../../api';
import FSpinner from 'react-native-loading-spinner-overlay';

import Ionicons from 'react-native-vector-icons/Ionicons';
import SQLite from "react-native-sqlite-storage";
const profileImage = require('../../../img/icons/no-image-available.png');
const resetAction = NavigationActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({
        routeName: 'Login'
    })],
});

class DeleteAccount extends Component {
    constructor(params) {
        super(params)
        this.state = {
           loader:false,
           sqlLiteDb:''
        }
    }
   
    deleteAccount()
    {
          this.setState({
              loader:true
          });
          AsyncStorage.getItem("UserDetails", (err, result) => {
          if(err)
          {
              this.setState({
                  loader:false
              })
          }
          else
          {
              const userDets=JSON.parse(result);
               api.post('users/deleteaccount.json', {user_id:userDets.details.id}).then((res)=>{
                if(res.ack=="1")
                {
                        AsyncStorage.setItem('UserDetails', '', (err, result) => {});
                         if (this.state.sqlLiteDb) {
                             this.state.sqlLiteDb.transaction(function (tx) {
                                 tx.executeSql('DROP TABLE IF EXISTS businessList');
                                 tx.executeSql('DROP TABLE IF EXISTS businessDetails');
                             })
                         }
                          this.props.navigation.dispatch(resetAction);
                }
                else
                {
                    Alert.alert("", res.message);
                }
               }).catch((err)=>{
                   this.setState({
                       loader:false
                   });
                   Alert.alert("", "Please try again later");
               })
          }
          })
    }

     componentDidMount() {
        
         const that = this;

         if (SQLite) {
             SQLite.openDatabase("localLync.db", "1.0", "Demo", -1, successCb, errorCb);
         }

         function successCb(db) {
             that.setState({
                 sqlLiteDb: db
             });

         }

         function errorCb(err) {
             console.log(err);

         }
     }

    render() {
        return (
             <Container >

                    <StatusBar
                        backgroundColor="#fff"
                        hidden={true}
                    />

                    <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    
                       <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText}>Delete Account</Text>
                    </Body>

                   
                        
                    </Header>

                    <Content style={[styles.mainContenne, { backgroundColor: '#fff' }]}>

                         <FSpinner loader={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                           <Image source={require('../../../img/danger.png')} style={{  height: 60,width: 60,alignSelf:'center', marginTop:30}} />
                        <View style={styles.feedbackHeading1}>
                            <Text style={[styles.feedbackText1, {textAlign:'center'}]}>All information associated with your account and profile (i.e basic info, cards, favourites, etc..)will be permanently deleted.</Text>
                            <Text style={[styles.feedbackText2, {padding:20}]}>Are you sure you want to permanently delete your account?</Text>
                           <View style={{flex:1, flexDirection:'row', justifyContent:'space-around'}}>
                                      <TouchableOpacity style={[styles.signInBtn2]} onPress={() => this.props.navigation.navigate('Home')}>
                <Text style={styles.signInBtnTxt2}>Cancel</Text>
              </TouchableOpacity>
                 <TouchableOpacity style={styles.signInBtn1}  onPress={() => this.deleteAccount()}>
                <Text style={styles.signInBtnTxt1}>Yes</Text>
              </TouchableOpacity>
                           </View>

                    <View style={{alignSelf:'center', padding:15,width:'100%'}}>
                        <TouchableOpacity style={styles.signInBtn3}  onPress={() => this.props.navigation.navigate('Feedback')}>
                <Text style={styles.signInBtnTxt1}>Have Feedback?</Text>
              </TouchableOpacity>
                    </View>

                        </View>
                       
                    </Content>

                </Container>
        );
    }
}

 export default DeleteAccount;

