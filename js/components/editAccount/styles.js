const React = require("react-native");
const { StyleSheet, Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
    accountMainWarp: {
        paddingLeft: 15,
        paddingRight: 15
    },
    accountItemWarp: {
        borderBottomColor: '#e9e9e9',
        borderBottomWidth: 1,
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    accountItemIcon: {
        color: '#999999',
        fontSize: 14,
        fontSize: 18
    },
    accountItemText: {
        color: '#999999',
        paddingLeft: 10,
        flex: 1,
        paddingLeft: 0,
    },
    accountItemTextWarp:{
        flex: 1
    },
    accountItemNext:{
        fontSize: 24,
        color: '#999999'
    },
    inputWarp: {
        borderWidth: 1,
        borderColor: '#e2e2e2',
        borderRadius: 40,
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        marginBottom: 15,
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputIcon: {
        fontSize: 20,
        color: '#888888'
    },
    input: {
        paddingLeft: 10,
        paddingRight: 10,
        flex: 1,
        height: 40,
        paddingBottom: 5,
        paddingTop: 8,
        fontSize: 12
    },
    signInBtn: {
        backgroundColor: '#3ab3ce',
        borderRadius: 40 / 2,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        flexDirection: 'row'
    },
    signInBtnTxt: {
        color: '#fff',
        fontSize: 13
    },
      signInBtn1: {
        backgroundColor: '#3ab3ce',
        borderRadius: 20 / 2,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        flexDirection: 'row',
        width:100
    },
        signInBtn2: {
            backgroundColor: '#3ab3ce',
            borderRadius: 20 / 2,
            height: 40,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: 15,
            flexDirection: 'row',
            width: 100
        },
           signInBtn3: {
               backgroundColor: '#C8C8C8',
               borderRadius: 20 / 2,
               height: 50,
               alignItems: 'center',
               justifyContent: 'center',
               marginTop: 15,
               flexDirection: 'row',
              
           },
    signInBtnTxt1: {
        color: '#fff',
        fontSize: 13
    },
    signInBtnTxt2: {
        color: '#fff',
        fontSize: 13
    },
    signInBtnIcon: {
        paddingRight: 10,
        color: '#fff'
    },
    feedbackHeading1:{
        paddingTop:30
    },
    feedbackText1:{
    fontSize:15
    },
    locationBtn: {
        backgroundColor: '#FCD5B2',
        borderRadius: 0
    },
    locationBtnIcon: {
        color: '#323232'
    },
    locationBtnTxt: {
        color: '#323232'
    },
};
