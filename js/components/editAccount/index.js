import React, { Component } from "react";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavigationActions } from "react-navigation";
import { LogedInData } from '../accounts/elements/authActions';
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, AsyncStorage } from "react-native";
import { Container, Header, Body, Content, Tabs, Tab, TabHeading, ScrollableTab, Footer, FooterTab, Button, Form, Item, Input, Label, ActionSheet } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import FSpinner from 'react-native-loading-spinner-overlay';
import ImagePicker from 'react-native-image-crop-picker';
import api from '../../api';
import { userUpdate } from '../accounts/elements/authActions';
const profileImage = require('../../../img/icons/no-image-available.png');

var BUTTONS = [
    { text: 'camera', icon: "ios-camera", iconColor: "#2c8ef4" },
    { text: 'file', icon: "ios-images", iconColor: "#f42ced" }
];


class EditAccount extends Component {
    constructor(params) {
        super(params)
        this.state = {
            // editOffMode: true,
            userDetails: this.props.auth.data.details ? this.props.auth.data.details: '',
            loader: false,
        }
    }
    componentWillReceiveProps(){
    }

    componentDidMount(){
        console.log('userDetails',this.state.userDetails);
    }
    editItem(even){
        // this.setState({
        //     editOffMode: false,
        // })
        this.props.navigation.navigate('Edit', { evenType: even });
    }

    fileUploadType(buttonIndex) {
        if (buttonIndex == 0) {
            // this.captureFile();
            this.setState({ cameraButton: false });

            ImagePicker.openCamera({
                width: 400,
                height: 300,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                let pimg = 'data:' + response.mime + ';base64,' + response.data;
                this.setState({ visible: true });
                let uri;
                if (!response.path) {
                    uri = response.uri;
                } else {
                    uri = response.path;
                }
                const file = {
                    uri,
                    name: `${Math.floor((Math.random() * 100000000) + 1)}_.png`,
                    type: response.mime || 'image/png',
                };
                this.setState({ visible: false });
                AsyncStorage.getItem('UserDetails', (err, UserDetails) => {
                    let UserDetails_data = JSON.parse(UserDetails);
                    let id = UserDetails_data.details.id;
                    let data = new FormData();
                    data.append('image', file)
                    api.post('users/updateimage_service.json', { id: id, pimg: pimg }).then(ImageRes => {
                        if (ImageRes.ack == 1) {
                            this.props.LogedInData(id).then((resLogin) => {
                                this.setState({
                                    loader: false,
                                    userDetails: resLogin.details
                                });
                            }).catch((err) => {
                                console.log(err);
                                this.setState({
                                    loader: false,
                                });
                            });
                        }

                    }).catch((err) => {
                        this.setState({ loader: false });
                    })
                })
            }).catch((err) => {
                this.setState({ loader: false });
                this.setState({ cameraButton: true });
            });
        }
        if (buttonIndex == 1) {
            // this.attachFile();

            ImagePicker.openPicker({
                width: 400,
                height: 300,
                cropping: true,
                cropping: true,
                includeBase64: true,
                freeStyleCropEnabled: true,
            }).then((response) => {
                let pimg =  'data:' + response.mime + ';base64,' + response.data;
                this.setState({ visible: true });
                let uri;
                if (!response.path) {
                    uri = response.uri;
                } else {
                    uri = response.path;
                }
                const file = {
                    uri,
                    name: `${Math.floor((Math.random() * 100000000) + 1)}_.png`,
                    type: response.mime || 'image/png',
                };

                this.setState({ visible: false });
                AsyncStorage.getItem('UserDetails', (err, UserDetails) => {
                    let UserDetails_data = JSON.parse(UserDetails);                    
                    let id = UserDetails_data.details.id;
                    let data = new FormData();
                    data.append('image', file)
                    api.post('users/updateimage_service.json', { id: id, pimg: pimg}).then(ImageRes => {
                        if (ImageRes.ack == 1){
                            this.props.LogedInData(id).then((resLogin) => {
                                this.setState({
                                    loader: false,
                                    userDetails: resLogin.details
                                });
                            }).catch((err) => {
                                console.log(err);
                                this.setState({
                                    loader: false,
                                });
                            });
                        }
                        
                    }).catch((err) => {
                        this.setState({ loader: false });
                    })
                })
            }).catch((err) => {
                this.setState({ visible: false });
            });
        }
    }

    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText}>EDIT ACCOUNT</Text>
                    </Body>

                    <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                    </TouchableOpacity>

                </Header>
                {
                    this.state.userDetails?(
                        <Content style={{ flex: 1, backgroundColor: '#ffffff' }}>
                            <FSpinner visible={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />					                                                        
                            <View style={{ backgroundColor: '#10acf9', height: 160, alignItems: 'center', justifyContent: 'center' }}>
                                <TouchableOpacity
                                    onPress={() =>
                                        ActionSheet.show(
                                            {
                                                options: BUTTONS,
                                            },
                                            (buttonIndex) => {
                                                this.setState({ clicked: BUTTONS[buttonIndex] });
                                                this.fileUploadType(buttonIndex);
                                            },
                                        )}
                                >
                                {
                                        this.state.userDetails.pimg ? (<Image source={{ uri: this.state.userDetails.image_url + this.state.userDetails.pimg }} style={{ height: 80, width: 80, borderRadius: 80 / 2, borderColor: '#0077b5', borderWidth: 1 }} />):(
                                        <Image source={ profileImage } style={{ height: 80, width: 80, borderRadius: 80 / 2, borderColor: '#0077b5', borderWidth: 1 }} />                                        
                                    )

                                }
                                </TouchableOpacity>
                            </View>
                            <View style={styles.accountMainWarp}>
                                <TouchableOpacity style={styles.accountItemWarp} onPress={() => this.editItem( '1' )}>
                                    <View style={styles.accountItemTextWarp}>
                                        <Text style={styles.accountItemText2}>First Name</Text>
                                        <Text style={styles.accountItemText}>{this.state.userDetails.first_name}</Text>
                                    </View>
                                    <View>
                                        <MaterialIcons style={styles.accountItemNext} name='navigate-next' />
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.accountItemWarp} onPress={() => this.editItem('2')}>
                                    <View style={styles.accountItemTextWarp}>
                                        <Text style={styles.accountItemText2}>Last Name</Text>
                                        <Text style={styles.accountItemText}>{this.state.userDetails.last_name}</Text>
                                    </View>
                                    <View>
                                        <MaterialIcons style={styles.accountItemNext} name='navigate-next' />
                                    </View>
                                </TouchableOpacity>
                                {/* <TouchableOpacity style={styles.accountItemWarp} onPress={() => this.editItem('3')}>
                                    <View style={styles.accountItemTextWarp}>
                                        <Text style={styles.accountItemText2}>Date of Birth</Text>
                                        {
                                            this.state.userDetails.birth_month ? (<Text style={styles.accountItemText}>{this.state.userDetails.birth_month}</Text>): null
                                        }
                                    </View>
                                    <View>
                                        <MaterialIcons style={styles.accountItemNext} name='navigate-next' />
                                    </View>
                                </TouchableOpacity> */}
                                <TouchableOpacity style={styles.accountItemWarp}>
                                    <View style={styles.accountItemTextWarp}>
                                        <Text style={styles.accountItemText2}>Email</Text>
                                        <Text style={styles.accountItemText}>{this.state.userDetails.email}</Text>
                                    </View>
                                    {/* <View>
                                        <MaterialIcons style={styles.accountItemNext} name='navigate-next' />
                                    </View> */}
                                </TouchableOpacity>
                                
                                {

                                    this.state.userDetails.utype == 2 ? (
                                        <TouchableOpacity style={styles.accountItemWarp} onPress={() => this.editItem('6')}>
                                            <View style={styles.accountItemTextWarp}>
                                                <Text style={styles.accountItemText2}>Phone</Text>
                                                <Text style={styles.accountItemText}>{this.state.userDetails.phone}</Text>
                                            </View>
                                            <View>
                                                <MaterialIcons style={styles.accountItemNext} name='navigate-next' />
                                            </View>
                                        </TouchableOpacity>
                                    ) : null
                                }

                                {/* {
                                   
                                    this.state.userDetails.utype == 2? (
                                        <View style={styles.accountItemWarp} onPress={() => this.editItem('7')}>
                                            <View style={styles.accountItemTextWarp}>
                                                <Text style={styles.accountItemText2}>Customer Code</Text>
                                                <Text style={styles.accountItemText}>{this.state.userDetails.customer_code}</Text>
                                            </View>
                                        </View>
                                    ): null
                                } */}
                                
                                <TouchableOpacity style={styles.accountItemWarp} onPress={() => this.editItem('8')}>
                                    <View style={styles.accountItemTextWarp}>
                                        <Text style={styles.accountItemText2}>Country Name</Text>
                                        <Text style={styles.accountItemText}>{this.state.userDetails.country_name}</Text>
                                    </View>
                                    <View>
                                        <MaterialIcons style={styles.accountItemNext} name='navigate-next' />
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.accountItemWarp} onPress={() => this.editItem('9')}>
                                    <View style={styles.accountItemTextWarp}>
                                        <Text style={styles.accountItemText2}>City</Text>
                                        <Text style={styles.accountItemText}>{this.state.userDetails.city}</Text>
                                    </View>
                                    <View>
                                        <MaterialIcons style={styles.accountItemNext} name='navigate-next' />
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.accountItemWarp} onPress={() => this.editItem('5')}>
                                    <View style={styles.accountItemTextWarp}>
                                        <Text style={styles.accountItemText2}>Password</Text>
                                        <Text style={styles.accountItemText}>***********</Text>
                                    </View>
                                    <View>
                                        <MaterialIcons style={styles.accountItemNext} name='navigate-next' />
                                    </View>
                                </TouchableOpacity>

                                <TouchableOpacity style={styles.accountItemWarp} onPress={() => this.props.navigation.navigate('DeleteAccount')}>
                                    <View style={styles.accountItemTextWarp}>
                                     <Text style={{color:'red', textAlign:'center'}}>
                                          Delete Account
                                     </Text>
                                    </View>
                                    
                                </TouchableOpacity>
                            </View>
                        </Content>
                    ):(
                        <Content>
                            <Text>Loading...</Text>
                        </Content>
                    )
                }
                        
                

            </Container>
        );
    }
}


EditAccount.propTypes = {
    auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
}

const mapDispatchToProps = dispatch => ({
    LogedInData: (id) => dispatch(LogedInData(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(EditAccount);
