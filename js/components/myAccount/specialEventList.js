import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, Alert, AsyncStorage, ScrollView } from "react-native";
import { Container, Content, Header, Body, Radio , Right} from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import FSpinner from 'react-native-loading-spinner-overlay';
import api from '../../api';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';



class SpecialEvent extends Component {
    constructor(params) {
        super(params)
        this.state = {
            allselected: false,
            array: ['', '', '', '', '', ''],
            specialEventList: [],
            loader: false,
            user_id: '',
            IsShowDeleteSelectedOffer: false
        }
    }

    componentDidMount() {
       this.setState({
           loader:true
       });
       AsyncStorage.getItem('UserDetails', (err, result)=>{
           if(result)
           {
            const parsedDetails=JSON.parse(result);
            api.post('Users/listevent.json', {user_id:parsedDetails.details.id}).then((events)=>{
            this.setState({
                loader:false
            });
            if(events.ack==1)
            {
               
                events.details.map((item1)=>{
                    const formattedDate = new Date(item1.date);
                    const month = (formattedDate.getMonth() + 1).toString();
                    let month1 = month;
                    const date = (formattedDate.getDate()).toString();
                    let date1 = date;
                    if (month1.length == 1) {
                        month1 = "0" + month1;
                    }
                    if (date1.length == 1) {
                        date1 = "0" + date1;
                    }
                    item1.formattedDate = formattedDate.getFullYear() + "-" + month1 + "-" + date1;
                })
                this.setState({
                    specialEventList: events.details,
                    user_id: parsedDetails.details.id
                })
            }
            }).catch((err1)=>{
            this.setState({
                loader: true
            });
            Alert.alert('', "Please try again.");
            })
           } 
           else
           {
               this.setState({
                   loader:true
               });
             Alert.alert('', "Please login again.");
           }
       })
    }

   


orderEdit(item)
{
   
    this.props.navigation.navigate('AddSpecialEvent', {eventDetails:item})
}

 orderDelete(item) {
     console.log(item)
     Alert.alert(
         '',
         'Are you sure you want to delete this event?',
         [{
                 text: 'Cancel',
                 onPress: () => console.log('Cancel Pressed'),
                 style: 'cancel'
             },
             {
                 text: 'OK',
                 onPress: (() => {
                     this.setState({
                         loader: true
                     });
                     api.post('Users/deleteevent.json', {
                         event_id: item.id
                     }).then(res => {
                        
                         if (res.ack == 1) {
                             api.post('Users/listevent.json', {
                                 user_id: this.state.user_id
                             }).then(res => {
                                 if (res.ack == 1) {
                                    res.details.map((item1) => {
                                        const formattedDate = new Date(item1.date);
                                        const month = (formattedDate.getMonth() + 1).toString();
                                        let month1 = month;
                                         const date = (formattedDate.getDate()).toString();
                                         let date1 = date;
                                        if (month1.length == 1) {
                                            month1 = "0" + month1;
                                        }
                                        if (date1.length == 1) {
                                            date1 = "0" + date1;
                                        }
                                        item1.formattedDate = formattedDate.getFullYear() + "-" + month1 + "-" + date1;
                                    })
                                     this.setState({
                                         loader: false,
                                         specialEventList: res.details
                                     });
                                 } else {
                                     this.setState({
                                         loader: false,
                                         specialEventList:[]
                                     });
                                 }

                             }).catch((err) => {
                                 this.setState({
                                     loader: false
                                 })
                                 console.log(err);
                             })
                         };

                     }).catch((err) => {
                         this.setState({
                             loader: false
                         })
                         console.log(err);
                     })
                 })
             },
         ], {
             cancelable: false
         }
     )
 }



    render() {
        return (
            <Container >

                <StatusBar
                    backgroundColor="#fff"
                    hidden={true}
                />

                <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText}>Special Events</Text>
                    </Body>

                    <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                    </TouchableOpacity>

                </Header>

                <Content style={[styles.mainContenne, { backgroundColor: '#fff' }]}>

                    <FSpinner loader={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                    <View style={{ padding: 10 }}>
                       
                       <View style={styles.spacialEventText}>
                           <Text style={{color:'#000' , fontWeight:'bold', fontSize:16}}>
                               Do you have any special occasions or events coming up?
                           </Text>
                           <Text style={{paddingTop:10}}>
                               Let us know and get  personalized surprise offers from your business connections.
                           </Text>
                            
                       </View>
                        <View style={{ alignItems: 'flex-end' }}>
                            <TouchableOpacity style={styles.addEventTouch} onPress={() => this.props.navigation.navigate('AddSpecialEvent')}>
                                <Text style={styles.addEventText}>Add Event</Text>
                          </TouchableOpacity>
                        </View>
                        <View>
                           
                            <View style={styles.ordersummaryHdr}>
                                <Text style={styles.orderHeader}>Event</Text>
                                <Text style={styles.orderHeader}>Date</Text>
                                <Text style={styles.orderHeader}>Action</Text>
                            </View>
                            <View style={{borderWidth: 1, borderColor:'#D3D3D3', borderRadius:5, marginTop:10}}>
                                {/* <View style={styles.orderRow}>
                                    <Text style={styles.orderTxt}>Wedding</Text>
                                    <Text style={styles.orderTxt}>Jan 28,2018</Text>
                                    <View style={styles.actionBtnWarp}>
                                        <TouchableOpacity style={styles.actionBtn} onPress={() => this.orderDelete(item)}>
                                            <Feather name='trash-2' style={{ color: '#fc0303', fontSize: 18 }} />
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.actionBtn} onPress={() => this.orderEdit(key)}>
                                            <FontAwesome name='pencil-square-o' style={{ color: '#56c028', fontSize: 18 }} />
                                        </TouchableOpacity>

                                    </View>
                                </View>

                                <View style={styles.orderRow}>
                                    <Text style={styles.orderTxt}>Anniversary</Text>
                                    <Text style={styles.orderTxt}>Jan 28,2018</Text>
                                    <View style={styles.actionBtnWarp}>
                                        <TouchableOpacity style={styles.actionBtn} onPress={() => this.orderDelete(item)}>
                                            <Feather name='trash-2' style={{ color: '#fc0303', fontSize: 18 }} />
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.actionBtn} onPress={() => this.orderEdit(key)}>
                                            <FontAwesome name='pencil-square-o' style={{ color: '#56c028', fontSize: 18 }} />
                                        </TouchableOpacity>

                                    </View>
                                </View> */}
                                {this.state.specialEventList && this.state.specialEventList.length && this.state.specialEventList.length>0?(
                                   <View>
                                       {this.state.specialEventList.map((item, key)=>{
                                          return(
                                                  <View style={styles.orderRow} key={key}>
                                    <Text style={styles.orderTxt}>{item.special_event}</Text>
                                    <Text style={styles.orderTxt}>{item.formattedDate}</Text>
                                    <View style={styles.actionBtnWarp}>
                                        <TouchableOpacity style={styles.actionBtn} onPress={() => this.orderDelete(item)}>
                                            <Feather name='trash-2' style={{ color: '#fc0303', fontSize: 18 }} />
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.actionBtn} onPress={() => this.orderEdit(item)}>
                                            <FontAwesome name='pencil-square-o' style={{ color: '#56c028', fontSize: 18 }} />
                                        </TouchableOpacity>

                                    </View>
                                </View>
                                          )
                                       })}
                                   </View>
                                ):<View>
                                    <Text>No records found.</Text>
                                </View>}
                            </View>

                           
                        </View>
                    </View>
                </Content>

            </Container>
        );
    }


}


export default SpecialEvent;
