import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, Alert, AsyncStorage, ScrollView } from "react-native";
import { Container, Content, Header, Body, Radio, Right } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import FSpinner from 'react-native-loading-spinner-overlay';
import api from '../../api';
import Ionicons from 'react-native-vector-icons/Ionicons';

const deviceWidth=Dimensions.width;
class FeedbackRating extends Component {
    constructor(params) {
        super(params)
        this.state = {
            allselected: false,
            array: ['', '', '', '', '', ''],
            userOfferList: [],
            loader: false,
            user_id: '',
            IsShowDate: false,
            selected: '',
            IsSubmittedFeedback:false,
            ratings:0,
            comment:''
        }
    }

    componentDidMount() {

    }


submitFeedback()
{
    if(this.state.comment || this.state.ratings)
    {
      
      console.log(this.props.navigation.state.params);
      this.setState({
          loader:true
      });
      AsyncStorage.getItem("UserDetails").then((userDet)=>{
          if(userDet)
          {
            const parsedUserDet=JSON.parse(userDet);
            const data = {
                business_id: this.props.navigation.state.params.snapshot.businessId,
                comment: this.state.comment,
                ratings: this.state.ratings,
                user_id:parsedUserDet.details.id
            };
            api.post('Reviews/addreviewrating.json', data).then((response)=>{
                this.setState({loader:false, IsSubmittedFeedback:true});
            }).catch((err)=>{
                this.setState({
                    loader:false
                });
                Alert.alert('', 'Please try again later.');
            })

          }
      })
   
      
    }
    else
    {
        Alert.alert('', "Either rating or review is required.");
    }
}







    render() {
        if (!this.state.IsSubmittedFeedback) {
            return (
                <Container >

                    <StatusBar
                        backgroundColor="#fff"
                        hidden={true}
                    />

                    <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                        <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                            <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                        </TouchableOpacity>

                        <Body style={styleSelf.tac}>
                            <Text style={commonStyles.headerMiddleText}>Feedback</Text>
                        </Body>

                        <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                            {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                        </TouchableOpacity>

                    </Header>

                    <Content style={[styles.mainContenne, { backgroundColor: '#fff' }]}>

                        <FSpinner loader={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />

                        <View style={styles.feedbackHeading1}>
                            <Text style={styles.feedbackText1}>Transaction Completed</Text>
                            <Text style={styles.feedbackText2}>Give a review and collect LYNK Tokens</Text>
                              <Text style={styles.feedbackText2}>LYNK can be redeemed at any participating business-use it just like cash!</Text>
                              <Text style={styles.feedbackText2}>How was your experience at McDonalds</Text>
                            <View style={{ paddingTop: 20, width: '100%' }}>
                            <Text>Your rating</Text>
                              <View style={{flex:1, justifyContent:'space-between', width:250, flexDirection:'row'}}>
                                  <TouchableOpacity style={{borderRadius:20, borderColor:'rgba(255, 152, 0, 1)', borderWidth:1}} onPress={()=>this.setState({ratings:1})}>
                                  <View style={{flex:1, flexDirection:'row', alignItems:'center', paddingTop:5,paddingBottom:5,paddingRight:10,paddingLeft:5}}>
                                      <Text style={{paddingRight:5, color:'rgba(255, 152, 0, 1)', lineHeight:12}}> 1</Text>
                                      {
                                          this.state.ratings==1?(
                                             < Ionicons name = "ios-star" style={{color:'rgba(255, 152, 0, 1)',}}/>            
                                          ):
                                          (
                                              < Ionicons name = "ios-star-outline" style={{color:'rgba(255, 152, 0, 1)',}}/>
                                          )
                                      }
                                  
                                  </View>
                                  
                                  </TouchableOpacity>
                                   <TouchableOpacity style={{borderRadius:20, borderColor:'rgba(255, 152, 0, 1)', borderWidth:1}} onPress={()=>this.setState({ratings:2})}>
                                  <View style={{flex:1, flexDirection:'row', alignItems:'center', paddingTop:5,paddingBottom:5,paddingRight:10,paddingLeft:5}}>
                                      <Text style={{paddingRight:5, color:'rgba(255, 152, 0, 1)', lineHeight:12}}> 2</Text>
                                   {
                                          this.state.ratings==2?(
                                             < Ionicons name = "ios-star" style={{color:'rgba(255, 152, 0, 1)',}}/>            
                                          ):
                                          (
                                              < Ionicons name = "ios-star-outline" style={{color:'rgba(255, 152, 0, 1)',}}/>
                                          )
                                      }
                                  </View>
                                  
                                  </TouchableOpacity>
                                  <TouchableOpacity style={{borderRadius:20, borderColor:'rgba(255, 152, 0, 1)', borderWidth:1}} onPress={()=>this.setState({ratings:3})}>
                                  <View style={{flex:1, flexDirection:'row', alignItems:'center', paddingTop:5,paddingBottom:5,paddingRight:10,paddingLeft:5}}>
                                      <Text style={{paddingRight:5, color:'rgba(255, 152, 0, 1)', lineHeight:12}}> 3</Text>
                                     {
                                          this.state.ratings==3?(
                                             < Ionicons name = "ios-star" style={{color:'rgba(255, 152, 0, 1)',}}/>            
                                          ):
                                          (
                                              < Ionicons name = "ios-star-outline" style={{color:'rgba(255, 152, 0, 1)',}}/>
                                          )
                                      }
                                  </View>
                                  
                                  </TouchableOpacity>
                                  <TouchableOpacity style={{borderRadius:20, borderColor:'rgba(255, 152, 0, 1)', borderWidth:1}} onPress={()=>this.setState({ratings:4})}>
                                  <View style={{flex:1, flexDirection:'row', alignItems:'center', paddingTop:5,paddingBottom:5,paddingRight:10,paddingLeft:5}}>
                                      <Text style={{paddingRight:5, color:'rgba(255, 152, 0, 1)', lineHeight:12}}> 4</Text>
                                   {
                                          this.state.ratings==4?(
                                             < Ionicons name = "ios-star" style={{color:'rgba(255, 152, 0, 1)',}}/>            
                                          ):
                                          (
                                              < Ionicons name = "ios-star-outline" style={{color:'rgba(255, 152, 0, 1)',}}/>
                                          )
                                      }
                                  </View>
                                  
                                  </TouchableOpacity>
                                  <TouchableOpacity style={{borderRadius:20, borderColor:'rgba(255, 152, 0, 1)', borderWidth:1}} onPress={()=>this.setState({ratings:5})}>
                                  <View style={{flex:1, flexDirection:'row', alignItems:'center', paddingTop:5,paddingBottom:5,paddingRight:10,paddingLeft:5}}>
                                      <Text style={{paddingRight:5, color:'rgba(255, 152, 0, 1)', lineHeight:12}}> 5</Text>
                                   {
                                          this.state.ratings==5?(
                                             < Ionicons name = "ios-star" style={{color:'rgba(255, 152, 0, 1)',}}/>            
                                          ):
                                          (
                                              < Ionicons name = "ios-star-outline" style={{color:'rgba(255, 152, 0, 1)',}}/>
                                          )
                                      }
                                  </View>
                                  
                                  </TouchableOpacity>
                              </View>
                                <View style={styles.textAreaContainer} >
                                    <TextInput style={styles.textArea}
                                        underlineColorAndroid="transparent"
                                        placeholder={"Please enter your review text here"}
                                        placeholderTextColor={"grey"}
                                        multiline={true}
                                        value={this.state.comment}
                                        onChangeText={(text) => this.setState({ comment: text })}
                                        
                                    />
                                </View>
                            </View>


                        </View>
                        <View style={{ padding: 20 }}>
                            <TouchableOpacity onPress={()=>this.submitFeedback()}>
                                <Text style={{ textAlign: 'center', color: '#3ab3ce' }}>Submit</Text>
                            </TouchableOpacity>
                        </View>
                    </Content>

                </Container>
            );
        }
        else
        {
            return(
                <Container >

                    <StatusBar
                        backgroundColor="#fff"
                        hidden={true}
                    />

                    <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    
                     
                        
                    </Header>

                    <Content style={[styles.mainContenne, { backgroundColor: '#fff' }]}>

                       <View>
                           <TouchableOpacity onPress={()=>this.props.navigation.navigate('Home')}>
                               <Text style={{textAlign:'right', padding:10}}>Close</Text>
                           </TouchableOpacity>
                       </View>

                        <View style={styles.feedbackHeading1}>
                            <Text style={[styles.feedbackText1, {textAlign:'center'}]}>Thank you!</Text>
                            <Text style={[styles.feedbackText2, {padding:20}]}>We recieved your feedback and will use it to further improve.</Text>
                            <Text style={[styles.feedbackText2, { padding: 20 }]}>We won't be able to respond to every feedback individuality however you sure will experience even more improvements & features in next version update.</Text>
                            <Text style={[styles.feedbackText2, { padding: 20 }]}>Our principle is simple, we can do more  together than any one of us alone.</Text>
                           


                        </View>
                       
                    </Content>

                </Container>
            )
        }
        
    }


}


export default FeedbackRating;
