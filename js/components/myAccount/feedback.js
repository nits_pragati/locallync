import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, Alert, AsyncStorage, ScrollView,KeyboardAvoidingView } from "react-native";
import { Container, Content, Header, Body, Radio, Right } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import FSpinner from 'react-native-loading-spinner-overlay';
import api from '../../api';
import Ionicons from 'react-native-vector-icons/Ionicons';
const deviceWidth=Dimensions.width;
class Feedback extends Component {
    constructor(params) {
        super(params)
        this.state = {
            allselected: false,
            array: ['', '', '', '', '', ''],
            userOfferList: [],
            loader: false,
            user_id: '',
            IsShowDate: false,
            selected: '',
            IsSubmittedFeedback:false,
            comment:''
        }
    }

    componentDidMount() {

    }






submitFeedback() {

    if (this.state.comment) {

        this.setState({
            loader: true
        });
        AsyncStorage.getItem("UserDetails").then((userDet) => {
            if (userDet) {
                const parsedUserDet = JSON.parse(userDet);
                // const data = {
                //     business_id: this.props.navigation.state.params.snapshot.businessId,
                //     comment: this.state.comment,
                //     ratings: this.state.ratings,
                //     user_id: parsedUserDet.details.id
                // };
                // api.post('Reviews/addreviewrating.json', data).then((response) => {
                //     this.setState({
                //         loader: false,
                //         IsSubmittedFeedback: true
                //     });
                // }).catch((err) => {
                //     this.setState({
                //         loader: false
                //     });
                //     Alert.alert('', 'Please try again later.');
                // })

                const feedbackData = {
                        comment: this.state.comment,
                        user_id: parsedUserDet.details.id
                    };

                api.post('UserBusinessess/addfeedback.json', feedbackData).then((response) => {
                    debugger
                    if(response.ack ==1){
                        this.setState({
                            loader: false,
                            IsSubmittedFeedback: true
                        });
                    }else{
                        Alert.alert('', 'Please try again later.');
                    }
                    // let toPayAddress = { bizWalletId: business_token_list[i].bizWalletId,
                    //      bizWalletAddress: business_token_list[i].bizWalletAddress, 
                    //      custWalletAddress: business_token_list[i].custWalletAddress,
                    //       amount: Number(business_token_list[i].amount) };
                    // console.warn('toPayAddress',toPayAddress)
                    //     api.walletPost('reward-customer', toPayAddress).then((res) => {
                    //         resolve();
                    //     }).catch((err) => {
                    //         resolve();
                    //     })

                    
                }).catch((err) => {
                    debugger
                    this.setState({
                        loader: false
                    });
                    Alert.alert('', 'Please try again later.');
                })

                // this.setState({
                //     IsSubmittedFeedback:true
                // })

            }
        })


    } else {
        Alert.alert('', "Please give us your feedback.");
    }
}



    render() {
        if (!this.state.IsSubmittedFeedback) {
            return (
                <Container>
                
                    <StatusBar
                        backgroundColor="#fff"
                        hidden={false}
                    />

                    <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                        <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                            <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                        </TouchableOpacity>

                        <Body style={styleSelf.tac}>
                            <Text style={commonStyles.headerMiddleText}>Feedback</Text>
                        </Body>

                        <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                            {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                        </TouchableOpacity>

                    </Header>
                    {/* <KeyboardAvoidingView style={{flex: 0}}> */}
                    <Content style={[styles.mainContenne, { backgroundColor: '#fff' }]}>

                        <FSpinner loader={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />

                        <View style={styles.feedbackHeading1}>
                            <Text style={styles.feedbackText1}>How are we doing?</Text>
                            <Text style={styles.feedbackText2}>Ouick feedback helps us improve to better help you and your community save more</Text>
                            <View style={{ paddingTop: 20, width: '100%' }}>
                                <View style={styles.textAreaContainer} >
                                    <TextInput style={styles.textArea}
                                        underlineColorAndroid="transparent"
                                        placeholder={"Share your experience with us. What went well? What could have gone better?"}
                                        placeholderTextColor={"grey"}
                                        multiline={true}
                                        value={this.state.comment}
                                        onChangeText={(text) => this.setState({ comment: text })}
                                    />
                                </View>
                            </View>


                        </View>
                        <View style={{ padding: 20 }}>
                            <TouchableOpacity style={styles.addEventTouch} onPress={()=>this.submitFeedback()}>
                                <Text style={styles.addEventText}>Submit Feedback</Text>
                            </TouchableOpacity>
                        </View>
                    </Content>
                   {/* </KeyboardAvoidingView> */}
                </Container>
            );
        }
        else
        {
            return(
                <Container >

                    <StatusBar
                        backgroundColor="#fff"
                        hidden={true}
                    />

                    <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    
                     
                        
                    </Header>

                    <Content style={[styles.mainContenne, { backgroundColor: '#fff' }]}>

                       <View>
                           <TouchableOpacity onPress={()=>this.props.navigation.navigate('Home')}>
                               <Text style={{textAlign:'right', padding:10}}>Close</Text>
                           </TouchableOpacity>
                       </View>

                        <View style={styles.feedbackHeading1}>
                            <Text style={[styles.feedbackText1, {textAlign:'center'}]}>Thank you!</Text>
                            <Text style={[styles.feedbackText2, {padding:20}]}>We recieved your feedback and will use it to further improve.</Text>
                            <Text style={[styles.feedbackText2, { padding: 20 }]}>We won't be able to respond to every feedback individuality however you sure will experience even more improvements & features in next version update.</Text>
                            <Text style={[styles.feedbackText2, { padding: 20 }]}>Our principle is simple, we can do more  together than any one of us alone.</Text>
                           


                        </View>
                       
                    </Content>

                </Container>
            )
        }
        
    }


}


export default Feedback;
