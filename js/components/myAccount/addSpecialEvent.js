import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, Alert, AsyncStorage, ScrollView } from "react-native";
import { Container, Content, Header, Body, Radio, Right,Input, Button } from "native-base";
import styles from "./styles";
//import moment from 'moment';
import commonStyles from "../../assets/styles";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import FSpinner from 'react-native-loading-spinner-overlay';
import api from '../../api';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import DatePicker from 'react-native-datepicker';

class AddSpecialEvent extends Component {
    constructor(params) {
        super(params)
        this.state = {
            allselected: false,
            array: ['', '', '', '', '', ''],
            userOfferList: [],
            loader: false,
            user_id: '',
            IsShowDate: false,
            selected:'',
            IsShowDatePicker:false,
            special_event:'',
            List:[],
            date:'',
            btnText:'Add',
            currentDate:'',
            selectYear:'',
            selectYearError:''
        }
    }

    // yearHandler = (selectYear) => {
	// 	if (selectYear.length < 4 ) {
	// 		this.setState({
	// 			selectYear: selectYear,
    //             selectYearError: true,
    //             IsShowDatePicker:false
	// 		});
	// 	} else {
	// 		this.setState({
	// 			selectYear: selectYear,
    //             selectYearError: false,
                
	// 		});
	// 	}

    // }
    
    // openCalander= ()=>{
    //    // alert()
    //   //  this.setState({IsShowDatePicker:true});
    //         const toDays=new Date();
    //         const toDaysMonth=(toDays.getMonth()+1).toString();
    //         let monthNumber;
    //         let yearNumber;
    //         const toDaysDate=toDays.getDate().toString();
    //         let toDaysNumber;
    //         if(toDaysMonth.length==1)
    //         {
    //             monthNumber = "0" + toDaysMonth;
    //         }
    //         else
    //         {
    //             monthNumber=toDaysMonth;
    //         }
    //          if (toDaysDate.length == 1) {
    //              toDaysNumber = "0" + toDaysDate;
    //          } else {
    //              toDaysNumber = toDaysDate;
    //          }
    //         //  if(this.state.selectYear!=''){
    //         //     yearNumber=this.state.selectYear
    //         //  }else{
    //             yearNumber=toDays.getFullYear().toString();
    //         // }

    //         const currentDat = yearNumber + "-" + monthNumber + "-" + toDaysNumber;
           
    //         this.setState({
    //             currentDate: currentDat
    //         })
    

    // }


    componentDidMount() {
    AsyncStorage.getItem("UserDetails", (err, result)=>{

       api.post('Events/getEventList.json').then(resultEvent => {
            debugger;
            if (resultEvent.ack == 1) {
                debugger;
                if (resultEvent.eventlist.length != 0) {
                    resultEvent.eventlist.map((eventList) => {
                           this.state.List.push({name:eventList.event_name,id:eventList.id,isActive:eventList.is_active});
                                                                                   
                    });
                }
                
            }
            this.setState({
                loader: false,
                List:this.state.List
            })
            debugger
        }).catch((err) => {
            debugger;
            this.setState({ loader: false });
            console.log(err);
        })


    if(result)
    {
        const user_id=JSON.parse(result).details.id;
        this.setState({
            user_id:user_id
        })


        const toDays=new Date();
            const toDaysMonth=(toDays.getMonth()+1).toString();
            let monthNumber;
            let yearNumber;
            const toDaysDate=toDays.getDate().toString();
            let toDaysNumber;
            if(toDaysMonth.length==1)
            {
                monthNumber = "0" + toDaysMonth;
            }
            else
            {
                monthNumber=toDaysMonth;
            }
             if (toDaysDate.length == 1) {
                 toDaysNumber = "0" + toDaysDate;
             } else {
                 toDaysNumber = toDaysDate;
             }
            //  if(this.state.selectYear!=''){
            //     yearNumber=this.state.selectYear
            //  }else{
                yearNumber=toDays.getFullYear().toString();
            // }

            const currentDat = yearNumber + "-" + monthNumber + "-" + toDaysNumber;
           
            this.setState({
                currentDate: currentDat
            })
            

       
        if (this.props.navigation.state.params && this.props.navigation.state.params.eventDetails)
        {
           
            this.setState({
                special_event: this.props.navigation.state.params.eventDetails.special_event,
                date: this.props.navigation.state.params.eventDetails.formattedDate,
                btnText:'Save',
               // currentDate: this.props.navigation.state.params.eventDetails.formattedDate
            })
        }
              
    }
})
    }

    // onDaySelect(selectedDate)
    // {
     
    //     this.setState({
           
    //         date: selectedDate.dateString,
    //         currentDate: selectedDate.dateString
    //     })
    // }






addEvent()
{
    
  
     if(this.state.special_event && this.state.date)
     {
         if (this.props.navigation.state.params)
         {
 
            api.post('Users/editevent.json', {
                special_event: this.state.special_event,
                date: this.state.date,
                user_id: this.state.user_id,
                event_id: this.props.navigation.state.params.eventDetails.id
            }).then((res) => {
                if (res.ack == 1) {
                    this.props.navigation.navigate('SpecialEvent')
                } else {
                    Alert.alert("", res.message)
                }
            })
         }
         else
         {
          api.post('Users/specialevent.json', {
              special_event: this.state.special_event,
              date: this.state.date,
              user_id: this.state.user_id
          }).then((res) => {
              if (res.ack == 1) {
                  this.props.navigation.navigate('SpecialEvent')
              } else {
                  Alert.alert("", res.message)
              }
          })
         }
       
     }
     else
     {
         Alert.alert("", "Please select an event and a date.");
     }
}

    setSelectedValue(value, list) {
        debugger
        this.setState({
            special_event: value
        })
        debugger
    }

    render() {
        // const mark = {
        //     [this.state.currentDate]: {
        //         selected: true,
        //         marked: true,
        //         selectedColor: '#133567'
        //     },

        // }
        return (
            <Container >

                <StatusBar
                    backgroundColor="#fff"
                    hidden={true}
                />

                <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText}>Special Events</Text>
                    </Body>

                    <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                    </TouchableOpacity>

                </Header>

                <Content style={[styles.mainContenne, { backgroundColor: '#fff' }]}>

                    <FSpinner loader={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                     

                    <View style={{ alignItems: 'center' , paddingTop:30}}>
                        <TouchableOpacity  >
                            <Text style={{color:'#000000', fontWeight:'bold'}}>Add your special events</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ alignItems:'center', padding:20 }}>
                    <View style={{flex:1, flexDirection:'column', justifyContent:'space-between'}}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.txtev}>Select Event</Text>

                                <View style={styles.pickermstyle}>
                                                       
                                    <Picker 
                                        mode="dropdown"
                                        iosHeader="Select your SIM"                                   
                                        selectedValue={this.state.special_event}
                                        onValueChange={(value)=> this.setSelectedValue(value, this.state.List)}>
                                        {
                                            this.state.List.map((data, key) => {
                                                return (<Picker.Item label={data.name} value={data.name} key={data.id} />)
                                            })
                                        }
                                       
                                    </Picker>
                                </View>
                                {/* <TextInput style={[styles.input]} underlineColorAndroid='transparent' placeholder='old password' /> */}
                            </View>

                            {/* <View style={{ flex:1,flexDirection: 'row', justifyContent:'space-around' }}>
                                <Text style={styles.txtev}>Select Year</Text>
                                {this.state.selectYearError?
                                    <Input style={[styles.inputdtError]} underlineColorAndroid='transparent' keyboardType="numeric" placeholder='Select Year' onChangeText={(selectYear) => this.yearHandler(selectYear)}  value={this.state.selectYear} maxLength={4} />
                                   : <Input style={[styles.inputdt]} underlineColorAndroid='transparent' keyboardType="numeric" placeholder='Select Year' onChangeText={(selectYear) => this.yearHandler(selectYear)}  value={this.state.selectYear} maxLength={4} />
                                }
       
                            </View> */}

                            <View style={{ flex:1,flexDirection: 'row', justifyContent:'space-around' }}>
                                <Text style={styles.txtdt} >Date</Text>
                                {/* {this.state.IsShowDatePicker?( */}
                        
                            <DatePicker
                                    style={{ width: 200 }}
                                    date={this.state.date}
                                    mode="date"
                                    placeholder="select date"
                                    format="YYYY-MM-DD"
                                    minDate={this.state.currentDate}
                                    // maxDate={this.state.currentDate}
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    customStyles={{
                                        dateIcon: {
                                            position: 'absolute',
                                            left: 0,
                                            top: 4,
                                            marginLeft: 0
                                        },
                                        dateInput: {
                                            marginLeft: 36
                                        }
                                        // ... You can check the source to find the other keys.
                                    }}
                                    onDateChange={(date) => { this.setState({ date: date }) }}
                                />
                            
                                {/* ):(
                                 <TextInput style={[styles.inputdt]} underlineColorAndroid='transparent' placeholder='Select Date'  onFocus={() => this.openCalander()} value={this.state.date}/>
                                )} */}
                                
                               
                            </View>
                            <View style={{ alignItems: 'flex-end' }}>
                                <TouchableOpacity style={styles.addEventTouch} onPress={()=>this.addEvent()}>
                                    <Text style={styles.addEventText}>{this.state.btnText}</Text>
                                </TouchableOpacity>
                            </View>
                    </View>
                       
                    </View>
                    
                </Content>
                {/* <View>
                    <Button style={styles.ButtonClick} onPress={() => this.props.navigation.goBack()}><Text style={styles.TextClick}>Profile</Text></Button> 
                </View> */}
            </Container>
        );
    }


}


export default AddSpecialEvent;
