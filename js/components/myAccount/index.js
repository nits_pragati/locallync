import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, AsyncStorage, Alert } from "react-native";
import { Container, Header, Body, Content, Tabs, Tab, TabHeading, ScrollableTab, Footer, FooterTab, Button, } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import SQLite from "react-native-sqlite-storage";
import api from '../../api'
// import * as firebase from 'firebase';
const resetAction = NavigationActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'Login' })],
});
// const firebaseConfig = {
//     apiKey: "AIzaSyBSW6dhfaSEP2pAHGAbWfuZ-kAPF7uA63w",
//     authDomain: "locallync-859ff.firebaseapp.com",
//     databaseURL: "https://locallync-859ff.firebaseio.com",
//     projectId: "locallync-859ff",
//     storageBucket: "locallync-859ff.appspot.com",
//     messagingSenderId: "718735440621"
// };
class MyAccount extends Component {
    constructor(params) {
        super(params)
        this.state = {
            utype: '',
            sqlLiteDb: '',
            //chatRef: '',
            userId:'',
            unreadcount:0
        }

        // if (!firebase.apps.length) {
        //     firebase.initializeApp(firebaseConfig);
        //     this.state.chatRef = firebase.database().ref().child('messages');
        // } else {
        //     this.state.chatRef = firebase.database().ref().child('messages');
        // }
    }
    componentDidMount() {
        AsyncStorage.getItem('UserDetails', (err, result) => {
            let UserDetails = JSON.parse(result);
             api.post('notifications/unreadCount.json', {
                 user_id: UserDetails.details.id
             }).then((notification) => {
                 if (notification.ack == 1) {
                     this.setState({
                         unreadcount: notification.chat_unread+notification.connect_unread+notification.review_unread
                     })
                 }
             })
            this.setState({ utype: UserDetails.details.utype, userId:UserDetails.details.id })
            console.log(UserDetails);
        })
        const that = this;

        if (SQLite) {
            SQLite.openDatabase("localLync.db", "1.0", "Demo", -1, successCb, errorCb);
        }

        function successCb(db) {
            that.setState({
                sqlLiteDb: db
            });

        }

        function errorCb(err) {
            console.log(err);

        }
    }

    logOut() {
        Alert.alert(
            '',
            'Are you sure you want to logout?',
            [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                {
                    text: 'OK', onPress: (() => {
                        if(this.state.sqlLiteDb)
                        {
                            this.state.sqlLiteDb.transaction(function(tx){
                                tx.executeSql('DROP TABLE IF EXISTS businessList');
                                tx.executeSql('DROP TABLE IF EXISTS businessDetails');
                            })
                        }
                        AsyncStorage.getItem("UserDetails", (err, result)=>{
                            if(result)
                            {
                                const data = JSON.parse(result);
                                api.post('Users/UpdateLocation.json', {
                                    id: data.details.id,
                                    device_token_id: '',
                                    latitude: data.details.latitude, longitude: data.details.longitude, is_notify: data.details.is_notify
                                }).then((resEdit) => {
                                    AsyncStorage.setItem('UserDetails', '', (err, result) => { 
                                      
                                        this.props.navigation.dispatch(resetAction);
                                   
                                       
                                        console.log("success");
                                    });
                                   
            
                                }).catch((err) => { 
                                    console.log(err);
                                   
                                });
                            }
                        })
                       
                        // debugger;
                        //  this.state.chatRef.orderByChild('userId').equalTo(this.state.userId).once('value').then((snapshot) => {
                        //      if (snapshot && snapshot.val()) {
                        //          const key = Object.keys(snapshot.val())[0];
                        //          const ref = this.state.chatRef.child(key);
                        //          let toUpdate = snapshot.val();
                        //          toUpdate[key].IsOnline = false;
                 
                        //          ref.update(toUpdate[key]).then((res) => {

                        //          }).catch((Err) => {

                        //          })
                        //      }
                        //  })
                        //this.props.navigation.navigate('Login');
                       
                    })
                },
            ],
            { cancelable: false }
        )
    }


    render() {
        return (
            <Container >
                <StatusBar
                    backgroundColor="#133567"
                />

                <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                    <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                        <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                    </TouchableOpacity>

                    <Body style={styleSelf.tac}>
                        <Text style={commonStyles.headerMiddleText}>SETTING</Text>
                    </Body>

                    <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                        {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                    </TouchableOpacity>

                </Header>
                <Content style={{ flex: 1, backgroundColor: '#ffffff' }}>

                    <View style={styles.accountMainWarp}>
                        <TouchableOpacity style={styles.accountItemWarp} onPress={() => this.props.navigation.navigate('EditAccount')}>
                            <FontAwesome name="user" style={styles.accountItemIcon} />
                            <Text style={styles.accountItemText}>Account</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.accountItemWarp} onPress={() => this.props.navigation.navigate('SpecialEvent')}>
                            <MaterialCommunityIcons name="calendar" style={styles.accountItemIcon} />
                            <Text style={styles.accountItemText}>Special Events</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.accountItemWarp} onPress={() => this.props.navigation.navigate('CustomizeNotification')}>
                            <FontAwesome name="bell-o" style={styles.accountItemIcon} />
                            <Text style={styles.accountItemText}>Customized Notifications</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.accountItemWarp} onPress={() => this.props.navigation.navigate('Wallet')}>
                            <MaterialCommunityIcons name="wallet" style={styles.accountItemIcon} />
                            <Text style={styles.accountItemText}>View Wallet </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.accountItemWarp} onPress={()=>this.props.navigation.navigate('Feedback')}>
                            <FontAwesome name="star-o" style={styles.accountItemIcon} />
                            <Text style={styles.accountItemText}>Give Feedback</Text>
                        </TouchableOpacity>
                        {
                            this.state.utype == 1 ? (
                                <TouchableOpacity style={styles.accountItemWarp} onPress={() => this.props.navigation.navigate('MyBusiness')}>
                                    <FontAwesome name="building-o" style={styles.accountItemIcon} />
                                    <Text style={styles.accountItemText}>My Business</Text>
                                </TouchableOpacity>) : null
                        }
                        <TouchableOpacity style={styles.accountItemWarp} onPress={() => this.logOut()}>
                            <Feather name="log-out" style={styles.accountItemIcon} />
                            <Text style={styles.accountItemText}>Log Out</Text>
                        </TouchableOpacity>
                    </View>
                </Content>

                <Footer>
                    <FooterTab style={commonStyles.footerTab}>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Home')} >
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/iconHome_noActive.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={commonStyles.footerItemText}>Home</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Connections')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/connection.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={[commonStyles.footerItemText]}>Connection</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Favorite')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/love.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={[commonStyles.footerItemText]}>Favorite</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('Notification')}>
                            <View style={[commonStyles.footerItemImageWarp, { position: 'relative', }]}>
                            {this.state.unreadcount != 0 ?
                            <Text style={commonStyles.notificationText}>{this.state.unreadcount}</Text>
                            :
                                null
                            }
                                
                                <Image source={require('../../../img/icons/notification.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={commonStyles.footerItemText} >Notification</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={commonStyles.footerItemWarp} onPress={() => this.props.navigation.navigate('MyAccount')}>
                            <View style={commonStyles.footerItemImageWarp}>
                                <Image source={require('../../../img/icons/profile_active.png')} style={commonStyles.footerItemImage} />
                            </View>
                            <Text style={[commonStyles.footerItemText, commonStyles.footerItemTextActive]} >Profile</Text>
                        </TouchableOpacity>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}
export default MyAccount;
