const React = require("react-native");
const {
    StyleSheet,
    Dimensions,
    Platform
} = React;
const deviceHeight = Dimensions.get("window").height;

export default {
    accountMainWarp: {
        paddingLeft: 15,
        paddingRight: 15
    },
    orderSmryitem: {
        borderRadius: 4,
        backgroundColor: '#64a648',
        flex: 1,
        paddingTop: 10,
        paddingBottom: 10,
        margin: 1
    },
    ordersumaryHead: {
        textAlign: 'center',
        color: '#fff',
    },
    ordersumaryCount: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 12,
    },
    offerBtn: {
        backgroundColor: '#3ab3ce',
        borderRadius: 40 / 2,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15
    },
    offerBtnTxt: {
        color: '#fff',
        fontSize: 10,
        marginLeft: 5
    },
    offerBtnIcon: {
        color: '#fff'
    },
    ordersummaryHdr: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 10
    },
    addEventTouch: {
        borderColor: 'rgb(41, 104, 179)',
        borderWidth: 1,
        borderRadius: 4,
        marginTop: 15,
        marginBottom: 15,
        minWidth: 80
    },
    input: {
        fontSize: 14
    },
    addEventText: {
        color: 'rgb(41, 104, 179)',
        textAlign: 'center',
        paddingTop: 5,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 4
    },
    spacialEventText: {
        padding: 10
    },
    feedbackHeading1: {
        paddingTop: 20,
        padding: 10
    },
    feedbackText1: {
        fontWeight: 'bold',
        color: '#000000',
        fontSize: 20,
        textAlign: 'center',
        lineHeight: 30
    },
    feedbackText2: {
        paddingTop: 10,
        color: '#777777',
        fontSize: 13,
        textAlign: 'center'
    },
    lebel: {
        fontWeight: 'bold',
        color: '#000000',
        fontSize: 14,
        marginBottom: 10
    },
    textAreaContainer: {
        borderColor: 'grey',
        borderWidth: 1
    },
    textArea: {
        height: 150,
        textAlignVertical: 'top'
    },
    orderRow: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: '#D3D3D3',
        height: 50,
        alignItems: 'center',
    },
    orderHeader: {
        color: 'rgb(0, 150, 136)',
        textDecorationLine: 'underline',
        Width: '33%',
        borderWidth: 0,
        flex: 1,
        textAlign: 'center'
    },
    orderRedio: {
        width: 25
    },
    orderWhiteColor: {
        color: '#fff'
    },
    orderTxt: {
        fontSize: 12,
        padding: 5,
        width: '33%',
        textAlign: 'center'
    },
    discountWarp: {
        width: '33%',
    },
    statusWarp: {
        marginLeft: 5,
        width: '33%',
    },
    actionBtnWarp: {
        flexDirection: 'row',
        width: '33%',
        justifyContent: 'center'
    },
    actionBtn: {
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 6,
        paddingRight: 6
    },
    nameWarp: {
        flex: 1,
        marginLeft: 5
    },
    accountItemWarp: {
        flexDirection: 'row',
        borderBottomColor: '#e9e9e9',
        borderBottomWidth: 1,
        padding: 10,
        alignItems: 'center'
    },
    accountItemIcon: {
        color: '#999999',
        fontSize: 14,
        fontSize: 18
    },
    accountItemText: {
        color: '#999999',
        paddingLeft: 10,
        flex: 1
    },
    tabItemWarp: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 6
    },
    tabItemImageWarp: {
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#e1e1e1',
        height: 45,
        width: 45,
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabItemImage: {
        width: 40,
        height: 40
    },
    tabItemMdlTextWarp: {
        flex: 1,
        marginLeft: 15,
        justifyContent: 'center'
    },
    tabItemName: {
        fontSize: 13,
        fontFamily: 'Roboto-Medium'
    },
    tabItemPrice: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    tabItemPriceText: {
        lineHeight: 12,
        fontSize: 10
    },
    prsnt: {
        height: 12,
        width: 12,
        marginRight: 10
    },
    seeOffer: {
        borderWidth: 1,
        borderColor: '#3ab3ce',
        borderRadius: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    seeOfferText: {
        fontSize: 10,
        color: '#3ab3ce'
    },
    hder: {
        color: '#282828',
        fontFamily: 'Roboto-Medium',
        padding: 10,
        backgroundColor: 'rgb(245, 245, 245)'
    },
    border: {
        width: '100%',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 20
    },
    bordercolor: {
        width: 100,
        height: 2,
        backgroundColor: '#ddd'
    },
    circleborder: {
        width: 25,
        height: 25,
        borderRadius: 50,
        transform: [{
            scaleX: 1.5
        }],
        borderColor: 'rgba(255, 152, 0, 1)',
        borderWidth: 1,
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    nottrans: {
        transform: [{
            scaleX: 0
        }],
    },
    startext: {
        color: 'rgba(255, 152, 0, 1)',
        fontSize: 12
    },
    txtev: {
        width: '40%',
        paddingLeft: 0,
        marginTop: 8,
        marginBottom: 15
    },
    txtevError: {
        borderWidth: 1,
        borderColor: 'red',
        width: '40%',
        paddingLeft: 0,
        marginTop: 8,
        marginBottom: 15
    },
    pickermstyle: {
        width: '60%',
        borderWidth: 1,
        marginTop: 3,
        borderColor: '#ccc',
        borderRadius: 6,
        marginBottom: 15,
        padding: 2
    },
    pickerstyle: {
        padding: 3,
        height: 30,
        marginTop: 0,
    },
    txtdt: {
        width: '40%',
        textAlign: 'center',
        marginTop: 5
    },
    inputdt: {
        borderWidth: 1,
        borderColor: '#ccc',
        width: '60%',
        margin: 0,
        height: 34,
        padding: 5,
        color: '#777'

    },
    inputdtError: {
        borderWidth: 1,
        borderColor: 'red',
        width: '60%',
        margin: 0,
        height: 34,
        padding: 5,
        color: '#777'

    },
    ButtonClick:{
        width:'100%',
        backgroundColor: '#0077b5', 
    },
    TextClick:{
        color:'#fff',
        textAlign:'center',
        width:'100%',
        
    }
};
