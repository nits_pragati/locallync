import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, View, StatusBar, TouchableOpacity, Dimensions, Text, TextInput, Picker, Alert, AsyncStorage, ScrollView, Switch } from "react-native";
import { Container, Content, Header, Body, Radio, Right } from "native-base";
import styles from "./styles";
import commonStyles from "../../assets/styles";
import FSpinner from 'react-native-loading-spinner-overlay';
import api from '../../api';
import Ionicons from 'react-native-vector-icons/Ionicons';
const deviceWidth = Dimensions.width;
class CustomizeNotification extends Component {
    constructor(params) {
        super(params)
        this.state = {
            IsAllowNotification:true,
            IsAllowFavouritesNotification:true,
            category_list:[],
            userDetails:''
        }
    }

    componentDidMount() {
        AsyncStorage.getItem("UserDetails", (err, result)=>{
             if(result)
             {
                
                 const data=JSON.parse(result);
                 this.setState({
                     userDetails: data.details,
                     IsAllowNotification: data.details.is_notify==1?true:false
                 })
                 if (data.details.is_notify==1)
                 {
                      api.get('categories/list_category.json').then((reslist_category) => {
                          if (reslist_category.ack == 1) {
                              reslist_category.details.map((item) => {
                                  item.IsAllowed = true;
                              })
                              debugger
                              this.setState({
                                  category_list: reslist_category.details,
                                 // IsAllowFavouritesNotification:true
                              });
                              debugger
                              let toAddData1 = {
                                user_id: this.state.userDetails.id,
                                cat_id: []
                            };
                            let toChangeData1=this.state.category_list;
                            debugger;
                            toChangeData1.map((item) => {
                                item.IsAllowed = true
                                toAddData1.cat_id.push(item.id);
                            })
                            api.post('UserBusinessess/addusercategory.json', toAddData1).then((res) => {
                                debugger;
                                if (res.ack == 1) {
                 
                                }
                            })

                            debugger

                          }
                      })
                 }
                 else
                 {
                    api.post('UserBusinessess/listusercategory.json', {
                        user_id: data.details.id
                    }).then((res) => {
                        if (res.ack == 1) {
                            api.get('categories/list_category.json').then((reslist_category) => {
                                console.log(res)

                                if (reslist_category.ack == 1) {
                                    reslist_category.details.map((item) => {
                                        debugger;

                                        for (let i = 0; i < res.details.length; i++) {
                                            if (res.details[i].cat_id == item.id) {
                                                item.IsAllowed = true;
                                                break;
                                            } else {
                                                item.IsAllowed = false;
                                            }
                                        }
                                    })
                                    this.setState({
                                        category_list: reslist_category.details
                                    });
                                }
                            })
                        } else {
                            api.get('categories/list_category.json').then((reslist_category) => {
                                if (reslist_category.ack == 1) {
                                    reslist_category.details.map((item) => {
                                        item.IsAllowed = false;
                                    })
                                    this.setState({
                                        category_list: reslist_category.details,
                                        IsAllowFavouritesNotification:true
                                    });
                                }
                            })
                        }
                    })
                 }
                 

             }
        })

        

    }

    switchAllNotifications(value)
    {
        let toRemoveData = {
            user_id: this.state.userDetails.id,
            cat_id: []
        };
        let toAddData = {
            user_id: this.state.userDetails.id,
            cat_id: []
        };
        if(value==false)
        {
        let toChangeData=this.state.category_list;
        debugger;
        toChangeData.map((item)=>{
            item.IsAllowed=false
            toRemoveData.cat_id.push(item.id);
            toAddData.cat_id.push(item.id);
        })
 this.setState({
     IsAllowNotification: value,
     IsAllowFavouritesNotification:value,
     category_list:toChangeData
 });
 let toChange
        }
        else
        {
            let toChangeData=this.state.category_list;
            toChangeData.map((item) => {
                item.IsAllowed = true
                toAddData.cat_id.push(item.id);
            })
            this.setState({
                IsAllowNotification: value,
                IsAllowFavouritesNotification: value,
                category_list: toChangeData
            });

            //  this.setState({
            //      IsAllowNotification: value
            //  });
        }
        api.post('Users/UpdateLocation.json', {
            id: this.state.userDetails.id,
            latitude: this.state.userDetails.latitude,
            longitude: this.state.userDetails.longitude,
            device_token_id: this.state.userDetails.device_token_id,
            is_notify: value==true?1:0
        }).then((res1) => {
           AsyncStorage.getItem('UserDetails', (err, localResult)=>{
                 if (localResult)
                 {
                     const parsed = JSON.parse(localResult);
                     parsed.details.is_notify=value==true?1:0;
                     AsyncStorage.setItem("UserDetails", JSON.stringify(parsed));
                 }
           })
        })

        console.log('remove all data',toRemoveData);
        if(value!=true)
        {
           api.post('UserBusinessess/removeusercategory.json', toRemoveData).then((res) => {
               debugger;
               if (res.ack == 1) {

               }
           })
        }else{
            api.post('UserBusinessess/addusercategory.json', toAddData).then((res) => {
                debugger;
                if (res.ack == 1) {
 
                }
            })

        }
        


       
        
       
    }








switchValueChange(item, value)
{
    debugger;
    
    let toPostData={user_id:this.state.userDetails.id, is_notify:1, cat_id:[]};
    let toRemoveData={user_id:this.state.userDetails.id,  cat_id:[]};
    let toChangeValue=this.state.category_list;
    toChangeValue.map((item1)=>{
        if(item1.id==item.id)
        {
            item1.IsAllowed=value;
           if(value==true)
           {
                toPostData.cat_id.push(item1.id.toString());
           }
           else{
               toRemoveData.cat_id.push(item1.id.toString());
           }
        }
    })
    this.setState({
        category_list:toChangeValue
    })
    debugger;
    if(value==true)
    {
        api.post('UserBusinessess/addusercategory.json', toPostData).then((res) => {
            debugger;
            if (res.ack == 1) {

            }
        })
    }
    else
    {
        api.post('UserBusinessess/removeusercategory.json', toRemoveData).then((res) => {
            debugger;
            if (res.ack == 1) {

            }
        })
    }


}

    render() {
        if (!this.state.IsSubmittedFeedback) {
            return (
                <Container >

                    <StatusBar
                        backgroundColor="#fff"
                        hidden={true}
                    />

                    <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>

                        <TouchableOpacity style={commonStyles.headerBackBtn} onPress={() => this.props.navigation.goBack()} >
                            <Ionicons name='ios-arrow-back' style={commonStyles.headerBackIcon} />
                        </TouchableOpacity>

                        <Body style={styleSelf.tac}>
                            <Text style={commonStyles.headerMiddleText}>Manage Notifications</Text>
                        </Body>

                        <TouchableOpacity style={commonStyles.headerRightBtn} activeOpacity={1}>
                            {/* <SimpleLineIcons name='plus' style={{ color: '#fff', fontSize: 16 }} /> */}
                        </TouchableOpacity>

                    </Header>

                    <Content style={[styles.mainContenne, { backgroundColor: '#fff' }]}>

                        <FSpinner loader={this.state.loader} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />

                        <Text style={styles.hder}>ALL</Text>
                        <View style={styles.tabItemWarp}>
                            <View style={styles.tabItemMdlTextWarp}>
                                <View>
                                    <Text style={{ fontSize: 12, marginBottom: 4 }} numberOfLines={1}>Allow Notifications</Text>
                                    
                                </View>
                            </View>
                            <View style={[ { height: 50, width: 50 }]}>
                                <Switch value={this.state.IsAllowNotification} onTintColor="rgb(255, 89, 89)"
                                    thumbTintColor="#fff" acti onValueChange={(value)=> this.switchAllNotifications(value)}/>
                                  
                            </View>
                           
                        </View>
                        <Text style={styles.hder}>FAVOURITES</Text>
                        <View style={styles.tabItemWarp}>
                            <View style={styles.tabItemMdlTextWarp}>
                                <View>
                                    <Text style={{ fontSize: 12, marginBottom: 4 }} numberOfLines={1}>Favourites expiring soon</Text>

                                </View>
                            </View>
                            <View style={[{ height: 50, width: 50 }]}>
                                <Switch value={this.state.IsAllowFavouritesNotification} onValueChange={(value)=>this.setState({IsAllowFavouritesNotification:value})}/>
                            </View>

                        </View>

                        <Text style={styles.hder}>NEW OFFERS BY CATEGORY</Text>
                       {
                           this.state.category_list.map((item, key)=>{
                               return(
                                    <View style={styles.tabItemWarp} key={key}>
                                    <View style={styles.tabItemMdlTextWarp}>
                                        <View>
                                            <Text style={{ fontSize: 12, marginBottom: 4 }} numberOfLines={1}>{item.name}</Text>

                                        </View>
                                    </View>
                                    <View style={[{ height: 50, width: 50 }]}>
                                        <Switch value={item.IsAllowed} onValueChange={(value)=>this.switchValueChange(item, value)}/>
                                    </View>

                                </View>
                               )
                           })
                       }
                        
                    </Content>

                </Container>
            );
        }
        else {
            return (
                <Container >

                    <StatusBar
                        backgroundColor="#fff"
                        hidden={true}
                    />

                    <Header style={commonStyles.headerWarp} androidStatusBarColor="#133567" noShadow>




                    </Header>

                    <Content style={[styles.mainContenne, { backgroundColor: '#fff' }]}>

                        <View>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                                <Text style={{ textAlign: 'right', padding: 10 }}>Close</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.feedbackHeading1}>
                            <Text style={[styles.feedbackText1, { textAlign: 'center' }]}>Thank you!</Text>
                            <Text style={[styles.feedbackText2, { padding: 20 }]}>We recieved your feedback and will use it to further improve.</Text>
                            <Text style={[styles.feedbackText2, { padding: 20 }]}>We won't be able to respond to every feedback individuality however you sure will experience even more improvements & features in next version update.</Text>
                            <Text style={[styles.feedbackText2, { padding: 20 }]}>Our principle is simple, we can do more  together than any one of us alone.</Text>



                        </View>

                    </Content>

                </Container>
            )
        }

    }


}


export default CustomizeNotification;
