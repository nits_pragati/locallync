const React = require("react-native");

const { StyleSheet, Dimensions, Platform } = React;

const deviceHeight = Dimensions.get("window").height;

export default {
    headerWarp:{ 
        backgroundColor: '#2096AB', 
    },
    headerBtnRight: { 
        width: 40, 
        display: 'flex', 
        alignItems: 'flex-end',
        justifyContent: 'center' 
    },
    headerMiddleText:{ 
        color: '#fff' 
    },
    headerRightIcon:{ 
        color: '#fff', 
        fontSize: 16 
    },
    headerBackBtn: {
        width: 40,
        justifyContent: 'center'
    },
    headerBackIcon: {
        color: '#fff',
        fontSize: 20
    },
    headerRightBtn: {
        width: 40,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    // footer:Start
    footerTab: {
        backgroundColor: '#ffffff'
    },
    footerItemWarp: {
        flex: 1,
        width: '25%',
        maxWidth: '25%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    footerItemImageWarp: {
        height: 20
    },
    footerItemImage: {
        width: 20,
        height: 20
    },
    footerItemText: {
        color: '#838383',
        fontSize: 10
    },
    footerItemTextActive: {
        color: '#2096AB'
        // color: '#3ab3ce'
    },
    notificationText: {
        marginTop: -2,
        position: 'absolute',
        top: -5,
        right: -5,
        backgroundColor: '#2096AB',
        fontSize: 9,
        zIndex: 99,
        borderRadius: 10,
        paddingRight: 5,
        paddingLeft: 5,
        color: '#fff'
    },
    footerNext:{ 
        flex: 1, 
        alignItems: 'flex-end', 
        justifyContent: 'center', 
        backgroundColor: '#2096AB',
        paddingRight:15
    },
    footerNextText:{
        color: '#fff',
        fontSize:20
    },
    // footer:end
    
  // top blue search bar: start
  searchWarp: {
        backgroundColor: '#2096AB',
        padding: 10,
        paddingTop: 0
    },
    searchinnr: {
        backgroundColor: '#fff',
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    searchIcon: {
        color: '#444444',
        marginRight: 5,
        fontSize: 20,
        marginLeft: 15
    },
    searchInput: {
        height: 30,
        flex: 1,
        marginBottom: 0,
        paddingTop: 2,
        paddingBottom: 0,
        fontSize: 11
    }
  //top blue search bar: end
};
