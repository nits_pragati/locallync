import {combineReducers} from 'redux'
import auth from '../components/accounts/elements/authReducer';
import RouterOwn from './routerReducer';

const rootReducer = combineReducers({
	auth,
	RouterOwn
})

export default rootReducer
