import { AsyncStorage } from "react-native";
import config from '../config'
let headers = {
  'Accept':'application/json',
  'Content-Type': 'application/json',
}

const resolver = ()=>AsyncStorage.getItem('userToken',(err,result)=>{

  if(result){
    result = JSON.parse(result);
    //headers.Authorization = 'Bearer '+result.id
  }
})
//headers.Authorization = 'Bearer tdsiUzrbkMWy3HcVoB4UPaTxgDdxTpXeJduwkWKpiOX6XtZKYPBvJAsTXv9M';

class api {
  static post(endpoint,data){
    console.log("data :",data);
    return new Promise((resolve,reject)=>{
      resolver().then(()=>{
        fetch(config.base_api+endpoint,{
          method:'POST',
          headers:headers,
          body:JSON.stringify(data)
        }).then(response => {
          
            if(response.status === 200){
              resolve(response.json());
            }else{
              if(response.status === 204){
                resolve();
              }else{
                reject({"err":"401 found"})
              }
            }

        }).catch(error=>error)
      }).catch(err=>err)
    })

  }

  static put(endpoint,data){
    return new Promise((resolve,reject)=>{
      resolver().then(()=>{
        fetch(config.base_api+endpoint,{
          method:'PUT',
          headers:headers,
          body:JSON.stringify(data)
        }).then(response => {
            if(response.status === 200){
              resolve(response.json());
            }else{
              if(response.status === 204){
                resolve();
              }else{
                reject({"err":"401 found"})
              }

            }

        }).catch(error=>error)
      }).catch(err=>err)
    })

  }

  static get(endpoint){
    return new Promise((resolve,reject)=>{
     resolver().then(()=>{
       fetch(config.base_api+endpoint,{
        method:'GET',
        headers:headers
      }).then(response => {
        if(response.status === 200){
          resolve(response.json());
        }else{
          if(response.status === 204){
            resolve();
          }else{
            reject({"err":"401 found"})
          }
        }
      }).catch(error=>error)
    }).catch(error=>error)
    })
  }

  static delete(endpoint) {
    return new Promise((resolve, reject) => {
      fetch(config.base_api + endpoint, {
        method: 'DELETE',
        headers: headers
      }).then(response => {
        if (response.status === 200) {
          resolve(response.json());
        } else {
          if (response.status === 204) {
            resolve();
          } else {
            reject({ "err": "401 found" })
          }
        }
      }).catch(error => error)
    })
    
  }

  // static walletPost(endpoint, data) {
  //   console.log("data :", data);
  //   return new Promise((resolve, reject) => {
  //     resolver().then(() => {
  //       fetch(config.base_wallet_api + endpoint, {
  //         method: 'POST',
  //         headers: headers,
  //         body:JSON.stringify(data)
  //       }).then(response => {
       
  //         if (response.status === 200 | response.status===201) {
  //           resolve(response.json());
  //         } else {
  //           if (response.status === 204) {
  //             resolve();
  //           } else {
  //             reject({ "err": "401 found" })
  //           }
  //         }

  //       }).catch((error)=>{
  //         console.log(error);
  //       })
  //     }).catch(err => err)
  //   })

  // }

  

  // static walletGet(endpoint, data) {
  //   return new Promise((resolve, reject) => {
  //     resolver().then(() => {
  //       fetch(config.base_wallet_api + data + '/' + endpoint, {
  //         method: 'GET',
  //         headers: headers
  //       }).then(response => {
  //         if (response.status === 200) {
  //           resolve(response.json());
  //         } else {
  //           if (response.status === 204) {
  //             resolve();
  //           } else {
  //             reject({ "err": "401 found" })
  //           }
  //         }
  //       }).catch(error => error)
  //     }).catch(error => error)
  //   })
  // }

  static walletPost(endpoint, data) {
    console.log("data :", data);
    return new Promise((resolve, reject) => {
      resolver().then(() => {
        fetch(config.base_api+'SiteSettings/getSiteSettingsDetails.json', {
          method: 'GET',
          headers: headers
        }).then((apiKey)=>{
          const api_key=JSON.parse(apiKey._bodyInit);
          //debugger;
          console.log(config.base_wallet_api + endpoint +'/'+ api_key.api_key)
           debugger;
          fetch(config.base_wallet_api + endpoint +'/'+ api_key.api_key, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(data)
          }).then(response => {
            if (response.status === 200 | response.status === 201) {
              debugger
              resolve(response.json());
            } else {
              if (response.status === 204) {
                resolve();
              } else {
                reject({ "err": "401 found" })
              }
            }
  
          }).catch((error) => {
            debugger
            console.log(error);
          })
          
       }).catch(error =>error)
      
      }).catch(err => err)
    })

  }

  static walletGet(endpoint, data) {
    return new Promise((resolve, reject) => {
      resolver().then(() => {
        fetch(config.base_api+'SiteSettings/getSiteSettingsDetails.json', {
          method: 'GET',
          headers: headers
        }).then((apiKey)=>{
          const api_key=JSON.parse(apiKey._bodyInit);
          // console.log("api_key:",api_key);
          console.log(config.base_wallet_api +data+'/'+ endpoint +'/'+ api_key.api_key);
           debugger;
          fetch(config.base_wallet_api +data+'/'+ endpoint +'/'+ api_key.api_key, {
            method: 'GET',
            headers: headers
          }).then(response => {
            if (response.status === 200) {
              resolve(response.json());
            } else {
              if (response.status === 204) {
                resolve();
              } else {
                reject({ "err": "401 found" })
              }
            }
          }).catch(error => error)
          
       }).catch(error => error)
       }).catch(error => error)
    })
  }

  static getWalletBalance(endpoint, data) {
    return new Promise((resolve, reject) => {
      resolver().then(() => {
        fetch(config.base_api+'SiteSettings/getSiteSettingsDetails.json', {
          method: 'GET',
          headers: headers
        }).then((apiKey)=>{
          const api_key=JSON.parse(apiKey._bodyInit);
          // console.log("api_key:",api_key);
          console.log(config.base_wallet_api + endpoint +'/'+ api_key.api_key + '/' +data);
           debugger;
          fetch(config.base_wallet_api + endpoint +'/'+ api_key.api_key + '/' +data, {
            method: 'GET',
            headers: headers
          }).then(response => {
            if (response.status === 200) {
              resolve(response.json());
            } else {
              if (response.status === 204) {
                resolve();
              } else {
                reject({ "err": "401 found" })
              }
            }
          }).catch(error => error)
          
       }).catch(error => error)
       }).catch(error => error)
    })
  }


}

export default api
