/* @flow */

import React, { Component } from "react";

import { Platform, BackHandler, Alert, Easing, Animated } from "react-native";
import { Root } from "native-base";
import { StackNavigator, NavigationActions } from "react-navigation";
import { connect } from 'react-redux';
import { ChangeRoute } from './actions/routerAction';
import { setCustomText, setCustomTextInput } from 'react-native-global-props';

import Home from "./components/home/index"
import Intro from "./components/intro";
import Demo from "./components/intro/demo";
import Login from "./components/accounts/login";
import UserType from "./components/accounts/userType";
import Signup from "./components/accounts/signup";
import UserOtpVerification from "./components/accounts/user_otp_verification";
import Connections from "./components/connections/connections";
import Favorite from "./components/favorite/favorite";
import VendorHome from "./components/vendorHome/index";
import Location from "./components/accounts/location";
import NotificationSet from "./components/accounts/notificationSet";
import ConnectWithBusinesses from "./components/accounts/connectWithBusinesses";
import Businesses from "./components/businesses/businesses";
import Notification from "./components/notification/index";
import DistrictLocation from "./components/districtLocation/index";
import Details from "./components/details/index";
import Redeem from "./components/redeem/index";
import Map from "./components/map/index";
import ResetPassword from "./components/accounts/reset_password";
import ForgotPassword from "./components/accounts/forgot_password";
import MyAccount from "./components/myAccount/index";
import EditAccount from "./components/editAccount/index";
import EditProfile from "./components/accounts/edit_profile";
import Edit from "./components/editAccount/edit";
import AddBusiness from "./components/accounts/addBusiness";
import MyBusiness from "./components/myBusiness/index";
import EditBusiness from "./components/myBusiness/editBusiness";
import SubCategories from './components/home/allSubCategories';
import NearBysubCategories from './components/home/nearBysubCategories';
import LocationAutocomplete from './components/accounts/locationAutocomplete';
import BusinessOtpVerification from './components/accounts/business_otp_verification';
import DistrictLocationAutocomplete from './components/districtLocation/locationAutocomplete';
import AddNewBusiness from './components/myBusiness/addNewBusiness';
import BusinessDetails from './components/myBusiness/businessDetails';
import AddOutlet from './components/myBusiness/addOutlet';
import SpecialEvent from './components/myAccount/specialEventList';
import AddSpecialEvent from './components/myAccount/addSpecialEvent';
import Feedback from './components/myAccount/feedback';
import CustomizeNotification from './components/myAccount/customizeNotification';
import Wallet from './components/wallet/wallet';
import FeedbackRating from './components/myAccount/feedback_ratinng';
import Chat from './components/details/chat';
import DeleteAccount from './components/editAccount/deleteAccount';
import OfferShare from './components/details/offerShare';
import FeedbackList from './components/details/feedbackList';
import BookTable from './components/details/bookTable';
import PayWithToken from './components/redeem/PayWithToken';
const resetAction = NavigationActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'Home' })],
});
const customTextProps = {
    style: {
        color: '#444444',
        fontFamily: 'Roboto-Medium',
        textAlign: 'left'
    }
}

const customTextInputProps = {
    style: {
        color: '#444444',
        fontFamily: 'Roboto-Regular'
    }
}

setCustomText(customTextProps);
setCustomTextInput(customTextInputProps);


const transitionConfig = () => {
    return {
        transitionSpec: {
            duration: 750,
            easing: Easing.out(Easing.poly(4)),
            timing: Animated.timing,
            useNativeDriver: true,
        },
        screenInterpolator: sceneProps => {
            const { layout, position, scene } = sceneProps

            const thisSceneIndex = scene.index
            const width = layout.initWidth

            const translateX = position.interpolate({
                inputRange: [thisSceneIndex - 1, thisSceneIndex],
                outputRange: [width, 0],
            })

            return { transform: [{ translateX }] }
        },
    }
}

const AppNavigator = StackNavigator(
    {
        Home: { screen: Home },
        Intro: { screen: Intro },
        Demo: { screen: Demo },
        Login: { screen: Login },
        Signup: { screen: Signup },
        UserOtpVerification: { screen: UserOtpVerification },
        Connections: { screen: Connections },
        Favorite: { screen: Favorite },
        VendorHome: { screen: VendorHome },
        UserType: { screen: UserType },
        Location: { screen: Location },
        NotificationSet: { screen: NotificationSet },
        ConnectWithBusinesses: { screen: ConnectWithBusinesses },
        Businesses: { screen: Businesses },
        Notification: { screen: Notification },
        DistrictLocation: { screen: DistrictLocation },
        Details: { screen: Details },
        Redeem: { screen: Redeem },
        Map: { screen: Map },
        ForgotPassword: { screen: ForgotPassword },
        ResetPassword: { screen: ResetPassword },
        MyAccount: { screen: MyAccount },
        EditAccount: { screen: EditAccount },
        EditProfile: { screen: EditProfile },
        Edit: { screen: Edit },
        AddBusiness: { screen: AddBusiness },
        MyBusiness: { screen: MyBusiness },
        EditBusiness: { screen: EditBusiness },
        SubCategories: { screen: SubCategories },
        NearBysubCategories: { screen: NearBysubCategories },
        LocationAutocomplete: { screen: LocationAutocomplete },
        BusinessOtpVerification: { screen: BusinessOtpVerification },
        DistrictLocationAutocomplete: { screen: DistrictLocationAutocomplete },
        AddNewBusiness: { screen: AddNewBusiness },
        BusinessDetails: { screen: BusinessDetails },
        AddOutlet: { screen: AddOutlet },
        SpecialEvent: { screen: SpecialEvent },
        AddSpecialEvent: { screen: AddSpecialEvent },
        Feedback: { screen: Feedback },
        CustomizeNotification: { screen: CustomizeNotification },
        Wallet: { screen: Wallet },
        FeedbackRating: { screen: FeedbackRating },
        Chat: { screen: Chat },
        DeleteAccount: { screen: DeleteAccount },
        OfferShare: {
            screen: OfferShare
        },
        FeedbackList: {
            screen: FeedbackList
        },
        BookTable: {
            screen: BookTable
        },
        PayWithToken:{
            screen: PayWithToken
        }

    },
    {
        initialRouteName: "Login",
        headerMode: "none",
        transitionConfig
    }
);

// const defaultStackGetStateForAction =
//     AppNavigator.router.getStateForAction;
// AppNavigator.router.getStateForAction = (action, state) => {
//     if (state && action) {
//         if (state.index === 0 && action.type === NavigationActions.BACK) {
//             Alert.alert(
//                 'Confirm',
//                 'Are you sure to exit the app?',
//                 [
//                     { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
//                     { text: 'OK', onPress: () => BackHandler.exitApp() },
//                 ],
//                 { cancelable: false }
//             )


//             // BackHandler.exitApp();
//             //return null;
//         }

//         else {
//             if (state.routes && state.routes.length && state.routes.length > 1) {
//                 let index = state.routes.length - 1;
//                 if (state.routes[index].routeName == 'Confirmation') {
//                 }
//             }
//         }
//     }
//     return defaultStackGetStateForAction(action, state);
// };
class App extends Component {
    updateRedux(prevState, newState) {
        this.props.ChangeRoute(prevState, newState)
    }
    render() {
        return (
            <Root>
                <AppNavigator
                    onNavigationStateChange={(prevState, newState) => this.updateRedux(prevState, newState)}
                />
            </Root>
        );

    }
}

function mapStateToProps(state) {
    return {
        currentRoute: state.newState,
        prevRoute: state.prevRoute
    }
}

export default connect(mapStateToProps, { ChangeRoute })(App);